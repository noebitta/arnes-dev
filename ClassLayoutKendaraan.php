<?php

class LayoutKendaraan{
	
	//BODY
	
	var $kode_booking_dipilih; //variable untuk menampung kode booking yang dipilih. digunakan untuk membuat group ketika dipilih
	
	function MakeImageByStatus($num,$parameters){
	  global $userdata;
		global $db;

    $status									= $parameters['StatusKursi'];
    $status_bayar						= $parameters['StatusBayar'];
    $nama										= $parameters['Nama'];
    $no_tikets							= explode(",",$parameters['NoTiket']);
    $no_tiket							  = $parameters['NoTiket'];
    $kursi_dipilih					=strpos($parameters['KodeBookings'],($this->kode_booking_dipilih==""?false:$this->kode_booking_dipilih))!==false?true:false;
    $is_multi               = $parameters['IsMulti'];
    $transit							  = $parameters['Transit']<=1?$parameters['Transit']:"1";
    $petugas_penjuals				= explode(",",$parameters['PetugasPenjual']);
    $petugas_penjual        = $petugas_penjuals[0];
    $session_id_old					= $petugas_penjuals[0];
    $show_cetak_tiket_cepat	= $parameters['ShowCetakCepat'];

		$user_level=$userdata['user_level'];
		$session_id=$userdata['user_id'];

    $nama_cut	=substr($nama,0,10);
		
	  if ($status==0) {
			//kursi masih kosong
			
			$layout_kursi ="<div class='kursi_kosong' onclick='updateTemp($num)'>$num</div>";
			
	  } else
		if ($status==1 && (trim($no_tiket)=="")){
	    // Jika kursi sudah di flag
			
			if($session_id==$session_id_old){
			  //jika select memang benar dilakukan oleh user ini
					
					$layout_kursi ="<div class='kursi_dipilih' onclick='updateTemp($num)'>$num</div>";
			}
			else{
			  //jika select dilakukan bukan oleh user ini, maka user ini tidak berhak membatalkannya
		
				//if(in_array($user_level,array($LEVEL_ADMIN,$LEVEL_SUPERVISOR,$LEVEL_KOORDINATOR))){
				//update tanggal 5 mei 2011, siapapun tidak diperbolehkan membatalkan kursi yang sedang dipilih kecuali admin
				if($user_level==$USER_LEVEL_INDEX["ADMIN"]){
					//jika  admin/supervisor, berhak membatalkan transaksi orang lain
						
					$layout_kursi =
						"<div class='kursi_block' onclick='updateTemp($num)'>
							$num
							<div class='kursi_warning'>Kursi tidak dapat diakses!</div>
						</div>";
				}
				else{
					//jika bukan admin/ supervisor, tidak dapat membatalkan transaksi orang lain
					$layout_kursi =
						"<div class='kursi_block'>
							$num<br>
							<div class='kursi_warning'>Kursi tidak dapat diakses!</div>
						</div>";
					
				}//endif ($user_level==$LEVEL_ADMIN || $user_level==$LEVEL_SUPERVISOR)

			}//endif $array_kursi[$num]==$userdata['session_id']
		
	  } else
	  if ($status==1 && (trim($no_tiket)!="")){
	    // Jika kursi sudah di booking
      $show_icon_multi  = $is_multi==0?"":"<div class='kursimulti' title='kursi berpenumpang jamak'></div>";
			if($status_bayar!=1){
				//jika tiket belum dibayar
					
				if($petugas_penjual!=0 || $petugas_penjual==''){
					$tombol_cetak_tiket	= count($no_tikets)<=1? "<div onclick=\"cetakTiketCepat('$no_tiket');return false;\" class='cetaktiketcepat' title='mencetak tiket'>&nbsp;</div> $num<br><br>":"$num<br>";
					$class_kursi=($kursi_dipilih!=1)?"kursi_booking".$transit:"kursi_booking_dipilih".$transit;
				}
				else{
          //online user
					$tombol_cetak_tiket	= "$num<br>";
					$class_kursi=($kursi_dipilih!=1)?"kursi_booking".$transit."_ttx":"kursi_booking_dipilih".$transit."_ttx";
				}

        $layout_kursi =

					"<div onclick='showChair($num,\"$no_tiket\");' class='$class_kursi' title='nama:$nama'>
					$tombol_cetak_tiket
					$show_icon_multi
					<div class='kursi_nama'>$nama_cut</div><br>
					</div>";
			
			}
			else{
				//jika kursi sudah dibayar
        $show_icon_multi  = $is_multi==0?"":"<div class='kursimulti' title='kursi berpenumpang jamak'></div>";
				if($petugas_penjual!=0 || $petugas_penjual==''){
					$class_kursi=($kursi_dipilih!=1)?"kursi_bayar".$transit:"kursi_bayar_dipilih".$transit;
				}
				else{
					$class_kursi=($kursi_dipilih!=1)?"kursi_bayar".$transit."_ttx":"kursi_bayar_dipilih".$transit."_ttx";
				}
				
				$layout_kursi =
					"<div onclick='showChair($num,\"$no_tiket\");' class='$class_kursi' title='nama:$nama'>
						$num<br>
						$show_icon_multi
						<div class='kursi_nama'>$nama_cut</div>
					</div>";

			}
		
		}
		return $layout_kursi;
	}
	
	function MakeImageByStatusByNoKursi($num,$status,$status_bayar,$session_id_old,$nama,$t,$user_level,$kode_booking){
	  global $userdata;
		global $LEVEL_ADMIN;
		global $LEVEL_SUPERVISOR;
		global $LEVEL_KOORDINATOR;
		global $LEVEL_AGEN;
		global $db;
		
		$layout_kursi="";
		
		$user_level=$userdata['user_level'];
		$session_id=$userdata['user_id'];
		
		$nama	=substr($nama,0,10);
		
	  if ($status==0) {
			//kursi masih kosong
			
			$layout_kursi .="<div class='kursi_kosong' onclick='updateTemp($num)'>$num</div>";
			
	  } else
		if ($status==1 && (trim($t)=="")){
	    // Jika kursi sudah di flag
			
			if($session_id==$session_id_old){
			  //jika select memang benar dilakukan oleh user ini
					
					$layout_kursi .="
						<div class='kursi_dipilih' onclick='updateTemp($num)'>
							$num
						</div>";
			}
			else{
			  //jika select dilakukan bukan oleh user ini, maka user ini tidak berhak membatalkannya
		
				if(in_array($user_level,array($LEVEL_ADMIN,$LEVEL_SUPERVISOR,$LEVEL_KOORDINATOR))){
					//jika  admin/supervisor, berhak membatalkan transaksi orang lain
						
					$layout_kursi .="
						<div class='kursi_block' onclick='updateTemp($num)'>
							$num<br>
							<span class='kursi_warning'>Kursi tidak dapat diakses!</span>
						</div>";
				}
				else{
					//jika bukan admin/ supervisor, tidak dapat membatalkan transaksi orang lain
					$layout_kursi .="
						<div class='kursi_block'>
							$num<br>
							<span class='kursi_warning'>Kursi tidak dapat diakses!</span>
						</div>";
					
				}//endif ($user_level==$LEVEL_ADMIN || $user_level==$LEVEL_SUPERVISOR)

			}//endif $array_kursi[$num]==$userdata['session_id']
		
	  } else
	  if ($status==1 && (trim($t)!="")){
	    // Jika kursi sudah di booking
			
			//grouping kursi
			/*$sql = "SELECT f_reservasi_get_kode_booking_by_no_tiket('$t')";
							
			if ($result = $db->sql_query($sql)){
				$row = $db->sql_fetchrow($result);
				$kode_booking=$row[0];
			}
			else{
				exit;
			}*/
			
			if($status_bayar!=1){
				//jika tiket belum dibayar
				
				if($user_level!=$LEVEL_AGEN){
					//jika  bukan member, dapat melihat detail transaksi
					
					$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_booking":"kursi_booking_dipilih";
			
					$layout_kursi .="
						<td onclick='showChair($num,\"$t\");' class='$class_kursi'>
							$num
							<div class='kursi_nama'>$nama</div>
						</td>";
				}
				else{
					exit;
				}
			}
			else{
				//jika kursi sudah dibayar
				
				if($user_level!=$LEVEL_AGEN){
					//jika  bukan member, dapat melihat detail transaksi
					
					//grouping kursi
					$class_kursi=($kode_booking!=$this->kode_booking_dipilih)?"kursi_bayar":"kursi_bayar_dipilih";
					
					$layout_kursi .="
						<td onclick='showChair($num,\"$t\");' class='$class_kursi'>
							$num
							<div class='kursi_nama'>$nama</div>
						</td>";
				}
				else{
					exit;
				}
			}
		
		}
		return $layout_kursi;
	}
	
	function getStatusKursi($tgl_berangkat,$kode_jadwal,$no_kursi){
		global $userdata;
		global $db;
		global $SESSION_TIME_EXPIRED;
		
		//INISIALISASI
		
		//mengambil data user dan levelnya
		$user_level				= $userdata['user_level'];

		$temp_array_date	= explode("-",$tgl_berangkat);
		$temp_tgl_berangkat	= mktime(0, 0, 0,(int) $temp_array_date[1] ,(int) $temp_array_date[2]+2,(int) $temp_array_date[0]);
		
		$tanggal_lampau	= date('Y-m-d',$temp_tgl_berangkat) >= date('Y-m-d');
		
		$nama_tbl	= $tanggal_lampau==1?"tbl_posisi_detail":"tbl_posisi_detail";
		
		$sql = 
			"SELECT *
			FROM $nama_tbl
			WHERE 
				KodeJadwal LIKE '$kode_jadwal' 
				AND TglBerangkat='$tgl_berangkat'
				AND ((HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED 
				OR NoTiket!='' 
				OR NoTiket IS NOT NULL)
				AND NomorKursi='$no_kursi'";

		if (!$result = $db->sql_query($sql)){
		  echo("Err:".__LINE__);
			exit;
		}

    $row = $db->sql_fetchrow($result);

    $parameters['StatusKursi']		= $row['StatusKursi'];
    $parameters['StatusBayar']		= $row['StatusBayar'];
    $parameters['PetugasPenjual']	= $row['Session'];
    $parameters['Nama']						= $row['Nama'];
    $parameters['NoTiket']				= $row['NoTiket'];
    $parameters['KodeBooking']		= $row['KodeBooking'];

    $sql =
      "SELECT NomorKursi,KodeJadwal,PetugasPenjual,IdMember
			FROM tbl_reservasi
			WHERE NoTiket = '$row[NoTiket]'";

    if (!$result = $db->sql_query($sql)){
      //die_error('Cannot Load Transaksi');//,__LINE__,__FILE__,$sql);
      echo("Err:".__LINE__);
      exit;
    }

    $row = $db->sql_fetchrow($result);

    $parameters['SubJadwal']			= 0;
    $parameters['Transit']	      = 0;
    $parameters['ShowCetakCepat']	= $row['IdMember']==""?1:0;

		$return_kursi	= $this->MakeImageByStatus($no_kursi,$parameters);

		return $return_kursi;
	}

  function ambilDetailJadwal($kode_jadwal){

    //kamus
    global $db;

    $sql =
      "SELECT
				KodeJadwal,JamBerangkat,IdJurusan,
				KodeCabangAsal,KodeCabangTujuan,IdLayout,
				JumlahKursi,FlagSubJadwal,KodeJadwalUtama,
				CONCAT(KodeCabangAsal,',',IF(Via='','',CONCAT(Via,',')),KodeCabangTujuan) AS Rute,
				FlagAktif
			FROM tbl_md_jadwal
			WHERE KodeJadwal='$kode_jadwal';";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $row=$db->sql_fetchrow($result);

    $data_jadwal['KodeJadwal']			= $row['KodeJadwal'];
    $data_jadwal['JamBerangkat']		= $row['JamBerangkat'];
    $data_jadwal['IdJurusan']				= $row['IdJurusan'];
    $data_jadwal['KodeCabangAsal']	= $row['KodeCabangAsal'];
    $data_jadwal['KodeCabangTujuan']= $row['KodeCabangTujuan'];
    $data_jadwal['IdLayout']				= $row['IdLayout'];
    $data_jadwal['JumlahKursi']			= $row['JumlahKursi'];
    $data_jadwal['FlagSubJadwal']		= $row['FlagSubJadwal'];
    $data_jadwal['KodeJadwalUtama']	= $row['KodeJadwalUtama'];
    $data_jadwal['Rute']				    = $row['Rute'];
    $data_jadwal['FlagAktif']				= $row['FlagAktif'];

    if($data_jadwal['FlagSubJadwal']){
      $sql =
        "SELECT CONCAT(KodeCabangAsal,',',IF(Via='','',CONCAT(Via,',')),KodeCabangTujuan) AS Rute,
          IdLayout,FlagAktif
        FROM tbl_md_jadwal
        WHERE KodeJadwal='$data_jadwal[KodeJadwalUtama]';";

      if (!$result = $db->sql_query($sql)){
        die_error("Err: $this->ID_FILE".__LINE__);
      }

      $row=$db->sql_fetchrow($result);

      $data_jadwal['Rute'] = $row['Rute'];
      $data_jadwal['IdLayout']				= $row['IdLayout'];
      $data_jadwal['FlagAktif']				= $row['FlagAktif']?$data_jadwal['FlagAktif']:false;
      $data_jadwal['FlagAktifUtama']	= $row['FlagAktif'];
    }

    return $data_jadwal;

  }//  END ambilDetailJadwal
}
?>