/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : arnes_dev

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-01-07 17:26:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_ba_bnop
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ba_bnop`;
CREATE TABLE `tbl_ba_bnop` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeBA` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuTransaksi` datetime DEFAULT NULL,
  `JenisBiaya` int(2) DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `NamaPenerima` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0',
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
