/*
Navicat MySQL Data Transfer

Source Server         : MySQL - Local
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : arnes

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2016-02-25 20:00:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_api_log_in
-- ----------------------------
DROP TABLE IF EXISTS `tbl_api_log_in`;
CREATE TABLE `tbl_api_log_in` (
  `in_id` int(11) NOT NULL AUTO_INCREMENT,
  `in_request_uri` varchar(100) NOT NULL,
  `in_request_query` text NOT NULL,
  `in_request_method` varchar(5) NOT NULL,
  `in_user_id` int(11) NOT NULL,
  `in_access_token` varchar(150) NOT NULL,
  `in_client_type` varchar(30) NOT NULL,
  `in_client_id` varchar(150) NOT NULL,
  `in_hh_manufacturer` varchar(50) NOT NULL,
  `in_hh_model` varchar(50) NOT NULL,
  `in_hh_os` varchar(50) NOT NULL,
  `in_hh_sdk` smallint(6) NOT NULL,
  `in_hh_screen` varchar(12) NOT NULL,
  `in_hh_imei` varchar(50) NOT NULL,
  `in_network_operator` varchar(50) NOT NULL,
  `in_subscriber_id` varchar(50) NOT NULL,
  `in_msisdn` varchar(50) NOT NULL,
  `in_app_version_code` smallint(6) NOT NULL,
  `in_app_version_name` varchar(10) NOT NULL,
  `in_ip_address` varchar(25) NOT NULL,
  `in_user_agent` text NOT NULL,
  `in_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `in_response` text NOT NULL,
  PRIMARY KEY (`in_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_api_log_in
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_api_log_out
-- ----------------------------
DROP TABLE IF EXISTS `tbl_api_log_out`;
CREATE TABLE `tbl_api_log_out` (
  `out_id` int(11) NOT NULL AUTO_INCREMENT,
  `out_url` text NOT NULL,
  `out_params` text NOT NULL,
  `out_method` varchar(5) NOT NULL,
  `out_response` text NOT NULL,
  `out_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_api_log_out
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_auth_client
-- ----------------------------
DROP TABLE IF EXISTS `tbl_auth_client`;
CREATE TABLE `tbl_auth_client` (
  `client_id` varchar(150) NOT NULL,
  `client_secret` varchar(150) NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `client_email` varchar(100) NOT NULL,
  `client_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_auth_client
-- ----------------------------
INSERT INTO `tbl_auth_client` VALUES ('ANDROID', 'b014dcc2a3a984ebaea090dd96e6d0fc', 'Daytans Android', 'android@daytrans.tiketux.com', '2015-08-11 06:12:27');

-- ----------------------------
-- Table structure for tbl_auth_user_token
-- ----------------------------
DROP TABLE IF EXISTS `tbl_auth_user_token`;
CREATE TABLE `tbl_auth_user_token` (
  `access_token` varchar(150) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` varchar(150) NOT NULL,
  `access_token_secret` varchar(150) NOT NULL,
  `access_token_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `access_token_expire` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_auth_user_token
-- ----------------------------
INSERT INTO `tbl_auth_user_token` VALUES ('1410b503ac230d8980231b34e4537822056cea5db', '1', 'ANDROID', 'fe593bfcd5e5da6ad643f45f31a2ad29056cea5db', '2016-02-25 13:57:31', '2016-02-25 13:57:31');
INSERT INTO `tbl_auth_user_token` VALUES ('95669b0dafb52f119df597b735fd9cca056c7f34d', '43', 'ANDROID', '7bd71736cf76c6bd6782bfcb377e000b056c7f34d', '2016-02-20 12:02:05', '2016-02-20 12:02:05');

-- ----------------------------
-- Table structure for tbl_ba_bop
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ba_bop`;
CREATE TABLE `tbl_ba_bop` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeBA` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuTransaksi` datetime DEFAULT NULL,
  `JenisBiaya` int(2) DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `NoSPJ` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0',
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_ba_bop
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_ba_check
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ba_check`;
CREATE TABLE `tbl_ba_check` (
  `NoBA` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `TglBA` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeJurusan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahPenumpangCheck` int(11) DEFAULT NULL,
  `JumlahPaketCheck` int(11) DEFAULT NULL,
  `IdChecker` int(11) DEFAULT NULL,
  `NamaChecker` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `CatatanTambahanCheck` text COLLATE latin1_general_ci,
  `IsCheck` int(1) DEFAULT NULL,
  `isCetakVoucherBBM` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_ba_check
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_biaya_insentif_sopir
-- ----------------------------
DROP TABLE IF EXISTS `tbl_biaya_insentif_sopir`;
CREATE TABLE `tbl_biaya_insentif_sopir` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdPenjadwalan` int(11) DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeJadwal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeKendaraan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPembuat` int(11) DEFAULT NULL,
  `NamaPembuat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuat` datetime DEFAULT NULL,
  `IdApprover` int(11) DEFAULT NULL,
  `NamaApprover` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuApprove` datetime DEFAULT NULL,
  `NominalInsentif` double DEFAULT NULL,
  `IdReleaser` int(11) DEFAULT NULL,
  `NamaReleaser` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuRelease` datetime DEFAULT NULL,
  `CabangRelease` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IsRelease` int(1) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `IDX_1` (`TglBerangkat`) USING BTREE,
  KEY `IDX_2` (`IdJurusan`) USING BTREE,
  KEY `FK_5` (`IdPenjadwalan`) USING BTREE,
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  KEY `KodeKendaraan` (`KodeKendaraan`) USING BTREE,
  KEY `KodeSopir` (`KodeSopir`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_biaya_insentif_sopir
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_biaya_op
-- ----------------------------
DROP TABLE IF EXISTS `tbl_biaya_op`;
CREATE TABLE `tbl_biaya_op` (
  `IDBiayaOP` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkun` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagJenisBiaya` int(1) DEFAULT NULL,
  `TglTransaksi` date DEFAULT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Jumlah` double DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `IdPetugas` int(10) unsigned DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeCabang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCatat` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IDBiayaOP`),
  KEY `FK_1` (`NoSPJ`) USING BTREE,
  KEY `FK_2` (`IdJurusan`) USING BTREE,
  KEY `FK_3` (`TglTransaksi`) USING BTREE,
  KEY `FK_4` (`KodeSopir`) USING BTREE,
  KEY `FK_5` (`NoPolisi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_biaya_op
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_deposit_log_topup
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deposit_log_topup`;
CREATE TABLE `tbl_deposit_log_topup` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeReferensi` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `Jumlah` double NOT NULL DEFAULT '0',
  `OTP` varchar(6) COLLATE latin1_general_ci NOT NULL COMMENT 'menyimpan kode ',
  `IsVerified` int(1) unsigned NOT NULL DEFAULT '0' COMMENT 'jika OTP benar, akan diverifikasi',
  `WaktuVerifikasi` datetime DEFAULT NULL,
  `PetugasTopUp` int(10) unsigned NOT NULL,
  `PetugasVerifikasi` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_deposit_log_topup
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_deposit_log_trx
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deposit_log_trx`;
CREATE TABLE `tbl_deposit_log_trx` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `WaktuTransaksi` datetime NOT NULL,
  `IsDebit` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'debit=pengurangan saldo, kredit=penambahan saldo',
  `Keterangan` text COLLATE latin1_general_ci,
  `Jumlah` double NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_deposit_log_trx
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_jenis_discount
-- ----------------------------
DROP TABLE IF EXISTS `tbl_jenis_discount`;
CREATE TABLE `tbl_jenis_discount` (
  `IdDiscount` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeDiscount` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDiscount` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `JumlahDiscount` double NOT NULL,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `FlagLuarKota` int(1) DEFAULT '1',
  `IsHargaTetap` int(1) DEFAULT '0' COMMENT 'jika diset=1 maka nilai harganya bukan diskon melainkan harga tetap sesuai dengan field jenisdiskon',
  PRIMARY KEY (`IdDiscount`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_jenis_discount
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_kasbon_sopir
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kasbon_sopir`;
CREATE TABLE `tbl_kasbon_sopir` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeAkun` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `TglTransaksi` date NOT NULL,
  `KodeSopir` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` double NOT NULL,
  `IdKasir` int(10) unsigned NOT NULL,
  `DiberikanDiCabang` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `IsBatal` int(1) unsigned NOT NULL DEFAULT '0',
  `Pembatal` int(10) unsigned NOT NULL,
  `WaktuBatal` datetime NOT NULL,
  `WaktuCatatTransaksi` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_kasbon_sopir
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_kendaraan_administratif
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kendaraan_administratif`;
CREATE TABLE `tbl_kendaraan_administratif` (
  `id_kendaraan_administratif` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TanggalAktif` date DEFAULT NULL,
  `StatusKepemilikan` int(1) DEFAULT NULL,
  `Keterangan` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglValidSTNK` date DEFAULT NULL,
  `TglValidKIR` date DEFAULT NULL,
  `BiayaLeasing` double DEFAULT NULL,
  `NamaLeasing` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaAngsuran` double DEFAULT NULL,
  `AngsuranKe` int(2) DEFAULT NULL,
  `JumlahAngsuran` int(2) DEFAULT NULL,
  `JatuhTempoAngsuran` date DEFAULT NULL,
  PRIMARY KEY (`id_kendaraan_administratif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_kendaraan_administratif
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_cetakulang_voucher_bbm
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_cetakulang_voucher_bbm`;
CREATE TABLE `tbl_log_cetakulang_voucher_bbm` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahRupiah` double DEFAULT '0',
  `WaktuCetak` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `UserCetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `UserApproval` int(11) DEFAULT NULL,
  `NamaUserApproval` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_cetakulang_voucher_bbm
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_cetak_manifest
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_cetak_manifest`;
CREATE TABLE `tbl_log_cetak_manifest` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursiDisediakan` int(2) DEFAULT NULL,
  `JumlahPenumpang` int(2) DEFAULT '0',
  `JumlahPaket` int(3) DEFAULT NULL,
  `JumlahPaxPaket` int(3) DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalOmzetPaket` double DEFAULT NULL,
  `IsSubJadwal` int(1) DEFAULT '0',
  `WaktuCetak` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `UserCetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_cetak_manifest
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_cetak_tiket
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_cetak_tiket`;
CREATE TABLE `tbl_log_cetak_tiket` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenumpang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `CetakanKe` int(2) DEFAULT NULL,
  `IsCetakSPJ` int(1) DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  `UserPencetak` int(11) DEFAULT NULL,
  `NamaUserPencetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `UserOtorisasi` int(11) DEFAULT NULL,
  `NamaUserOtorisasi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_cetak_tiket
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_finpay
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_finpay`;
CREATE TABLE `tbl_log_finpay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `module` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `action` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `url` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `data` text COLLATE latin1_general_ci,
  `user_id` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_finpay
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_koreksi_disc
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_koreksi_disc`;
CREATE TABLE `tbl_log_koreksi_disc` (
  `IdLog` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoTiket` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabang` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) unsigned DEFAULT NULL,
  `HargaTiket` double DEFAULT NULL,
  `Charge` double DEFAULT NULL,
  `SubTotal` double DEFAULT NULL,
  `DiscountMula` double DEFAULT NULL,
  `DiscountBaru` double DEFAULT NULL,
  `Total` double DEFAULT NULL,
  `PetugasPenjual` int(10) unsigned DEFAULT NULL,
  `CetakTiket` int(1) unsigned DEFAULT NULL,
  `PetugasCetakTiket` int(10) unsigned DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `JenisDiscountMula` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscountBaru` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) unsigned DEFAULT NULL,
  `FlagBatal` int(1) unsigned DEFAULT NULL,
  `JenisPenumpangMula` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpangBaru` char(2) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPengkoreksi` int(10) unsigned DEFAULT NULL,
  `PetugasOtorisasi` int(10) unsigned DEFAULT NULL,
  `WaktuKoreksi` datetime DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  PRIMARY KEY (`IdLog`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_koreksi_disc
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_mutasi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_mutasi`;
CREATE TABLE `tbl_log_mutasi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBookingSebelumnya` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenumpang` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkatSebelumnya` date DEFAULT NULL,
  `JamBerangkatSebelumnya` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwalSebelumnya` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusanSebelumnya` int(11) DEFAULT NULL,
  `NomorKursiSebelumnya` int(2) DEFAULT NULL,
  `HargaTiketSebelumnya` double DEFAULT NULL,
  `KodeBookingMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkatMutasi` date DEFAULT NULL,
  `JamBerangkatMutasi` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwalMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NomorKursiMutasi` int(2) DEFAULT NULL,
  `IdJurusanMutasi` int(11) DEFAULT NULL,
  `HargaTiketMutasi` double DEFAULT NULL,
  `IsCetakTiket` int(1) DEFAULT NULL,
  `IsCetakSPJ` int(1) DEFAULT NULL,
  `Charge` double DEFAULT NULL,
  `TotalDiskon` double DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakSPJ` datetime DEFAULT NULL,
  `UserMutasi` int(11) DEFAULT NULL,
  `NamaUserMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `KodeJadwalSebelumnya` (`KodeJadwalSebelumnya`) USING BTREE,
  KEY `FK2` (`KodeJadwalMutasi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_mutasi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_sms
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_sms`;
CREATE TABLE `tbl_log_sms` (
  `IdLogSms` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenerima` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuKirim` datetime DEFAULT NULL,
  `JumlahPesan` int(2) DEFAULT NULL,
  `FlagTipePengiriman` int(2) DEFAULT NULL,
  `KodeReferensi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdLogSms`),
  KEY `IDX_WAKTUKIRIM` (`WaktuKirim`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_sms
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_token_sms
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_token_sms`;
CREATE TABLE `tbl_log_token_sms` (
  `IdLog` int(11) NOT NULL AUTO_INCREMENT,
  `WaktuCatat` datetime NOT NULL,
  `JumlahSisaToken` double NOT NULL DEFAULT '0',
  `JumlahSMSDikirim` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdLog`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_token_sms
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log_user`;
CREATE TABLE `tbl_log_user` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `waktu_login` datetime DEFAULT NULL,
  `waktu_logout` datetime DEFAULT NULL,
  `user_ip` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_log_user
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_mac_address
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mac_address`;
CREATE TABLE `tbl_mac_address` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MacAddress` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NamaKomputer` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `AddBy` int(10) unsigned NOT NULL,
  `AddByName` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `WaktuTambah` datetime NOT NULL,
  `IsAktif` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_mac_address
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_cabang
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_cabang`;
CREATE TABLE `tbl_md_cabang` (
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Kota` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Fax` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `FlagAgen` int(1) DEFAULT NULL,
  `IsPusat` int(1) NOT NULL DEFAULT '0',
  `COAAR` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COASalesTiket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COAMarkDownTiket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COASalesPaket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COAMarkDownPaket` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COACash` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `COABank` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`KodeCabang`),
  KEY `TbMDCabang_index4992` (`Nama`,`Alamat`,`Kota`) USING BTREE,
  KEY `TbMDCabang_index4994` (`Telp`) USING BTREE,
  KEY `TbMDCabang_index4995` (`FlagAgen`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_cabang
-- ----------------------------
INSERT INTO `tbl_md_cabang` VALUES ('BALTOS', 'Balubur Town Square', 'Jl Taman Sari Mall Baltos Lantai D1-O-01,02 & 07 Bandung', 'BANDUNG', '', '', '0', '0', null, null, null, null, null, null, null);
INSERT INTO `tbl_md_cabang` VALUES ('BTC', 'Bandung Trade Centre', 'Jl Pasteur Loby Mall BTC Bandung', 'BANDUNG', '', '', '0', '0', null, null, null, null, null, null, null);
INSERT INTO `tbl_md_cabang` VALUES ('CGN', 'Ciganea', 'Jl Ciganea Dekat Gedung DPRD Purwakarta', 'PURWAKARTA', '', '', '0', '0', null, null, null, null, null, null, null);
INSERT INTO `tbl_md_cabang` VALUES ('JTN1', 'Jatinangor', 'Jl Raya Jatinangor Sebelum Belokan Sayang', 'JATINANGOR', '', '', '0', '0', null, null, null, null, null, null, null);
INSERT INTO `tbl_md_cabang` VALUES ('JTN2', 'Madtari', 'Jl Raya Jatinangor', 'JATINANGOR', '', '', '0', '0', null, null, null, null, null, null, null);
INSERT INTO `tbl_md_cabang` VALUES ('PNC', 'Superindo Pancoran', 'Jl Pancoran Loby Superindo Pancoran Jakarta', 'JAKARTA', '', '', '0', '0', null, null, null, null, null, null, null);
INSERT INTO `tbl_md_cabang` VALUES ('STS', 'Sadang Town Square', 'Jl Raya Sadang Mall STS Lantai 1', 'PURWAKARTA', '', '', '0', '0', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for tbl_md_harga_promo
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_harga_promo`;
CREATE TABLE `tbl_md_harga_promo` (
  `KodeHargaPromo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `BerlakuMula` date NOT NULL,
  `BerlakuAkhir` date NOT NULL,
  `HargaPromo` double NOT NULL,
  `FlagAktif` int(1) DEFAULT NULL,
  PRIMARY KEY (`KodeHargaPromo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_harga_promo
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_jadwal
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_jadwal`;
CREATE TABLE `tbl_md_jadwal` (
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(11) NOT NULL,
  `KodeCabangAsal` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JamBerangkat` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL,
  `FlagSubJadwal` int(1) DEFAULT '0',
  `KodeJadwalUtama` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopir1` double DEFAULT NULL,
  `BiayaSopir2` double DEFAULT NULL,
  `BiayaSopir3` double DEFAULT NULL,
  `IsBiayaSopirKumulatif` int(1) DEFAULT '0',
  `BiayaTol` double DEFAULT NULL,
  `BiayaParkir` double DEFAULT NULL,
  `BiayaBBM` double DEFAULT NULL,
  `IsBBMVoucher` int(1) DEFAULT '0',
  `Via` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `IsOnline` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`KodeJadwal`),
  KEY `TbMDJadwal_index5011` (`IdJurusan`,`JamBerangkat`,`FlagAktif`) USING BTREE,
  KEY `TbMDJadwal_index5013` (`IdJurusan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_jadwal
-- ----------------------------
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0500', '6', 'BALTOS', 'CGN', '05:00', '14', '14', '1', 'BALTOS-STS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0530', '6', 'BALTOS', 'CGN', '05:30', '14', '14', '1', 'BALTOS-STS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0600', '6', 'BALTOS', 'CGN', '06:00', '14', '14', '1', 'BALTOS-STS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0630', '6', 'BALTOS', 'CGN', '06:30', '14', '14', '1', 'BALTOS-STS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0700', '6', 'BALTOS', 'CGN', '07:00', '14', '14', '1', 'BALTOS-STS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0730', '6', 'BALTOS', 'CGN', '07:30', '14', '14', '1', 'BALTOS-STS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0800', '6', 'BALTOS', 'CGN', '08:00', '14', '14', '1', 'BALTOS-STS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0830', '6', 'BALTOS', 'CGN', '08:30', '14', '14', '1', 'BALTOS-STS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0900', '6', 'BALTOS', 'CGN', '09:00', '14', '14', '1', 'BALTOS-STS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN0930', '6', 'BALTOS', 'CGN', '09:30', '14', '14', '1', 'BALTOS-STS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1000', '6', 'BALTOS', 'CGN', '10:00', '14', '14', '1', 'BALTOS-STS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1030', '6', 'BALTOS', 'CGN', '10:30', '14', '14', '1', 'BALTOS-STS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1100', '6', 'BALTOS', 'CGN', '11:00', '14', '14', '1', 'BALTOS-STS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1130', '6', 'BALTOS', 'CGN', '11:30', '14', '14', '1', 'BALTOS-STS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1200', '6', 'BALTOS', 'CGN', '12:00', '14', '14', '1', 'BALTOS-STS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1230', '6', 'BALTOS', 'CGN', '12:30', '14', '14', '1', 'BALTOS-STS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1300', '6', 'BALTOS', 'CGN', '13:00', '14', '14', '1', 'BALTOS-STS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1330', '6', 'BALTOS', 'CGN', '13:30', '14', '14', '1', 'BALTOS-STS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1400', '6', 'BALTOS', 'CGN', '14:00', '14', '14', '1', 'BALTOS-STS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1430', '6', 'BALTOS', 'CGN', '14:30', '14', '14', '1', 'BALTOS-STS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1500', '6', 'BALTOS', 'CGN', '15:00', '14', '14', '1', 'BALTOS-STS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1530', '6', 'BALTOS', 'CGN', '15:30', '14', '14', '1', 'BALTOS-STS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1600', '6', 'BALTOS', 'CGN', '16:00', '14', '14', '1', 'BALTOS-STS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1630', '6', 'BALTOS', 'CGN', '16:30', '14', '14', '1', 'BALTOS-STS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1700', '6', 'BALTOS', 'CGN', '17:00', '14', '14', '1', 'BALTOS-STS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1730', '6', 'BALTOS', 'CGN', '17:30', '14', '14', '1', 'BALTOS-STS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1800', '6', 'BALTOS', 'CGN', '18:00', '14', '14', '1', 'BALTOS-STS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1830', '6', 'BALTOS', 'CGN', '18:30', '14', '14', '1', 'BALTOS-STS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1900', '6', 'BALTOS', 'CGN', '19:00', '14', '14', '1', 'BALTOS-STS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN1930', '6', 'BALTOS', 'CGN', '19:30', '14', '14', '1', 'BALTOS-STS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN2000', '6', 'BALTOS', 'CGN', '20:00', '14', '14', '1', 'BALTOS-STS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN2030', '6', 'BALTOS', 'CGN', '20:30', '14', '14', '1', 'BALTOS-STS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN2100', '6', 'BALTOS', 'CGN', '21:00', '14', '14', '1', 'BALTOS-STS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN2130', '6', 'BALTOS', 'CGN', '21:30', '14', '14', '1', 'BALTOS-STS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-CGN2200', '6', 'BALTOS', 'CGN', '22:00', '14', '14', '1', 'BALTOS-STS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10500', '1', 'BALTOS', 'JTN1', '05:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', 'BTC', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10515', '1', 'BALTOS', 'JTN1', '05:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', 'BTC', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10530', '1', 'BALTOS', 'JTN1', '05:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10545', '1', 'BALTOS', 'JTN1', '05:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10600', '1', 'BALTOS', 'JTN1', '06:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10615', '1', 'BALTOS', 'JTN1', '06:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10630', '1', 'BALTOS', 'JTN1', '06:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10645', '1', 'BALTOS', 'JTN1', '06:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10700', '1', 'BALTOS', 'JTN1', '07:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10715', '1', 'BALTOS', 'JTN1', '07:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10730', '1', 'BALTOS', 'JTN1', '07:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10745', '1', 'BALTOS', 'JTN1', '07:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10800', '1', 'BALTOS', 'JTN1', '08:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10815', '1', 'BALTOS', 'JTN1', '08:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10830', '1', 'BALTOS', 'JTN1', '08:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10845', '1', 'BALTOS', 'JTN1', '08:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10900', '1', 'BALTOS', 'JTN1', '09:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10915', '1', 'BALTOS', 'JTN1', '09:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10930', '1', 'BALTOS', 'JTN1', '09:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN10945', '1', 'BALTOS', 'JTN1', '09:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11000', '1', 'BALTOS', 'JTN1', '10:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11015', '1', 'BALTOS', 'JTN1', '10:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11030', '1', 'BALTOS', 'JTN1', '10:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11045', '1', 'BALTOS', 'JTN1', '10:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11100', '1', 'BALTOS', 'JTN1', '11:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11115', '1', 'BALTOS', 'JTN1', '11:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11130', '1', 'BALTOS', 'JTN1', '11:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11145', '1', 'BALTOS', 'JTN1', '11:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11200', '1', 'BALTOS', 'JTN1', '12:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11215', '1', 'BALTOS', 'JTN1', '12:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11230', '1', 'BALTOS', 'JTN1', '12:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11245', '1', 'BALTOS', 'JTN1', '12:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11300', '1', 'BALTOS', 'JTN1', '13:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11315', '1', 'BALTOS', 'JTN1', '13:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11330', '1', 'BALTOS', 'JTN1', '13:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11345', '1', 'BALTOS', 'JTN1', '13:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11400', '1', 'BALTOS', 'JTN1', '14:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11415', '1', 'BALTOS', 'JTN1', '14:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11430', '1', 'BALTOS', 'JTN1', '14:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11445', '1', 'BALTOS', 'JTN1', '14:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11500', '1', 'BALTOS', 'JTN1', '15:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11515', '1', 'BALTOS', 'JTN1', '15:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11530', '1', 'BALTOS', 'JTN1', '15:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11545', '1', 'BALTOS', 'JTN1', '15:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11600', '1', 'BALTOS', 'JTN1', '16:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11615', '1', 'BALTOS', 'JTN1', '16:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11630', '1', 'BALTOS', 'JTN1', '16:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11645', '1', 'BALTOS', 'JTN1', '16:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11700', '1', 'BALTOS', 'JTN1', '17:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11715', '1', 'BALTOS', 'JTN1', '17:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11730', '1', 'BALTOS', 'JTN1', '17:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11745', '1', 'BALTOS', 'JTN1', '17:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11800', '1', 'BALTOS', 'JTN1', '18:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11815', '1', 'BALTOS', 'JTN1', '18:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11830', '1', 'BALTOS', 'JTN1', '18:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11845', '1', 'BALTOS', 'JTN1', '18:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11900', '1', 'BALTOS', 'JTN1', '19:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11915', '1', 'BALTOS', 'JTN1', '19:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11930', '1', 'BALTOS', 'JTN1', '19:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN11945', '1', 'BALTOS', 'JTN1', '19:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12000', '1', 'BALTOS', 'JTN1', '20:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12015', '1', 'BALTOS', 'JTN1', '20:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12030', '1', 'BALTOS', 'JTN1', '20:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12045', '1', 'BALTOS', 'JTN1', '20:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12100', '1', 'BALTOS', 'JTN1', '21:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12115', '1', 'BALTOS', 'JTN1', '21:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12130', '1', 'BALTOS', 'JTN1', '21:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12145', '1', 'BALTOS', 'JTN1', '21:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12200', '1', 'BALTOS', 'JTN1', '22:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12215', '1', 'BALTOS', 'JTN1', '22:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12230', '1', 'BALTOS', 'JTN1', '22:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12245', '1', 'BALTOS', 'JTN1', '22:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN12300', '1', 'BALTOS', 'JTN1', '23:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20500', '18', 'BALTOS', 'JTN2', '05:00', '14', '14', '1', 'BALTOS-JTN10500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20515', '18', 'BALTOS', 'JTN2', '05:15', '14', '14', '1', 'BALTOS-JTN10515', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20530', '18', 'BALTOS', 'JTN2', '05:30', '14', '14', '1', 'BALTOS-JTN10530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20545', '18', 'BALTOS', 'JTN2', '05:45', '14', '14', '1', 'BALTOS-JTN10545', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20600', '18', 'BALTOS', 'JTN2', '06:00', '14', '14', '1', 'BALTOS-JTN10600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20615', '18', 'BALTOS', 'JTN2', '06:15', '14', '14', '1', 'BALTOS-JTN10615', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20630', '18', 'BALTOS', 'JTN2', '06:30', '14', '14', '1', 'BALTOS-JTN10630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20645', '18', 'BALTOS', 'JTN2', '06:45', '14', '14', '1', 'BALTOS-JTN10645', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20700', '18', 'BALTOS', 'JTN2', '07:00', '14', '14', '1', 'BALTOS-JTN10700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20715', '18', 'BALTOS', 'JTN2', '07:15', '14', '14', '1', 'BALTOS-JTN10715', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20730', '18', 'BALTOS', 'JTN2', '07:30', '14', '14', '1', 'BALTOS-JTN10730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20745', '18', 'BALTOS', 'JTN2', '07:45', '14', '14', '1', 'BALTOS-JTN10745', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20800', '18', 'BALTOS', 'JTN2', '08:00', '14', '14', '1', 'BALTOS-JTN10800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20815', '18', 'BALTOS', 'JTN2', '08:15', '14', '14', '1', 'BALTOS-JTN10815', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20830', '18', 'BALTOS', 'JTN2', '08:30', '14', '14', '1', 'BALTOS-JTN10830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20845', '18', 'BALTOS', 'JTN2', '08:45', '14', '14', '1', 'BALTOS-JTN10845', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20900', '18', 'BALTOS', 'JTN2', '09:00', '14', '14', '1', 'BALTOS-JTN10900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20915', '18', 'BALTOS', 'JTN2', '09:15', '14', '14', '1', 'BALTOS-JTN10915', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20930', '18', 'BALTOS', 'JTN2', '09:30', '14', '14', '1', 'BALTOS-JTN10930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN20945', '18', 'BALTOS', 'JTN2', '09:45', '14', '14', '1', 'BALTOS-JTN10945', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21000', '18', 'BALTOS', 'JTN2', '10:00', '14', '14', '1', 'BALTOS-JTN11000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21015', '18', 'BALTOS', 'JTN2', '10:15', '14', '14', '1', 'BALTOS-JTN11015', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21030', '18', 'BALTOS', 'JTN2', '10:30', '14', '14', '1', 'BALTOS-JTN11030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21045', '18', 'BALTOS', 'JTN2', '10:45', '14', '14', '1', 'BALTOS-JTN11045', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21100', '18', 'BALTOS', 'JTN2', '11:00', '14', '14', '1', 'BALTOS-JTN11100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21115', '18', 'BALTOS', 'JTN2', '11:15', '14', '14', '1', 'BALTOS-JTN11115', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21130', '18', 'BALTOS', 'JTN2', '11:30', '14', '14', '1', 'BALTOS-JTN11130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21145', '18', 'BALTOS', 'JTN2', '11:45', '14', '14', '1', 'BALTOS-JTN11145', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21200', '18', 'BALTOS', 'JTN2', '12:00', '14', '14', '1', 'BALTOS-JTN11200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21215', '18', 'BALTOS', 'JTN2', '12:15', '14', '14', '1', 'BALTOS-JTN11215', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21230', '18', 'BALTOS', 'JTN2', '12:30', '14', '14', '1', 'BALTOS-JTN11230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21245', '18', 'BALTOS', 'JTN2', '12:45', '14', '14', '1', 'BALTOS-JTN11245', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21300', '18', 'BALTOS', 'JTN2', '13:00', '14', '14', '1', 'BALTOS-JTN11300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21315', '18', 'BALTOS', 'JTN2', '13:15', '14', '14', '1', 'BALTOS-JTN11315', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21330', '18', 'BALTOS', 'JTN2', '13:30', '14', '14', '1', 'BALTOS-JTN11330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21345', '18', 'BALTOS', 'JTN2', '13:45', '14', '14', '1', 'BALTOS-JTN11345', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21400', '18', 'BALTOS', 'JTN2', '14:00', '14', '14', '1', 'BALTOS-JTN11400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21415', '18', 'BALTOS', 'JTN2', '14:15', '14', '14', '1', 'BALTOS-JTN11415', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21430', '18', 'BALTOS', 'JTN2', '14:30', '14', '14', '1', 'BALTOS-JTN11430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21445', '18', 'BALTOS', 'JTN2', '14:45', '14', '14', '1', 'BALTOS-JTN11445', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21500', '18', 'BALTOS', 'JTN2', '15:00', '14', '14', '1', 'BALTOS-JTN11500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21515', '18', 'BALTOS', 'JTN2', '15:15', '14', '14', '1', 'BALTOS-JTN11515', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21530', '18', 'BALTOS', 'JTN2', '15:30', '14', '14', '1', 'BALTOS-JTN11530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21545', '18', 'BALTOS', 'JTN2', '15:45', '14', '14', '1', 'BALTOS-JTN11545', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21600', '18', 'BALTOS', 'JTN2', '16:00', '14', '14', '1', 'BALTOS-JTN11600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21615', '18', 'BALTOS', 'JTN2', '16:15', '14', '14', '1', 'BALTOS-JTN11615', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21630', '18', 'BALTOS', 'JTN2', '16:30', '14', '14', '1', 'BALTOS-JTN11630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21645', '18', 'BALTOS', 'JTN2', '16:45', '14', '14', '1', 'BALTOS-JTN11645', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21700', '18', 'BALTOS', 'JTN2', '17:00', '14', '14', '1', 'BALTOS-JTN11700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21715', '18', 'BALTOS', 'JTN2', '17:15', '14', '14', '1', 'BALTOS-JTN11715', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21730', '18', 'BALTOS', 'JTN2', '17:30', '14', '14', '1', 'BALTOS-JTN11730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21745', '18', 'BALTOS', 'JTN2', '17:45', '14', '14', '1', 'BALTOS-JTN11745', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21800', '18', 'BALTOS', 'JTN2', '18:00', '14', '14', '1', 'BALTOS-JTN11800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21815', '18', 'BALTOS', 'JTN2', '18:15', '14', '14', '1', 'BALTOS-JTN11815', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21830', '18', 'BALTOS', 'JTN2', '18:30', '14', '14', '1', 'BALTOS-JTN11830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21845', '18', 'BALTOS', 'JTN2', '18:45', '14', '14', '1', 'BALTOS-JTN11845', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21900', '18', 'BALTOS', 'JTN2', '19:00', '14', '14', '1', 'BALTOS-JTN11900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21915', '18', 'BALTOS', 'JTN2', '19:15', '14', '14', '1', 'BALTOS-JTN11915', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21930', '18', 'BALTOS', 'JTN2', '19:30', '14', '14', '1', 'BALTOS-JTN11930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN21945', '18', 'BALTOS', 'JTN2', '19:45', '14', '14', '1', 'BALTOS-JTN11945', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22000', '18', 'BALTOS', 'JTN2', '20:00', '14', '14', '1', 'BALTOS-JTN12000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22015', '18', 'BALTOS', 'JTN2', '20:15', '14', '14', '1', 'BALTOS-JTN12015', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22030', '18', 'BALTOS', 'JTN2', '20:30', '14', '14', '1', 'BALTOS-JTN12030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22045', '18', 'BALTOS', 'JTN2', '20:45', '14', '14', '1', 'BALTOS-JTN12045', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22100', '18', 'BALTOS', 'JTN2', '21:00', '14', '14', '1', 'BALTOS-JTN12100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22115', '18', 'BALTOS', 'JTN2', '21:15', '14', '14', '1', 'BALTOS-JTN12115', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22130', '18', 'BALTOS', 'JTN2', '21:30', '14', '14', '1', 'BALTOS-JTN12130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22145', '18', 'BALTOS', 'JTN2', '21:45', '14', '14', '1', 'BALTOS-JTN12145', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22200', '18', 'BALTOS', 'JTN2', '22:00', '14', '14', '1', 'BALTOS-JTN12200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22215', '18', 'BALTOS', 'JTN2', '22:15', '14', '14', '1', 'BALTOS-JTN12215', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22230', '18', 'BALTOS', 'JTN2', '22:30', '14', '14', '1', 'BALTOS-JTN12230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22245', '18', 'BALTOS', 'JTN2', '22:45', '14', '14', '1', 'BALTOS-JTN12245', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-JTN22300', '18', 'BALTOS', 'JTN2', '23:00', '14', '14', '1', 'BALTOS-JTN12300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0500', '4', 'BALTOS', 'STS', '05:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', 'BTC', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0530', '4', 'BALTOS', 'STS', '05:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0600', '4', 'BALTOS', 'STS', '06:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0630', '4', 'BALTOS', 'STS', '06:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0700', '4', 'BALTOS', 'STS', '07:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0730', '4', 'BALTOS', 'STS', '07:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0800', '4', 'BALTOS', 'STS', '08:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0830', '4', 'BALTOS', 'STS', '08:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0900', '4', 'BALTOS', 'STS', '09:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS0930', '4', 'BALTOS', 'STS', '09:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1000', '4', 'BALTOS', 'STS', '10:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1030', '4', 'BALTOS', 'STS', '10:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1100', '4', 'BALTOS', 'STS', '11:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1130', '4', 'BALTOS', 'STS', '11:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1200', '4', 'BALTOS', 'STS', '12:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1230', '4', 'BALTOS', 'STS', '12:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1300', '4', 'BALTOS', 'STS', '13:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1330', '4', 'BALTOS', 'STS', '13:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1400', '4', 'BALTOS', 'STS', '14:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1430', '4', 'BALTOS', 'STS', '14:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1500', '4', 'BALTOS', 'STS', '15:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1530', '4', 'BALTOS', 'STS', '15:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1600', '4', 'BALTOS', 'STS', '16:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1630', '4', 'BALTOS', 'STS', '16:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1700', '4', 'BALTOS', 'STS', '17:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1730', '4', 'BALTOS', 'STS', '17:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1800', '4', 'BALTOS', 'STS', '18:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1830', '4', 'BALTOS', 'STS', '18:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1900', '4', 'BALTOS', 'STS', '19:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS1930', '4', 'BALTOS', 'STS', '19:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS2000', '4', 'BALTOS', 'STS', '20:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS2030', '4', 'BALTOS', 'STS', '20:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS2100', '4', 'BALTOS', 'STS', '21:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS2130', '4', 'BALTOS', 'STS', '21:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BALTOS-STS2200', '4', 'BALTOS', 'STS', '22:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0500', '7', 'BTC', 'CGN', '05:00', '14', '14', '1', 'BALTOS-STS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0530', '7', 'BTC', 'CGN', '05:30', '14', '14', '1', 'BALTOS-STS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0600', '7', 'BTC', 'CGN', '06:00', '14', '14', '1', 'BALTOS-STS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0630', '7', 'BTC', 'CGN', '06:30', '14', '14', '1', 'BALTOS-STS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0700', '7', 'BTC', 'CGN', '07:00', '14', '14', '1', 'BALTOS-STS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0730', '7', 'BTC', 'CGN', '07:30', '14', '14', '1', 'BALTOS-STS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0800', '7', 'BTC', 'CGN', '08:00', '14', '14', '1', 'BALTOS-STS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0830', '7', 'BTC', 'CGN', '08:30', '14', '14', '1', 'BALTOS-STS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0900', '7', 'BTC', 'CGN', '09:00', '14', '14', '1', 'BALTOS-STS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN0930', '7', 'BTC', 'CGN', '09:30', '14', '14', '1', 'BALTOS-STS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1000', '7', 'BTC', 'CGN', '10:00', '14', '14', '1', 'BALTOS-STS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1030', '7', 'BTC', 'CGN', '10:30', '14', '14', '1', 'BALTOS-STS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1100', '7', 'BTC', 'CGN', '11:00', '14', '14', '1', 'BALTOS-STS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1130', '7', 'BTC', 'CGN', '11:30', '14', '14', '1', 'BALTOS-STS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1200', '7', 'BTC', 'CGN', '12:00', '14', '14', '1', 'BALTOS-STS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1230', '7', 'BTC', 'CGN', '12:30', '14', '14', '1', 'BALTOS-STS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1300', '7', 'BTC', 'CGN', '13:00', '14', '14', '1', 'BALTOS-STS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1330', '7', 'BTC', 'CGN', '13:30', '14', '14', '1', 'BALTOS-STS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1400', '7', 'BTC', 'CGN', '14:00', '14', '14', '1', 'BALTOS-STS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1430', '7', 'BTC', 'CGN', '14:30', '14', '14', '1', 'BALTOS-STS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1500', '7', 'BTC', 'CGN', '15:00', '14', '14', '1', 'BALTOS-STS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1530', '7', 'BTC', 'CGN', '15:30', '14', '14', '1', 'BALTOS-STS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1600', '7', 'BTC', 'CGN', '16:00', '14', '14', '1', 'BALTOS-STS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1630', '7', 'BTC', 'CGN', '16:30', '14', '14', '1', 'BALTOS-STS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1700', '7', 'BTC', 'CGN', '17:00', '14', '14', '1', 'BALTOS-STS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1730', '7', 'BTC', 'CGN', '17:30', '14', '14', '1', 'BALTOS-STS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1800', '7', 'BTC', 'CGN', '18:00', '14', '14', '1', 'BALTOS-STS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1830', '7', 'BTC', 'CGN', '18:30', '14', '14', '1', 'BALTOS-STS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1900', '7', 'BTC', 'CGN', '19:00', '14', '14', '1', 'BALTOS-STS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN1930', '7', 'BTC', 'CGN', '19:30', '14', '14', '1', 'BALTOS-STS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN2000', '7', 'BTC', 'CGN', '20:00', '14', '14', '1', 'BALTOS-STS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN2030', '7', 'BTC', 'CGN', '20:30', '14', '14', '1', 'BALTOS-STS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN2100', '7', 'BTC', 'CGN', '21:00', '14', '14', '1', 'BALTOS-STS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN2130', '7', 'BTC', 'CGN', '21:30', '14', '14', '1', 'BALTOS-STS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-CGN2200', '7', 'BTC', 'CGN', '22:00', '14', '14', '1', 'BALTOS-STS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10500', '2', 'BTC', 'JTN1', '05:00', '14', '14', '1', 'BALTOS-JTN10500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10515', '2', 'BTC', 'JTN1', '05:15', '14', '14', '1', 'BALTOS-JTN10515', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10530', '2', 'BTC', 'JTN1', '05:30', '14', '14', '1', 'BALTOS-JTN10530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10545', '2', 'BTC', 'JTN1', '05:45', '14', '14', '1', 'BALTOS-JTN10545', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10600', '2', 'BTC', 'JTN1', '06:00', '14', '14', '1', 'BALTOS-JTN10600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10615', '2', 'BTC', 'JTN1', '06:15', '14', '14', '1', 'BALTOS-JTN10615', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10630', '2', 'BTC', 'JTN1', '06:30', '14', '14', '1', 'BALTOS-JTN10600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10645', '2', 'BTC', 'JTN1', '06:45', '14', '14', '1', 'BALTOS-JTN10645', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10700', '2', 'BTC', 'JTN1', '07:00', '14', '14', '1', 'BALTOS-JTN10700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10715', '2', 'BTC', 'JTN1', '07:15', '14', '14', '1', 'BALTOS-JTN10715', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10730', '2', 'BTC', 'JTN1', '07:30', '14', '14', '1', 'BALTOS-JTN10730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10745', '2', 'BTC', 'JTN1', '07:45', '14', '14', '1', 'BALTOS-JTN10745', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10800', '2', 'BTC', 'JTN1', '08:00', '14', '14', '1', 'BALTOS-JTN10800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10815', '2', 'BTC', 'JTN1', '08:15', '14', '14', '1', 'BALTOS-JTN10815', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10830', '2', 'BTC', 'JTN1', '08:30', '14', '14', '1', 'BALTOS-JTN10830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10845', '2', 'BTC', 'JTN1', '08:45', '14', '14', '1', 'BALTOS-JTN10845', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10900', '2', 'BTC', 'JTN1', '09:00', '14', '14', '1', 'BALTOS-JTN10900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10915', '2', 'BTC', 'JTN1', '09:15', '14', '14', '1', 'BALTOS-JTN10915', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10930', '2', 'BTC', 'JTN1', '09:30', '14', '14', '1', 'BALTOS-JTN10930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN10945', '2', 'BTC', 'JTN1', '09:45', '14', '14', '1', 'BALTOS-JTN10945', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11000', '2', 'BTC', 'JTN1', '10:00', '14', '14', '1', 'BALTOS-JTN11000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11015', '2', 'BTC', 'JTN1', '10:15', '14', '14', '1', 'BALTOS-JTN11015', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11030', '2', 'BTC', 'JTN1', '10:30', '14', '14', '1', 'BALTOS-JTN11030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11045', '2', 'BTC', 'JTN1', '10:45', '14', '14', '1', 'BALTOS-JTN11045', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11100', '2', 'BTC', 'JTN1', '11:00', '14', '14', '1', 'BALTOS-JTN11100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11115', '2', 'BTC', 'JTN1', '11:15', '14', '14', '1', 'BALTOS-JTN11115', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11130', '2', 'BTC', 'JTN1', '11:30', '14', '14', '1', 'BALTOS-JTN11130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11145', '2', 'BTC', 'JTN1', '11:45', '14', '14', '1', 'BALTOS-JTN11145', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11200', '2', 'BTC', 'JTN1', '12:00', '14', '14', '1', 'BALTOS-JTN11200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11215', '2', 'BTC', 'JTN1', '12:15', '14', '14', '1', 'BALTOS-JTN11215', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11230', '2', 'BTC', 'JTN1', '12:30', '14', '14', '1', 'BALTOS-JTN11230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11245', '2', 'BTC', 'JTN1', '12:45', '14', '14', '1', 'BALTOS-JTN11245', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11300', '2', 'BTC', 'JTN1', '13:00', '14', '14', '1', 'BALTOS-JTN11300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11315', '2', 'BTC', 'JTN1', '13:15', '14', '14', '1', 'BALTOS-JTN11315', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11330', '2', 'BTC', 'JTN1', '13:30', '14', '14', '1', 'BALTOS-JTN11330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11345', '2', 'BTC', 'JTN1', '13:45', '14', '14', '1', 'BALTOS-JTN11345', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11400', '2', 'BTC', 'JTN1', '14:00', '14', '14', '1', 'BALTOS-JTN11400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11415', '2', 'BTC', 'JTN1', '14:15', '14', '14', '1', 'BALTOS-JTN11415', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11430', '2', 'BTC', 'JTN1', '14:30', '14', '14', '1', 'BALTOS-JTN11430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11445', '2', 'BTC', 'JTN1', '14:45', '14', '14', '1', 'BALTOS-JTN11445', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11500', '2', 'BTC', 'JTN1', '15:00', '14', '14', '1', 'BALTOS-JTN11500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11515', '2', 'BTC', 'JTN1', '15:15', '14', '14', '1', 'BALTOS-JTN11515', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11530', '2', 'BTC', 'JTN1', '15:30', '14', '14', '1', 'BALTOS-JTN11530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11545', '2', 'BTC', 'JTN1', '15:45', '14', '14', '1', 'BALTOS-JTN11545', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11600', '2', 'BTC', 'JTN1', '16:00', '14', '14', '1', 'BALTOS-JTN11600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11615', '2', 'BTC', 'JTN1', '16:15', '14', '14', '1', 'BALTOS-JTN11615', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11630', '2', 'BTC', 'JTN1', '16:30', '14', '14', '1', 'BALTOS-JTN11630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11645', '2', 'BTC', 'JTN1', '16:45', '14', '14', '1', 'BALTOS-JTN11645', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11700', '2', 'BTC', 'JTN1', '17:00', '14', '14', '1', 'BALTOS-JTN11700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11715', '2', 'BTC', 'JTN1', '17:15', '14', '14', '1', 'BALTOS-JTN11715', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11730', '2', 'BTC', 'JTN1', '17:30', '14', '14', '1', 'BALTOS-JTN11730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11745', '2', 'BTC', 'JTN1', '17:45', '14', '14', '1', 'BALTOS-JTN11745', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11800', '2', 'BTC', 'JTN1', '18:00', '14', '14', '1', 'BALTOS-JTN11800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11815', '2', 'BTC', 'JTN1', '18:15', '14', '14', '1', 'BALTOS-JTN11815', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11830', '2', 'BTC', 'JTN1', '18:30', '14', '14', '1', 'BALTOS-JTN11830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11845', '2', 'BTC', 'JTN1', '18:45', '14', '14', '1', 'BALTOS-JTN11845', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11900', '2', 'BTC', 'JTN1', '19:00', '14', '14', '1', 'BALTOS-JTN11900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11915', '2', 'BTC', 'JTN1', '19:15', '14', '14', '1', 'BALTOS-JTN11915', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11930', '2', 'BTC', 'JTN1', '19:30', '14', '14', '1', 'BALTOS-JTN11930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN11945', '2', 'BTC', 'JTN1', '19:45', '14', '14', '1', 'BALTOS-JTN11945', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12000', '2', 'BTC', 'JTN1', '20:00', '14', '14', '1', 'BALTOS-JTN12000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12015', '2', 'BTC', 'JTN1', '20:15', '14', '14', '1', 'BALTOS-JTN12015', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12030', '2', 'BTC', 'JTN1', '20:30', '14', '14', '1', 'BALTOS-JTN12030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12045', '2', 'BTC', 'JTN1', '20:45', '14', '14', '1', 'BALTOS-JTN12045', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12100', '2', 'BTC', 'JTN1', '21:00', '14', '14', '1', 'BALTOS-JTN12100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12115', '2', 'BTC', 'JTN1', '21:15', '14', '14', '1', 'BALTOS-JTN12115', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12130', '2', 'BTC', 'JTN1', '21:30', '14', '14', '1', 'BALTOS-JTN12130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12145', '2', 'BTC', 'JTN1', '21:45', '14', '14', '1', 'BALTOS-JTN12145', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12200', '2', 'BTC', 'JTN1', '22:00', '14', '14', '1', 'BALTOS-JTN12200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12215', '2', 'BTC', 'JTN1', '22:15', '14', '14', '1', 'BALTOS-JTN12215', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12230', '2', 'BTC', 'JTN1', '22:30', '14', '14', '1', 'BALTOS-JTN12230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12245', '2', 'BTC', 'JTN1', '22:45', '14', '14', '1', 'BALTOS-JTN12245', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-JTN12300', '2', 'BTC', 'JTN1', '23:00', '14', '14', '1', 'BALTOS-JTN12300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0500', '5', 'BTC', 'STS', '05:00', '14', '14', '1', 'BALTOS-STS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0530', '5', 'BTC', 'STS', '05:30', '14', '14', '1', 'BALTOS-STS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0600', '5', 'BTC', 'STS', '06:00', '14', '14', '1', 'BALTOS-STS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0630', '5', 'BTC', 'STS', '06:30', '14', '14', '1', 'BALTOS-STS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0700', '5', 'BTC', 'STS', '07:00', '14', '14', '1', 'BALTOS-STS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0730', '5', 'BTC', 'STS', '07:30', '14', '14', '1', 'BALTOS-STS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0800', '5', 'BTC', 'STS', '08:00', '14', '14', '1', 'BALTOS-STS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0830', '5', 'BTC', 'STS', '08:30', '14', '14', '1', 'BALTOS-STS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0900', '5', 'BTC', 'STS', '09:00', '14', '14', '1', 'BALTOS-STS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS0930', '5', 'BTC', 'STS', '09:30', '14', '14', '1', 'BALTOS-STS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1000', '5', 'BTC', 'STS', '10:00', '14', '14', '1', 'BALTOS-STS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1030', '5', 'BTC', 'STS', '10:30', '14', '14', '1', 'BALTOS-STS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1100', '5', 'BTC', 'STS', '11:00', '14', '14', '1', 'BALTOS-STS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1130', '5', 'BTC', 'STS', '11:30', '14', '14', '1', 'BALTOS-STS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1200', '5', 'BTC', 'STS', '12:00', '14', '14', '1', 'BALTOS-STS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1230', '5', 'BTC', 'STS', '12:30', '14', '14', '1', 'BALTOS-STS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1300', '5', 'BTC', 'STS', '13:00', '14', '14', '1', 'BALTOS-STS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1330', '5', 'BTC', 'STS', '13:30', '14', '14', '1', 'BALTOS-STS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1400', '5', 'BTC', 'STS', '14:00', '14', '14', '1', 'BALTOS-STS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1430', '5', 'BTC', 'STS', '14:30', '14', '14', '1', 'BALTOS-STS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1500', '5', 'BTC', 'STS', '15:00', '14', '14', '1', 'BALTOS-STS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1530', '5', 'BTC', 'STS', '15:30', '14', '14', '1', 'BALTOS-STS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1600', '5', 'BTC', 'STS', '16:00', '14', '14', '1', 'BALTOS-STS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1630', '5', 'BTC', 'STS', '16:30', '14', '14', '1', 'BALTOS-STS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1700', '5', 'BTC', 'STS', '17:00', '14', '14', '1', 'BALTOS-STS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1730', '5', 'BTC', 'STS', '17:30', '14', '14', '1', 'BALTOS-STS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1800', '5', 'BTC', 'STS', '18:00', '14', '14', '1', 'BALTOS-STS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1830', '5', 'BTC', 'STS', '18:30', '14', '14', '1', 'BALTOS-STS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1900', '5', 'BTC', 'STS', '19:00', '14', '14', '1', 'BALTOS-STS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS1930', '5', 'BTC', 'STS', '19:30', '14', '14', '1', 'BALTOS-STS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS2000', '5', 'BTC', 'STS', '20:00', '14', '14', '1', 'BALTOS-STS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS2030', '5', 'BTC', 'STS', '20:30', '14', '14', '1', 'BALTOS-STS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS2100', '5', 'BTC', 'STS', '21:00', '14', '14', '1', 'BALTOS-STS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS2130', '5', 'BTC', 'STS', '21:30', '14', '14', '1', 'BALTOS-STS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('BTC-STS2200', '5', 'BTC', 'STS', '22:00', '14', '14', '1', 'BALTOS-STS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0500', '15', 'CGN', 'BALTOS', '05:00', '14', '14', '1', 'STS-BALTOS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0530', '15', 'CGN', 'BALTOS', '05:30', '14', '14', '1', 'STS-BALTOS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0600', '15', 'CGN', 'BALTOS', '06:00', '14', '14', '1', 'STS-BALTOS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0630', '15', 'CGN', 'BALTOS', '06:30', '14', '14', '1', 'STS-BALTOS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0700', '15', 'CGN', 'BALTOS', '07:00', '14', '14', '1', 'STS-BALTOS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0730', '15', 'CGN', 'BALTOS', '07:30', '14', '14', '1', 'STS-BALTOS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0800', '15', 'CGN', 'BALTOS', '08:00', '14', '14', '1', 'STS-BALTOS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0830', '15', 'CGN', 'BALTOS', '08:30', '14', '14', '1', 'STS-BALTOS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0900', '15', 'CGN', 'BALTOS', '09:00', '14', '14', '1', 'STS-BALTOS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS0930', '15', 'CGN', 'BALTOS', '09:30', '14', '14', '1', 'STS-BALTOS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1000', '15', 'CGN', 'BALTOS', '10:00', '14', '14', '1', 'STS-BALTOS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1030', '15', 'CGN', 'BALTOS', '10:30', '14', '14', '1', 'STS-BALTOS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1100', '15', 'CGN', 'BALTOS', '11:00', '14', '14', '1', 'STS-BALTOS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1130', '15', 'CGN', 'BALTOS', '11:30', '14', '14', '1', 'STS-BALTOS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1200', '15', 'CGN', 'BALTOS', '12:00', '14', '14', '1', 'STS-BALTOS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1230', '15', 'CGN', 'BALTOS', '12:30', '14', '14', '1', 'STS-BALTOS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1300', '15', 'CGN', 'BALTOS', '13:00', '14', '14', '1', 'STS-BALTOS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1330', '15', 'CGN', 'BALTOS', '13:30', '14', '14', '1', 'STS-BALTOS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1400', '15', 'CGN', 'BALTOS', '14:00', '14', '14', '1', 'STS-BALTOS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1430', '15', 'CGN', 'BALTOS', '14:30', '14', '14', '1', 'STS-BALTOS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1500', '15', 'CGN', 'BALTOS', '15:00', '14', '14', '1', 'STS-BALTOS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1530', '15', 'CGN', 'BALTOS', '15:30', '14', '14', '1', 'STS-BALTOS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1600', '15', 'CGN', 'BALTOS', '16:00', '14', '14', '1', 'STS-BALTOS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1630', '15', 'CGN', 'BALTOS', '16:30', '14', '14', '1', 'STS-BALTOS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1700', '15', 'CGN', 'BALTOS', '17:00', '14', '14', '1', 'STS-BALTOS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1730', '15', 'CGN', 'BALTOS', '17:30', '14', '14', '1', 'STS-BALTOS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1800', '15', 'CGN', 'BALTOS', '18:00', '14', '14', '1', 'STS-BALTOS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1830', '15', 'CGN', 'BALTOS', '18:30', '14', '14', '1', 'STS-BALTOS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1900', '15', 'CGN', 'BALTOS', '19:00', '14', '14', '1', 'STS-BALTOS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS1930', '15', 'CGN', 'BALTOS', '19:30', '14', '14', '1', 'STS-BALTOS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS2000', '15', 'CGN', 'BALTOS', '20:00', '14', '14', '1', 'STS-BALTOS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS2030', '15', 'CGN', 'BALTOS', '20:30', '14', '14', '1', 'STS-BALTOS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS2100', '15', 'CGN', 'BALTOS', '21:00', '14', '14', '1', 'STS-BALTOS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS2130', '15', 'CGN', 'BALTOS', '21:30', '14', '14', '1', 'STS-BALTOS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BALTOS2200', '15', 'CGN', 'BALTOS', '22:00', '14', '14', '1', 'STS-BALTOS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0500', '14', 'CGN', 'BTC', '05:00', '14', '14', '1', 'STS-BALTOS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0530', '14', 'CGN', 'BTC', '05:30', '14', '14', '1', 'STS-BALTOS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0600', '14', 'CGN', 'BTC', '06:00', '14', '14', '1', 'STS-BALTOS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0630', '14', 'CGN', 'BTC', '06:30', '14', '14', '1', 'STS-BALTOS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0700', '14', 'CGN', 'BTC', '07:00', '14', '14', '1', 'STS-BALTOS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0730', '14', 'CGN', 'BTC', '07:30', '14', '14', '1', 'STS-BALTOS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0800', '14', 'CGN', 'BTC', '08:00', '14', '14', '1', 'STS-BALTOS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0830', '14', 'CGN', 'BTC', '08:30', '14', '14', '1', 'STS-BALTOS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0900', '14', 'CGN', 'BTC', '09:00', '14', '14', '1', 'STS-BALTOS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC0930', '14', 'CGN', 'BTC', '09:30', '14', '14', '1', 'STS-BALTOS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1000', '14', 'CGN', 'BTC', '10:00', '14', '14', '1', 'STS-BALTOS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1030', '14', 'CGN', 'BTC', '10:30', '14', '14', '1', 'STS-BALTOS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1100', '14', 'CGN', 'BTC', '11:00', '14', '14', '1', 'STS-BALTOS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1130', '14', 'CGN', 'BTC', '11:30', '14', '14', '1', 'STS-BALTOS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1200', '14', 'CGN', 'BTC', '12:00', '14', '14', '1', 'STS-BALTOS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1230', '14', 'CGN', 'BTC', '12:30', '14', '14', '1', 'STS-BALTOS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1300', '14', 'CGN', 'BTC', '13:00', '14', '14', '1', 'STS-BALTOS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1330', '14', 'CGN', 'BTC', '13:30', '14', '14', '1', 'STS-BALTOS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1400', '14', 'CGN', 'BTC', '14:00', '14', '14', '1', 'STS-BALTOS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1430', '14', 'CGN', 'BTC', '14:30', '14', '14', '1', 'STS-BALTOS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1500', '14', 'CGN', 'BTC', '15:00', '14', '14', '1', 'STS-BALTOS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1530', '14', 'CGN', 'BTC', '15:30', '14', '14', '1', 'STS-BALTOS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1600', '14', 'CGN', 'BTC', '16:00', '14', '14', '1', 'STS-BALTOS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1630', '14', 'CGN', 'BTC', '16:30', '14', '14', '1', 'STS-BALTOS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1700', '14', 'CGN', 'BTC', '17:00', '14', '14', '1', 'STS-BALTOS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1730', '14', 'CGN', 'BTC', '17:30', '14', '14', '1', 'STS-BALTOS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1800', '14', 'CGN', 'BTC', '18:00', '14', '14', '1', 'STS-BALTOS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1830', '14', 'CGN', 'BTC', '18:30', '14', '14', '1', 'STS-BALTOS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1900', '14', 'CGN', 'BTC', '19:00', '14', '14', '1', 'STS-BALTOS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC1930', '14', 'CGN', 'BTC', '19:30', '14', '14', '1', 'STS-BALTOS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC2000', '14', 'CGN', 'BTC', '20:00', '14', '14', '1', 'STS-BALTOS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC2030', '14', 'CGN', 'BTC', '20:30', '14', '14', '1', 'STS-BALTOS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC2100', '14', 'CGN', 'BTC', '21:00', '14', '14', '1', 'STS-BALTOS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC2130', '14', 'CGN', 'BTC', '21:30', '14', '14', '1', 'STS-BALTOS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('CGN-BTC2200', '14', 'CGN', 'BTC', '22:00', '14', '14', '1', 'STS-BALTOS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0500', '20', 'JTN1', 'BALTOS', '05:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', 'JTN2', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0515', '20', 'JTN1', 'BALTOS', '05:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0530', '20', 'JTN1', 'BALTOS', '05:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0545', '20', 'JTN1', 'BALTOS', '05:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0600', '20', 'JTN1', 'BALTOS', '06:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0615', '20', 'JTN1', 'BALTOS', '06:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0630', '20', 'JTN1', 'BALTOS', '06:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0645', '20', 'JTN1', 'BALTOS', '06:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0700', '20', 'JTN1', 'BALTOS', '07:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0715', '20', 'JTN1', 'BALTOS', '07:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0730', '20', 'JTN1', 'BALTOS', '07:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0745', '20', 'JTN1', 'BALTOS', '07:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0800', '20', 'JTN1', 'BALTOS', '08:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0815', '20', 'JTN1', 'BALTOS', '08:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0830', '20', 'JTN1', 'BALTOS', '08:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0845', '20', 'JTN1', 'BALTOS', '08:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0900', '20', 'JTN1', 'BALTOS', '09:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0915', '20', 'JTN1', 'BALTOS', '09:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0930', '20', 'JTN1', 'BALTOS', '09:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS0945', '20', 'JTN1', 'BALTOS', '09:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1000', '20', 'JTN1', 'BALTOS', '10:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1015', '20', 'JTN1', 'BALTOS', '10:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1030', '20', 'JTN1', 'BALTOS', '10:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1045', '20', 'JTN1', 'BALTOS', '10:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1100', '20', 'JTN1', 'BALTOS', '11:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1115', '20', 'JTN1', 'BALTOS', '11:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1130', '20', 'JTN1', 'BALTOS', '11:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1145', '20', 'JTN1', 'BALTOS', '11:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1200', '20', 'JTN1', 'BALTOS', '12:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1215', '20', 'JTN1', 'BALTOS', '12:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1230', '20', 'JTN1', 'BALTOS', '12:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1245', '20', 'JTN1', 'BALTOS', '12:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1300', '20', 'JTN1', 'BALTOS', '13:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1315', '20', 'JTN1', 'BALTOS', '13:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1330', '20', 'JTN1', 'BALTOS', '13:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1345', '20', 'JTN1', 'BALTOS', '13:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1400', '20', 'JTN1', 'BALTOS', '14:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1415', '20', 'JTN1', 'BALTOS', '14:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1430', '20', 'JTN1', 'BALTOS', '14:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1445', '20', 'JTN1', 'BALTOS', '14:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1500', '20', 'JTN1', 'BALTOS', '15:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1515', '20', 'JTN1', 'BALTOS', '15:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1530', '20', 'JTN1', 'BALTOS', '15:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1545', '20', 'JTN1', 'BALTOS', '15:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1600', '20', 'JTN1', 'BALTOS', '16:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1615', '20', 'JTN1', 'BALTOS', '16:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1630', '20', 'JTN1', 'BALTOS', '16:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1645', '20', 'JTN1', 'BALTOS', '16:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1700', '20', 'JTN1', 'BALTOS', '17:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1715', '20', 'JTN1', 'BALTOS', '17:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1730', '20', 'JTN1', 'BALTOS', '17:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1745', '20', 'JTN1', 'BALTOS', '17:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1800', '20', 'JTN1', 'BALTOS', '18:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1815', '20', 'JTN1', 'BALTOS', '18:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1830', '20', 'JTN1', 'BALTOS', '18:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1845', '20', 'JTN1', 'BALTOS', '18:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1900', '20', 'JTN1', 'BALTOS', '19:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1915', '20', 'JTN1', 'BALTOS', '19:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1930', '20', 'JTN1', 'BALTOS', '19:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS1945', '20', 'JTN1', 'BALTOS', '19:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2000', '20', 'JTN1', 'BALTOS', '20:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2015', '20', 'JTN1', 'BALTOS', '20:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2030', '20', 'JTN1', 'BALTOS', '20:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2045', '20', 'JTN1', 'BALTOS', '20:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2100', '20', 'JTN1', 'BALTOS', '21:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2115', '20', 'JTN1', 'BALTOS', '21:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2130', '20', 'JTN1', 'BALTOS', '21:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2145', '20', 'JTN1', 'BALTOS', '21:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2200', '20', 'JTN1', 'BALTOS', '22:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2215', '20', 'JTN1', 'BALTOS', '22:15', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2230', '20', 'JTN1', 'BALTOS', '22:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2245', '20', 'JTN1', 'BALTOS', '22:45', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BALTOS2300', '20', 'JTN1', 'BALTOS', '23:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0500', '19', 'JTN1', 'BTC', '05:00', '14', '14', '1', 'JTN1-BALTOS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0515', '19', 'JTN1', 'BTC', '05:15', '14', '14', '1', 'JTN1-BALTOS0515', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0530', '19', 'JTN1', 'BTC', '05:30', '14', '14', '1', 'JTN1-BALTOS0530', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0545', '19', 'JTN1', 'BTC', '05:45', '14', '14', '1', 'JTN1-BALTOS0545', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0600', '19', 'JTN1', 'BTC', '06:00', '14', '14', '1', 'JTN1-BALTOS0600', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0615', '19', 'JTN1', 'BTC', '06:15', '14', '14', '1', 'JTN1-BALTOS0615', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0630', '19', 'JTN1', 'BTC', '06:30', '14', '14', '1', 'JTN1-BALTOS0630', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0645', '19', 'JTN1', 'BTC', '06:45', '14', '14', '1', 'JTN1-BALTOS0645', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0700', '19', 'JTN1', 'BTC', '07:00', '14', '14', '1', 'JTN1-BALTOS0700', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0715', '19', 'JTN1', 'BTC', '07:15', '14', '14', '1', 'JTN1-BALTOS0715', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0730', '19', 'JTN1', 'BTC', '07:30', '14', '14', '1', 'JTN1-BALTOS0730', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0745', '19', 'JTN1', 'BTC', '07:45', '14', '14', '1', 'JTN1-BALTOS0745', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0800', '19', 'JTN1', 'BTC', '08:00', '14', '14', '1', 'JTN1-BALTOS0800', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0815', '19', 'JTN1', 'BTC', '08:15', '14', '14', '1', 'JTN1-BALTOS0815', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0830', '19', 'JTN1', 'BTC', '08:30', '14', '14', '1', 'JTN1-BALTOS0830', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0845', '19', 'JTN1', 'BTC', '08:45', '14', '14', '1', 'JTN1-BALTOS0845', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0900', '19', 'JTN1', 'BTC', '09:00', '14', '14', '1', 'JTN1-BALTOS0900', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0915', '19', 'JTN1', 'BTC', '09:15', '14', '14', '1', 'JTN1-BALTOS0915', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0930', '19', 'JTN1', 'BTC', '09:30', '14', '14', '1', 'JTN1-BALTOS0930', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC0945', '19', 'JTN1', 'BTC', '09:45', '14', '14', '1', 'JTN1-BALTOS0945', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1000', '19', 'JTN1', 'BTC', '10:00', '14', '14', '1', 'JTN1-BALTOS1000', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1015', '19', 'JTN1', 'BTC', '10:15', '14', '14', '1', 'JTN1-BALTOS1015', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1030', '19', 'JTN1', 'BTC', '10:30', '14', '14', '1', 'JTN1-BALTOS1030', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1045', '19', 'JTN1', 'BTC', '10:45', '14', '14', '1', 'JTN1-BALTOS1045', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1100', '19', 'JTN1', 'BTC', '11:00', '14', '14', '1', 'JTN1-BALTOS1100', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1115', '19', 'JTN1', 'BTC', '11:15', '14', '14', '1', 'JTN1-BALTOS1115', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1130', '19', 'JTN1', 'BTC', '11:30', '14', '14', '1', 'JTN1-BALTOS1130', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1145', '19', 'JTN1', 'BTC', '11:45', '14', '14', '1', 'JTN1-BALTOS1145', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1200', '19', 'JTN1', 'BTC', '12:00', '14', '14', '1', 'JTN1-BALTOS1200', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1215', '19', 'JTN1', 'BTC', '12:15', '14', '14', '1', 'JTN1-BALTOS1215', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1230', '19', 'JTN1', 'BTC', '12:30', '14', '14', '1', 'JTN1-BALTOS1230', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1245', '19', 'JTN1', 'BTC', '12:45', '14', '14', '1', 'JTN1-BALTOS1245', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1300', '19', 'JTN1', 'BTC', '13:00', '14', '14', '1', 'JTN1-BALTOS1300', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1315', '19', 'JTN1', 'BTC', '13:15', '14', '14', '1', 'JTN1-BALTOS1315', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1330', '19', 'JTN1', 'BTC', '13:30', '14', '14', '1', 'JTN1-BALTOS1330', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1345', '19', 'JTN1', 'BTC', '13:45', '14', '14', '1', 'JTN1-BALTOS1345', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1400', '19', 'JTN1', 'BTC', '14:00', '14', '14', '1', 'JTN1-BALTOS1400', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1415', '19', 'JTN1', 'BTC', '14:15', '14', '14', '1', 'JTN1-BALTOS1415', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1430', '19', 'JTN1', 'BTC', '14:30', '14', '14', '1', 'JTN1-BALTOS1430', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1445', '19', 'JTN1', 'BTC', '14:45', '14', '14', '1', 'JTN1-BALTOS1445', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1500', '19', 'JTN1', 'BTC', '15:00', '14', '14', '1', 'JTN1-BALTOS1500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1515', '19', 'JTN1', 'BTC', '15:15', '14', '14', '1', 'JTN1-BALTOS1515', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1530', '19', 'JTN1', 'BTC', '15:30', '14', '14', '1', 'JTN1-BALTOS1530', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1545', '19', 'JTN1', 'BTC', '15:45', '14', '14', '1', 'JTN1-BALTOS1545', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1600', '19', 'JTN1', 'BTC', '16:00', '14', '14', '1', 'JTN1-BALTOS1600', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1615', '19', 'JTN1', 'BTC', '16:15', '14', '14', '1', 'JTN1-BALTOS1615', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1630', '19', 'JTN1', 'BTC', '16:30', '14', '14', '1', 'JTN1-BALTOS1630', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1645', '19', 'JTN1', 'BTC', '16:45', '14', '14', '1', 'JTN1-BALTOS1645', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1700', '19', 'JTN1', 'BTC', '17:00', '14', '14', '1', 'JTN1-BALTOS1700', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1715', '19', 'JTN1', 'BTC', '17:15', '14', '14', '1', 'JTN1-BALTOS1715', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1730', '19', 'JTN1', 'BTC', '17:30', '14', '14', '1', 'JTN1-BALTOS1730', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1745', '19', 'JTN1', 'BTC', '17:45', '14', '14', '1', 'JTN1-BALTOS1745', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1800', '19', 'JTN1', 'BTC', '18:00', '14', '14', '1', 'JTN1-BALTOS1800', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1815', '19', 'JTN1', 'BTC', '18:15', '14', '14', '1', 'JTN1-BALTOS1815', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1830', '19', 'JTN1', 'BTC', '18:30', '14', '14', '1', 'JTN1-BALTOS1830', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1845', '19', 'JTN1', 'BTC', '18:45', '14', '14', '1', 'JTN1-BALTOS1845', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1900', '19', 'JTN1', 'BTC', '19:00', '14', '14', '1', 'JTN1-BALTOS1900', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1915', '19', 'JTN1', 'BTC', '19:15', '14', '14', '1', 'JTN1-BALTOS1915', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN1-BTC1930', '19', 'JTN1', 'BTC', '19:30', '14', '14', '1', 'JTN1-BALTOS1930', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0500', '3', 'JTN2', 'BALTOS', '05:00', '14', '14', '1', 'JTN1-BALTOS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0515', '3', 'JTN2', 'BALTOS', '05:15', '14', '14', '1', 'JTN1-BALTOS0515', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0530', '3', 'JTN2', 'BALTOS', '05:30', '14', '14', '1', 'JTN1-BALTOS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0545', '3', 'JTN2', 'BALTOS', '05:45', '14', '14', '1', 'JTN1-BALTOS0545', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0600', '3', 'JTN2', 'BALTOS', '06:00', '14', '14', '1', 'JTN1-BALTOS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0615', '3', 'JTN2', 'BALTOS', '06:15', '14', '14', '1', 'JTN1-BALTOS0615', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0630', '3', 'JTN2', 'BALTOS', '06:30', '14', '14', '1', 'JTN1-BALTOS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0645', '3', 'JTN2', 'BALTOS', '06:45', '14', '14', '1', 'JTN1-BALTOS0645', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0700', '3', 'JTN2', 'BALTOS', '07:00', '14', '14', '1', 'JTN1-BALTOS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0715', '3', 'JTN2', 'BALTOS', '07:15', '14', '14', '1', 'JTN1-BALTOS0715', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0730', '3', 'JTN2', 'BALTOS', '07:30', '14', '14', '1', 'JTN1-BALTOS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0745', '3', 'JTN2', 'BALTOS', '07:45', '14', '14', '1', 'JTN1-BALTOS0745', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0800', '3', 'JTN2', 'BALTOS', '08:00', '14', '14', '1', 'JTN1-BALTOS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0815', '3', 'JTN2', 'BALTOS', '08:15', '14', '14', '1', 'JTN1-BALTOS0815', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0830', '3', 'JTN2', 'BALTOS', '08:30', '14', '14', '1', 'JTN1-BALTOS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0845', '3', 'JTN2', 'BALTOS', '08:45', '14', '14', '1', 'JTN1-BALTOS0845', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0900', '3', 'JTN2', 'BALTOS', '09:00', '14', '14', '1', 'JTN1-BALTOS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0915', '3', 'JTN2', 'BALTOS', '09:15', '14', '14', '1', 'JTN1-BALTOS0915', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0930', '3', 'JTN2', 'BALTOS', '09:30', '14', '14', '1', 'JTN1-BALTOS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS0945', '3', 'JTN2', 'BALTOS', '09:45', '14', '14', '1', 'JTN1-BALTOS0945', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1000', '3', 'JTN2', 'BALTOS', '10:00', '14', '14', '1', 'JTN1-BALTOS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1015', '3', 'JTN2', 'BALTOS', '10:15', '14', '14', '1', 'JTN1-BALTOS1015', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1030', '3', 'JTN2', 'BALTOS', '10:30', '14', '14', '1', 'JTN1-BALTOS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1045', '3', 'JTN2', 'BALTOS', '10:45', '14', '14', '1', 'JTN1-BALTOS1045', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1100', '3', 'JTN2', 'BALTOS', '11:00', '14', '14', '1', 'JTN1-BALTOS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1115', '3', 'JTN2', 'BALTOS', '11:15', '14', '14', '1', 'JTN1-BALTOS1115', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1130', '3', 'JTN2', 'BALTOS', '11:30', '14', '14', '1', 'JTN1-BALTOS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1145', '3', 'JTN2', 'BALTOS', '11:45', '14', '14', '1', 'JTN1-BALTOS1145', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1200', '3', 'JTN2', 'BALTOS', '12:00', '14', '14', '1', 'JTN1-BALTOS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1215', '3', 'JTN2', 'BALTOS', '12:15', '14', '14', '1', 'JTN1-BALTOS1215', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1230', '3', 'JTN2', 'BALTOS', '12:30', '14', '14', '1', 'JTN1-BALTOS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1245', '3', 'JTN2', 'BALTOS', '12:45', '14', '14', '1', 'JTN1-BALTOS1245', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1300', '3', 'JTN2', 'BALTOS', '13:00', '14', '14', '1', 'JTN1-BALTOS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1315', '3', 'JTN2', 'BALTOS', '13:15', '14', '14', '1', 'JTN1-BALTOS1315', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1330', '3', 'JTN2', 'BALTOS', '13:30', '14', '14', '1', 'JTN1-BALTOS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1345', '3', 'JTN2', 'BALTOS', '13:45', '14', '14', '1', 'JTN1-BALTOS1345', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1400', '3', 'JTN2', 'BALTOS', '14:00', '14', '14', '1', 'JTN1-BALTOS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1415', '3', 'JTN2', 'BALTOS', '14:15', '14', '14', '1', 'JTN1-BALTOS1415', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1430', '3', 'JTN2', 'BALTOS', '14:30', '14', '14', '1', 'JTN1-BALTOS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1445', '3', 'JTN2', 'BALTOS', '14:45', '14', '14', '1', 'JTN1-BALTOS1445', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1500', '3', 'JTN2', 'BALTOS', '15:00', '14', '14', '1', 'JTN1-BALTOS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1515', '3', 'JTN2', 'BALTOS', '15:15', '14', '14', '1', 'JTN1-BALTOS1515', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1530', '3', 'JTN2', 'BALTOS', '15:30', '14', '14', '1', 'JTN1-BALTOS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1545', '3', 'JTN2', 'BALTOS', '15:45', '14', '14', '1', 'JTN1-BALTOS1545', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1600', '3', 'JTN2', 'BALTOS', '16:00', '14', '14', '1', 'JTN1-BALTOS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1615', '3', 'JTN2', 'BALTOS', '16:15', '14', '14', '1', 'JTN1-BALTOS1615', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1630', '3', 'JTN2', 'BALTOS', '16:30', '14', '14', '1', 'JTN1-BALTOS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1645', '3', 'JTN2', 'BALTOS', '16:45', '14', '14', '1', 'JTN1-BALTOS1645', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1700', '3', 'JTN2', 'BALTOS', '17:00', '14', '14', '1', 'JTN1-BALTOS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1715', '3', 'JTN2', 'BALTOS', '17:15', '14', '14', '1', 'JTN1-BALTOS1715', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1730', '3', 'JTN2', 'BALTOS', '17:30', '14', '14', '1', 'JTN1-BALTOS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1745', '3', 'JTN2', 'BALTOS', '17:45', '14', '14', '1', 'JTN1-BALTOS1745', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1800', '3', 'JTN2', 'BALTOS', '18:00', '14', '14', '1', 'JTN1-BALTOS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1815', '3', 'JTN2', 'BALTOS', '18:15', '14', '14', '1', 'JTN1-BALTOS1815', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1830', '3', 'JTN2', 'BALTOS', '18:30', '14', '14', '1', 'JTN1-BALTOS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1845', '3', 'JTN2', 'BALTOS', '18:45', '14', '14', '1', 'JTN1-BALTOS1845', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1900', '3', 'JTN2', 'BALTOS', '19:00', '14', '14', '1', 'JTN1-BALTOS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1915', '3', 'JTN2', 'BALTOS', '19:15', '14', '14', '1', 'JTN1-BALTOS1915', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1930', '3', 'JTN2', 'BALTOS', '19:30', '14', '14', '1', 'JTN1-BALTOS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS1945', '3', 'JTN2', 'BALTOS', '19:45', '14', '14', '1', 'JTN1-BALTOS1945', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2000', '3', 'JTN2', 'BALTOS', '20:00', '14', '14', '1', 'JTN1-BALTOS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2015', '3', 'JTN2', 'BALTOS', '20:15', '14', '14', '1', 'JTN1-BALTOS2015', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2030', '3', 'JTN2', 'BALTOS', '20:30', '14', '14', '1', 'JTN1-BALTOS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2045', '3', 'JTN2', 'BALTOS', '20:45', '14', '14', '1', 'JTN1-BALTOS2045', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2100', '3', 'JTN2', 'BALTOS', '21:00', '14', '14', '1', 'JTN1-BALTOS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2115', '3', 'JTN2', 'BALTOS', '21:15', '14', '14', '1', 'JTN1-BALTOS2115', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2130', '3', 'JTN2', 'BALTOS', '21:30', '14', '14', '1', 'JTN1-BALTOS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2145', '3', 'JTN2', 'BALTOS', '21:45', '14', '14', '1', 'JTN1-BALTOS2145', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2200', '3', 'JTN2', 'BALTOS', '22:00', '14', '14', '1', 'JTN1-BALTOS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2215', '3', 'JTN2', 'BALTOS', '22:15', '14', '14', '1', 'JTN1-BALTOS2215', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2230', '3', 'JTN2', 'BALTOS', '22:30', '14', '14', '1', 'JTN1-BALTOS2230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2245', '3', 'JTN2', 'BALTOS', '22:45', '14', '14', '1', 'JTN1-BALTOS2245', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('JTN2-BALTOS2300', '3', 'JTN2', 'BALTOS', '23:00', '14', '14', '1', 'JTN1-BALTOS2300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0500', '17', 'STS', 'BALTOS', '05:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', 'CGN', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0530', '17', 'STS', 'BALTOS', '05:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0600', '17', 'STS', 'BALTOS', '06:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0630', '17', 'STS', 'BALTOS', '06:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0700', '17', 'STS', 'BALTOS', '07:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0730', '17', 'STS', 'BALTOS', '07:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0800', '17', 'STS', 'BALTOS', '08:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0830', '17', 'STS', 'BALTOS', '08:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0900', '17', 'STS', 'BALTOS', '09:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS0930', '17', 'STS', 'BALTOS', '09:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1000', '17', 'STS', 'BALTOS', '10:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1030', '17', 'STS', 'BALTOS', '10:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1100', '17', 'STS', 'BALTOS', '11:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1130', '17', 'STS', 'BALTOS', '11:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1200', '17', 'STS', 'BALTOS', '12:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1230', '17', 'STS', 'BALTOS', '12:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1300', '17', 'STS', 'BALTOS', '13:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1330', '17', 'STS', 'BALTOS', '13:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1400', '17', 'STS', 'BALTOS', '14:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1430', '17', 'STS', 'BALTOS', '14:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1500', '17', 'STS', 'BALTOS', '15:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1530', '17', 'STS', 'BALTOS', '15:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1600', '17', 'STS', 'BALTOS', '16:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1630', '17', 'STS', 'BALTOS', '16:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1700', '17', 'STS', 'BALTOS', '17:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1730', '17', 'STS', 'BALTOS', '17:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1800', '17', 'STS', 'BALTOS', '18:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1830', '17', 'STS', 'BALTOS', '18:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1900', '17', 'STS', 'BALTOS', '19:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS1930', '17', 'STS', 'BALTOS', '19:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS2000', '17', 'STS', 'BALTOS', '20:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS2030', '17', 'STS', 'BALTOS', '20:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS2100', '17', 'STS', 'BALTOS', '21:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS2130', '17', 'STS', 'BALTOS', '21:30', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BALTOS2200', '17', 'STS', 'BALTOS', '22:00', '14', '14', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0500', '16', 'STS', 'BTC', '05:00', '14', '14', '1', 'STS-BALTOS0500', '0', '0', '0', '0', '0', '0', '0', '0', null, '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0530', '16', 'STS', 'BTC', '05:30', '14', '14', '1', 'STS-BALTOS0530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0600', '16', 'STS', 'BTC', '06:00', '14', '14', '1', 'STS-BALTOS0600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0630', '16', 'STS', 'BTC', '06:30', '14', '14', '1', 'STS-BALTOS0630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0700', '16', 'STS', 'BTC', '07:00', '14', '14', '1', 'STS-BALTOS0700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0730', '16', 'STS', 'BTC', '07:30', '14', '14', '1', 'STS-BALTOS0730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0800', '16', 'STS', 'BTC', '08:00', '14', '14', '1', 'STS-BALTOS0800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0830', '16', 'STS', 'BTC', '08:30', '14', '14', '1', 'STS-BALTOS0830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0900', '16', 'STS', 'BTC', '09:00', '14', '14', '1', 'STS-BALTOS0900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC0930', '16', 'STS', 'BTC', '09:30', '14', '14', '1', 'STS-BALTOS0930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1000', '16', 'STS', 'BTC', '10:00', '14', '14', '1', 'STS-BALTOS1000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1030', '16', 'STS', 'BTC', '10:30', '14', '14', '1', 'STS-BALTOS1030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1100', '16', 'STS', 'BTC', '11:00', '14', '14', '1', 'STS-BALTOS1100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1130', '16', 'STS', 'BTC', '11:30', '14', '14', '1', 'STS-BALTOS1130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1200', '16', 'STS', 'BTC', '12:00', '14', '14', '1', 'STS-BALTOS1200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1230', '16', 'STS', 'BTC', '12:30', '14', '14', '1', 'STS-BALTOS1230', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1300', '16', 'STS', 'BTC', '13:00', '14', '14', '1', 'STS-BALTOS1300', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1330', '16', 'STS', 'BTC', '13:00', '14', '14', '1', 'STS-BALTOS1330', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1400', '16', 'STS', 'BTC', '14:00', '14', '14', '1', 'STS-BALTOS1400', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1430', '16', 'STS', 'BTC', '14:30', '14', '14', '1', 'STS-BALTOS1430', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1500', '16', 'STS', 'BTC', '15:00', '14', '14', '1', 'STS-BALTOS1500', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1530', '16', 'STS', 'BTC', '15:30', '14', '14', '1', 'STS-BALTOS1530', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1600', '16', 'STS', 'BTC', '16:00', '14', '14', '1', 'STS-BALTOS1600', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1630', '16', 'STS', 'BTC', '16:30', '14', '14', '1', 'STS-BALTOS1630', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1700', '16', 'STS', 'BTC', '17:00', '14', '14', '1', 'STS-BALTOS1700', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1730', '16', 'STS', 'BTC', '17:30', '14', '14', '1', 'STS-BALTOS1730', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1800', '16', 'STS', 'BTC', '18:00', '14', '14', '1', 'STS-BALTOS1800', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1830', '16', 'STS', 'BTC', '18:30', '14', '14', '1', 'STS-BALTOS1830', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1900', '16', 'STS', 'BTC', '19:00', '14', '14', '1', 'STS-BALTOS1900', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC1930', '16', 'STS', 'BTC', '19:30', '14', '14', '1', 'STS-BALTOS1930', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC2000', '16', 'STS', 'BTC', '20:00', '14', '14', '1', 'STS-BALTOS2000', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC2030', '16', 'STS', 'BTC', '20:30', '14', '14', '1', 'STS-BALTOS2030', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC2100', '16', 'STS', 'BTC', '21:00', '14', '14', '1', 'STS-BALTOS2100', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC2130', '16', 'STS', 'BTC', '21:30', '14', '14', '1', 'STS-BALTOS2130', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');
INSERT INTO `tbl_md_jadwal` VALUES ('STS-BTC2200', '16', 'STS', 'BTC', '22:00', '14', '14', '1', 'STS-BALTOS2200', '0', '0', '0', '0', '0', '0', '0', '0', '', '1', '0');

-- ----------------------------
-- Table structure for tbl_md_jurusan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_jurusan`;
CREATE TABLE `tbl_md_jurusan` (
  `IdJurusan` int(11) NOT NULL AUTO_INCREMENT,
  `KodeJurusan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `HargaTiket` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaTiketTuslah` double DEFAULT NULL,
  `FlagTiketTuslah` int(1) DEFAULT '0',
  `KodeAkunPendapatanPenumpang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunPendapatanPaket` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunCharge` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunBiayaSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopir` double DEFAULT '0',
  `KodeAkunBiayaSopirAkumulatif` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaSopirAkumulatif` double DEFAULT '0',
  `KodeAkunBiayaTol` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaTol` double DEFAULT '0',
  `KodeAkunBiayaParkir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaParkir` double DEFAULT '0',
  `KodeAkunBiayaBBM` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `BiayaBBM` double DEFAULT NULL,
  `LiterBBM` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPenumpangSopir` double DEFAULT '0',
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `KodeAkunKomisiPaketSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketSopir` double DEFAULT '0',
  `KodeAkunKomisiPaketCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketCSO` double DEFAULT '0',
  `FlagAktif` int(1) DEFAULT '1',
  `FlagLuarKota` int(1) DEFAULT '1',
  `HargaPaket1KiloPertama` double DEFAULT '0',
  `HargaPaket1KiloBerikut` double DEFAULT '0',
  `HargaPaket2KiloPertama` double DEFAULT '0',
  `HargaPaket2KiloBerikut` double DEFAULT '0',
  `HargaPaket3KiloPertama` double DEFAULT '0',
  `HargaPaket3KiloBerikut` double DEFAULT '0',
  `HargaPaket4KiloPertama` double DEFAULT '0',
  `HargaPaket4KiloBerikut` double DEFAULT '0',
  `HargaPaket5KiloPertama` double DEFAULT '0',
  `HargaPaket5KiloBerikut` double DEFAULT '0',
  `HargaPaket6KiloPertama` double NOT NULL DEFAULT '0',
  `HargaPaket6KiloBerikut` double NOT NULL DEFAULT '0',
  `FlagOperasionalJurusan` int(1) unsigned DEFAULT '0' COMMENT '0=reguler; 1=paket; 2=paket dan reguler;3=non-reguler;4=charter',
  `IsBiayaSopirKumulatif` int(1) unsigned DEFAULT '0',
  `IsVoucherBBM` int(1) NOT NULL DEFAULT '0' COMMENT '0=cash; 1=voucher BBM',
  `IsOnline` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdJurusan`),
  KEY `TbMDJurusan_index4998` (`BiayaSopir`) USING BTREE,
  KEY `TbMDJurusan_index4999` (`KodeJurusan`) USING BTREE,
  KEY `fk1mdjur` (`KodeCabangAsal`) USING BTREE,
  KEY `fk2mdjur` (`KodeCabangTujuan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_jurusan
-- ----------------------------
INSERT INTO `tbl_md_jurusan` VALUES ('1', 'BALTOS-JTN1', 'BALTOS', 'JTN1', '20000', '20000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;7=;9=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '20000', '0', '20000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('2', 'BTC-JTN1', 'BTC', 'JTN1', '20000', '20000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '20000', '0', '20000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('3', 'JTN2-BALTOS', 'JTN2', 'BALTOS', '20000', '20000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '20000', '0', '20000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('4', 'BALTOS-STS', 'BALTOS', 'STS', '35000', '35000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('5', 'BTC-STS', 'BTC', 'STS', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('6', 'BALTOS-CGN', 'BALTOS', 'CGN', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;7=;9=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('7', 'BTC-CGN', 'BTC', 'CGN', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('8', 'BALTOS-PNC', 'BALTOS', 'PNC', '80000', '80000', '0', null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '80000', '0', '80000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('9', 'BTC-PNC', 'BTC', 'PNC', '80000', '80000', '0', null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '80000', '0', '80000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('10', 'STS-PNC', 'STS', 'PNC', '45000', '45000', '0', null, null, null, null, '15000', null, '59000', null, '36000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '45000', '0', '45000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('11', 'PNC-STS', 'PNC', 'STS', '45000', '45000', '0', null, null, null, null, '15000', null, '59000', null, '36000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '45000', '0', '45000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('12', 'PNC-BTC', 'PNC', 'BTC', '80000', '80000', '0', null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '80000', '0', '80000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('13', 'PNC-BALTOS', 'PNC', 'BALTOS', '80000', '80000', '0', null, null, null, null, '0', null, '0', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '80000', '0', '80000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('14', 'CGN-BTC', 'CGN', 'BTC', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('15', 'CGN-BALTOS', 'CGN', 'BALTOS', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('16', 'STS-BTC', 'STS', 'BTC', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('17', 'STS-BALTOS', 'STS', 'BALTOS', '35000', '35000', '0', null, null, null, null, '13000', null, '42000', null, '0', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '35000', '0', '35000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('18', 'BALTOS-JTN2', 'BALTOS', 'JTN2', '20000', '20000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '20000', '0', '20000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('19', 'JTN1-BTC', 'JTN1', 'BTC', '20000', '20000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '20000', '0', '20000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `tbl_md_jurusan` VALUES ('20', 'JTN1-BALTOS', 'JTN1', 'BALTOS', '20000', '20000', '0', null, null, null, null, '14000', null, '20000', null, '18000', null, '0', null, null, '5=;14=;31=;', null, '0', null, '0', null, '6000', null, '0', '1', '1', '20000', '0', '20000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', '0');

-- ----------------------------
-- Table structure for tbl_md_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_kendaraan`;
CREATE TABLE `tbl_md_kendaraan` (
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Jenis` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Merek` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Tahun` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Warna` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) NOT NULL,
  `KodeSopir1` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeSopir2` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoSTNK` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoBPKB` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoRangka` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NoMesin` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KilometerAkhir` double DEFAULT '0',
  `Remark` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) NOT NULL DEFAULT '1',
  `IsCargo` int(1) DEFAULT '0',
  PRIMARY KEY (`KodeKendaraan`),
  KEY `TbMDKendaraan_index5022` (`NoPolisi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_kendaraan
-- ----------------------------
INSERT INTO `tbl_md_kendaraan` VALUES ('001', 'BALTOS', 'D1790PH', 'APV', 'SUZUKI', '2015', '', '5', '5', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('004', 'BALTOS', 'D7517AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('005', 'BALTOS', 'D7516AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('008', 'BALTOS', 'D7849AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('009', 'BALTOS', 'D7850AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('011', 'BALTOS', 'D7852AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('012', 'BALTOS', 'D7798AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('013', 'BALTOS', 'D7832AO', 'ELF', 'HINO', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('014', 'BALTOS', 'D7831A0', 'ELF', 'HINO', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('015', 'BALTOS', 'D7829AO', 'ELF', 'HINO', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('016', 'BALTOS', 'D7805AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('017', 'BALTOS', 'D7801AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('018', 'BALTOS', 'D7806AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('019', 'BALTOS', 'D7804AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('020', 'BALTOS', 'D7802AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('021', 'BALTOS', 'D7851AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('022', 'BALTOS', 'D7576AN', 'ELF', 'HINO', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('023', 'BALTOS', 'D7574AN', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('024', 'BALTOS', 'D7852AM', 'ELF', 'ISUZU', '2015', '', '14', '14', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');
INSERT INTO `tbl_md_kendaraan` VALUES ('026', 'BALTOS', 'D8015AN', 'MEDIUM BUS', 'HINO', '2015', '', '31', '31', 'A024', 'A024', '', '', '', '', '0', null, '1', '0');

-- ----------------------------
-- Table structure for tbl_md_kendaraan_layout
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_kendaraan_layout`;
CREATE TABLE `tbl_md_kendaraan_layout` (
  `Id` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `LayoutBody` int(3) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_kendaraan_layout
-- ----------------------------
INSERT INTO `tbl_md_kendaraan_layout` VALUES ('14', '14');
INSERT INTO `tbl_md_kendaraan_layout` VALUES ('31', '31');
INSERT INTO `tbl_md_kendaraan_layout` VALUES ('5', '5');
INSERT INTO `tbl_md_kendaraan_layout` VALUES ('7', '7');
INSERT INTO `tbl_md_kendaraan_layout` VALUES ('9', '9');

-- ----------------------------
-- Table structure for tbl_md_kota
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_kota`;
CREATE TABLE `tbl_md_kota` (
  `KodeKota` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `NamaKota` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`KodeKota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_kota
-- ----------------------------
INSERT INTO `tbl_md_kota` VALUES ('BDG', 'BANDUNG');
INSERT INTO `tbl_md_kota` VALUES ('JKT', 'JAKARTA');
INSERT INTO `tbl_md_kota` VALUES ('JTN', 'JATINANGOR');
INSERT INTO `tbl_md_kota` VALUES ('PWK', 'PURWAKARTA');

-- ----------------------------
-- Table structure for tbl_md_libur
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_libur`;
CREATE TABLE `tbl_md_libur` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TglLibur` date DEFAULT NULL,
  `KeteranganLibur` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `DiubahOleh` text COLLATE latin1_general_ci,
  `WaktuUbah` text COLLATE latin1_general_ci,
  PRIMARY KEY (`Id`),
  KEY `IDX_1` (`TglLibur`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_libur
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_member
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_member`;
CREATE TABLE `tbl_md_member` (
  `IdMember` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JenisKelamin` int(1) NOT NULL DEFAULT '0',
  `KategoriMember` int(1) DEFAULT NULL,
  `TempatLahir` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `NoKTP` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglRegistrasi` date DEFAULT NULL,
  `Alamat` text COLLATE latin1_general_ci,
  `Kota` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodePos` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Handphone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `Pekerjaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Point` double DEFAULT NULL,
  `ExpiredDate` date DEFAULT NULL,
  `WaktuTransaksiTerakhir` datetime DEFAULT NULL,
  `IdKartu` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSeriKartu` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KataSandi` text COLLATE latin1_general_ci,
  `FlagAktif` int(1) DEFAULT '1',
  `CabangDaftar` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `SaldoDeposit` double DEFAULT '0',
  `Signature` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdMember`),
  KEY `TbMDMember_index5245` (`IdKartu`) USING BTREE,
  KEY `TbMDMember_index5246` (`NoSeriKartu`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_member
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_paket_daftar_layanan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_paket_daftar_layanan`;
CREATE TABLE `tbl_md_paket_daftar_layanan` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeLayanan` char(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaLayanan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaKiloPertama` double DEFAULT NULL,
  `HargaKiloBerikutnya` double DEFAULT NULL,
  `FlagAktif` int(1) DEFAULT '1',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_paket_daftar_layanan
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_plan_asuransi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_plan_asuransi`;
CREATE TABLE `tbl_md_plan_asuransi` (
  `IdPlanAsuransi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NamaPlan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BesarPremi` double DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  PRIMARY KEY (`IdPlanAsuransi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_plan_asuransi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_promo
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_promo`;
CREATE TABLE `tbl_md_promo` (
  `KodePromo` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `LevelPromo` int(2) NOT NULL,
  `KodeCabangAsal` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `BerlakuMula` date NOT NULL,
  `BerlakuAkhir` date NOT NULL,
  `JumlahPenumpangMinimum` int(2) DEFAULT NULL,
  `JumlahPoint` double NOT NULL,
  `JumlahDiscount` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `FlagDiscount` int(1) NOT NULL DEFAULT '0',
  `FlagAktif` int(1) DEFAULT NULL,
  `FlagTargetPromo` tinyint(3) unsigned DEFAULT NULL,
  `JamMulai` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JamAkhir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPromo` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisMobil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NomorKursi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `BerlakuHari` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `IsPromoPP` int(1) DEFAULT '0',
  PRIMARY KEY (`KodePromo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_promo
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_md_sopir
-- ----------------------------
DROP TABLE IF EXISTS `tbl_md_sopir`;
CREATE TABLE `tbl_md_sopir` (
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Nama` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `HP` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `Alamat` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoSIM` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `FlagAktif` int(1) DEFAULT '1',
  `IsCargo` int(1) DEFAULT '0',
  PRIMARY KEY (`KodeSopir`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_md_sopir
-- ----------------------------
INSERT INTO `tbl_md_sopir` VALUES ('A001', 'AJAT', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A002', 'YOGA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A003', 'HERMAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A004', 'ADES', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A005', 'TEGUH', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A006', 'IVAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A007', 'RIZA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A008', 'CASMITA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A009', 'SOLIHIN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A010', 'DADANG', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A011', 'AGUS', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A012', 'AHMAD', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A013', 'SATYA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A014', 'DENI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A015', 'HADI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A016', 'DEDI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A017', 'SUMARDI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A018', 'JOKO', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A019', 'RUDI A', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A020', 'CECEP', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A021', 'YOMAS YOGI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A022', 'INDRA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A023', 'ASEP B', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A024', 'ABING', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A025', 'DINDIN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A026', 'BUDI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A027', 'SAKA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A028', 'IWAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A029', 'RIYANTO', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A030', 'TATANG', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A031', 'RUDI PWK', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A032', 'SAEFULLOH', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A033', 'PRI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A034', 'EDI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A035', 'ARI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A036', 'MAMAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A037', 'RENDI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A038', 'HEN HEN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A039', 'RIDWAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A040', 'NANAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A041', 'DISMAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A042', 'KOSWARA', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A043', 'HERLAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A044', 'RUDI MS', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A045', 'ASEP OLAY', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A046', 'RODI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A047', 'MUSLIM', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A048', 'ADANG', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A049', 'ARIS', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A050', 'MUSIRAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A051', 'IMAN', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A052', 'ADE', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A053', 'YUDI', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A054', 'DARISNO', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A055', 'USEP', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A056', 'NUROHIM', '', '', '', '1', '0');
INSERT INTO `tbl_md_sopir` VALUES ('A057', 'DARSONO', '', '', '', '1', '0');

-- ----------------------------
-- Table structure for tbl_member_deposit_topup_log
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member_deposit_topup_log`;
CREATE TABLE `tbl_member_deposit_topup_log` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMember` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeReferensi` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `Jumlah` double NOT NULL DEFAULT '0',
  `PetugasTopUp` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_member_deposit_topup_log
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_member_deposit_trx_log
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member_deposit_trx_log`;
CREATE TABLE `tbl_member_deposit_trx_log` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMember` int(11) DEFAULT NULL,
  `WaktuTransaksi` datetime NOT NULL,
  `IsDebit` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'debit=pengurangan saldo, kredit=penambahan saldo',
  `Keterangan` text COLLATE latin1_general_ci,
  `Jumlah` double NOT NULL DEFAULT '0',
  `Saldo` double NOT NULL DEFAULT '0',
  `Signature` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_member_deposit_trx_log
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_member_frekwensi_berangkat
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member_frekwensi_berangkat`;
CREATE TABLE `tbl_member_frekwensi_berangkat` (
  `IdFrekwensi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `FrekwensiBerangkat` double DEFAULT NULL,
  PRIMARY KEY (`IdFrekwensi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_member_frekwensi_berangkat
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_member_mutasi_point
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member_mutasi_point`;
CREATE TABLE `tbl_member_mutasi_point` (
  `IdMutasiPoint` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Referensi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` text COLLATE latin1_general_ci,
  `JenisMutasi` int(1) DEFAULT NULL,
  `JumlahPoint` double DEFAULT NULL,
  `Operator` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`IdMutasiPoint`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_member_mutasi_point
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_member_promo_point
-- ----------------------------
DROP TABLE IF EXISTS `tbl_member_promo_point`;
CREATE TABLE `tbl_member_promo_point` (
  `KodeProduk` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NamaProduk` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JumlahPointTukar` double NOT NULL,
  `FlagAktif` int(1) NOT NULL,
  PRIMARY KEY (`KodeProduk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_member_promo_point
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_page
-- ----------------------------
DROP TABLE IF EXISTS `tbl_page`;
CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` float NOT NULL,
  `nama_page` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_page
-- ----------------------------
INSERT INTO `tbl_page` VALUES ('2', '202', 'Reservasi non reguler');
INSERT INTO `tbl_page` VALUES ('3', '203', 'Penjadwalan Kendaraan');
INSERT INTO `tbl_page` VALUES ('4', '204', 'Pengumuman');
INSERT INTO `tbl_page` VALUES ('5', '205', 'Jenis Discount');
INSERT INTO `tbl_page` VALUES ('6', '206', 'Status On-line user');
INSERT INTO `tbl_page` VALUES ('7', '207', 'Promo');
INSERT INTO `tbl_page` VALUES ('8', '208', 'Daftar Manifest');
INSERT INTO `tbl_page` VALUES ('9', '209', 'Pembatalan');
INSERT INTO `tbl_page` VALUES ('10', '210', 'Laporan Pembatalan');
INSERT INTO `tbl_page` VALUES ('11', '211', 'Laporan Koreksi Discount');
INSERT INTO `tbl_page` VALUES ('12', '212', 'Dashboard');
INSERT INTO `tbl_page` VALUES ('13', '213', 'General Total');
INSERT INTO `tbl_page` VALUES ('14', '214', 'Berita Acara Insentif Sopir');
INSERT INTO `tbl_page` VALUES ('15', '215', 'Berita Acara Biaya Operasional');
INSERT INTO `tbl_page` VALUES ('16', '216', 'CSO');
INSERT INTO `tbl_page` VALUES ('17', '217', 'Cabang');
INSERT INTO `tbl_page` VALUES ('18', '218', 'Jurusan');
INSERT INTO `tbl_page` VALUES ('19', '219', 'Berdasarkan Jam');
INSERT INTO `tbl_page` VALUES ('20', '220', 'Keseluruhan');
INSERT INTO `tbl_page` VALUES ('21', '221', 'Kendaraan');
INSERT INTO `tbl_page` VALUES ('23', '223', 'Voucher');
INSERT INTO `tbl_page` VALUES ('24', '224', 'Laporan Data Paket');
INSERT INTO `tbl_page` VALUES ('25', '225', 'Laporan Paket Belum Diambil');
INSERT INTO `tbl_page` VALUES ('26', '226', 'Laporan Omzet Per Cabang');
INSERT INTO `tbl_page` VALUES ('27', '227', 'Laporan Omzet Per Jurusan');
INSERT INTO `tbl_page` VALUES ('28', '228', 'Cargo');
INSERT INTO `tbl_page` VALUES ('29', '229', 'Setoran CSO');
INSERT INTO `tbl_page` VALUES ('30', '230', 'Cabang');
INSERT INTO `tbl_page` VALUES ('31', '231', 'Harian');
INSERT INTO `tbl_page` VALUES ('32', '232', 'Laporan Rekon Fee');
INSERT INTO `tbl_page` VALUES ('33', '233', 'Biaya Insentif Sopir');
INSERT INTO `tbl_page` VALUES ('34', '234', 'Laporan Voucher BBM');
INSERT INTO `tbl_page` VALUES ('35', '235', 'Laporan Biaya Operasional');
INSERT INTO `tbl_page` VALUES ('36', '236', 'Penjualan Tiketux');
INSERT INTO `tbl_page` VALUES ('37', '237', 'Top Up Deposit');
INSERT INTO `tbl_page` VALUES ('38', '238', 'Laporan Transaksi Deposit');
INSERT INTO `tbl_page` VALUES ('39', '239', 'Data Member');
INSERT INTO `tbl_page` VALUES ('40', '240', 'Member Berulang Tahun');
INSERT INTO `tbl_page` VALUES ('41', '241', 'Pelanggan Potensi Jadi Member');
INSERT INTO `tbl_page` VALUES ('42', '242', 'Frekwensi Berangkat');
INSERT INTO `tbl_page` VALUES ('43', '243', 'Member Hampir Exipred');
INSERT INTO `tbl_page` VALUES ('44', '244', 'Kota');
INSERT INTO `tbl_page` VALUES ('45', '245', 'Cabang');
INSERT INTO `tbl_page` VALUES ('46', '246', 'Jurusan');
INSERT INTO `tbl_page` VALUES ('47', '247', 'Jadwal');
INSERT INTO `tbl_page` VALUES ('48', '248', 'Pengaturan Umum');
INSERT INTO `tbl_page` VALUES ('49', '249', 'Sopir');
INSERT INTO `tbl_page` VALUES ('50', '250', 'Mobil');
INSERT INTO `tbl_page` VALUES ('51', '251', 'User');
INSERT INTO `tbl_page` VALUES ('53', '252', 'Pengaturan Page');
INSERT INTO `tbl_page` VALUES ('55', '222', 'Sopir');
INSERT INTO `tbl_page` VALUES ('57', '201', 'Reservasi');

-- ----------------------------
-- Table structure for tbl_paket
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paket`;
CREATE TABLE `tbl_paket` (
  `NoTiket` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPengirim` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPengirim` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengirim` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NamaPenerima` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPenerima` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenerima` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPaket` double DEFAULT NULL,
  `Diskon` double NOT NULL DEFAULT '0',
  `TotalBayar` double NOT NULL DEFAULT '0',
  `Dimensi` char(3) COLLATE latin1_general_ci DEFAULT NULL,
  `IsVolumetrik` int(11) DEFAULT '0',
  `Panjang` int(11) DEFAULT '0',
  `Lebar` int(11) DEFAULT '0',
  `Tinggi` int(11) DEFAULT '0',
  `JumlahKoli` double DEFAULT NULL,
  `Berat` double DEFAULT NULL,
  `KeteranganPaket` text COLLATE latin1_general_ci,
  `InstruksiKhusus` text COLLATE latin1_general_ci,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPenjual` int(11) DEFAULT NULL,
  `KomisiPaketSopir` double DEFAULT NULL,
  `KodeAkunKomisiPaketSopir` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KomisiPaketCSO` double DEFAULT NULL,
  `KodeAkunKomisiPaketCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `FlagBatal` int(1) DEFAULT '0',
  `JenisPembayaran` int(1) DEFAULT NULL,
  `CaraPembayaran` int(1) DEFAULT NULL,
  `CetakTiket` int(1) DEFAULT '0',
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `PetugasPembatalan` int(10) unsigned DEFAULT NULL,
  `StatusDiambil` int(1) DEFAULT '0',
  `NamaPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoKTPPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengambil` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPengambilan` datetime DEFAULT NULL,
  `PetugasPemberi` int(10) unsigned DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(10) unsigned DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisBarang` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisLayanan` char(4) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMutasi` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuMutasi` text COLLATE latin1_general_ci NOT NULL,
  `UserPemutasi` text COLLATE latin1_general_ci NOT NULL,
  `IsSampai` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuSampai` datetime DEFAULT NULL,
  `UserCheckIn` int(11) DEFAULT NULL,
  `KodePelanggan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCheck` date DEFAULT NULL,
  `IdChecker` int(11) DEFAULT NULL,
  `IsCheck` int(1) DEFAULT '0',
  `LogMutasi` text COLLATE latin1_general_ci,
  PRIMARY KEY (`NoTiket`),
  KEY `tbl_paket_index5692` (`TelpPengirim`,`AlamatPenerima`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_paket
-- ----------------------------
INSERT INTO `tbl_paket` VALUES ('PG12DHHC1C', 'BALTOS', 'BALTOS-JTN10500', '1', '', '', '2016-02-17', '05:00', 'test', '', '08123456789', '2016-02-17 14:38:12', 'test', '', '08123456789', '20000', '0', '20000', '1', '0', '0', '0', '0', '1', '1', 'asdfg', '', '', '43', '6000', '', '0', '', 'MNFBAL1602175757', '0000-00-00 00:00:00', '1', '0', '0', '0', '1', '2016-02-17 14:38:14', null, null, '0', null, null, null, null, null, null, null, null, null, null, 'P2P', '0', '', '', '0', null, null, '', null, null, '0', null);
INSERT INTO `tbl_paket` VALUES ('PG22J1HHN1U', 'BALTOS', 'BALTOS-JTN10500', '1', '', '', '2016-02-17', '05:00', 'test', '', '08123456789', '2016-02-17 13:49:30', 'test', '', '08123456789', '20000', '0', '20000', '1', '0', '0', '0', '0', '1', '1', 'asdfg', '', '', '43', '0', '', '0', '', 'MNFBAL1602175757', '0000-00-00 00:00:00', '1', '0', '0', '0', '1', '2016-02-17 13:49:33', null, null, '0', null, null, null, null, null, null, null, null, null, null, 'P2P', '0', '', '', '0', null, null, '', null, null, '0', null);
INSERT INTO `tbl_paket` VALUES ('PGI122HHH3', 'BALTOS', 'BALTOS-JTN10500', '1', '', '', '2016-02-17', '05:00', 'test', '', '08123456789', '2016-02-17 14:17:03', 'test', '', '08123456789', '20000', '0', '20000', '1', '0', '0', '0', '0', '1', '1', 'asdfg', '', '', '43', '6000', '', '0', '', 'MNFBAL1602175757', '0000-00-00 00:00:00', '1', '0', '0', '0', '1', '2016-02-17 14:17:05', null, null, '0', null, null, null, null, null, null, null, null, null, null, 'P2P', '0', '', '', '0', null, null, '', null, null, '0', null);

-- ----------------------------
-- Table structure for tbl_paket_cargo
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paket_cargo`;
CREATE TABLE `tbl_paket_cargo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoAWB` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPengirim` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPengirim` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPengirim` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPenerima` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPenerima` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPenerima` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDiterima` date DEFAULT NULL,
  `KodeAsal` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `Asal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeTujuan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `Tujuan` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisBarang` varchar(3) COLLATE latin1_general_ci DEFAULT 'PRC' COMMENT 'PRC=PARCEL; DOK=DOKUMEN',
  `Via` varchar(3) COLLATE latin1_general_ci DEFAULT 'DRT' COMMENT 'DRT=Darat; UDR=Udara; LUT=Laut',
  `Layanan` varchar(3) COLLATE latin1_general_ci DEFAULT 'REG' COMMENT 'URG="Urgent"; REG="Reguler"',
  `Koli` int(2) DEFAULT '0' COMMENT 'Pax',
  `IsVolumetric` int(1) DEFAULT '0' COMMENT '0=perhitungan berdasarkan berat aktual; 1= perhitungan berdasarkan berat voume metric',
  `DimensiPanjang` double DEFAULT '0' COMMENT 'dalam satuan centimeter',
  `DimensiLebar` double DEFAULT '0' COMMENT 'satuan dalam centimeter',
  `DimensiTinggi` double DEFAULT '0' COMMENT 'dalam satuan centimeter',
  `Berat` double DEFAULT '0' COMMENT 'dalam kilogram',
  `LeadTime` varchar(10) COLLATE latin1_general_ci DEFAULT '' COMMENT 'string',
  `BiayaKirim` double DEFAULT '0',
  `BiayaTambahan` double DEFAULT '0',
  `BiayaPacking` double DEFAULT '0',
  `IsAsuransi` int(1) DEFAULT '0' COMMENT '0=tidak pake asuransi; 1=pake asuransi (besarnya asuransi adalah 2 permil dari harga pengakuan)',
  `HargaDiakui` double DEFAULT '0' COMMENT 'harga yang diakui pelanggan utk menghitung asuransi',
  `BiayaAsuransi` double DEFAULT '0',
  `TotalBiaya` double DEFAULT '0',
  `IsTunai` int(1) DEFAULT '1' COMMENT '1=Tunai;  0=kredit',
  `DeskripsiBarang` text COLLATE latin1_general_ci,
  `WaktuCatat` datetime DEFAULT NULL,
  `DicatatOleh` int(11) DEFAULT NULL,
  `NamaPencatat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetakAWB` datetime DEFAULT NULL,
  `DicetakOleh` int(11) DEFAULT NULL,
  `NamaPencetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `PoinPickedUp` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUp` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IsPickedUp` int(1) DEFAULT '0' COMMENT '0=belum di pickup; 1=sudah di pickup',
  `WaktuPickedUp` datetime DEFAULT NULL,
  `PetugasPemberi` int(11) DEFAULT NULL,
  `NamaPetugasPemberi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPenerima` int(11) DEFAULT NULL,
  `NamaPetugasPenerima` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `IsBatal` int(1) DEFAULT '0',
  `PetugasBatal` int(11) DEFAULT NULL,
  `NamaPetugasBatal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBatal` datetime DEFAULT NULL,
  `IsMutasi` int(1) DEFAULT '0',
  `TglDiterimaLama` date DEFAULT NULL,
  `PoinPickedUpLama` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUpLama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `PetugasMutasi` int(11) DEFAULT NULL,
  `NamaPetugasMutasi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `LogMutasi` text COLLATE latin1_general_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_paket_cargo
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_paket_cargo_md_harga
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paket_cargo_md_harga`;
CREATE TABLE `tbl_paket_cargo_md_harga` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeAsal` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeTujuan` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPerKg` double DEFAULT NULL,
  `LeadTime` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_paket_cargo_md_harga
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_paket_cargo_md_kota
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paket_cargo_md_kota`;
CREATE TABLE `tbl_paket_cargo_md_kota` (
  `KodeKota` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `NamaKota` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeProvinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`KodeKota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_paket_cargo_md_kota
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_paket_cargo_md_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paket_cargo_md_provinsi`;
CREATE TABLE `tbl_paket_cargo_md_provinsi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NamaProvinsi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_paket_cargo_md_provinsi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_paket_cargo_spj
-- ----------------------------
DROP TABLE IF EXISTS `tbl_paket_cargo_spj`;
CREATE TABLE `tbl_paket_cargo_spj` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDiterima` date DEFAULT NULL,
  `PoinPickedUp` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaPoinPickedUp` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPickedUp` datetime DEFAULT NULL,
  `PetugasPemberi` int(11) DEFAULT NULL,
  `NamaPetugasPemberi` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `PetugasCetak` int(11) DEFAULT NULL,
  `NamaPetugasCetak` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `BodyUnit` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Driver` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDriver` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahTransaksi` int(3) DEFAULT NULL,
  `JumlahKoli` int(3) DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalBerat` double DEFAULT NULL,
  `PetugasPickedUp` int(11) DEFAULT NULL,
  `NamaPetugasPickedUp` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `LogCetak` text COLLATE latin1_general_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_paket_cargo_spj
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pelanggan`;
CREATE TABLE `tbl_pelanggan` (
  `IdPelanggan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoHP` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NoTelp` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `TglPertamaTransaksi` date DEFAULT NULL,
  `TglTerakhirTransaksi` date DEFAULT NULL,
  `CSOTerakhir` int(10) unsigned DEFAULT NULL,
  `KodeJurusanTerakhir` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `FrekwensiPergi` double DEFAULT '0',
  `FlagMember` int(1) DEFAULT '0',
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdPelanggan`),
  KEY `IDX_01` (`NoHP`) USING BTREE,
  KEY `IDX_02` (`NoTelp`) USING BTREE,
  KEY `IDX_03` (`FrekwensiPergi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_pelanggan
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_pengaturan_parameter
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pengaturan_parameter`;
CREATE TABLE `tbl_pengaturan_parameter` (
  `NamaParameter` varchar(25) COLLATE latin1_general_ci NOT NULL,
  `NilaiParameter` text COLLATE latin1_general_ci NOT NULL,
  `Deskripsi` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`NamaParameter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_pengaturan_parameter
-- ----------------------------
INSERT INTO `tbl_pengaturan_parameter` VALUES ('BBM_SOLAR', '7500', 'harga solar per liter');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('BBM_SPBU_57', 'SBPU 57', '');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('BBM_SPBU_BDO', 'SBPU BDO', '');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('DEPOSIT_MIN', '2000000', 'saldo minimum yang harus tersimpan');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('DEPOSIT_SALDO', '10000000', 'Saldo Deposit Tiketux');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('FEE_TIKET', '1500', 'Fee tiket pertama & kedua');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('KOMISITTX1', '10000', 'Besar Komisi untuk tiketux');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('PERUSH_ALAMAT', 'alamat', 'Alamat perusahaan');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('PERUSH_EMAIL', 'email', 'email perusahaan');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('PERUSH_NAMA', 'HELDA TRAVEL', 'nama perusahaan penyedia jasa transportasi');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('PERUSH_TELP', '022', 'telp perusahaan');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('PERUSH_WEB', 'web', 'web perusahaan');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('PESAN_DITIKET', '', 'Menampilkan pesan di tiket');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('POLIS_ASURANSI', '1233232323', 'nomor polis induk asuransi');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('TGL_AKHIR_TUSLAH1', '2013-08-12 ', 'Tanggal Akhir Tuslah Luar Kota');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('TGL_AKHIR_TUSLAH2', '2013-08-11 ', 'tgl berakhirnya tuslah dalam kota');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('TGL_MULAI_TUSLAH1', '2013-08-02 ', 'Tanggal Mulai Tuslah Luar Kota');
INSERT INTO `tbl_pengaturan_parameter` VALUES ('TGL_MULAI_TUSLAH2', '2013-08-05 ', 'tgl mulai tuslah dalam kota');

-- ----------------------------
-- Table structure for tbl_pengaturan_umum
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pengaturan_umum`;
CREATE TABLE `tbl_pengaturan_umum` (
  `IdPengaturanUmum` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PesanSambutan` text COLLATE latin1_general_ci,
  `PesanDiTiket` text COLLATE latin1_general_ci,
  `FeeTiket` double DEFAULT NULL,
  `FeePaket` double DEFAULT NULL,
  `FeeSMS` double DEFAULT NULL,
  `NamaPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `AlamatPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TelpPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `EmailPerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WebSitePerusahaan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `HargaPaketMinimum` double DEFAULT NULL,
  `TglMulaiTuslah` date DEFAULT NULL,
  `TglAkhirTuslah` date DEFAULT NULL,
  `FlagBlokir` int(1) NOT NULL DEFAULT '0',
  `PemblokirUnblokir` int(11) NOT NULL,
  `WaktuBlokirUnblokir` datetime NOT NULL,
  PRIMARY KEY (`IdPengaturanUmum`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_pengaturan_umum
-- ----------------------------
INSERT INTO `tbl_pengaturan_umum` VALUES ('1', '', '', '5000', '0', '100', 'ARNES TRAVEL', '', '', '', 'www.tiketux.com', '10000', '2011-08-23', '2011-08-31', '0', '25', '2013-09-03 15:04:42');

-- ----------------------------
-- Table structure for tbl_pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pengumuman`;
CREATE TABLE `tbl_pengumuman` (
  `IdPengumuman` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodePengumuman` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JudulPengumuman` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Pengumuman` text COLLATE latin1_general_ci,
  `Kota` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PembuatPengumuman` int(10) unsigned DEFAULT NULL,
  `WaktuPembuatanPengumuman` datetime DEFAULT NULL,
  PRIMARY KEY (`IdPengumuman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_pengumuman
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_penjadwalan_kendaraan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_penjadwalan_kendaraan`;
CREATE TABLE `tbl_penjadwalan_kendaraan` (
  `IdPenjadwalan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeKendaraan` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `LayoutKursi` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursi` int(2) DEFAULT NULL,
  `KodeDriver` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaDriver` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `StatusAktif` int(1) DEFAULT '0',
  `Remark` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IdPenjadwalan`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_penjadwalan_kendaraan
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_penjadwalan_kendaraan_ba_penutupan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_penjadwalan_kendaraan_ba_penutupan`;
CREATE TABLE `tbl_penjadwalan_kendaraan_ba_penutupan` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `Remark` text COLLATE latin1_general_ci,
  `PetugasPenutup` int(11) DEFAULT NULL,
  `NamaPetugasPenutup` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPenutupan` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FX_1` (`TglBerangkat`) USING BTREE,
  KEY `FX_2` (`TglBerangkat`,`KodeJadwal`) USING BTREE,
  KEY `FX_3` (`IdJurusan`) USING BTREE,
  KEY `FX_4` (`PetugasPenutup`) USING BTREE,
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_penjadwalan_kendaraan_ba_penutupan
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_permission
-- ----------------------------
DROP TABLE IF EXISTS `tbl_permission`;
CREATE TABLE `tbl_permission` (
  `page_id` float NOT NULL,
  `user_level` decimal(3,1) NOT NULL,
  PRIMARY KEY (`page_id`,`user_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_permission
-- ----------------------------
INSERT INTO `tbl_permission` VALUES ('100', '0.0');
INSERT INTO `tbl_permission` VALUES ('200', '0.0');
INSERT INTO `tbl_permission` VALUES ('201', '0.0');
INSERT INTO `tbl_permission` VALUES ('201', '1.0');
INSERT INTO `tbl_permission` VALUES ('201', '1.2');
INSERT INTO `tbl_permission` VALUES ('201', '1.3');
INSERT INTO `tbl_permission` VALUES ('201', '1.4');
INSERT INTO `tbl_permission` VALUES ('201', '2.0');
INSERT INTO `tbl_permission` VALUES ('201', '2.1');
INSERT INTO `tbl_permission` VALUES ('201', '2.2');
INSERT INTO `tbl_permission` VALUES ('201', '3.0');
INSERT INTO `tbl_permission` VALUES ('202', '0.0');
INSERT INTO `tbl_permission` VALUES ('202', '1.0');
INSERT INTO `tbl_permission` VALUES ('202', '1.2');
INSERT INTO `tbl_permission` VALUES ('202', '1.3');
INSERT INTO `tbl_permission` VALUES ('202', '1.4');
INSERT INTO `tbl_permission` VALUES ('202', '2.0');
INSERT INTO `tbl_permission` VALUES ('202', '2.1');
INSERT INTO `tbl_permission` VALUES ('202', '2.2');
INSERT INTO `tbl_permission` VALUES ('202', '3.0');
INSERT INTO `tbl_permission` VALUES ('203', '0.0');
INSERT INTO `tbl_permission` VALUES ('203', '1.0');
INSERT INTO `tbl_permission` VALUES ('203', '1.2');
INSERT INTO `tbl_permission` VALUES ('203', '1.3');
INSERT INTO `tbl_permission` VALUES ('203', '1.4');
INSERT INTO `tbl_permission` VALUES ('203', '3.0');
INSERT INTO `tbl_permission` VALUES ('204', '0.0');
INSERT INTO `tbl_permission` VALUES ('204', '1.0');
INSERT INTO `tbl_permission` VALUES ('204', '1.2');
INSERT INTO `tbl_permission` VALUES ('204', '1.3');
INSERT INTO `tbl_permission` VALUES ('204', '1.4');
INSERT INTO `tbl_permission` VALUES ('204', '3.0');
INSERT INTO `tbl_permission` VALUES ('205', '0.0');
INSERT INTO `tbl_permission` VALUES ('205', '1.0');
INSERT INTO `tbl_permission` VALUES ('205', '1.2');
INSERT INTO `tbl_permission` VALUES ('206', '0.0');
INSERT INTO `tbl_permission` VALUES ('206', '1.0');
INSERT INTO `tbl_permission` VALUES ('206', '1.2');
INSERT INTO `tbl_permission` VALUES ('207', '0.0');
INSERT INTO `tbl_permission` VALUES ('207', '1.0');
INSERT INTO `tbl_permission` VALUES ('207', '1.2');
INSERT INTO `tbl_permission` VALUES ('208', '0.0');
INSERT INTO `tbl_permission` VALUES ('208', '1.0');
INSERT INTO `tbl_permission` VALUES ('208', '1.2');
INSERT INTO `tbl_permission` VALUES ('208', '1.3');
INSERT INTO `tbl_permission` VALUES ('208', '1.4');
INSERT INTO `tbl_permission` VALUES ('208', '5.0');
INSERT INTO `tbl_permission` VALUES ('209', '0.0');
INSERT INTO `tbl_permission` VALUES ('209', '1.0');
INSERT INTO `tbl_permission` VALUES ('209', '1.2');
INSERT INTO `tbl_permission` VALUES ('209', '1.3');
INSERT INTO `tbl_permission` VALUES ('209', '1.4');
INSERT INTO `tbl_permission` VALUES ('210', '0.0');
INSERT INTO `tbl_permission` VALUES ('210', '1.0');
INSERT INTO `tbl_permission` VALUES ('210', '1.2');
INSERT INTO `tbl_permission` VALUES ('210', '1.3');
INSERT INTO `tbl_permission` VALUES ('210', '1.4');
INSERT INTO `tbl_permission` VALUES ('210', '5.0');
INSERT INTO `tbl_permission` VALUES ('211', '0.0');
INSERT INTO `tbl_permission` VALUES ('211', '1.0');
INSERT INTO `tbl_permission` VALUES ('211', '1.2');
INSERT INTO `tbl_permission` VALUES ('211', '1.3');
INSERT INTO `tbl_permission` VALUES ('211', '1.4');
INSERT INTO `tbl_permission` VALUES ('211', '5.0');
INSERT INTO `tbl_permission` VALUES ('212', '0.0');
INSERT INTO `tbl_permission` VALUES ('212', '1.0');
INSERT INTO `tbl_permission` VALUES ('212', '1.2');
INSERT INTO `tbl_permission` VALUES ('212', '1.3');
INSERT INTO `tbl_permission` VALUES ('212', '1.4');
INSERT INTO `tbl_permission` VALUES ('212', '3.0');
INSERT INTO `tbl_permission` VALUES ('213', '0.0');
INSERT INTO `tbl_permission` VALUES ('213', '1.0');
INSERT INTO `tbl_permission` VALUES ('214', '0.0');
INSERT INTO `tbl_permission` VALUES ('214', '1.0');
INSERT INTO `tbl_permission` VALUES ('214', '1.2');
INSERT INTO `tbl_permission` VALUES ('214', '1.3');
INSERT INTO `tbl_permission` VALUES ('214', '1.4');
INSERT INTO `tbl_permission` VALUES ('214', '5.0');
INSERT INTO `tbl_permission` VALUES ('215', '0.0');
INSERT INTO `tbl_permission` VALUES ('215', '1.0');
INSERT INTO `tbl_permission` VALUES ('215', '1.2');
INSERT INTO `tbl_permission` VALUES ('215', '1.3');
INSERT INTO `tbl_permission` VALUES ('215', '1.4');
INSERT INTO `tbl_permission` VALUES ('215', '5.0');
INSERT INTO `tbl_permission` VALUES ('216', '0.0');
INSERT INTO `tbl_permission` VALUES ('216', '1.0');
INSERT INTO `tbl_permission` VALUES ('216', '1.2');
INSERT INTO `tbl_permission` VALUES ('216', '1.3');
INSERT INTO `tbl_permission` VALUES ('216', '1.4');
INSERT INTO `tbl_permission` VALUES ('216', '5.0');
INSERT INTO `tbl_permission` VALUES ('217', '0.0');
INSERT INTO `tbl_permission` VALUES ('217', '1.0');
INSERT INTO `tbl_permission` VALUES ('217', '1.2');
INSERT INTO `tbl_permission` VALUES ('217', '1.3');
INSERT INTO `tbl_permission` VALUES ('217', '1.4');
INSERT INTO `tbl_permission` VALUES ('217', '5.0');
INSERT INTO `tbl_permission` VALUES ('218', '0.0');
INSERT INTO `tbl_permission` VALUES ('218', '1.0');
INSERT INTO `tbl_permission` VALUES ('218', '1.2');
INSERT INTO `tbl_permission` VALUES ('218', '5.0');
INSERT INTO `tbl_permission` VALUES ('219', '0.0');
INSERT INTO `tbl_permission` VALUES ('219', '1.0');
INSERT INTO `tbl_permission` VALUES ('219', '1.2');
INSERT INTO `tbl_permission` VALUES ('219', '1.3');
INSERT INTO `tbl_permission` VALUES ('219', '5.0');
INSERT INTO `tbl_permission` VALUES ('220', '0.0');
INSERT INTO `tbl_permission` VALUES ('220', '1.0');
INSERT INTO `tbl_permission` VALUES ('220', '1.2');
INSERT INTO `tbl_permission` VALUES ('220', '1.3');
INSERT INTO `tbl_permission` VALUES ('220', '5.0');
INSERT INTO `tbl_permission` VALUES ('221', '0.0');
INSERT INTO `tbl_permission` VALUES ('221', '1.0');
INSERT INTO `tbl_permission` VALUES ('221', '1.2');
INSERT INTO `tbl_permission` VALUES ('221', '5.0');
INSERT INTO `tbl_permission` VALUES ('222', '0.0');
INSERT INTO `tbl_permission` VALUES ('222', '1.0');
INSERT INTO `tbl_permission` VALUES ('222', '1.2');
INSERT INTO `tbl_permission` VALUES ('222', '5.0');
INSERT INTO `tbl_permission` VALUES ('223', '0.0');
INSERT INTO `tbl_permission` VALUES ('223', '1.0');
INSERT INTO `tbl_permission` VALUES ('223', '5.0');
INSERT INTO `tbl_permission` VALUES ('224', '0.0');
INSERT INTO `tbl_permission` VALUES ('224', '1.0');
INSERT INTO `tbl_permission` VALUES ('224', '1.2');
INSERT INTO `tbl_permission` VALUES ('224', '1.3');
INSERT INTO `tbl_permission` VALUES ('224', '1.4');
INSERT INTO `tbl_permission` VALUES ('224', '2.0');
INSERT INTO `tbl_permission` VALUES ('224', '2.1');
INSERT INTO `tbl_permission` VALUES ('224', '2.2');
INSERT INTO `tbl_permission` VALUES ('224', '3.0');
INSERT INTO `tbl_permission` VALUES ('225', '0.0');
INSERT INTO `tbl_permission` VALUES ('225', '1.0');
INSERT INTO `tbl_permission` VALUES ('225', '1.2');
INSERT INTO `tbl_permission` VALUES ('225', '1.3');
INSERT INTO `tbl_permission` VALUES ('225', '1.4');
INSERT INTO `tbl_permission` VALUES ('225', '2.0');
INSERT INTO `tbl_permission` VALUES ('225', '2.1');
INSERT INTO `tbl_permission` VALUES ('225', '2.2');
INSERT INTO `tbl_permission` VALUES ('225', '3.0');
INSERT INTO `tbl_permission` VALUES ('226', '0.0');
INSERT INTO `tbl_permission` VALUES ('226', '1.0');
INSERT INTO `tbl_permission` VALUES ('226', '1.2');
INSERT INTO `tbl_permission` VALUES ('226', '1.3');
INSERT INTO `tbl_permission` VALUES ('226', '1.4');
INSERT INTO `tbl_permission` VALUES ('226', '2.0');
INSERT INTO `tbl_permission` VALUES ('226', '2.1');
INSERT INTO `tbl_permission` VALUES ('226', '2.2');
INSERT INTO `tbl_permission` VALUES ('226', '3.0');
INSERT INTO `tbl_permission` VALUES ('227', '0.0');
INSERT INTO `tbl_permission` VALUES ('227', '1.0');
INSERT INTO `tbl_permission` VALUES ('227', '1.2');
INSERT INTO `tbl_permission` VALUES ('227', '1.3');
INSERT INTO `tbl_permission` VALUES ('227', '1.4');
INSERT INTO `tbl_permission` VALUES ('227', '2.0');
INSERT INTO `tbl_permission` VALUES ('227', '2.1');
INSERT INTO `tbl_permission` VALUES ('227', '2.2');
INSERT INTO `tbl_permission` VALUES ('227', '3.0');
INSERT INTO `tbl_permission` VALUES ('228', '0.0');
INSERT INTO `tbl_permission` VALUES ('228', '1.0');
INSERT INTO `tbl_permission` VALUES ('228', '1.2');
INSERT INTO `tbl_permission` VALUES ('228', '1.3');
INSERT INTO `tbl_permission` VALUES ('228', '1.4');
INSERT INTO `tbl_permission` VALUES ('228', '2.0');
INSERT INTO `tbl_permission` VALUES ('228', '2.1');
INSERT INTO `tbl_permission` VALUES ('228', '2.2');
INSERT INTO `tbl_permission` VALUES ('228', '3.0');
INSERT INTO `tbl_permission` VALUES ('229', '0.0');
INSERT INTO `tbl_permission` VALUES ('229', '1.0');
INSERT INTO `tbl_permission` VALUES ('229', '1.2');
INSERT INTO `tbl_permission` VALUES ('229', '1.3');
INSERT INTO `tbl_permission` VALUES ('229', '2.0');
INSERT INTO `tbl_permission` VALUES ('229', '2.1');
INSERT INTO `tbl_permission` VALUES ('229', '5.0');
INSERT INTO `tbl_permission` VALUES ('230', '0.0');
INSERT INTO `tbl_permission` VALUES ('230', '1.0');
INSERT INTO `tbl_permission` VALUES ('230', '1.2');
INSERT INTO `tbl_permission` VALUES ('230', '5.0');
INSERT INTO `tbl_permission` VALUES ('231', '0.0');
INSERT INTO `tbl_permission` VALUES ('231', '1.0');
INSERT INTO `tbl_permission` VALUES ('231', '1.2');
INSERT INTO `tbl_permission` VALUES ('231', '5.0');
INSERT INTO `tbl_permission` VALUES ('232', '0.0');
INSERT INTO `tbl_permission` VALUES ('232', '1.0');
INSERT INTO `tbl_permission` VALUES ('232', '1.2');
INSERT INTO `tbl_permission` VALUES ('232', '5.0');
INSERT INTO `tbl_permission` VALUES ('233', '0.0');
INSERT INTO `tbl_permission` VALUES ('233', '1.0');
INSERT INTO `tbl_permission` VALUES ('233', '1.2');
INSERT INTO `tbl_permission` VALUES ('233', '5.0');
INSERT INTO `tbl_permission` VALUES ('234', '0.0');
INSERT INTO `tbl_permission` VALUES ('234', '1.0');
INSERT INTO `tbl_permission` VALUES ('234', '1.2');
INSERT INTO `tbl_permission` VALUES ('234', '5.0');
INSERT INTO `tbl_permission` VALUES ('235', '0.0');
INSERT INTO `tbl_permission` VALUES ('235', '1.0');
INSERT INTO `tbl_permission` VALUES ('235', '1.2');
INSERT INTO `tbl_permission` VALUES ('235', '5.0');
INSERT INTO `tbl_permission` VALUES ('236', '0.0');
INSERT INTO `tbl_permission` VALUES ('236', '1.0');
INSERT INTO `tbl_permission` VALUES ('236', '5.0');
INSERT INTO `tbl_permission` VALUES ('237', '0.0');
INSERT INTO `tbl_permission` VALUES ('237', '1.0');
INSERT INTO `tbl_permission` VALUES ('237', '5.0');
INSERT INTO `tbl_permission` VALUES ('238', '0.0');
INSERT INTO `tbl_permission` VALUES ('238', '1.0');
INSERT INTO `tbl_permission` VALUES ('238', '5.0');
INSERT INTO `tbl_permission` VALUES ('239', '0.0');
INSERT INTO `tbl_permission` VALUES ('239', '1.0');
INSERT INTO `tbl_permission` VALUES ('239', '1.2');
INSERT INTO `tbl_permission` VALUES ('239', '6.0');
INSERT INTO `tbl_permission` VALUES ('240', '0.0');
INSERT INTO `tbl_permission` VALUES ('240', '1.0');
INSERT INTO `tbl_permission` VALUES ('240', '1.2');
INSERT INTO `tbl_permission` VALUES ('241', '0.0');
INSERT INTO `tbl_permission` VALUES ('241', '1.0');
INSERT INTO `tbl_permission` VALUES ('241', '1.2');
INSERT INTO `tbl_permission` VALUES ('242', '0.0');
INSERT INTO `tbl_permission` VALUES ('242', '1.0');
INSERT INTO `tbl_permission` VALUES ('242', '1.2');
INSERT INTO `tbl_permission` VALUES ('243', '0.0');
INSERT INTO `tbl_permission` VALUES ('243', '1.0');
INSERT INTO `tbl_permission` VALUES ('243', '1.2');
INSERT INTO `tbl_permission` VALUES ('244', '0.0');
INSERT INTO `tbl_permission` VALUES ('244', '1.0');
INSERT INTO `tbl_permission` VALUES ('245', '0.0');
INSERT INTO `tbl_permission` VALUES ('245', '1.0');
INSERT INTO `tbl_permission` VALUES ('245', '1.2');
INSERT INTO `tbl_permission` VALUES ('245', '1.4');
INSERT INTO `tbl_permission` VALUES ('246', '0.0');
INSERT INTO `tbl_permission` VALUES ('246', '1.0');
INSERT INTO `tbl_permission` VALUES ('246', '1.2');
INSERT INTO `tbl_permission` VALUES ('246', '1.4');
INSERT INTO `tbl_permission` VALUES ('247', '0.0');
INSERT INTO `tbl_permission` VALUES ('247', '1.0');
INSERT INTO `tbl_permission` VALUES ('247', '1.2');
INSERT INTO `tbl_permission` VALUES ('247', '1.4');
INSERT INTO `tbl_permission` VALUES ('248', '0.0');
INSERT INTO `tbl_permission` VALUES ('248', '1.0');
INSERT INTO `tbl_permission` VALUES ('249', '0.0');
INSERT INTO `tbl_permission` VALUES ('249', '1.0');
INSERT INTO `tbl_permission` VALUES ('249', '1.2');
INSERT INTO `tbl_permission` VALUES ('249', '1.4');
INSERT INTO `tbl_permission` VALUES ('250', '0.0');
INSERT INTO `tbl_permission` VALUES ('250', '1.0');
INSERT INTO `tbl_permission` VALUES ('250', '1.2');
INSERT INTO `tbl_permission` VALUES ('250', '1.4');
INSERT INTO `tbl_permission` VALUES ('250', '7.0');
INSERT INTO `tbl_permission` VALUES ('251', '0.0');
INSERT INTO `tbl_permission` VALUES ('251', '1.0');
INSERT INTO `tbl_permission` VALUES ('252', '0.0');
INSERT INTO `tbl_permission` VALUES ('300', '0.0');
INSERT INTO `tbl_permission` VALUES ('400', '0.0');

-- ----------------------------
-- Table structure for tbl_posisi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_posisi`;
CREATE TABLE `tbl_posisi` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `TglBerangkat` date NOT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `JumlahKursi` int(2) NOT NULL DEFAULT '0',
  `SisaKursi` int(2) NOT NULL DEFAULT '0',
  `TglCetakSPJ` datetime DEFAULT NULL,
  `PetugasCetakSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Memo` text COLLATE latin1_general_ci,
  `PembuatMemo` varchar(150) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuBuatMemo` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagMemo` int(1) DEFAULT '0',
  `FlagOperasionalJurusan` int(1) DEFAULT '0' COMMENT '0=reguler; 1=paket; 2=paket dan reguler;3=non-reguler;4=charter',
  PRIMARY KEY (`ID`),
  KEY `TbPosisi_index5116` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbPosisi_index5117` (`NoSPJ`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_posisi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_posisi_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_posisi_detail`;
CREATE TABLE `tbl_posisi_detail` (
  `NomorKursi` int(2) NOT NULL,
  `ID` int(10) unsigned DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `JamBerangkat` time DEFAULT NULL,
  `IsSubJadwal` int(1) DEFAULT '0',
  `KodeJadwalUtama` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangAsal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeCabangTujuan` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date NOT NULL DEFAULT '0000-00-00',
  `StatusKursi` int(1) DEFAULT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Nama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Session` int(10) DEFAULT NULL,
  `StatusBayar` int(1) DEFAULT '0',
  `SessionTime` datetime DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`NomorKursi`,`KodeJadwal`,`TglBerangkat`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  KEY `idx2` (`KodeJadwal`,`TglBerangkat`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_posisi_detail
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_rekon_data
-- ----------------------------
DROP TABLE IF EXISTS `tbl_rekon_data`;
CREATE TABLE `tbl_rekon_data` (
  `IdRekonData` int(11) NOT NULL AUTO_INCREMENT,
  `TglRekonData` date DEFAULT NULL,
  `PetugasRekonData` int(10) unsigned DEFAULT NULL,
  `JumlahSPJ` double DEFAULT NULL,
  `JumlahPenumpang` double DEFAULT NULL,
  `JumlahTiketOnline` double DEFAULT NULL,
  `JumlahTiketBatal` double DEFAULT NULL,
  `JumlahFeeTiket` double DEFAULT NULL,
  `JumlahPaket` double DEFAULT NULL,
  `JumlahFeePaket` double DEFAULT NULL,
  `JumlahSMS` double DEFAULT NULL,
  `JumlahFeeSMS` double DEFAULT NULL,
  `TotalFee` double DEFAULT NULL,
  `TotalDiskonFee` double DEFAULT '0',
  `TotalBayarFee` double DEFAULT '0',
  `FlagDibayar` int(1) DEFAULT NULL,
  `WaktuCatatBayar` datetime DEFAULT NULL,
  `PetugasCatatBayar` int(10) unsigned DEFAULT NULL,
  `FeePerTiket` double NOT NULL,
  `FeePerPaket` double NOT NULL,
  `FeePerSMS` double DEFAULT NULL,
  PRIMARY KEY (`IdRekonData`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_rekon_data
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_reservasi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_reservasi`;
CREATE TABLE `tbl_reservasi` (
  `NoTiket` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PointMember` int(3) DEFAULT '0',
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HP` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `HargaTiket` double DEFAULT '0',
  `Charge` double DEFAULT '0',
  `BiayaTol` double DEFAULT NULL,
  `SubTotal` double DEFAULT '0',
  `Discount` double DEFAULT '0',
  `PPN` double DEFAULT '0',
  `Total` double DEFAULT '0',
  `PetugasPenjual` int(11) DEFAULT NULL,
  `FlagPesanan` int(1) DEFAULT '0',
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `CetakTiket` int(1) DEFAULT '0',
  `PetugasCetakTiket` int(11) DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `FlagSetor` int(1) DEFAULT '0',
  `PetugasCetakSPJ` int(11) DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscount` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(11) DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagRekonData` int(1) DEFAULT '0',
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagBatal` int(1) DEFAULT '0',
  `PetugasPembatalan` int(11) DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpang` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCode` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCodeExpiredTime` datetime DEFAULT NULL,
  `Signature` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Pemutasi` int(10) unsigned DEFAULT NULL,
  `MutasiDari` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `OTPUsed` int(1) NOT NULL DEFAULT '0',
  `IsSettlement` int(1) NOT NULL DEFAULT '1',
  `Komisi` double NOT NULL DEFAULT '0',
  `HargaTiketux` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`NoTiket`),
  KEY `TbReservasi_index5119` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5120` (`IdJurusan`) USING BTREE,
  KEY `TbReservasi_index5121` (`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5123` (`KodeCabang`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5124` (`CetakTiket`,`TglBerangkat`,`KodeJadwal`) USING BTREE,
  KEY `TbReservasi_index5125` (`KodeCabang`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5126` (`IdJurusan`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5127` (`KodeKendaraan`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5128` (`TglBerangkat`,`KodeKendaraan`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5129` (`PetugasPenjual`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5130` (`TglBerangkat`,`PetugasCetakTiket`,`CetakTiket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_reservasi
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_reservasi_olap
-- ----------------------------
DROP TABLE IF EXISTS `tbl_reservasi_olap`;
CREATE TABLE `tbl_reservasi_olap` (
  `NoTiket` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeJadwal` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(11) DEFAULT NULL,
  `KodeKendaraan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeBooking` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdMember` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PointMember` int(3) DEFAULT '0',
  `Nama` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Alamat` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `Telp` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `HP` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuPesan` datetime DEFAULT NULL,
  `NomorKursi` int(2) DEFAULT NULL,
  `HargaTiket` double DEFAULT '0',
  `Charge` double DEFAULT '0',
  `SubTotal` double DEFAULT '0',
  `Discount` double DEFAULT '0',
  `PPN` double DEFAULT '0',
  `Total` double DEFAULT '0',
  `PetugasPenjual` int(11) DEFAULT NULL,
  `FlagPesanan` int(1) DEFAULT '0',
  `NoSPJ` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `TglCetakSPJ` datetime DEFAULT NULL,
  `CetakSPJ` int(1) DEFAULT '0',
  `CetakTiket` int(1) DEFAULT '0',
  `PetugasCetakTiket` int(11) DEFAULT NULL,
  `WaktuCetakTiket` datetime DEFAULT NULL,
  `KomisiPenumpangCSO` double DEFAULT '0',
  `FlagSetor` int(1) DEFAULT '0',
  `PetugasCetakSPJ` int(11) DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisDiscount` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPembayaran` int(1) DEFAULT NULL,
  `WaktuSetor` datetime DEFAULT NULL,
  `PenerimaSetoran` int(11) DEFAULT NULL,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagRekonData` int(1) DEFAULT '0',
  `IdRekonData` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `FlagBatal` int(1) DEFAULT '0',
  `PetugasPembatalan` int(11) DEFAULT NULL,
  `WaktuPembatalan` datetime DEFAULT NULL,
  `KodeAkunPendapatan` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `JenisPenumpang` char(6) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeAkunKomisiPenumpangCSO` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `PaymentCode` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuMutasi` datetime DEFAULT NULL,
  `Pemutasi` int(10) unsigned DEFAULT NULL,
  `MutasiDari` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `OTP` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `OTPUsed` int(1) NOT NULL DEFAULT '0',
  `IsSettlement` int(1) NOT NULL DEFAULT '1',
  `Komisi` double NOT NULL DEFAULT '0',
  `HargaTiketux` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`NoTiket`),
  KEY `TbReservasi_index5119` (`KodeJadwal`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5120` (`IdJurusan`) USING BTREE,
  KEY `TbReservasi_index5121` (`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5123` (`KodeCabang`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5124` (`CetakTiket`,`TglBerangkat`,`KodeJadwal`) USING BTREE,
  KEY `TbReservasi_index5125` (`KodeCabang`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5126` (`IdJurusan`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5127` (`KodeKendaraan`,`TglBerangkat`) USING BTREE,
  KEY `TbReservasi_index5128` (`TglBerangkat`,`KodeKendaraan`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5129` (`PetugasPenjual`,`TglBerangkat`,`CetakTiket`) USING BTREE,
  KEY `TbReservasi_index5130` (`TglBerangkat`,`PetugasCetakTiket`,`CetakTiket`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_reservasi_olap
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_sessions
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sessions`;
CREATE TABLE `tbl_sessions` (
  `session_id` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `session_user_id` float NOT NULL,
  `session_start` float NOT NULL,
  `session_time` float NOT NULL,
  `session_ip` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `session_page` float NOT NULL,
  `session_logged_in` float NOT NULL,
  `session_admin` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_sessions
-- ----------------------------
INSERT INTO `tbl_sessions` VALUES ('1f54ccc9c63fcd9e8409a53f1371311f', '-1', '1456410000', '1456410000', '7f000001', '201', '0', '0');

-- ----------------------------
-- Table structure for tbl_spj
-- ----------------------------
DROP TABLE IF EXISTS `tbl_spj`;
CREATE TABLE `tbl_spj` (
  `NoSPJ` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `TglSPJ` datetime DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` datetime DEFAULT NULL,
  `JamBerangkat` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `IdLayout` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahKursiDisediakan` int(2) DEFAULT NULL,
  `JumlahPenumpang` int(2) DEFAULT '0',
  `JumlahPaket` int(3) DEFAULT NULL,
  `JumlahPaxPaket` int(3) DEFAULT NULL,
  `NoPolisi` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeDriver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `CSO` int(10) unsigned DEFAULT NULL,
  `CSOPaket` int(10) DEFAULT NULL,
  `Driver` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TotalOmzet` double DEFAULT NULL,
  `TotalOmzetPaket` double DEFAULT NULL,
  `FlagAmbilBiayaOP` int(1) DEFAULT NULL,
  `TglTransaksi` datetime DEFAULT NULL,
  `CabangBayarOP` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `InsentifSopir` double DEFAULT '0',
  `IsEkspedisi` int(1) DEFAULT '0' COMMENT '0=travel; 1=paket',
  `IsCetakVoucherBBM` int(1) DEFAULT '0',
  `IsSubJadwal` int(1) DEFAULT '0' COMMENT '0=not verified; 1=verified',
  `IsCheck` int(1) DEFAULT '0',
  `IdChecker` int(11) DEFAULT NULL,
  `NamaChecker` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `WaktuCheck` datetime DEFAULT NULL,
  `JumlahPenumpangCheck` int(2) DEFAULT NULL,
  `PathFoto` text COLLATE latin1_general_ci,
  `JumlahPaketCheck` int(2) DEFAULT '0',
  `JumlahPenumpangTambahanCheck` int(2) DEFAULT '0',
  `JumlahPenumpangTanpaTiketCheck` int(2) DEFAULT '0',
  `CatatanTambahanCheck` text COLLATE latin1_general_ci,
  `IdSetoran` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`NoSPJ`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE,
  KEY `IDX_2` (`NoPolisi`) USING BTREE,
  KEY `IDX_3` (`TglSPJ`) USING BTREE,
  KEY `IDX_4` (`TglSPJ`,`NoPolisi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_spj
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_surat_jalan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_surat_jalan`;
CREATE TABLE `tbl_surat_jalan` (
  `NoSJ` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `TglSJ` date NOT NULL,
  `WaktuSJ` varchar(6) COLLATE latin1_general_ci NOT NULL,
  `WaktuCetak` datetime NOT NULL,
  `IdPool` int(10) unsigned NOT NULL,
  `IdPetugas` int(10) unsigned NOT NULL,
  `NamaPetugas` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `NoBody` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoPolisi` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NamaSopir` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `JumlahTrip` int(2) unsigned NOT NULL DEFAULT '0',
  `IsBatal` int(1) unsigned NOT NULL DEFAULT '0',
  `WaktuBatal` datetime NOT NULL,
  `Pembatal` int(10) unsigned NOT NULL,
  `NamaPembatal` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`NoSJ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_surat_jalan
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_surat_jalan_biaya
-- ----------------------------
DROP TABLE IF EXISTS `tbl_surat_jalan_biaya`;
CREATE TABLE `tbl_surat_jalan_biaya` (
  `IdBiaya` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JenisBiaya` int(1) NOT NULL,
  `COA` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `NoSJ` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `IdJurusan` int(11) NOT NULL,
  `KodeJurusan` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `NamaBiaya` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `Jumlah` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `IsRealized` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdBiaya`),
  KEY `Index_2` (`NoSJ`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_surat_jalan_biaya
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `KodeCabang` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `user_active` float NOT NULL,
  `username` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_session_time` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_session_page` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_lastvisit` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `user_level` decimal(3,1) NOT NULL,
  `user_timezone` float NOT NULL,
  `berlaku` datetime NOT NULL,
  `NRP` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `birthday` datetime NOT NULL,
  `telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `hp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `status_online` int(1) NOT NULL,
  `waktu_update_terakhir` datetime DEFAULT NULL,
  `waktu_login` datetime NOT NULL,
  `waktu_logout` datetime NOT NULL,
  `path_foto` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `jumlah_pengumuman_baru` int(11) DEFAULT '0',
  `cetak_bbm` int(1) DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('1', '', '1', 'demo', '202cb962ac59075b964b07152d234b70', '1456404691', '201', '1456225016', '0.0', '0', '2018-04-23 15:18:29', 'TBK007', 'DEMO', '0000-00-00 00:00:00', '', '', '', '', '1', '2016-02-25 19:51:31', '2016-02-25 14:07:39', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('2', '', '1', 'ryan', '202cb962ac59075b964b07152d234b70', '1450860847', '200', '1450764150', '0.0', '0', '2099-04-23 15:18:29', 'TBK008', 'ryan', '0000-00-00 00:00:00', '', '', '', '', '1', '2015-12-23 15:54:07', '2016-01-14 14:05:37', '0000-00-00 00:00:00', '', '0', '0');
INSERT INTO `tbl_user` VALUES ('3', 'BALTOS', '1', 'nurman_numeiri', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '1', 'NURMAN NUMEIRI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('4', 'BALTOS', '1', 'iwan_priyadi', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '2', 'IWAN PRIYADI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('5', 'BALTOS', '1', 'saefudin', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '3', 'SAEFUDIN', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('6', 'BALTOS', '1', 'annette_sahetapy', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '4', 'ANNETTE SAHETAPY', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('7', 'BALTOS', '1', 'faisal_nursukma', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '5', 'FAISAL NURSUKMA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('8', 'BALTOS', '1', 'yanti_d', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '6', 'YANTI D', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('9', 'BALTOS', '1', 'resti_mugniatul_amanah', '202cb962ac59075b964b07152d234b70', '', '', '', '1.0', '0', '2016-12-23 00:00:00', '7', 'RESTI MUGNIATUL AMANAH', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('10', 'BALTOS', '1', 'siti_nureni', '202cb962ac59075b964b07152d234b70', '', '', '', '5.0', '0', '2016-12-23 00:00:00', '8', 'SITI NURENI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('11', 'BALTOS', '1', 'lian_yuliandani', '202cb962ac59075b964b07152d234b70', '', '', '', '5.0', '0', '2016-12-23 00:00:00', '9', 'LIAN YULIANDANI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('12', 'BALTOS', '1', 'rahmayanti', '202cb962ac59075b964b07152d234b70', '', '', '', '5.0', '0', '2016-12-23 00:00:00', '10', 'RAHMAYANTI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('13', 'BALTOS', '1', 'ari_febrian', '202cb962ac59075b964b07152d234b70', '', '', '', '5.0', '0', '2016-12-23 00:00:00', '11', 'ARI FEBRIAN', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('14', 'BALTOS', '1', 'ichsan_ruhimat_taufik', '202cb962ac59075b964b07152d234b70', '', '', '', '5.0', '0', '2016-12-23 00:00:00', '12', 'ICHSAN RUHIMAT TAUFIK', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('15', 'BALTOS', '1', 'nono_achmad_sukmana', '202cb962ac59075b964b07152d234b70', '', '', '', '8.0', '0', '2016-12-23 00:00:00', '13', 'NONO ACHMAD SUKMANA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('16', 'BALTOS', '1', 'wawan', '202cb962ac59075b964b07152d234b70', '', '', '', '8.0', '0', '2016-12-23 00:00:00', '14', 'WAWAN', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('17', 'BALTOS', '1', 'riana_egi_ginanjar', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '15', 'RIANA EGI GINANJAR', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('18', 'BALTOS', '1', 'robi_mahesa', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '16', 'ROBI MAHESA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('19', 'BALTOS', '1', 'fahmi_amarullah', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '17', 'FAHMI AMARULLAH', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('20', 'BALTOS', '1', 'rima_melani', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '18', 'RIMA MELANI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('21', 'BALTOS', '1', 'rike_novitasari', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '19', 'RIKE NOVITASARI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('22', 'BALTOS', '1', 'juwita_italia', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '20', 'JUWITA ITALIA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('23', 'BALTOS', '1', 'christina_yudhitya_sz', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '21', 'CHRISTINA YUDHITYA SZ', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('24', 'BALTOS', '1', 'gandi_nurachim', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '22', 'GANDI NURACHIM', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('25', 'BALTOS', '1', 'desi_nur_ismi', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '23', 'DESI NUR ISMI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('26', 'BALTOS', '1', 'ganjar_nur_fadilah', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '24', 'GANJAR NUR FADILAH', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('27', 'BALTOS', '1', 'dini_permata_sari', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '25', 'DINI PERMATA SARI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('28', 'BALTOS', '1', 'yusuf_bahtiar', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '26', 'YUSUF BAHTIAR', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('29', 'BALTOS', '1', 'rahmat_saeful', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '27', 'RAHMAT SAEFUL', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('30', 'BALTOS', '1', 'ivani_damayanti', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '28', 'IVANI DAMAYANTI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('31', 'BALTOS', '1', 'nurhasanah', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '29', 'NURHASANAH', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('32', 'BALTOS', '1', 'ai_sri_rahayu', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '30', 'AI SRI RAHAYU', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('33', 'BALTOS', '1', 'nita_normagufita_indrawati', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '31', 'NITA NORMAGUFITA INDRAWATI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('34', 'BALTOS', '1', 'herlina', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '32', 'HERLINA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('35', 'BALTOS', '1', 'siti_annisaida', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '33', 'SITI ANNISAIDA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('36', 'BALTOS', '1', 'anik_sukiarti', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '34', 'ANIK SUKIARTI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('37', 'BALTOS', '1', 'vikih', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '35', 'VIKIH', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('38', 'BALTOS', '1', 'sobarna', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '36', 'SOBARNA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('39', 'BALTOS', '1', 'ayu_ismaliani', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '37', 'AYU ISMALIANI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('40', 'BALTOS', '1', 'yani_andriyani', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '38', 'YANI ANDRIYANI', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('41', 'BALTOS', '1', 'yusbastian', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '39', 'YUSBASTIAN', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('42', 'BALTOS', '1', 'asep_febriyatna', '202cb962ac59075b964b07152d234b70', '', '', '', '1.3', '0', '2016-12-23 00:00:00', '40', 'ASEP FEBRIYATNA', '0000-00-00 00:00:00', '', '', '', '', '0', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '0', '0');
INSERT INTO `tbl_user` VALUES ('43', 'BALTOS', '1', 'adm', '202cb962ac59075b964b07152d234b70', '1455852088', '202', '1455851673', '0.0', '0', '2017-02-17 00:00:00', '123444', 'adm', '0000-00-00 00:00:00', '', '', '', '', '1', '2016-02-19 10:21:28', '2016-02-20 12:02:05', '0000-00-00 00:00:00', null, '0', '0');

-- ----------------------------
-- Table structure for tbl_user_baca_pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_baca_pengumuman`;
CREATE TABLE `tbl_user_baca_pengumuman` (
  `user_id` int(11) NOT NULL,
  `IdPengumuman` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`IdPengumuman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_user_baca_pengumuman
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_user_setoran
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_setoran`;
CREATE TABLE `tbl_user_setoran` (
  `IdSetoran` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `WaktuSetoran` datetime DEFAULT NULL,
  `IdUser` int(11) DEFAULT NULL,
  `NamaUser` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `JumlahTiketUmum` double DEFAULT '0',
  `JumlahTiketDiskon` double DEFAULT '0',
  `OmzetPenumpangTunai` double DEFAULT '0',
  `OmzetPenumpangDebit` double DEFAULT '0',
  `OmzetPenumpangKredit` double DEFAULT '0',
  `TotalDiskon` double DEFAULT '0',
  `TotalPaket` double DEFAULT '0',
  `OmzetPaket` double DEFAULT '0',
  `TotalBiaya` double DEFAULT '0',
  PRIMARY KEY (`IdSetoran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_user_setoran
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_voucher
-- ----------------------------
DROP TABLE IF EXISTS `tbl_voucher`;
CREATE TABLE `tbl_voucher` (
  `KodeVoucher` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `NoTiket` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `CabangBerangkat` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `CabangTujuan` varchar(25) COLLATE latin1_general_ci DEFAULT NULL,
  `PetugasPencetak` int(10) unsigned DEFAULT NULL,
  `WaktuCetak` datetime DEFAULT NULL,
  `ExpiredDate` date DEFAULT NULL,
  `WaktuDigunakan` datetime DEFAULT NULL,
  `PetugasPengguna` int(10) unsigned DEFAULT NULL,
  `NilaiVoucher` double NOT NULL,
  `IsHargaTetap` int(1) NOT NULL DEFAULT '0' COMMENT 'jika voucher nilainya adalah sebagai harga tertera, maka nilainya = 1',
  `IsBolehWeekEnd` int(1) NOT NULL DEFAULT '0',
  `IsReturn` int(1) NOT NULL DEFAULT '0',
  `IdJurusanBerangkat` int(11) DEFAULT NULL,
  `KodeJadwalBerangkat` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `NoTiketBerangkat` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Keterangan` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `IsBatal` int(1) DEFAULT '0',
  `DibatalkanOleh` int(11) DEFAULT NULL,
  `WaktuBatal` datetime DEFAULT NULL,
  PRIMARY KEY (`KodeVoucher`),
  KEY `KodeJadwal` (`KodeJadwal`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_voucher
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_voucher_bbm
-- ----------------------------
DROP TABLE IF EXISTS `tbl_voucher_bbm`;
CREATE TABLE `tbl_voucher_bbm` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `KodeVoucher` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `SecretKey` int(7) DEFAULT NULL,
  `NoSPJ` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglBerangkat` date DEFAULT NULL,
  `KodeJadwal` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IdJurusan` int(10) unsigned DEFAULT NULL,
  `NoBody` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `KodeSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSopir` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Kilometer` double DEFAULT NULL,
  `JenisBBM` int(1) DEFAULT NULL,
  `JumlahLiter` int(3) DEFAULT NULL,
  `JumlahBiaya` double DEFAULT NULL,
  `IdPetugas` int(10) unsigned DEFAULT NULL,
  `NamaPetugas` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `TglDicatat` date DEFAULT NULL,
  `KodeSPBU` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `NamaSPBU` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `IsApprove` int(1) DEFAULT '0',
  `WaktuApprove` datetime DEFAULT NULL,
  `PetugasApprove` int(11) DEFAULT NULL,
  `WaktuEdit` datetime DEFAULT NULL,
  `PetugasEdit` int(11) DEFAULT NULL,
  `CatatanEdit` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_voucher_bbm
-- ----------------------------

-- ----------------------------
-- Procedure structure for sp_biaya_op_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_biaya_op_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_biaya_op_tambah`(
  p_no_spj VARCHAR(20), p_kode_akun VARCHAR(20), p_flag_jenis_biaya INT(1),
  p_kode_kendaraan VARCHAR(20),p_kode_sopir VARCHAR(50),p_jumlah DOUBLE,
  p_id_petugas INTEGER,p_kode_jadwal VARCHAR(30),p_kode_cabang VARCHAR(50))
BEGIN

   INSERT INTO tbl_biaya_op (
     NoSPJ, KodeAkun, FlagJenisBiaya,
     TglTransaksi, NoPolisi, KodeSopir,
     Jumlah, IdPetugas, IdJurusan,
     KodeCabang)
   VALUES (
     p_no_spj, p_kode_akun, p_flag_jenis_biaya,
     NOW(),p_kode_kendaraan,p_kode_sopir,
     p_jumlah, p_id_petugas, f_jadwal_ambil_id_jurusan_by_kode_jadwal(p_kode_jadwal),
     p_kode_cabang);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_cabang_hapus
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_cabang_hapus`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_hapus`(p_list_kode VARCHAR(255))
BEGIN

  DELETE FROM tbl_md_cabang
  WHERE KodeCabang IN(p_list_kode);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_cabang_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_cabang_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_tambah`(
  p_kode VARCHAR(20), p_nama VARCHAR(70),
  p_alamat VARCHAR(255),p_kota VARCHAR(20),
  p_telp VARCHAR(50),p_fax VARCHAR(50),
  p_flag_agen INT(1))
BEGIN

   INSERT INTO tbl_md_cabang (KodeCabang,Nama, Alamat,Kota,Telp,Fax,FlagAgen)
   VALUES (p_kode,p_nama,p_alamat,p_kota,p_telp,p_fax,p_flag_agen);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_cabang_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_cabang_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cabang_ubah`(
  p_kode_old VARCHAR(20),
  p_kode VARCHAR(20), p_nama VARCHAR(70),
  p_alamat VARCHAR(255),p_kota VARCHAR(20),
  p_telp VARCHAR(50),p_fax VARCHAR(50),
  p_flag_agen INT(1))
BEGIN

   UPDATE tbl_md_cabang
   SET
     KodeCabang=p_kode, Nama=p_nama, Alamat=p_alamat, Kota=p_kota, Telp=p_telp,
     Fax=p_fax,FlagAgen=p_flag_agen
   WHERE KodeCabang=p_kode_old;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_deposit_debit
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_deposit_debit`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_debit`(

  p_kode_booking VARCHAR(30),
  p_jumlah DOUBLE,
  p_keterangan TEXT)
BEGIN

  DECLARE saldo_deposit DOUBLE;
  DECLARE p_komisi DOUBLE;

  START TRANSACTION;

    
    SELECT NilaiParameter INTO saldo_deposit FROM tbl_pengaturan_parameter WHERE NamaParameter ='DEPOSIT_SALDO';

    
    SELECT SUM(Komisi) INTO p_komisi
    FROM tbl_reservasi
    WHERE KodeBooking=p_kode_booking;

    IF saldo_deposit>=p_jumlah-p_komisi THEN
      

      
      UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter-(p_jumlah-p_komisi) WHERE NamaParameter='DEPOSIT_SALDO';

      
      INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
        VALUES(NOW(),1,p_keterangan,p_jumlah-p_komisi,saldo_deposit-(p_jumlah-p_komisi));

      
      UPDATE tbl_reservasi SET IsSettlement=0 WHERE KodeBooking=p_kode_booking;

    END IF;
    

  COMMIT;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_deposit_topup
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_deposit_topup`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deposit_topup`(p_kode_referensi VARCHAR(25))
BEGIN


  DECLARE jumlah_topup DOUBLE;
  DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;
  SELECT Jumlah INTO jumlah_topup FROM tbl_deposit_log_topup WHERE KodeReferensi=p_kode_referensi;

  UPDATE tbl_pengaturan_parameter SET NilaiParameter=NilaiParameter+jumlah_topup WHERE NamaParameter='DEPOSIT_SALDO';

  SELECT NilaiParameter INTO saldo_terakhir FROM tbl_pengaturan_parameter WHERE NamaParameter='DEPOSIT_SALDO';

  INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
    VALUES (NOW(),0,CONCAT('TOP UP DEPOSIT ',p_kode_referensi),jumlah_topup,saldo_terakhir);


  COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jadwal_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jadwal_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_tambah`(
  p_kode_jadwal VARCHAR(30), p_id_jurusan INTEGER,
  p_jam VARCHAR(5),p_jumlah_kursi INT(2),
  p_flag_sub_jadwal INT(1),p_kode_jadwal_utama VARCHAR(30),
  p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_md_jadwal (
    KodeJadwal,IdJurusan,JamBerangkat,
    JumlahKursi,FlagSubJadwal,KodeJadwalUtama,
    FlagAktif)
  VALUES(
    p_kode_jadwal,p_id_jurusan,p_jam,
    p_jumlah_kursi,p_flag_sub_jadwal,p_kode_jadwal_utama,
    p_flag_aktif);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jadwal_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jadwal_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah`(
  p_kode_jadwal_old VARCHAR(30),
  p_kode_jadwal VARCHAR(30), p_id_jurusan INTEGER,
  p_jam VARCHAR(5),p_jumlah_kursi INT(2),
  p_flag_sub_jadwal INT(1),p_kode_jadwal_utama VARCHAR(30),
  p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_md_jadwal SET
    KodeJadwal=p_kode_jadwal,IdJurusan=p_id_jurusan,
    JamBerangkat=p_jam,JumlahKursi=p_jumlah_kursi,
    FlagSubJadwal=p_flag_sub_jadwal,KodeJadwalUtama=p_kode_jadwal_utama,
    FlagAktif=p_flag_aktif
  WHERE KodeJadwal = p_kode_jadwal_old;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jadwal_ubah_kode_jadwal
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jadwal_ubah_kode_jadwal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah_kode_jadwal`(kode_jadwal_lama VARCHAR(30),kode_jadwal_baru VARCHAR(30))
BEGIN
  UPDATE tbl_posisi SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
  UPDATE tbl_posisi_backup SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_posisi_detail SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
  UPDATE tbl_posisi_detail_backup SET KodeJadwal=kode_jadwal_baru  WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_reservasi SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
  UPDATE tbl_reservasi_olap SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_spj SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;

  UPDATE tbl_penjadwalan_kendaraan SET KodeJadwal=kode_jadwal_baru WHERE KodeJadwal=kode_jadwal_lama;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jadwal_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jadwal_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jadwal_ubah_status_aktif`(p_kode_jadwal VARCHAR(30))
BEGIN

   UPDATE tbl_md_jadwal SET
     FlagAktif=1-FlagAktif
   WHERE KodeJadwal=p_kode_jadwal;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jenis_discount_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jenis_discount_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jenis_discount_ubah_status_aktif`(p_id_discount INTEGER)
BEGIN

   UPDATE tbl_jenis_discount SET
     FlagAktif=1-FlagAktif
   WHERE IdDiscount=p_id_discount;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jurusan_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jurusan_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_tambah`(
  p_kode_jurusan VARCHAR(10), p_kode_cabang_asal VARCHAR(20), p_kode_cabang_tujuan VARCHAR(20),
  p_harga_tiket DOUBLE,p_harga_tiket_tuslah DOUBLE,
  p_flag_tiket_tuslah INT(1),
  p_kode_akun_pendapatan_penumpang VARCHAR(20),
  p_kode_akun_pendapatan_paket VARCHAR(20),
  p_kode_akun_charge VARCHAR(20),
  p_kode_akun_biaya_sopir VARCHAR(20),
  p_biaya_sopir DOUBLE,
  p_kode_akun_biaya_tol VARCHAR(20),
  p_biaya_tol DOUBLE,
  p_kode_akun_biaya_parkir VARCHAR(20),
  p_biaya_parkir DOUBLE,
  p_kode_akun_biaya_bbm VARCHAR(20),
  p_biaya_bbm DOUBLE,
  p_kode_akun_komisi_penumpang_sopir VARCHAR(20),
  p_komisi_penumpang_sopir DOUBLE,
  p_kode_akun_komisi_penumpang_cso VARCHAR(20),
  p_komisi_penumpang_cso DOUBLE,
  p_kode_akun_komisi_paket_sopir VARCHAR(20),
  p_komisi_paket_sopir DOUBLE,
  p_kode_akun_komisi_paket_cso VARCHAR(20),
  p_komisi_paket_cso DOUBLE,
  p_flag_aktif INT(1),p_flag_luar_kota INT(1),
  p_harga_paket_1_kilo_pertama DOUBLE,p_harga_paket_1_kilo_berikut DOUBLE,
  p_harga_paket_2_kilo_pertama DOUBLE,p_harga_paket_2_kilo_berikut DOUBLE,
  p_harga_paket_3_kilo_pertama DOUBLE,p_harga_paket_3_kilo_berikut DOUBLE,
  p_harga_paket_4_kilo_pertama DOUBLE,p_harga_paket_4_kilo_berikut DOUBLE,
  p_harga_paket_5_kilo_pertama DOUBLE,p_harga_paket_5_kilo_berikut DOUBLE,
  p_harga_paket_6_kilo_pertama DOUBLE,p_harga_paket_6_kilo_berikut DOUBLE,
  p_flag_op_jurusan INT(1),p_is_biaya_sopir_kumulatif INT(1),p_is_voucher_bbm INT(1)
  )
BEGIN

   INSERT INTO tbl_md_jurusan (
     KodeJurusan, KodeCabangAsal, KodeCabangTujuan, HargaTiket,
     HargaTiketTuslah, FlagTiketTuslah,KodeAkunPendapatanPenumpang, KodeAkunPendapatanPaket,
     KodeAkunCharge, KodeAkunBiayaSopir, BiayaSopir, KodeAkunKomisiPenumpangSopir,
     KomisiPenumpangSopir, KodeAkunKomisiPenumpangCSO, KomisiPenumpangCSO, KodeAkunKomisiPaketSopir,
     KomisiPaketSopir, KodeAkunKomisiPaketCSO, KomisiPaketCSO,FlagAktif,
     KodeAkunBiayaTol,BiayaTol,
     KodeAkunBiayaParkir,BiayaParkir,
     KodeAkunBiayaBBM,BiayaBBM,FlagLuarKota,
     HargaPaket1KiloPertama,HargaPaket1KiloBerikut,
     HargaPaket2KiloPertama,HargaPaket2KiloBerikut,
     HargaPaket3KiloPertama,HargaPaket3KiloBerikut,
     HargaPaket4KiloPertama,HargaPaket4KiloBerikut,
     HargaPaket5KiloPertama,HargaPaket5KiloBerikut,
     HargaPaket6KiloPertama,HargaPaket6KiloBerikut,
     FlagOperasionalJurusan,IsBiayaSopirKumulatif,
     IsVoucherBBM)
   VALUES (
     p_kode_jurusan, p_kode_cabang_asal, p_kode_cabang_tujuan, p_harga_tiket,
     p_harga_tiket_tuslah, p_flag_tiket_tuslah, p_kode_akun_pendapatan_penumpang, p_kode_akun_pendapatan_paket,
     p_kode_akun_charge, p_kode_akun_biaya_sopir, p_biaya_sopir, p_kode_akun_komisi_penumpang_sopir,
     p_komisi_penumpang_sopir, p_kode_akun_komisi_penumpang_cso, p_komisi_penumpang_cso, p_kode_akun_komisi_paket_sopir,
     p_komisi_paket_sopir, p_kode_akun_komisi_paket_cso, p_komisi_paket_cso,p_flag_aktif,
     p_kode_akun_biaya_tol,p_biaya_tol,
     p_kode_akun_biaya_parkir,p_biaya_parkir,
     p_kode_akun_biaya_bbm,p_biaya_bbm,p_flag_luar_kota,
     p_harga_paket_1_kilo_pertama,p_harga_paket_1_kilo_berikut,
     p_harga_paket_2_kilo_pertama,p_harga_paket_2_kilo_berikut,
     p_harga_paket_3_kilo_pertama,p_harga_paket_3_kilo_berikut,
     p_harga_paket_4_kilo_pertama,p_harga_paket_4_kilo_berikut,
     p_harga_paket_5_kilo_pertama,p_harga_paket_5_kilo_berikut,
     p_harga_paket_6_kilo_pertama,p_harga_paket_6_kilo_berikut,
     p_flag_op_jurusan,p_is_biaya_sopir_kumulatif,
     p_is_voucher_bbm);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jurusan_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jurusan_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah`(
  p_id_jurusan INTEGER,
  p_kode_jurusan VARCHAR(10), p_kode_cabang_asal VARCHAR(20), p_kode_cabang_tujuan VARCHAR(20),
  p_harga_tiket DOUBLE,p_harga_tiket_tuslah DOUBLE,
  p_flag_tiket_tuslah INT(1),
  p_kode_akun_pendapatan_penumpang VARCHAR(20),
  p_kode_akun_pendapatan_paket VARCHAR(20),
  p_kode_akun_charge VARCHAR(20),
  p_kode_akun_biaya_sopir VARCHAR(20),
  p_biaya_sopir DOUBLE,
  p_kode_akun_biaya_tol VARCHAR(20),
  p_biaya_tol DOUBLE,
  p_kode_akun_biaya_parkir VARCHAR(20),
  p_biaya_parkir DOUBLE,
  p_kode_akun_biaya_bbm VARCHAR(20),
  p_biaya_bbm DOUBLE,
  p_kode_akun_komisi_penumpang_sopir VARCHAR(20),
  p_komisi_penumpang_sopir DOUBLE,
  p_kode_akun_komisi_penumpang_cso VARCHAR(20),
  p_komisi_penumpang_cso DOUBLE,
  p_kode_akun_komisi_paket_sopir VARCHAR(20),
  p_komisi_paket_sopir DOUBLE,
  p_kode_akun_komisi_paket_cso VARCHAR(20),
  p_komisi_paket_cso DOUBLE,
  p_flag_aktif INT(1),p_flag_luar_kota INT(1),
  p_harga_paket_1_kilo_pertama DOUBLE,p_harga_paket_1_kilo_berikut DOUBLE,
  p_harga_paket_2_kilo_pertama DOUBLE,p_harga_paket_2_kilo_berikut DOUBLE,
  p_harga_paket_3_kilo_pertama DOUBLE,p_harga_paket_3_kilo_berikut DOUBLE,
  p_harga_paket_4_kilo_pertama DOUBLE,p_harga_paket_4_kilo_berikut DOUBLE,
  p_harga_paket_5_kilo_pertama DOUBLE,p_harga_paket_5_kilo_berikut DOUBLE,
  p_harga_paket_6_kilo_pertama DOUBLE,p_harga_paket_6_kilo_berikut DOUBLE,
  p_flag_op_jurusan INT(1),p_is_biaya_sopir_kumulatif INT(1),p_is_voucher_bbm INT(1))
BEGIN

   UPDATE tbl_md_jurusan SET
     KodeJurusan=p_kode_jurusan,
     KodeCabangAsal=p_kode_cabang_asal,KodeCabangTujuan=p_kode_cabang_tujuan,
     HargaTiket=p_harga_tiket,HargaTiketTuslah=p_harga_tiket_tuslah,
     FlagTiketTuslah=p_flag_tiket_tuslah,KodeAkunPendapatanPenumpang=p_kode_akun_pendapatan_penumpang,
     KodeAkunPendapatanPaket=p_kode_akun_pendapatan_paket,KodeAkunCharge=p_kode_akun_charge,
     KodeAkunBiayaSopir=p_kode_akun_biaya_sopir, BiayaSopir=p_biaya_sopir,
     KodeAkunKomisiPenumpangSopir=p_kode_akun_komisi_penumpang_sopir, KomisiPenumpangSopir=p_komisi_penumpang_sopir,
     KodeAkunKomisiPenumpangCSO=p_kode_akun_komisi_penumpang_cso, KomisiPenumpangCSO=p_komisi_penumpang_cso,
     KodeAkunKomisiPaketSopir=p_kode_akun_komisi_paket_sopir, KomisiPaketSopir=p_komisi_paket_sopir,
     KodeAkunKomisiPaketCSO=p_kode_akun_komisi_paket_cso, KomisiPaketCSO=p_komisi_paket_cso,
     FlagAktif=p_flag_aktif,KodeAkunBiayaTol=p_kode_akun_biaya_tol,
     BiayaTol=p_biaya_tol,KodeAkunBiayaParkir=p_kode_akun_biaya_parkir,
     BiayaParkir=p_biaya_parkir,KodeAkunBiayaBBM=p_kode_akun_biaya_bbm,
     BiayaBBM=p_biaya_bbm,FlagLuarKota=p_flag_luar_kota,
     HargaPaket1KiloPertama=p_harga_paket_1_kilo_pertama,HargaPaket1KiloBerikut=p_harga_paket_1_kilo_berikut,
     HargaPaket2KiloPertama=p_harga_paket_2_kilo_pertama,HargaPaket2KiloBerikut=p_harga_paket_2_kilo_berikut,
     HargaPaket3KiloPertama=p_harga_paket_3_kilo_pertama,HargaPaket3KiloBerikut=p_harga_paket_3_kilo_berikut,
     HargaPaket4KiloPertama=p_harga_paket_4_kilo_pertama,HargaPaket4KiloBerikut=p_harga_paket_4_kilo_berikut,
     HargaPaket5KiloPertama=p_harga_paket_5_kilo_pertama,HargaPaket5KiloBerikut=p_harga_paket_5_kilo_berikut,
     HargaPaket6KiloPertama=p_harga_paket_6_kilo_pertama,HargaPaket6KiloBerikut=p_harga_paket_6_kilo_berikut,
     FlagOperasionalJurusan=p_flag_op_jurusan,IsBiayaSopirKumulatif=p_is_biaya_sopir_kumulatif,
     IsVoucherBBM=p_is_voucher_bbm
   WHERE IdJurusan=p_id_jurusan;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jurusan_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jurusan_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah_status_aktif`(p_id_jurusan INTEGER)
BEGIN

   UPDATE tbl_md_jurusan SET
     FlagAktif=1-FlagAktif
   WHERE IdJurusan=p_id_jurusan;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_jurusan_ubah_tuslah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_jurusan_ubah_tuslah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_jurusan_ubah_tuslah`(p_id_jurusan INTEGER,p_tuslah INT(1))
BEGIN

   UPDATE tbl_md_jurusan SET
     FlagTiketTuslah=p_tuslah
   WHERE IdJurusan=p_id_jurusan;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_kendaraan_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_kendaraan_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_tambah`(
  p_kode_kendaraan VARCHAR(20), p_kode_cabang VARCHAR(20), p_no_polisi VARCHAR(20),
  p_jenis VARCHAR(50),p_merek VARCHAR(50), p_tahun VARCHAR(50), p_warna VARCHAR(50),
  p_jumlah_kursi INT(2), p_kode_sopir1 VARCHAR(50), p_kode_sopir2 VARCHAR(50),
  p_no_STNK VARCHAR(50), p_no_BPKB VARCHAR(50), p_no_rangka VARCHAR(50),
  p_no_mesin VARCHAR(50), p_kilometer_akhir DOUBLE, p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_md_kendaraan (
    KodeKendaraan, KodeCabang, NoPolisi,
    Jenis,Merek, Tahun, Warna,
    JumlahKursi, KodeSopir1, KodeSopir2,
    NoSTNK, NoBPKB, NoRangka,
    NoMesin, KilometerAkhir, FlagAktif)
  VALUES(
   p_kode_kendaraan, p_kode_cabang, p_no_polisi,
    p_jenis,p_merek, p_tahun, p_warna,
    p_jumlah_kursi, p_kode_sopir1, p_kode_sopir2,
    p_no_STNK, p_no_BPKB, p_no_rangka,
    p_no_mesin, p_kilometer_akhir, p_flag_aktif);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_kendaraan_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_kendaraan_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_ubah`(
  p_kode_kendaraan_old VARCHAR(20),
  p_kode_kendaraan VARCHAR(20), p_kode_cabang VARCHAR(20), p_no_polisi VARCHAR(20),
  p_jenis VARCHAR(50),p_merek VARCHAR(50), p_tahun VARCHAR(50), p_warna VARCHAR(50),
  p_jumlah_kursi INT(2), p_kode_sopir1 VARCHAR(50), p_kode_sopir2 VARCHAR(50),
  p_no_STNK VARCHAR(50), p_no_BPKB VARCHAR(50), p_no_rangka VARCHAR(50),
  p_no_mesin VARCHAR(50), p_kilometer_akhir DOUBLE, p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_md_kendaraan SET
    KodeKendaraan=p_kode_kendaraan, KodeCabang=p_kode_cabang, NoPolisi=p_no_polisi,
    Jenis=p_jenis,Merek=p_merek, Tahun=p_tahun, Warna=p_warna,
    JumlahKursi=p_jumlah_kursi, KodeSopir1=p_kode_sopir1, KodeSopir2=p_kode_sopir2,
    NoSTNK=p_no_STNK, NoBPKB=p_no_BPKB, NoRangka=p_no_rangka,
    NoMesin=p_no_mesin, KilometerAkhir=p_kilometer_akhir, FlagAktif=p_flag_aktif
  WHERE KodeKendaraan = p_kode_kendaraan_old;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_kendaraan_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_kendaraan_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_kendaraan_ubah_status_aktif`(p_kode_kendaraan VARCHAR(50))
BEGIN

   UPDATE tbl_md_kendaraan SET
     FlagAktif=1-FlagAktif
   WHERE KodeKendaraan=p_kode_kendaraan;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_log_user_login
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_log_user_login`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_user_login`(
  p_user_id INTEGER, p_user_ip VARCHAR(255))
BEGIN

   INSERT INTO  tbl_log_user
     (user_id, waktu_login, user_ip)
   VALUES (p_user_id, NOW(), p_user_ip);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_log_user_logout
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_log_user_logout`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_log_user_logout`(
  p_user_id INTEGER)
BEGIN

  DECLARE p_id_log INTEGER;

  SELECT id_log INTO p_id_log
  FROM tbl_log_user
  WHERE
    user_id=p_user_id
    AND waktu_logout IS NULL
  ORDER BY waktu_login DESC LIMIT 0,1;

  UPDATE tbl_log_user
  SET waktu_logout=NOW()
  WHERE id_log=p_id_log;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_member_deposit_topup
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_member_deposit_topup`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_deposit_topup`(p_id_member VARCHAR(30),p_jumlah_topup DOUBLE,p_kode_referensi VARCHAR(30),p_petugas_topup INT(11))
BEGIN

  DECLARE saldo_terakhir DOUBLE;

  START TRANSACTION;
  
  
  INSERT INTO tbl_member_deposit_topup_log (IdMember,KodeReferensi,WaktuTransaksi,Jumlah,PetugasTopUp)
  VALUES(p_id_member,p_kode_referensi,NOW(),p_jumlah_topup,p_petugas_topup);

  
  UPDATE tbl_md_member SET 
    SaldoDeposit=SaldoDeposit+p_jumlah_topup,WaktuTransaksiTerakhir=NOW(),
    Signature=MD5(CONCAT(WaktuTransaksiTerakhir,'#',Handphone,'#',IdMember,'#indonesiatanahairbeta#',Nama,'#',SaldoDeposit,'#')) 
  WHERE IdMember=p_id_member;

  
  SELECT SaldoDeposit INTO saldo_terakhir FROM tbl_md_member WHERE IdMember=p_id_member;

  
  INSERT INTO tbl_member_deposit_trx_log (IdMember,WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo,Signature)
    VALUES (p_id_member,NOW(),0,CONCAT('TOP UP DEPOSIT ',p_id_member,' ',p_kode_referensi),p_jumlah_topup,saldo_terakhir,
      MD5(CONCAT(NOW(),'#',p_id_member,'#indonesiatanahairbeta#',p_jumlah_topup,'#',saldo_terakhir,'#')) );

  COMMIT;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_member_deposit_trx_log
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_member_deposit_trx_log`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_deposit_trx_log`(p_id_member VARCHAR(30),p_kode_booking VARCHAR(30), p_jumlah DOUBLE, p_keterangan TEXT)
BEGIN

  DECLARE saldo_deposit DOUBLE;

  START TRANSACTION;

    
    SELECT SaldoDeposit INTO saldo_deposit FROM tbl_md_member WHERE IdMember =p_id_member;

    IF saldo_deposit>=p_jumlah THEN
      

      
      UPDATE tbl_md_member SET
       SaldoDeposit=SaldoDeposit-p_jumlah,WaktuTransaksiTerakhir=NOW(),
       Signature=MD5(CONCAT(WaktuTransaksiTerakhir,'#',Handphone,'#',IdMember,'#indonesiatanahairbeta#',Nama,'#',SaldoDeposit,'#'))
      WHERE IdMember=p_id_member;

      
      INSERT INTO tbl_deposit_log_trx (WaktuTransaksi,IsDebit,Keterangan,Jumlah,Saldo)
        VALUES(NOW(),1,p_keterangan,p_jumlah,saldo_deposit-p_jumlah,
          MD5(CONCAT(NOW(),'#',p_id_member,'#indonesiatanahairbeta#',p_jumlah,'#',saldo_deposit-p_jumlah,'#')) );

    END IF;
    

  COMMIT;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_member_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_member_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_tambah`(
  p_id_member VARCHAR(25), p_nama VARCHAR(150), p_jenis_kelamin INT(1),
  p_kategori_member INT(1),p_tempat_lahir VARCHAR(30),p_tgl_lahir DATE,
  p_no_ktp VARCHAR(30),p_tgl_registrasi DATE,p_alamat TEXT,
  p_kota VARCHAR(30),p_kode_pos VARCHAR(6),p_telp VARCHAR(20),
  p_handphone VARCHAR(20),p_email VARCHAR(30),p_pekerjaan VARCHAR(50),
  p_point DOUBLE,p_expired_date DATE, p_id_kartu VARCHAR(255),
  p_no_seri_kartu VARCHAR(30),p_kata_sandi TEXT,p_flag_aktif INT(1),
  p_cabang_daftar VARCHAR(20))
BEGIN

  INSERT INTO tbl_md_member (
    IdMember, Nama, JenisKelamin,
    KategoriMember, TempatLahir, TglLahir,
    NoKTP, TglRegistrasi, Alamat,
    Kota, KodePos, Telp,
    Handphone, Email, Pekerjaan,
    Point, ExpiredDate, IdKartu,
    NoSeriKartu, KataSandi, FlagAktif,
    CabangDaftar)
  VALUES(
    p_id_member, p_nama, p_jenis_kelamin,
    p_kategori_member,p_tempat_lahir,p_tgl_lahir,
    p_no_ktp, p_tgl_registrasi, p_alamat,
    p_kota, p_kode_pos, p_telp,
    p_handphone, p_email, p_pekerjaan,
    p_point, p_expired_date, p_id_kartu,
    p_no_seri_kartu, p_kata_sandi, p_flag_aktif,
    p_cabang_daftar);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_member_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_member_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_ubah`(
  p_id_member VARCHAR(25), p_nama VARCHAR(150), p_jenis_kelamin INT(1),
  p_kategori_member INT(1),p_tempat_lahir VARCHAR(30),p_tgl_lahir DATE,
  p_no_ktp VARCHAR(30),p_tgl_registrasi DATE,p_alamat TEXT,
  p_kota VARCHAR(30),p_kode_pos VARCHAR(6),p_telp VARCHAR(20),
  p_handphone VARCHAR(20),p_email VARCHAR(30),p_pekerjaan VARCHAR(50),
  p_point DOUBLE,p_expired_date DATE, p_id_kartu VARCHAR(255),
  p_no_seri_kartu VARCHAR(30),p_kata_sandi TEXT,p_flag_aktif INT(1),
  p_cabang_daftar VARCHAR(20))
BEGIN

  UPDATE tbl_md_member SET
    Nama=p_nama, JenisKelamin=p_jenis_kelamin,
    KategoriMember=p_kategori_member, TempatLahir=p_tempat_lahir, TglLahir=p_tgl_lahir,
    NoKTP=p_no_ktp, TglRegistrasi=p_tgl_registrasi, Alamat=p_alamat,
    Kota=p_kota, KodePos=p_kode_pos, Telp=p_telp,
    Handphone=p_handphone, Email=p_email, Pekerjaan=p_pekerjaan,
    Point=p_point, ExpiredDate=p_expired_date, IdKartu=p_id_kartu,
    NoSeriKartu=p_no_seri_kartu, KataSandi=p_kata_sandi, FlagAktif=p_flag_aktif,
    CabangDaftar=p_cabang_daftar
  WHERE IdMember=p_id_member;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_member_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_member_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_member_ubah_status_aktif`(p_id_member VARCHAR(25))
BEGIN

   UPDATE tbl_md_member SET
     FlagAktif=1-FlagAktif
   WHERE IdMember=p_id_member;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_ambil_paket
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_ambil_paket`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ambil_paket`(
  p_no_tiket VARCHAR(50),p_nama_pengambil VARCHAR(20),p_no_ktp_pengambil VARCHAR(20),
  p_petugas_pemberi INTEGER)
BEGIN

  UPDATE tbl_paket
  SET
    NamaPengambil=p_nama_pengambil,
    NoKTPPengambil=p_no_ktp_pengambil,
    PetugasPemberi=p_petugas_pemberi,
    WaktuPengambilan=NOW(),
    StatusDiambil=1
  WHERE
    NoTiket = p_no_tiket;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_batal
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_batal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_batal`(
  p_NoTiket VARCHAR(50),p_PetugasPembatalan INTEGER,p_WaktuPembatalan DATETIME)
BEGIN

  UPDATE tbl_paket SET
    FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
    WaktuPembatalan=p_WaktuPembatalan
  WHERE NoTiket=p_NoTiket;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_mutasi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_mutasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_mutasi`(
  p_kode_jadwal VARCHAR(50),p_id_jurusan INTEGER, p_tgl_berangkat DATE,p_jam_berangkat VARCHAR(10),
  p_user_pemutasi TEXT,p_no_tiket VARCHAR(20))
BEGIN

  UPDATE tbl_paket
  SET
    KodeJadwal=p_kode_jadwal,
    IdJurusan=p_id_jurusan,
    TglBerangkat=p_tgl_berangkat,
    JamBerangkat=p_jam_berangkat,
    FlagMutasi=1,
    WaktuMutasi=CONCAT(NOW(),';',WaktuMutasi),
    UserPemutasi=CONCAT(p_user_pemutasi,';',UserPemutasi)
  WHERE
    NoTiket = p_no_tiket;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_tambah`(
  p_no_tiket VARCHAR(50), p_kode_cabang VARCHAR(20), p_kode_jadwal VARCHAR(30),
  p_id_jurusan INTEGER, p_kode_kendaraan VARCHAR(20), p_kode_sopir VARCHAR(50),
  p_tgl_berangkat DATE, p_jam_berangkat VARCHAR(10), p_nama_pengirim VARCHAR(100),
  p_alamat_pengirim VARCHAR(100), p_telp_pengirim VARCHAR(15), p_waktu_pesan DATETIME,
  p_nama_penerima VARCHAR(100), p_alamat_penerima VARCHAR(100), p_telp_penerima VARCHAR(15),
  p_harga_paket DOUBLE,p_keterangan_paket TEXT, p_petugas_penjual INTEGER,
  p_no_SPJ VARCHAR(35), p_tgl_cetak_SPJ DATE,
  p_cetak_SPJ INT(1), p_komisi_paket_CSO DOUBLE, p_komisi_paket_sopir DOUBLE,
  p_kode_akun_pendapatan VARCHAR(20), p_kode_akun_komisi_paket_CSO VARCHAR(20), p_kode_akun_komisi_paket_sopir VARCHAR(20),
  p_jumlah_koli DOUBLE,p_berat DOUBLE,p_jenis_barang VARCHAR(10),
  p_layanan CHAR(2),p_cara_bayar INT(1),p_is_ekspedisi INT(1))
BEGIN

  INSERT INTO tbl_paket (
    NoTiket, KodeCabang, KodeJadwal,
    IdJurusan, KodeKendaraan, KodeSopir,
    TglBerangkat, JamBerangkat, NamaPengirim,
    AlamatPengirim, TelpPengirim, WaktuPesan,
    NamaPenerima, AlamatPenerima, TelpPenerima,
    HargaPaket,KeteranganPaket, PetugasPenjual,
    NoSPJ, TglCetakSPJ,
    CetakSPJ, KomisiPaketCSO, KomisiPaketSopir,
    KodeAkunPendapatan,KodeAkunKomisiPaketCSO,KodeAkunKomisiPaketSopir,
    JumlahKoli,Berat,JenisBarang,
    Layanan,CaraPembayaran,
    FlagBatal,IsEkspedisi)
  VALUES(
    p_no_tiket, p_kode_cabang, p_kode_jadwal,
    p_id_jurusan, p_kode_kendaraan , p_kode_sopir ,
    p_tgl_berangkat, p_jam_berangkat , p_nama_pengirim ,
    p_alamat_pengirim, p_telp_pengirim, p_waktu_pesan,
    p_nama_penerima, p_alamat_penerima, p_telp_penerima,
    p_harga_paket,p_keterangan_paket, p_petugas_penjual,
    p_no_SPJ, p_tgl_cetak_SPJ,
    p_cetak_SPJ, p_komisi_paket_CSO, p_komisi_paket_sopir,
    p_kode_akun_pendapatan, p_kode_akun_komisi_paket_CSO, p_kode_akun_komisi_paket_sopir,
    p_jumlah_koli,p_berat,p_jenis_barang,
    p_layanan,p_cara_bayar,
    0,p_is_ekspedisi);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_ubah_data
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_ubah_data`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ubah_data`(
  p_no_tiket VARCHAR(50),
  p_nama_pengirim VARCHAR(100),
  p_alamat_pengirim VARCHAR(100),
  p_telp_pengirim VARCHAR(15),
  p_nama_penerima VARCHAR(100),
  p_alamat_penerima VARCHAR(100),
  p_telp_penerima VARCHAR(15))
BEGIN

  UPDATE tbl_paket SET
    NamaPengirim=p_nama_pengirim,
    AlamatPengirim=p_alamat_pengirim,
    TelpPengirim=p_telp_pengirim,
    NamaPenerima=p_nama_penerima,
    AlamatPenerima=p_alamat_penerima,
    TelpPenerima=p_telp_penerima
  WHERE
    NoTiket=p_no_tiket;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_ubah_data_after_spj
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_ubah_data_after_spj`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_ubah_data_after_spj`(
  p_kode_jadwal VARCHAR(50), p_tgl_berangkat DATE, p_sopir_dipilih VARCHAR(50),
  p_mobil_dipilih VARCHAR(50), p_no_spj VARCHAR(50))
BEGIN

  UPDATE tbl_paket
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    TglCetakSPJ=NOW(),
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_paket_update_cetak_tiket
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_paket_update_cetak_tiket`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_paket_update_cetak_tiket`(
  p_no_tiket VARCHAR(50), p_jenis_pembayaran INT(1),p_cabang_transaksi VARCHAR(30))
BEGIN

  UPDATE tbl_paket
  SET
    CetakTiket=1,
    JenisPembayaran=p_jenis_pembayaran,
    KodeCabang=p_cabang_transaksi
  WHERE
    NoTiket = p_no_tiket;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_pelanggan_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_pelanggan_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pelanggan_tambah`(
  p_no_hp VARCHAR(20), p_no_telp VARCHAR(20),p_nama VARCHAR(150),
  p_alamat VARCHAR(50), p_tgl_pertama_transaksi DATE, p_tgl_terakhir_transaksi DATE,
  p_cso_terakhir INTEGER, p_kode_jurusan_terakhir VARCHAR(10),p_frekwensi_pergi DOUBLE,
  p_flag_member INT(1))
BEGIN

  INSERT INTO tbl_pelanggan (
    NoHP, NoTelp, Nama,
    Alamat, TglPertamaTransaksi, TglTerakhirTransaksi,
    CSOTerakhir, KodeJurusanTerakhir, FrekwensiPergi,
    FlagMember)
  VALUES(
    p_no_hp, p_no_telp,p_nama,
    p_alamat, p_tgl_pertama_transaksi, p_tgl_terakhir_transaksi,
    p_cso_terakhir, p_kode_jurusan_terakhir,p_frekwensi_pergi,
    p_flag_member);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_pelanggan_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_pelanggan_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pelanggan_ubah`(
  p_no_telp_old VARCHAR(20),p_no_hp_old VARCHAR(20),
  p_no_hp VARCHAR(20), p_no_telp VARCHAR(20),p_nama VARCHAR(150),
  p_alamat VARCHAR(50), p_tgl_terakhir_transaksi DATE,
  p_cso_terakhir INTEGER, p_kode_jurusan_terakhir VARCHAR(10))
BEGIN

  UPDATE tbl_pelanggan SET
    NoHP=p_no_hp, NoTelp=p_no_telp, Nama=p_nama,
    TglTerakhirTransaksi=p_tgl_terakhir_transaksi,
    CSOTerakhir=p_cso_terakhir, KodeJurusanTerakhir=p_kode_jurusan_terakhir, FrekwensiPergi=FrekwensiPergi+1
  WHERE NoTelp=p_no_telp_old;

  IF p_alamat!='' THEN
    UPDATE tbl_pelanggan SET
      Alamat=p_alamat
    WHERE NoTelp=p_no_telp_old;
  END IF;

  END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_pengumuman_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_pengumuman_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengumuman_tambah`(
  p_kode_pengumuman VARCHAR(20),p_judul_pengumuman VARCHAR(255), p_pengumuman TEXT,
  p_kota VARCHAR(20), p_pembuat_pengumuman INTEGER, p_waktu_pembuatan DATE)
BEGIN

  INSERT INTO tbl_pengumuman (
    KodePengumuman,JudulPengumuman, Pengumuman,
    Kota,PembuatPengumuman, WaktuPembuatanPengumuman)
  VALUES(
    p_kode_pengumuman,p_judul_pengumuman, p_pengumuman,
    p_kota, p_pembuat_pengumuman, p_waktu_pembuatan);

  CALL sp_user_update_pengumuman_baru();
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_pengumuman_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_pengumuman_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_pengumuman_ubah`(
  p_id_pengumuman INTEGER,p_kode_pengumuman_old VARCHAR(20),
  p_kode_pengumuman VARCHAR(20),p_judul_Pengumuman VARCHAR(255), p_pengumuman TEXT,
  p_kota VARCHAR(20), p_pembuat_pengumuman INTEGER, p_waktu_pembuatan DATE)
BEGIN

  UPDATE tbl_pengumuman SET
    KodePengumuman=p_kode_pengumuman, JudulPengumuman=p_judul_pengumuman, Pengumuman=p_pengumuman,
    Kota=p_kota, PembuatPengumuman=p_pembuat_pengumuman, WaktuPembuatanPengumuman=p_waktu_pembuatan
  WHERE IdPengumuman=p_id_pengumuman;

  CALL sp_user_update_pengumuman_baru();
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_penjadwalan_kendaraan_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_penjadwalan_kendaraan_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_penjadwalan_kendaraan_ubah_status_aktif`(p_id_jadwal INTEGER)
BEGIN

   UPDATE tbl_penjadwalan_kendaraan SET
     StatusAktif=1-StatusAktif
   WHERE IdPenjadwalan=p_id_jadwal;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_posisi_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_posisi_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_posisi_tambah`(p_kode_jadwal VARCHAR(50),p_tgl_berangkat DATE,p_jam_berangkat TIME,
  p_jumlah_kursi INT(2),p_kode_kendaraan VARCHAR(20),p_kode_sopir VARCHAR(50))
BEGIN

  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_tgl_berangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN
    INSERT INTO tbl_posisi
      (KodeJadwal,TglBerangkat,JamBerangkat,JumlahKursi,SisaKursi,
      KodeKendaraan,KodeSopir)
    VALUES(
      p_kode_jadwal,p_tgl_berangkat,p_jam_berangkat,p_jumlah_kursi,p_jumlah_kursi,
      p_kode_kendaraan,p_kode_sopir);
  ELSE
    INSERT INTO tbl_posisi
      (KodeJadwal,TglBerangkat,JamBerangkat,JumlahKursi,SisaKursi,
      KodeKendaraan,KodeSopir)
    VALUES(
      p_kode_jadwal,p_tgl_berangkat,p_jam_berangkat,p_jumlah_kursi,p_jumlah_kursi,
      p_kode_kendaraan,p_kode_sopir);
  END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_promo_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_promo_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_promo_ubah_status_aktif`(p_kode varchar(50))
BEGIN

   UPDATE tbl_md_promo SET
     FlagAktif=1-FlagAktif
   WHERE KodePromo=p_kode;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_batal
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_batal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_batal`(
  p_NoTiket VARCHAR(50),p_PetugasPembatalan INTEGER,p_WaktuPembatalan DATETIME)
BEGIN

  UPDATE tbl_reservasi SET
    FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
    WaktuPembatalan=p_WaktuPembatalan
  WHERE NoTiket=p_NoTiket;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      FlagBatal=1,PetugasPembatalan=p_PetugasPembatalan,
      WaktuPembatalan=p_WaktuPembatalan
    WHERE NoTiket=p_NoTiket;
  END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_insert_status_kursi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_insert_status_kursi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_insert_status_kursi`(
  p_NomorKursi INT(2), p_Session INTEGER, p_KodeJadwal VARCHAR(50),
  p_TglBerangkat DATE)
BEGIN

  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN

    INSERT INTO tbl_posisi_detail (
      NomorKursi, KodeJadwal, TglBerangkat,
      StatusKursi, Session,SessionTime)
    VALUES(
      p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
      1, p_Session,NOW());
  ELSE
    INSERT INTO tbl_posisi_detail_backup (
      NomorKursi, KodeJadwal, TglBerangkat,
      StatusKursi, Session,SessionTime)
    VALUES(
      p_NomorKursi, p_KodeJadwal, p_TglBerangkat,
      1, p_Session,NOW());
  END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_mutasi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_mutasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_mutasi`(
  p_NoTiket VARCHAR(50), p_KodeJadwal VARCHAR(50), p_IdJurusan INTEGER,
  p_KodeKendaraan VARCHAR(20), p_KodeSopir VARCHAR(50), p_TglBerangkat DATE,
  p_JamBerangkat VARCHAR(10), p_NomorKursi INT(2), p_HargaTiket DOUBLE,
  p_Charge DOUBLE, p_SubTotal DOUBLE, p_Discount DOUBLE,
  p_PPN DOUBLE, p_Total DOUBLE, p_NoSPJ VARCHAR(35),
  p_TglCetakSPJ DATETIME, p_CetakSPJ INT(1), p_KomisiPenumpangCSO DOUBLE,
  p_PetugasCetakSPJ INTEGER, p_Keterangan VARCHAR(100), p_JenisDiscount VARCHAR(50),
  p_KodeAkunPendapatan VARCHAR(20), p_KodeAkunKomisiPenumpangCSO VARCHAR(20), p_PaymentCode VARCHAR(20),
  p_Nama VARCHAR(50),p_StatusBayar INT(1),p_KodeJadwalLama VARCHAR(50),
  p_TglBerangkatLama DATE,p_NomorKursiLama INT(2),p_Pemutasi INTEGER)
BEGIN

  

  DECLARE p_KodeBooking VARCHAR(50);
  DECLARE p_KodeJadwalOld VARCHAR(50);
  DECLARE p_TglBerangkatOld DATE;

  SELECT
    KodeBooking,KodeJadwal,TglBerangkat
    INTO p_KodeBooking,p_KodeJadwalOld,p_TglBerangkatOld
  FROM tbl_reservasi WHERE NoTiket=p_NoTiket;

  UPDATE tbl_reservasi SET
    KodeJadwal=p_KodeJadwal, IdJurusan=p_IdJurusan,
    KodeKendaraan=p_KodeKendaraan, KodeSopir=p_KodeSopir, TglBerangkat=p_TglBerangkat,
    JamBerangkat=p_JamBerangkat, NomorKursi=p_NomorKursi, NoSPJ=p_NoSPJ,
    TglCetakSPJ=p_TglCetakSPJ, CetakSPJ=p_CetakSPJ, KomisiPenumpangCSO=p_KomisiPenumpangCSO,
    PetugasCetakSPJ=p_PetugasCetakSPJ, Keterangan=p_Keterangan, JenisDiscount=p_JenisDiscount,
    KodeAkunPendapatan=p_KodeAkunPendapatan, KodeAkunKomisiPenumpangCSO=p_KodeAkunKomisiPenumpangCSO,PaymentCode=p_PaymentCode,
    WaktuMutasi=NOW(),Pemutasi=p_Pemutasi,
    KodeBooking=if(p_KodeJadwalOld=p_KodeJadwal AND p_TglBerangkatOld=p_TglBerangkat,KodeBooking,CONCAT(KodeBooking,"M"))
  WHERE NoTiket=p_NoTiket;

  
  CALL sp_reservasi_set_status_kursi(
    p_NomorKursi,null, f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal),
    p_TglBerangkat,0,0);

  
  UPDATE tbl_posisi_detail SET
    StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
    KodeBooking=if(f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwalOld)=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal) AND p_TglBerangkatOld=p_TglBerangkat,p_KodeBooking,CONCAT(p_KodeBooking,"M")),
    Session=NULL,StatusBayar=p_StatusBayar
  WHERE
    NomorKursi=p_NomorKursi
    AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
    AND TglBerangkat=p_TglBerangkat;


  
  UPDATE tbl_posisi_detail SET
    StatusKursi=0,Nama=NULL,NoTiket=NULL,
    KodeBooking=NULL,Session=NULL,StatusBayar=0
  WHERE
    NomorKursi=p_NomorKursiLama
    AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwalLama)
    AND TglBerangkat=p_TglBerangkatLama;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_set_status_kursi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_set_status_kursi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_set_status_kursi`(p_NomorKursi INT(2), p_Session INTEGER, p_KodeJadwal VARCHAR(50),
  p_TglBerangkat DATE,p_JamBerangkat VARCHAR(8), p_KodeCabangAsal VARCHAR(10), p_KodeCabangTujuan VARCHAR(10), p_StatusKursiAdmin INT(1),p_session_time_expired int)
BEGIN

  DECLARE p_ditemukan INT;
  DECLARE p_selisih_hari INTEGER;

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN
    SELECT COUNT(NomorKursi) INTO p_ditemukan FROM tbl_posisi_detail WHERE NomorKursi=p_NomorKursi AND TglBerangkat=p_TglBerangkat AND KodeJadwal=P_KodeJadwal;
  ELSE
    SELECT COUNT(NomorKursi) INTO p_ditemukan FROM tbl_posisi_detail_backup WHERE NomorKursi=p_NomorKursi AND TglBerangkat=p_TglBerangkat AND KodeJadwal=P_KodeJadwal;
  END IF;

  IF(p_ditemukan>0) THEN
    CALL sp_reservasi_update_status_kursi(p_NomorKursi,p_Session,p_KodeJadwal,p_TglBerangkat,p_StatusKursiAdmin,p_session_time_expired);
  ELSE
    CALL sp_reservasi_insert_status_kursi(p_NomorKursi,p_Session,p_KodeJadwal,p_TglBerangkat);
  END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_tambah`(IN `p_NoTiket` VARCHAR(50), IN `p_KodeCabang` VARCHAR(20), IN `p_KodeJadwal` VARCHAR(50), IN `p_IdJurusan` INT, IN `p_KodeKendaraan` VARCHAR(20), IN `p_KodeSopir` VARCHAR(50), IN `p_TglBerangkat` DATE, IN `p_JamBerangkat` VARCHAR(10), IN `p_KodeBooking` VARCHAR(50), IN `p_IdMember` VARCHAR(25), IN `p_PointMember` INT(3), IN `p_Nama` VARCHAR(100), IN `p_Alamat` VARCHAR(100), IN `p_Telp` VARCHAR(15), IN `p_HP` VARCHAR(15), IN `p_WaktuPesan` DATETIME, IN `p_NomorKursi` INT(2), IN `p_HargaTiket` DOUBLE, IN `p_Charge` DOUBLE, IN `p_SubTotal` DOUBLE, IN `p_Discount` DOUBLE, IN `p_PPN` DOUBLE, IN `p_Total` DOUBLE, IN `p_PetugasPenjual` INT, IN `p_FlagPesanan` INT(1), IN `p_NoSPJ` VARCHAR(35), IN `p_TglCetakSPJ` DATETIME, IN `p_CetakSPJ` INT(1), IN `p_KomisiPenumpangCSO` DOUBLE, IN `p_FlagSetor` INT(1), IN `p_PetugasCetakSPJ` INT, IN `p_Keterangan` VARCHAR(100), IN `p_JenisDiscount` VARCHAR(50), IN `p_KodeAkunPendapatan` VARCHAR(20), IN `p_JenisPenumpang` CHAR(6), IN `p_KodeAkunKomisiPenumpangCSO` VARCHAR(20), IN `p_PaymentCode` VARCHAR(20))
BEGIN

    INSERT INTO tbl_reservasi (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0);

    UPDATE tbl_posisi_detail SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwal
      AND TglBerangkat=p_TglBerangkat;
  
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_tambah_with_komisi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_tambah_with_komisi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_tambah_with_komisi`(
  p_NoTiket VARCHAR(50), p_KodeCabang VARCHAR(20), p_KodeJadwal VARCHAR(50),
  p_IdJurusan INTEGER, p_KodeKendaraan VARCHAR(20), p_KodeSopir VARCHAR(50),
  p_TglBerangkat DATE, p_JamBerangkat VARCHAR(10), p_KodeBooking VARCHAR(50),
  p_IdMember VARCHAR(25), p_PointMember INT(3), p_Nama VARCHAR(100),
  p_Alamat VARCHAR(100), p_Telp VARCHAR(15), p_HP VARCHAR(15),
  p_WaktuPesan DATETIME, p_NomorKursi INT(2), p_HargaTiket DOUBLE,
  p_Charge DOUBLE, p_SubTotal DOUBLE, p_Discount DOUBLE,
  p_PPN DOUBLE, p_Total DOUBLE, p_PetugasPenjual INTEGER,
  p_FlagPesanan INT(1), p_NoSPJ VARCHAR(35), p_TglCetakSPJ DATETIME,
  p_CetakSPJ INT(1), p_KomisiPenumpangCSO DOUBLE, p_FlagSetor INT(1) ,
  p_PetugasCetakSPJ INTEGER, p_Keterangan VARCHAR(100), p_JenisDiscount VARCHAR(50),
  p_KodeAkunPendapatan VARCHAR(20), p_JenisPenumpang CHAR(2), p_KodeAkunKomisiPenumpangCSO VARCHAR(20),
  p_PaymentCode VARCHAR(20))
BEGIN

  DECLARE p_selisih_hari INTEGER;
  DECLARE p_komisi DOUBLE;

  
  SELECT
    IF((SELECT FlagLuarKota FROM tbl_md_jurusan WHERE IdJurusan=p_IdJurusan)=1
      AND p_JenisPenumpang!='R',NilaiParameter,0) INTO p_komisi
  FROM tbl_pengaturan_parameter
  WHERE NamaParameter='KOMISITTX1';

  SELECT DATEDIFF(CURDATE(),DATE_ADD(p_TglBerangkat,INTERVAL 2 DAY)) INTO p_selisih_hari;

  IF p_selisih_hari<=0 THEN

    INSERT INTO tbl_reservasi (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal,Komisi)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0,p_komisi);

    UPDATE tbl_posisi_detail SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
      AND TglBerangkat=p_TglBerangkat;
  ELSE
    INSERT INTO tbl_reservasi_olap (
      NoTiket, KodeCabang, KodeJadwal,
      IdJurusan, KodeKendaraan, KodeSopir,
      TglBerangkat, JamBerangkat, KodeBooking,
      IdMember, PointMember, Nama,
      Alamat, Telp, HP,
      WaktuPesan, NomorKursi, HargaTiket,
      Charge, SubTotal, Discount,
      PPN, Total, PetugasPenjual,
      FlagPesanan, NoSPJ, TglCetakSPJ,
      CetakSPJ, KomisiPenumpangCSO, FlagSetor,
      PetugasCetakSPJ, Keterangan, JenisDiscount,
      KodeAkunPendapatan, JenisPenumpang, KodeAkunKomisiPenumpangCSO,
      PaymentCode,CetakTiket,FlagBatal,Komisi)
    VALUES(
      p_NoTiket, p_KodeCabang, p_KodeJadwal,
      p_IdJurusan, p_KodeKendaraan, p_KodeSopir,
      p_TglBerangkat, p_JamBerangkat, p_KodeBooking,
      p_IdMember, p_PointMember, p_Nama,
      p_Alamat, p_Telp, p_HP,
      p_WaktuPesan, p_NomorKursi, p_HargaTiket,
      p_Charge, p_SubTotal, p_Discount,
      p_PPN, p_Total, p_PetugasPenjual,
      p_FlagPesanan, p_NoSPJ, p_TglCetakSPJ,
      p_CetakSPJ, p_KomisiPenumpangCSO, p_FlagSetor,
      p_PetugasCetakSPJ, p_Keterangan, p_JenisDiscount,
      p_KodeAkunPendapatan, p_JenisPenumpang, p_KodeAkunKomisiPenumpangCSO,
      p_PaymentCode,0,0,p_komisi);

    UPDATE tbl_posisi_detail_backup SET
      StatusKursi=1,Nama=p_Nama,NoTiket=p_NoTiket,
      KodeBooking=p_KodeBooking,Session=NULL
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=f_jadwal_ambil_kodeutama_by_kodejadwal(p_KodeJadwal)
      AND TglBerangkat=p_TglBerangkat;
  END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_ubah_data_after_spj
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_ubah_data_after_spj`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_data_after_spj`(p_kode_jadwal VARCHAR(50), p_tgl_berangkat DATE, p_sopir_dipilih VARCHAR(50),
  p_mobil_dipilih VARCHAR(50), p_no_spj VARCHAR(50))
BEGIN

  UPDATE tbl_reservasi
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap
    SET
      KodeSopir=p_sopir_dipilih,
      KodeKendaraan=p_mobil_dipilih,
      NoSPJ=p_no_spj,
      CetakSPJ=1
    WHERE
      TglBerangkat = p_tgl_berangkat
      AND KodeJadwal=p_kode_jadwal;
  END IF;

  UPDATE tbl_paket
  SET
    KodeSopir=p_sopir_dipilih,
    KodeKendaraan=p_mobil_dipilih,
    NoSPJ=p_no_spj,
    CetakSPJ=1
  WHERE
    TglBerangkat = p_tgl_berangkat
    AND KodeJadwal=p_kode_jadwal;


END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_ubah_data_penumpang
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_ubah_data_penumpang`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_data_penumpang`(
  p_no_tiket VARCHAR(50),p_nama VARCHAR(100),p_alamat VARCHAR(100),p_telp VARCHAR(15),
  p_id_member VARCHAR(25))
BEGIN

  UPDATE tbl_reservasi SET
    Nama=p_nama,Alamat=p_alamat,Telp=p_telp,IdMember=p_id_member
  WHERE NoTiket=p_no_tiket AND FlagBatal!=1 AND CetakTiket!=1;

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      Nama=p_nama,Alamat=p_alamat,Telp=p_telp,IdMember=p_id_member
    WHERE NoTiket=p_no_tiket AND FlagBatal!=1 AND CetakTiket!=1;
  END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_ubah_discount
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_ubah_discount`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_ubah_discount`(IN `p_NoTiket` VARCHAR(50), IN `p_kode_discount` CHAR(6), IN `p_NamaDiscount` VARCHAR(30), IN `p_JumlahDiscount` DOUBLE)
BEGIN

  DECLARE p_kode_booking VARCHAR(50);

  

  

  

  UPDATE tbl_reservasi SET
    JenisPenumpang=p_kode_discount,
    Discount=IF(p_JumlahDiscount>1,IF(p_JumlahDiscount<=HargaTiket,p_JumlahDiscount,HargaTiket),p_JumlahDiscount*HargaTiket),
    JenisDiscount=p_NamaDiscount,
    Total=SubTotal-Discount
  WHERE NoTiket IN(p_NoTiket);

  IF ROW_COUNT()<=0 THEN
    UPDATE tbl_reservasi_olap SET
      JenisPenumpang=p_kode_discount,
      Discount=IF(p_JumlahDiscount>1,IF(p_JumlahDiscount<=HargaTiket,p_JumlahDiscount,HargaTiket),p_JumlahDiscount*HargaTiket),
      JenisDiscount=p_NamaDiscount,
      Total=SubTotal-Discount
  WHERE NoTiket IN(p_NoTiket);
  END IF;



END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_reservasi_update_status_kursi
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_reservasi_update_status_kursi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reservasi_update_status_kursi`(p_NomorKursi INT(2), p_Session INTEGER, p_KodeJadwalDipilih VARCHAR(50), p_JamBerangkat TIME,
 p_KodeCabangAsal VARCHAR(50), p_KodeCabangTujuan VARCHAR(50), p_IsSubJadwal INT(1), p_KodeJadwalUtama VARCHAR(50), p_TglBerangkat DATE,p_StatusKursiAdmin INT(1),p_session_time_expired int)
BEGIN


  DECLARE p_selisih_hari INTEGER;
  DECLARE p_ditemukan INTEGER;

  

  

    SELECT COUNT(1) INTO p_ditemukan FROM tbl_posisi_detail
    WHERE
      NomorKursi=p_NomorKursi
      AND KodeJadwal=p_KodeJadwalDipilih
			AND KodeJadwalUtama=p_KodeJadwalUtama
			AND IsSubJadwal=p_IsSubJadwal
      AND TglBerangkat=p_TglBerangkat
      AND (NoTiket='' OR NoTiket IS NULL)
      AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    IF p_ditemukan<=0 THEN
     INSERT INTO tbl_posisi_detail (
        NomorKursi, KodeJadwal, JamBerangkat,
				IsSubJadwal, KodeCabangAsal,KodeCabangTujuan, 
				KodeJadwalUtama,TglBerangkat,StatusKursi, 
				Session,SessionTime)
      VALUES(
        p_NomorKursi, p_KodeJadwalDipilih , p_JamBerangkat,
				p_IsSubJadwal, p_KodeCabangAsal ,p_KodeCabangTujuan ,
				p_KodeJadwalUtama, p_TglBerangkat,1, 
				p_Session,NOW());
    ELSE
      UPDATE tbl_posisi_detail SET
        StatusKursi=IF(f_reservasi_session_time_selisih(SessionTime)<=p_session_time_expired OR StatusKursi=0,1-StatusKursi,StatusKursi), Session=p_Session , SessionTime=NOW()
      WHERE
        NomorKursi=p_NomorKursi
        AND KodeJadwal=p_KodeJadwalDipilih
				AND KodeJadwalUtama=p_KodeJadwalUtama
				AND IsSubJadwal=p_IsSubJadwal
        AND TglBerangkat=p_TglBerangkat
        AND (NoTiket='' OR NoTiket IS NULL)
        AND ((StatusKursi=1 AND f_reservasi_session_time_selisih(SessionTime)>p_session_time_expired) OR StatusKursi=0 OR StatusKursi IS NULL OR Session=p_Session OR StatusKursi=p_StatusKursiAdmin);

    END IF;

   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sessions_create
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sessions_create`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sessions_create`(
  p_user_id INTEGER, p_session_id VARCHAR(255),
  p_user_ip VARCHAR(255),p_current_time FLOAT(15),
  p_page_id FLOAT(15),p_login FLOAT(15),
  p_admin FLOAT(15))
BEGIN

   DELETE FROM tbl_sessions
   WHERE session_user_id=-1 OR session_user_id=p_user_id;

   INSERT INTO  tbl_sessions
     (session_id, session_user_id, session_start, session_time, session_ip, session_page, session_logged_in, session_admin)
   VALUES (p_session_id, p_user_id, p_current_time, p_current_time,p_user_ip, p_page_id, p_login, p_admin);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sessions_end
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sessions_end`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sessions_end`(
  p_user_id INTEGER, p_session_id VARCHAR(255))
BEGIN

   DELETE FROM  tbl_sessions
   WHERE session_id = p_session_id
   AND session_user_id = p_user_id;

   UPDATE tbl_user
   SET status_online=0,waktu_logout=NOW()
   WHERE user_id = p_user_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sopir_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sopir_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_tambah`(
  p_kode_sopir VARCHAR(50), p_nama VARCHAR(150), p_HP VARCHAR(50),
  p_alamat VARCHAR(50), p_no_SIM VARCHAR(50), p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_md_sopir (
    KodeSopir, Nama, HP, Alamat, NoSIM, FlagAktif)
  VALUES(
    p_kode_sopir, p_nama, p_HP, p_alamat, p_no_SIM, p_flag_aktif);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sopir_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sopir_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_ubah`(
  p_kode_sopir_old VARCHAR(50),
  p_kode_sopir VARCHAR(50), p_nama VARCHAR(150), p_HP VARCHAR(50),
  p_alamat VARCHAR(50), p_no_SIM VARCHAR(50), p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_md_sopir SET
    KodeSopir=p_kode_sopir, Nama=p_nama, HP=p_HP,
    Alamat=p_alamat, NoSIM=p_no_SIM, FlagAktif=p_flag_aktif
  WHERE KodeSopir=p_kode_sopir_old;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_sopir_ubah_status_aktif
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_sopir_ubah_status_aktif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_sopir_ubah_status_aktif`(p_kode_sopir VARCHAR(50))
BEGIN

   UPDATE tbl_md_sopir SET
     FlagAktif=1-FlagAktif
   WHERE KodeSopir=p_kode_sopir;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_spj_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_spj_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_tambah`(
  p_no_spj VARCHAR(50), p_kode_jadwal VARCHAR(50), p_tgl_berangkat DATE,
  p_jam_berangkat VARCHAR(10), p_layout_kursi INT(2), p_jumlah_penumpang INT(2),
  p_mobil_dipilih VARCHAR(50), p_cso INTEGER, p_sopir_dipilih VARCHAR(50),
  p_nama_sopir VARCHAR(50),p_total_omzet DOUBLE,
  p_jumlah_paket INT(3), p_total_omzet_paket DOUBLE,p_is_ekspedisi INT(1))
BEGIN

  INSERT INTO tbl_spj
    (NoSPJ,KodeJadwal,TglBerangkat,JamBerangkat,
    JumlahKursiDisediakan,JumlahPenumpang,TglSPJ,
    NoPolisi,CSO,KodeDriver,
    Driver,TotalOmzet,IdJurusan,
    FlagAmbilBiayaOP, JumlahPaket,TotalOmzetPaket,
    IsEkspedisi)
  VALUES(
    p_no_spj,p_kode_jadwal,p_tgl_berangkat, p_jam_berangkat,
    p_layout_kursi,p_jumlah_penumpang,NOW(),
    p_mobil_dipilih,p_cso,p_sopir_dipilih,
    p_nama_sopir,p_total_omzet,f_jadwal_ambil_id_jurusan_by_kode_jadwal(p_kode_jadwal),
    0,p_jumlah_paket,p_total_omzet_paket,
    p_is_ekspedisi);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_spj_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_spj_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_ubah`(p_no_spj VARCHAR(50),p_jumlah_penumpang INT(2), p_mobil_dipilih VARCHAR(50),
  p_cso INTEGER, p_sopir_dipilih VARCHAR(50),p_nama_sopir VARCHAR(50),
  p_total_omzet DOUBLE,
  p_jumlah_paket INT(3),p_total_omzet_paket DOUBLE)
BEGIN

  UPDATE tbl_spj SET
    JumlahPenumpang=p_jumlah_penumpang,
    NoPolisi=p_mobil_dipilih,
    CSO=p_cso,
    KodeDriver=p_sopir_dipilih,
    Driver=p_nama_sopir,
    TotalOmzet=p_total_omzet,
    JumlahPaket=p_jumlah_paket,
    TotalOmzetPaket=p_total_omzet_paket
  WHERE NoSPJ=p_no_spj;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_spj_ubah_insentif_sopir
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_spj_ubah_insentif_sopir`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_spj_ubah_insentif_sopir`(
  p_no_spj VARCHAR(50),p_insentif_sopir DOUBLE)
BEGIN

  UPDATE tbl_spj SET
    InsentifSopir=p_insentif_sopir
  WHERE NoSPJ=p_no_spj;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_baca_pengumuman_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_baca_pengumuman_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_baca_pengumuman_tambah`(
  p_id_pengumuman INTEGER,p_user_id INTEGER)
BEGIN

  INSERT INTO tbl_user_baca_pengumuman (
    user_id,IdPengumuman)
  VALUES(
    p_user_id, p_id_pengumuman);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_reset_pengumuman_baru
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_reset_pengumuman_baru`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_reset_pengumuman_baru`(p_user_id INTEGER)
BEGIN

  UPDATE tbl_user SET jumlah_pengumuman_baru=0
  WHERE user_id=p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_tambah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_tambah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_tambah`(
  p_username VARCHAR(50), p_user_password VARCHAR(255),p_NRP VARCHAR(20),
  p_nama VARCHAR(255),p_telp VARCHAR(20), p_hp VARCHAR(20),
  p_email VARCHAR(50),p_address VARCHAR(255), p_KodeCabang VARCHAR(20),
  p_status_online BIT, p_berlaku DATETIME, p_user_level DECIMAL(3,1),
  p_flag_aktif INT(1))
BEGIN

  INSERT INTO tbl_user (
    username, user_password, NRP,
    nama, telp, hp,
    email, address, KodeCabang,
    status_online,berlaku,user_level,
    user_active)
  VALUES(
    p_username, p_user_password,p_NRP,
    p_nama,p_telp, p_hp,
    p_email,p_address, p_KodeCabang,
    p_status_online, p_berlaku, p_user_level,
    p_flag_aktif);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_ubah
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_ubah`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_ubah`(
  p_user_id INTEGER,
  p_username VARCHAR(50), p_NRP VARCHAR(20),
  p_nama VARCHAR(255),p_telp VARCHAR(20), p_hp VARCHAR(20),
  p_email VARCHAR(50),p_address VARCHAR(255), p_KodeCabang VARCHAR(20),
  p_status_online BIT, p_berlaku DATETIME, p_user_level DECIMAL(3,1),
  p_flag_aktif INT(1))
BEGIN

  UPDATE tbl_user SET
    username=p_username, NRP=p_NRP,
    nama=p_nama, telp=p_telp, hp=p_hp,
    email=p_email, address=p_address, KodeCabang=p_KodeCabang,
    status_online=p_status_online,berlaku=p_berlaku,user_level=p_user_level,
    user_active=p_flag_aktif
  WHERE user_id=p_user_id;    
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for sp_user_update_pengumuman_baru
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_user_update_pengumuman_baru`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_user_update_pengumuman_baru`()
BEGIN

  UPDATE tbl_user SET jumlah_pengumuman_baru=jumlah_pengumuman_baru+1;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_cabang_get_kota_by_kode_cabang
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cabang_get_kota_by_kode_cabang`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_get_kota_by_kode_cabang`(p_kode VARCHAR(30)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kota VARCHAR(50);

  SELECT
    Kota INTO p_kota
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_kota;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_cabang_get_name_by_kode
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cabang_get_name_by_kode`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_get_name_by_kode`(p_kode VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(50);

  SELECT
    Nama INTO p_nama
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_nama;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_cabang_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_cabang_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_cabang_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeCabang) INTO p_jum_data
  FROM tbl_md_cabang
  WHERE KodeCabang=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_discount_get_jumlah_by_id
-- ----------------------------
DROP FUNCTION IF EXISTS `f_discount_get_jumlah_by_id`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_discount_get_jumlah_by_id`(p_id INTEGER) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT JumlahDiscount INTO p_jumlah
  FROM tbl_jenis_discount
  WHERE IdDiscount=p_id;

  RETURN p_jumlah;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jadwal_ambil_id_jurusan_by_kode_jadwal
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jadwal_ambil_id_jurusan_by_kode_jadwal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_id_jurusan_by_kode_jadwal`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_id_jurusan INTEGER;

  SELECT  IdJurusan INTO p_id_jurusan
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_id_jurusan;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jadwal_ambil_jumlah_kursi_by_kode_jadwal
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jadwal_ambil_jumlah_kursi_by_kode_jadwal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_jumlah_kursi_by_kode_jadwal`(p_kode VARCHAR(50)) RETURNS int(2)
BEGIN
  DECLARE p_jumlah_kursi INT(2);

  SELECT  JumlahKursi INTO p_jumlah_kursi
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_jumlah_kursi;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jadwal_ambil_kodeutama_by_kodejadwal
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jadwal_ambil_kodeutama_by_kodejadwal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_ambil_kodeutama_by_kodejadwal`(p_kode VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kode_jadwal_utama VARCHAR(50);

  SELECT  IF(FlagSubJadwal!=1,KodeJadwal,KodeJadwalUtama) INTO p_kode_jadwal_utama
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_kode_jadwal_utama;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jadwal_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jadwal_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jadwal_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeJadwal) INTO p_jum_data
  FROM tbl_md_jadwal
  WHERE KodeJadwal=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jurusan_get_harga_tiket_by_id_jurusan
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jurusan_get_harga_tiket_by_id_jurusan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_harga_tiket_by_id_jurusan`(
  p_id_jurusan INT(11),p_tgl_berangkat DATE) RETURNS double
BEGIN
  DECLARE p_harga_tiket DOUBLE;
  DECLARE p_tgl_awal_tuslah1 DATE;
  DECLARE p_tgl_akhir_tuslah1 DATE;
  DECLARE p_tgl_awal_tuslah2 DATE;
  DECLARE p_tgl_akhir_tuslah2 DATE;

  SELECT NilaiParameter INTO p_tgl_awal_tuslah1
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_MULAI_TUSLAH1';

  SELECT NilaiParameter INTO p_tgl_akhir_tuslah1
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_AKHIR_TUSLAH1';
  
  SELECT NilaiParameter INTO p_tgl_awal_tuslah2
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_MULAI_TUSLAH2';

  SELECT NilaiParameter INTO p_tgl_akhir_tuslah2
  FROM tbl_pengaturan_parameter WHERE NamaParameter='TGL_AKHIR_TUSLAH2';

  SELECT 
    IF(FlagLuarKota=1,IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah1 AND p_tgl_akhir_tuslah1,HargaTiketTuslah,HargaTiket),IF(p_tgl_berangkat BETWEEN p_tgl_awal_tuslah2 AND p_tgl_akhir_tuslah2,HargaTiketTuslah,HargaTiket)) INTO p_harga_tiket
  FROM tbl_md_jurusan
  WHERE IdJurusan=p_id_jurusan;
  
  RETURN p_harga_tiket;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jurusan_get_harga_tiket_by_kode_jadwal
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jurusan_get_harga_tiket_by_kode_jadwal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_harga_tiket_by_kode_jadwal`(
  p_kode_jadwal VARCHAR(30),p_tgl_berangkat DATE) RETURNS double
BEGIN
  DECLARE p_harga_tiket DOUBLE;
  DECLARE p_tgl_awal_tuslah DATE;
  DECLARE p_tgl_akhir_tuslah DATE;

  SELECT TglMulaiTuslah,TglAkhirTuslah INTO p_tgl_awal_tuslah,p_tgl_akhir_tuslah
  FROM tbl_pengaturan_umum LIMIT 0,1;

  SELECT IF(p_tgl_berangkat NOT BETWEEN p_tgl_awal_tuslah AND p_tgl_akhir_tuslah,HargaTiket,HargaTiketTuslah) INTO p_harga_tiket
  FROM tbl_md_jurusan AS tmjr INNER JOIN tbl_md_jadwal tmj ON tmjr.IdJurusan=tmj.IdJurusan
  WHERE tmj.KodeJadwal=p_kode_jadwal;
  
  RETURN p_harga_tiket;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jurusan_get_kode_cabang_asal_by_jurusan
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jurusan_get_kode_cabang_asal_by_jurusan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_kode_cabang_asal_by_jurusan`(p_id_jurusan INTEGER) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_kode_cabang_asal VARCHAR(20);

    SELECT KodeCabangAsal INTO p_kode_cabang_asal FROM tbl_md_jurusan WHERE IdJurusan=p_id_jurusan;
  
    RETURN p_kode_cabang_asal;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jurusan_get_kode_cabang_tujuan_by_jurusan
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jurusan_get_kode_cabang_tujuan_by_jurusan`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_get_kode_cabang_tujuan_by_jurusan`(p_id_jurusan INTEGER) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_kode_cabang_tujuan VARCHAR(20);

    SELECT KodeCabangTujuan INTO p_kode_cabang_tujuan FROM tbl_md_jurusan WHERE IdJurusan=p_id_jurusan;
  
    RETURN p_kode_cabang_tujuan;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_jurusan_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_jurusan_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_jurusan_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeJurusan) INTO p_jum_data
  FROM tbl_md_jurusan
  WHERE KodeJurusan=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_kendaraan_ambil_nopol_by_kode
-- ----------------------------
DROP FUNCTION IF EXISTS `f_kendaraan_ambil_nopol_by_kode`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_ambil_nopol_by_kode`(p_kode VARCHAR(50)) RETURNS varchar(20) CHARSET latin1
BEGIN
  DECLARE p_nopol VARCHAR(20);

  SELECT NoPolisi INTO p_nopol
  FROM tbl_md_kendaraan
  WHERE KodeKendaraan=p_kode;

  RETURN p_nopol;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_kendaraan_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_kendaraan_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeKendaraan) INTO p_jum_data
  FROM tbl_md_kendaraan
  WHERE KodeKendaraan=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_kendaraan_periksa_duplikasi_by_nopol
-- ----------------------------
DROP FUNCTION IF EXISTS `f_kendaraan_periksa_duplikasi_by_nopol`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_kendaraan_periksa_duplikasi_by_nopol`(p_nopol VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeKendaraan) INTO p_jum_data
  FROM tbl_md_kendaraan
  WHERE NoPolisi=p_nopol;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_laporan_user_get_summary
-- ----------------------------
DROP FUNCTION IF EXISTS `f_laporan_user_get_summary`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_laporan_user_get_summary`(
  p_userid INTEGER,
  p_tgl_awal DATE,
  p_tgl_akhir DATE,
  p_jenis_pembayaran INT(1)) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT IF(SUM(Total) IS NOT NULL,SUM(Total),0) INTO p_jumlah
  FROM tbl_reservasi
  WHERE PetugasPenjual LIKE p_userid
  AND (DATE(WaktuPesan) BETWEEN p_tgl_awal AND p_tgl_akhir)
  AND (CetakTiket=1 )
  AND Jenispembayaran=p_jenis_pembayaran
  AND FlagBatal!=1;

  RETURN p_jumlah;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_laporan_user_get_total_discount
-- ----------------------------
DROP FUNCTION IF EXISTS `f_laporan_user_get_total_discount`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_laporan_user_get_total_discount`(
  p_userid INTEGER,
  p_tgl_awal DATE,
  p_tgl_akhir DATE) RETURNS double
BEGIN
  DECLARE p_jumlah DOUBLE;

  SELECT IF(SUM(Discount) IS NOT NULL,SUM(Discount),0) INTO p_jumlah
  FROM tbl_reservasi
  WHERE PetugasPenjual LIKE p_userid
  AND (DATE(WaktuPesan) BETWEEN p_tgl_awal AND p_tgl_akhir)
  AND (CetakTiket=1 OR TglBerangkat>=DATE(NOW()))
  AND FlagBatal!=1;

  RETURN p_jumlah;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_log_user_get_last_login_by_user_id
-- ----------------------------
DROP FUNCTION IF EXISTS `f_log_user_get_last_login_by_user_id`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_log_user_get_last_login_by_user_id`(p_userid INTEGER) RETURNS int(11)
BEGIN
  DECLARE p_id_log INTEGER;

  SELECT id_log INTO p_id_log
  FROM tbl_log_user
  WHERE
    user_id=p_userid
    AND waktu_logout IS NULL
  ORDER BY waktu_login DESC LIMIT 0,1;

  RETURN p_id_log;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_member_hitung_frekwensi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_member_hitung_frekwensi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_hitung_frekwensi`(
  p_id_member VARCHAR(25)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT IS_NULL(SUM(FrekwensiBerangkat),0) INTO p_jum_data
  FROM tbl_member_frekwensi_berangkat
  WHERE IdMember=p_id_member ;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_member_hitung_frekwensi_by_tanggal
-- ----------------------------
DROP FUNCTION IF EXISTS `f_member_hitung_frekwensi_by_tanggal`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_hitung_frekwensi_by_tanggal`(
  p_id_member VARCHAR(25),p_tgl_awal DATE, p_tgl_akhir DATE) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT IS_NULL(SUM(FrekwensiBerangkat),0) INTO p_jum_data
  FROM tbl_member_frekwensi_berangkat
  WHERE IdMember=p_id_member
    AND  (LEFT(TglBerangkat,7) BETWEEN LEFT(p_tgl_awal,7) AND LEFT(p_tgl_akhir,7));

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_member_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_member_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_member_periksa_duplikasi`(
  p_id_member VARCHAR(25),p_email VARCHAR(30),p_handphone VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdMember) INTO p_jum_data
  FROM tbl_md_member
  WHERE IdMember=p_id_member OR Email=p_email OR handphone=p_handphone; 

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_paket_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_paket_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_paket_periksa_duplikasi`(p_no_tiket VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(NoTiket) INTO p_jum_data
  FROM tbl_paket
  WHERE NoTiket=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_pelanggan_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_pelanggan_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_pelanggan_periksa_duplikasi`(p_no_telp VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdPelanggan) INTO p_jum_data
  FROM tbl_pelanggan
  WHERE NoTelp=p_no_telp;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_pengumuman_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_pengumuman_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_pengumuman_periksa_duplikasi`(p_kode VARCHAR(20)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(IdPengumuman) INTO p_jum_data
  FROM tbl_pengumuman
  WHERE KodePengumuman=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_promo_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_promo_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_promo_periksa_duplikasi`(
  p_kode VARCHAR(50),p_asal VARCHAR(20),p_tujuan VARCHAR(20),
  p_berlaku_mula DATETIME,p_berlaku_akhir DATETIME,p_target_promo INT,
  p_level_promo INT) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodePromo) INTO p_jum_data
  FROM tbl_md_promo
  WHERE KodePromo=p_kode
    OR (
      ((p_berlaku_mula BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir))
        OR (p_berlaku_akhir BETWEEN CONCAT(BerlakuMula,' ',JamMulai) AND CONCAT(BerlakuAkhir,' ',JamAkhir)))
    AND ((KodeCabangAsal LIKE CONCAT(p_asal,'%') AND KodeCabangTujuan LIKE CONCAT(p_tujuan,'%'))
         OR (KodeCabangAsal LIKE CONCAT(p_tujuan,'%') AND KodeCabangTujuan LIKE CONCAT(p_asal,'%')))
    AND LevelPromo=p_level_promo
    AND (FlagTargetPromo=0 OR FlagTargetPromo=p_target_promo)
    );

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_reservasi_cabang_get_name_by_kode
-- ----------------------------
DROP FUNCTION IF EXISTS `f_reservasi_cabang_get_name_by_kode`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_cabang_get_name_by_kode`(p_kode VARCHAR(50)) RETURNS varchar(100) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(100);
  
    SELECT nama INTO p_nama FROM tbl_md_cabang WHERE KodeCabang=p_kode;
  
    RETURN p_nama;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_reservasi_get_kode_booking_by_no_tiket
-- ----------------------------
DROP FUNCTION IF EXISTS `f_reservasi_get_kode_booking_by_no_tiket`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_get_kode_booking_by_no_tiket`(p_no_tiket VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_kode_booking VARCHAR(50);

  SELECT KodeBooking INTO p_kode_booking
  FROM tbl_reservasi
  WHERE NoTiket=p_no_tiket;

  RETURN p_kode_booking;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_reservasi_session_time_selisih
-- ----------------------------
DROP FUNCTION IF EXISTS `f_reservasi_session_time_selisih`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_reservasi_session_time_selisih`(
  p_session_time DATETIME) RETURNS int(11)
BEGIN
  DECLARE p_selisih INTEGER;

  SELECT
   HOUR(TIMEDIFF(p_session_time,NOW()))*3600 + MINUTE(TIMEDIFF(p_session_time,NOW()))*60 + SECOND(TIMEDIFF(p_session_time,NOW())) INTO p_selisih;

  RETURN p_selisih;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_sopir_get_insentif
-- ----------------------------
DROP FUNCTION IF EXISTS `f_sopir_get_insentif`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_get_insentif`(
  p_kode VARCHAR(20),p_tgl_awal DATE,p_tgl_akhir DATE) RETURNS double
BEGIN
  DECLARE p_return DOUBLE;

  SELECT SUM(InsentifSopir) INTO p_return
  FROM tbl_spj
  WHERE KodeDriver=p_kode
    AND (TglBerangkat BETWEEN p_tgl_awal AND p_tgl_akhir);

  RETURN p_return;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_sopir_get_nama_by_id
-- ----------------------------
DROP FUNCTION IF EXISTS `f_sopir_get_nama_by_id`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_get_nama_by_id`(p_kode VARCHAR(20)) RETURNS varchar(150) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(150);

  SELECT Nama INTO p_nama
  FROM tbl_md_sopir
  WHERE KodeSopir=p_kode;

  RETURN p_nama;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_sopir_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_sopir_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_sopir_periksa_duplikasi`(p_kode VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(KodeSopir) INTO p_jum_data
  FROM tbl_md_sopir
  WHERE KodeSopir=p_kode;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_user_get_jumlah_pengumuman_baru
-- ----------------------------
DROP FUNCTION IF EXISTS `f_user_get_jumlah_pengumuman_baru`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_get_jumlah_pengumuman_baru`(p_userid INTEGER) RETURNS int(11)
BEGIN
  DECLARE p_jumlah INT;

  SELECT jumlah_pengumuman_baru INTO p_jumlah
  FROM tbl_user
  WHERE user_id=p_userid;

  RETURN p_jumlah;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_user_get_nama_by_userid
-- ----------------------------
DROP FUNCTION IF EXISTS `f_user_get_nama_by_userid`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_get_nama_by_userid`(p_userid INTEGER) RETURNS varchar(50) CHARSET latin1
BEGIN
  DECLARE p_nama VARCHAR(50);

  SELECT nama INTO p_nama
  FROM tbl_user
  WHERE user_id=p_userid;

  RETURN p_nama;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for f_user_periksa_duplikasi
-- ----------------------------
DROP FUNCTION IF EXISTS `f_user_periksa_duplikasi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `f_user_periksa_duplikasi`(p_username VARCHAR(50)) RETURNS int(11)
BEGIN
  DECLARE p_jum_data INTEGER;

  SELECT
    COUNT(user_id) INTO p_jum_data
  FROM tbl_user
  WHERE username=p_username;

  RETURN p_jum_data;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for IS_NULL
-- ----------------------------
DROP FUNCTION IF EXISTS `IS_NULL`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `IS_NULL`(p_input DOUBLE,p_pengganti DOUBLE) RETURNS double
BEGIN
  DECLARE p_output DOUBLE;

  SELECT IF(!ISNULL(p_input),p_input,p_pengganti) INTO p_output;
  
  RETURN p_output;
END
;;
DELIMITER ;
