/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : arnes_dev

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-01-05 12:42:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_page
-- ----------------------------
DROP TABLE IF EXISTS `tbl_page`;
CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` float NOT NULL,
  `nama_page` varchar(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_page
-- ----------------------------
INSERT INTO `tbl_page` VALUES ('2', '202', 'Reservasi non reguler');
INSERT INTO `tbl_page` VALUES ('3', '203', 'Penjadwalan Kendaraan');
INSERT INTO `tbl_page` VALUES ('4', '204', 'Pengumuman');
INSERT INTO `tbl_page` VALUES ('5', '205', 'Jenis Discount');
INSERT INTO `tbl_page` VALUES ('6', '206', 'Status On-line user');
INSERT INTO `tbl_page` VALUES ('7', '207', 'Promo');
INSERT INTO `tbl_page` VALUES ('8', '208', 'Daftar Manifest');
INSERT INTO `tbl_page` VALUES ('9', '209', 'Pembatalan');
INSERT INTO `tbl_page` VALUES ('10', '210', 'Laporan Pembatalan');
INSERT INTO `tbl_page` VALUES ('11', '211', 'Laporan Koreksi Discount');
INSERT INTO `tbl_page` VALUES ('12', '212', 'Dashboard');
INSERT INTO `tbl_page` VALUES ('13', '213', 'General Total');
INSERT INTO `tbl_page` VALUES ('14', '214', 'Berita Acara Insentif Sopir');
INSERT INTO `tbl_page` VALUES ('15', '215', 'Berita Acara Biaya Operasional');
INSERT INTO `tbl_page` VALUES ('16', '216', 'CSO');
INSERT INTO `tbl_page` VALUES ('17', '217', 'Cabang');
INSERT INTO `tbl_page` VALUES ('18', '218', 'Jurusan');
INSERT INTO `tbl_page` VALUES ('19', '219', 'Berdasarkan Jam');
INSERT INTO `tbl_page` VALUES ('20', '220', 'Keseluruhan');
INSERT INTO `tbl_page` VALUES ('21', '221', 'Kendaraan');
INSERT INTO `tbl_page` VALUES ('23', '223', 'Voucher');
INSERT INTO `tbl_page` VALUES ('24', '224', 'Laporan Data Paket');
INSERT INTO `tbl_page` VALUES ('25', '225', 'Laporan Paket Belum Diambil');
INSERT INTO `tbl_page` VALUES ('26', '226', 'Laporan Omzet Per Cabang');
INSERT INTO `tbl_page` VALUES ('27', '227', 'Laporan Omzet Per Jurusan');
INSERT INTO `tbl_page` VALUES ('28', '228', 'Cargo');
INSERT INTO `tbl_page` VALUES ('29', '229', 'Setoran CSO');
INSERT INTO `tbl_page` VALUES ('30', '230', 'Cabang');
INSERT INTO `tbl_page` VALUES ('31', '231', 'Harian');
INSERT INTO `tbl_page` VALUES ('32', '232', 'Laporan Rekon Fee');
INSERT INTO `tbl_page` VALUES ('33', '233', 'Biaya Insentif Sopir');
INSERT INTO `tbl_page` VALUES ('34', '234', 'Laporan Voucher BBM');
INSERT INTO `tbl_page` VALUES ('35', '235', 'Laporan Biaya Operasional');
INSERT INTO `tbl_page` VALUES ('36', '236', 'Penjualan Tiketux');
INSERT INTO `tbl_page` VALUES ('37', '237', 'Top Up Deposit');
INSERT INTO `tbl_page` VALUES ('38', '238', 'Laporan Transaksi Deposit');
INSERT INTO `tbl_page` VALUES ('39', '239', 'Data Member');
INSERT INTO `tbl_page` VALUES ('40', '240', 'Member Berulang Tahun');
INSERT INTO `tbl_page` VALUES ('41', '241', 'Pelanggan Potensi Jadi Member');
INSERT INTO `tbl_page` VALUES ('42', '242', 'Frekwensi Berangkat');
INSERT INTO `tbl_page` VALUES ('43', '243', 'Member Hampir Exipred');
INSERT INTO `tbl_page` VALUES ('44', '244', 'Kota');
INSERT INTO `tbl_page` VALUES ('45', '245', 'Cabang');
INSERT INTO `tbl_page` VALUES ('46', '246', 'Jurusan');
INSERT INTO `tbl_page` VALUES ('47', '247', 'Jadwal');
INSERT INTO `tbl_page` VALUES ('48', '248', 'Pengaturan Umum');
INSERT INTO `tbl_page` VALUES ('49', '249', 'Sopir');
INSERT INTO `tbl_page` VALUES ('50', '250', 'Mobil');
INSERT INTO `tbl_page` VALUES ('51', '251', 'User');
INSERT INTO `tbl_page` VALUES ('53', '252', 'Pengaturan Page');
INSERT INTO `tbl_page` VALUES ('55', '222', 'Sopir');
INSERT INTO `tbl_page` VALUES ('57', '201', 'Reservasi');

-- ----------------------------
-- Table structure for tbl_permission
-- ----------------------------
DROP TABLE IF EXISTS `tbl_permission`;
CREATE TABLE `tbl_permission` (
  `page_id` float NOT NULL,
  `user_level` decimal(3,1) NOT NULL,
  PRIMARY KEY (`page_id`,`user_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tbl_permission
-- ----------------------------
INSERT INTO `tbl_permission` VALUES ('100', '0.0');
INSERT INTO `tbl_permission` VALUES ('200', '0.0');
INSERT INTO `tbl_permission` VALUES ('201', '0.0');
INSERT INTO `tbl_permission` VALUES ('201', '1.0');
INSERT INTO `tbl_permission` VALUES ('201', '1.2');
INSERT INTO `tbl_permission` VALUES ('201', '1.3');
INSERT INTO `tbl_permission` VALUES ('201', '1.4');
INSERT INTO `tbl_permission` VALUES ('201', '2.0');
INSERT INTO `tbl_permission` VALUES ('201', '2.1');
INSERT INTO `tbl_permission` VALUES ('201', '2.2');
INSERT INTO `tbl_permission` VALUES ('201', '3.0');
INSERT INTO `tbl_permission` VALUES ('202', '0.0');
INSERT INTO `tbl_permission` VALUES ('202', '1.0');
INSERT INTO `tbl_permission` VALUES ('202', '1.2');
INSERT INTO `tbl_permission` VALUES ('202', '1.3');
INSERT INTO `tbl_permission` VALUES ('202', '1.4');
INSERT INTO `tbl_permission` VALUES ('202', '2.0');
INSERT INTO `tbl_permission` VALUES ('202', '2.1');
INSERT INTO `tbl_permission` VALUES ('202', '2.2');
INSERT INTO `tbl_permission` VALUES ('202', '3.0');
INSERT INTO `tbl_permission` VALUES ('203', '0.0');
INSERT INTO `tbl_permission` VALUES ('203', '1.0');
INSERT INTO `tbl_permission` VALUES ('203', '1.2');
INSERT INTO `tbl_permission` VALUES ('203', '1.3');
INSERT INTO `tbl_permission` VALUES ('203', '1.4');
INSERT INTO `tbl_permission` VALUES ('203', '3.0');
INSERT INTO `tbl_permission` VALUES ('204', '0.0');
INSERT INTO `tbl_permission` VALUES ('204', '1.0');
INSERT INTO `tbl_permission` VALUES ('204', '1.2');
INSERT INTO `tbl_permission` VALUES ('204', '1.3');
INSERT INTO `tbl_permission` VALUES ('204', '1.4');
INSERT INTO `tbl_permission` VALUES ('204', '3.0');
INSERT INTO `tbl_permission` VALUES ('205', '0.0');
INSERT INTO `tbl_permission` VALUES ('205', '1.0');
INSERT INTO `tbl_permission` VALUES ('205', '1.2');
INSERT INTO `tbl_permission` VALUES ('206', '0.0');
INSERT INTO `tbl_permission` VALUES ('206', '1.0');
INSERT INTO `tbl_permission` VALUES ('206', '1.2');
INSERT INTO `tbl_permission` VALUES ('207', '0.0');
INSERT INTO `tbl_permission` VALUES ('207', '1.0');
INSERT INTO `tbl_permission` VALUES ('207', '1.2');
INSERT INTO `tbl_permission` VALUES ('208', '0.0');
INSERT INTO `tbl_permission` VALUES ('208', '1.0');
INSERT INTO `tbl_permission` VALUES ('208', '1.2');
INSERT INTO `tbl_permission` VALUES ('208', '1.3');
INSERT INTO `tbl_permission` VALUES ('208', '1.4');
INSERT INTO `tbl_permission` VALUES ('208', '5.0');
INSERT INTO `tbl_permission` VALUES ('209', '0.0');
INSERT INTO `tbl_permission` VALUES ('209', '1.0');
INSERT INTO `tbl_permission` VALUES ('209', '1.2');
INSERT INTO `tbl_permission` VALUES ('209', '1.3');
INSERT INTO `tbl_permission` VALUES ('209', '1.4');
INSERT INTO `tbl_permission` VALUES ('210', '0.0');
INSERT INTO `tbl_permission` VALUES ('210', '1.0');
INSERT INTO `tbl_permission` VALUES ('210', '1.2');
INSERT INTO `tbl_permission` VALUES ('210', '1.3');
INSERT INTO `tbl_permission` VALUES ('210', '1.4');
INSERT INTO `tbl_permission` VALUES ('210', '5.0');
INSERT INTO `tbl_permission` VALUES ('211', '0.0');
INSERT INTO `tbl_permission` VALUES ('211', '1.0');
INSERT INTO `tbl_permission` VALUES ('211', '1.2');
INSERT INTO `tbl_permission` VALUES ('211', '1.3');
INSERT INTO `tbl_permission` VALUES ('211', '1.4');
INSERT INTO `tbl_permission` VALUES ('211', '5.0');
INSERT INTO `tbl_permission` VALUES ('212', '0.0');
INSERT INTO `tbl_permission` VALUES ('212', '1.0');
INSERT INTO `tbl_permission` VALUES ('212', '1.2');
INSERT INTO `tbl_permission` VALUES ('212', '1.3');
INSERT INTO `tbl_permission` VALUES ('212', '1.4');
INSERT INTO `tbl_permission` VALUES ('212', '3.0');
INSERT INTO `tbl_permission` VALUES ('213', '0.0');
INSERT INTO `tbl_permission` VALUES ('213', '1.0');
INSERT INTO `tbl_permission` VALUES ('214', '0.0');
INSERT INTO `tbl_permission` VALUES ('214', '1.0');
INSERT INTO `tbl_permission` VALUES ('214', '1.2');
INSERT INTO `tbl_permission` VALUES ('214', '1.3');
INSERT INTO `tbl_permission` VALUES ('214', '1.4');
INSERT INTO `tbl_permission` VALUES ('214', '5.0');
INSERT INTO `tbl_permission` VALUES ('215', '0.0');
INSERT INTO `tbl_permission` VALUES ('215', '1.0');
INSERT INTO `tbl_permission` VALUES ('215', '1.2');
INSERT INTO `tbl_permission` VALUES ('215', '1.3');
INSERT INTO `tbl_permission` VALUES ('215', '1.4');
INSERT INTO `tbl_permission` VALUES ('215', '5.0');
INSERT INTO `tbl_permission` VALUES ('216', '0.0');
INSERT INTO `tbl_permission` VALUES ('216', '1.0');
INSERT INTO `tbl_permission` VALUES ('216', '1.2');
INSERT INTO `tbl_permission` VALUES ('216', '1.3');
INSERT INTO `tbl_permission` VALUES ('216', '1.4');
INSERT INTO `tbl_permission` VALUES ('216', '5.0');
INSERT INTO `tbl_permission` VALUES ('217', '0.0');
INSERT INTO `tbl_permission` VALUES ('217', '1.0');
INSERT INTO `tbl_permission` VALUES ('217', '1.2');
INSERT INTO `tbl_permission` VALUES ('217', '1.3');
INSERT INTO `tbl_permission` VALUES ('217', '1.4');
INSERT INTO `tbl_permission` VALUES ('217', '5.0');
INSERT INTO `tbl_permission` VALUES ('218', '0.0');
INSERT INTO `tbl_permission` VALUES ('218', '1.0');
INSERT INTO `tbl_permission` VALUES ('218', '1.2');
INSERT INTO `tbl_permission` VALUES ('218', '5.0');
INSERT INTO `tbl_permission` VALUES ('219', '0.0');
INSERT INTO `tbl_permission` VALUES ('219', '1.0');
INSERT INTO `tbl_permission` VALUES ('219', '1.2');
INSERT INTO `tbl_permission` VALUES ('219', '1.3');
INSERT INTO `tbl_permission` VALUES ('219', '5.0');
INSERT INTO `tbl_permission` VALUES ('220', '0.0');
INSERT INTO `tbl_permission` VALUES ('220', '1.0');
INSERT INTO `tbl_permission` VALUES ('220', '1.2');
INSERT INTO `tbl_permission` VALUES ('220', '1.3');
INSERT INTO `tbl_permission` VALUES ('220', '5.0');
INSERT INTO `tbl_permission` VALUES ('221', '0.0');
INSERT INTO `tbl_permission` VALUES ('221', '1.0');
INSERT INTO `tbl_permission` VALUES ('221', '1.2');
INSERT INTO `tbl_permission` VALUES ('221', '5.0');
INSERT INTO `tbl_permission` VALUES ('222', '0.0');
INSERT INTO `tbl_permission` VALUES ('222', '1.0');
INSERT INTO `tbl_permission` VALUES ('222', '1.2');
INSERT INTO `tbl_permission` VALUES ('222', '5.0');
INSERT INTO `tbl_permission` VALUES ('223', '0.0');
INSERT INTO `tbl_permission` VALUES ('223', '1.0');
INSERT INTO `tbl_permission` VALUES ('223', '5.0');
INSERT INTO `tbl_permission` VALUES ('224', '0.0');
INSERT INTO `tbl_permission` VALUES ('224', '1.0');
INSERT INTO `tbl_permission` VALUES ('224', '1.2');
INSERT INTO `tbl_permission` VALUES ('224', '1.3');
INSERT INTO `tbl_permission` VALUES ('224', '1.4');
INSERT INTO `tbl_permission` VALUES ('224', '2.0');
INSERT INTO `tbl_permission` VALUES ('224', '2.1');
INSERT INTO `tbl_permission` VALUES ('224', '2.2');
INSERT INTO `tbl_permission` VALUES ('224', '3.0');
INSERT INTO `tbl_permission` VALUES ('225', '0.0');
INSERT INTO `tbl_permission` VALUES ('225', '1.0');
INSERT INTO `tbl_permission` VALUES ('225', '1.2');
INSERT INTO `tbl_permission` VALUES ('225', '1.3');
INSERT INTO `tbl_permission` VALUES ('225', '1.4');
INSERT INTO `tbl_permission` VALUES ('225', '2.0');
INSERT INTO `tbl_permission` VALUES ('225', '2.1');
INSERT INTO `tbl_permission` VALUES ('225', '2.2');
INSERT INTO `tbl_permission` VALUES ('225', '3.0');
INSERT INTO `tbl_permission` VALUES ('226', '0.0');
INSERT INTO `tbl_permission` VALUES ('226', '1.0');
INSERT INTO `tbl_permission` VALUES ('226', '1.2');
INSERT INTO `tbl_permission` VALUES ('226', '1.3');
INSERT INTO `tbl_permission` VALUES ('226', '1.4');
INSERT INTO `tbl_permission` VALUES ('226', '2.0');
INSERT INTO `tbl_permission` VALUES ('226', '2.1');
INSERT INTO `tbl_permission` VALUES ('226', '2.2');
INSERT INTO `tbl_permission` VALUES ('226', '3.0');
INSERT INTO `tbl_permission` VALUES ('227', '0.0');
INSERT INTO `tbl_permission` VALUES ('227', '1.0');
INSERT INTO `tbl_permission` VALUES ('227', '1.2');
INSERT INTO `tbl_permission` VALUES ('227', '1.3');
INSERT INTO `tbl_permission` VALUES ('227', '1.4');
INSERT INTO `tbl_permission` VALUES ('227', '2.0');
INSERT INTO `tbl_permission` VALUES ('227', '2.1');
INSERT INTO `tbl_permission` VALUES ('227', '2.2');
INSERT INTO `tbl_permission` VALUES ('227', '3.0');
INSERT INTO `tbl_permission` VALUES ('228', '0.0');
INSERT INTO `tbl_permission` VALUES ('228', '1.0');
INSERT INTO `tbl_permission` VALUES ('228', '1.2');
INSERT INTO `tbl_permission` VALUES ('228', '1.3');
INSERT INTO `tbl_permission` VALUES ('228', '1.4');
INSERT INTO `tbl_permission` VALUES ('228', '2.0');
INSERT INTO `tbl_permission` VALUES ('228', '2.1');
INSERT INTO `tbl_permission` VALUES ('228', '2.2');
INSERT INTO `tbl_permission` VALUES ('228', '3.0');
INSERT INTO `tbl_permission` VALUES ('229', '0.0');
INSERT INTO `tbl_permission` VALUES ('229', '1.0');
INSERT INTO `tbl_permission` VALUES ('229', '1.2');
INSERT INTO `tbl_permission` VALUES ('229', '1.3');
INSERT INTO `tbl_permission` VALUES ('229', '2.0');
INSERT INTO `tbl_permission` VALUES ('229', '2.1');
INSERT INTO `tbl_permission` VALUES ('229', '5.0');
INSERT INTO `tbl_permission` VALUES ('230', '0.0');
INSERT INTO `tbl_permission` VALUES ('230', '1.0');
INSERT INTO `tbl_permission` VALUES ('230', '1.2');
INSERT INTO `tbl_permission` VALUES ('230', '5.0');
INSERT INTO `tbl_permission` VALUES ('231', '0.0');
INSERT INTO `tbl_permission` VALUES ('231', '1.0');
INSERT INTO `tbl_permission` VALUES ('231', '1.2');
INSERT INTO `tbl_permission` VALUES ('231', '5.0');
INSERT INTO `tbl_permission` VALUES ('232', '0.0');
INSERT INTO `tbl_permission` VALUES ('232', '1.0');
INSERT INTO `tbl_permission` VALUES ('232', '1.2');
INSERT INTO `tbl_permission` VALUES ('232', '5.0');
INSERT INTO `tbl_permission` VALUES ('233', '0.0');
INSERT INTO `tbl_permission` VALUES ('233', '1.0');
INSERT INTO `tbl_permission` VALUES ('233', '1.2');
INSERT INTO `tbl_permission` VALUES ('233', '5.0');
INSERT INTO `tbl_permission` VALUES ('234', '0.0');
INSERT INTO `tbl_permission` VALUES ('234', '1.0');
INSERT INTO `tbl_permission` VALUES ('234', '1.2');
INSERT INTO `tbl_permission` VALUES ('234', '5.0');
INSERT INTO `tbl_permission` VALUES ('235', '0.0');
INSERT INTO `tbl_permission` VALUES ('235', '1.0');
INSERT INTO `tbl_permission` VALUES ('235', '1.2');
INSERT INTO `tbl_permission` VALUES ('235', '5.0');
INSERT INTO `tbl_permission` VALUES ('236', '0.0');
INSERT INTO `tbl_permission` VALUES ('236', '1.0');
INSERT INTO `tbl_permission` VALUES ('236', '5.0');
INSERT INTO `tbl_permission` VALUES ('237', '0.0');
INSERT INTO `tbl_permission` VALUES ('237', '1.0');
INSERT INTO `tbl_permission` VALUES ('237', '5.0');
INSERT INTO `tbl_permission` VALUES ('238', '0.0');
INSERT INTO `tbl_permission` VALUES ('238', '1.0');
INSERT INTO `tbl_permission` VALUES ('238', '5.0');
INSERT INTO `tbl_permission` VALUES ('239', '0.0');
INSERT INTO `tbl_permission` VALUES ('239', '1.0');
INSERT INTO `tbl_permission` VALUES ('239', '1.2');
INSERT INTO `tbl_permission` VALUES ('239', '6.0');
INSERT INTO `tbl_permission` VALUES ('240', '0.0');
INSERT INTO `tbl_permission` VALUES ('240', '1.0');
INSERT INTO `tbl_permission` VALUES ('240', '1.2');
INSERT INTO `tbl_permission` VALUES ('241', '0.0');
INSERT INTO `tbl_permission` VALUES ('241', '1.0');
INSERT INTO `tbl_permission` VALUES ('241', '1.2');
INSERT INTO `tbl_permission` VALUES ('242', '0.0');
INSERT INTO `tbl_permission` VALUES ('242', '1.0');
INSERT INTO `tbl_permission` VALUES ('242', '1.2');
INSERT INTO `tbl_permission` VALUES ('243', '0.0');
INSERT INTO `tbl_permission` VALUES ('243', '1.0');
INSERT INTO `tbl_permission` VALUES ('243', '1.2');
INSERT INTO `tbl_permission` VALUES ('244', '0.0');
INSERT INTO `tbl_permission` VALUES ('244', '1.0');
INSERT INTO `tbl_permission` VALUES ('245', '0.0');
INSERT INTO `tbl_permission` VALUES ('245', '1.0');
INSERT INTO `tbl_permission` VALUES ('245', '1.2');
INSERT INTO `tbl_permission` VALUES ('245', '1.4');
INSERT INTO `tbl_permission` VALUES ('246', '0.0');
INSERT INTO `tbl_permission` VALUES ('246', '1.0');
INSERT INTO `tbl_permission` VALUES ('246', '1.2');
INSERT INTO `tbl_permission` VALUES ('246', '1.4');
INSERT INTO `tbl_permission` VALUES ('247', '0.0');
INSERT INTO `tbl_permission` VALUES ('247', '1.0');
INSERT INTO `tbl_permission` VALUES ('247', '1.2');
INSERT INTO `tbl_permission` VALUES ('247', '1.4');
INSERT INTO `tbl_permission` VALUES ('248', '0.0');
INSERT INTO `tbl_permission` VALUES ('248', '1.0');
INSERT INTO `tbl_permission` VALUES ('249', '0.0');
INSERT INTO `tbl_permission` VALUES ('249', '1.0');
INSERT INTO `tbl_permission` VALUES ('249', '1.2');
INSERT INTO `tbl_permission` VALUES ('249', '1.4');
INSERT INTO `tbl_permission` VALUES ('250', '0.0');
INSERT INTO `tbl_permission` VALUES ('250', '1.0');
INSERT INTO `tbl_permission` VALUES ('250', '1.2');
INSERT INTO `tbl_permission` VALUES ('250', '1.4');
INSERT INTO `tbl_permission` VALUES ('250', '7.0');
INSERT INTO `tbl_permission` VALUES ('251', '0.0');
INSERT INTO `tbl_permission` VALUES ('251', '1.0');
INSERT INTO `tbl_permission` VALUES ('252', '0.0');
INSERT INTO `tbl_permission` VALUES ('300', '0.0');
INSERT INTO `tbl_permission` VALUES ('400', '0.0');
