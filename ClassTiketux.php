<?
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	//exit;
}
//#############################################################################
require_once('./includes/sha256.inc.php');

class Tiketux{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $config; //config
	
	//CONSTRUCTOR
	function Tiketux(){
		$this->ID_FILE="C-TTX";
		
		$this->config['url_tiketux']="http://api.3trust.com/tiketux/v1/finnet/payment.json"; //server development tiketux di 3trust
		//$this->config['url_mc']="https://demos.finnet-indonesia.com/telkom/PaymentUser.action"; //server development finnet
		//$this->config['merchant_id']="CPG002";
		//$this->config['merchant_password']="CIPG%202";
		//$this->config['timeout']="180"; //menit
		//$this->config['return_url']="http://localhost/bimotux/tiketux_ws_receiver_parameters.php"; //dummy testing
		//$this->config['return_url']="http://bimo.tiketux.com/dayax/api/tiketux_ws_receiver_parameters.php"; //dummy testing
	}
	
	function merSignature($array){
		$output = $array['merchant_id'];
		$output .= '%'.$array['invoice'];
		$output .= '%'.$array['amount'];
		$output .= $array['add_info1']!=""?'%'.$array['add_info1']:"";
		$output .= $array['add_info2']!=""?'%'.$array['add_info2']:"";
		$output .= $array['add_info3']!=""?'%'.$array['add_info3']:"";
		$output .= $array['add_info4']!=""?'%'.$array['add_info4']:"";
		$output .= $array['add_info5']!=""?'%'.$array['add_info5']:"";
		$output .= $array['timeout']!=""?'%'.$array['timeout']:"";
		$output .= $array['return_url']!=""?'%'.$array['return_url']:"";
		
		return strtoupper($output);
	}

	function hash256($input){
		return hash("sha256",$input);
	}
	
	function sendParameterRequest($parameters){
		global $db;
		
		//set parameter default
		$parameters_sent='';
		
		$parameters_sent = "?kode_agen=BMT";
		$parameters_sent .= "&kode_booking=".$parameters['kode_booking'];
		$parameters_sent .= "&jumlah=".$parameters['jumlah'];
		
		$length = strlen($parameters_sent);
		$url = $this->config['url_tiketux'].$parameters_sent;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_ENCODING, "");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_POST, 0);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters_sent);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("application/x-www-form-urlencoded", "Content-length: $length"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		
		return $response;
		//echo ">".$response."<";
	}

}
?>