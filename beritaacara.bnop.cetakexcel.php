<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraBNOP.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSopir.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['KEUANGAN']))){
  redirect('index.'.$phpEx,true);
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$expired_time = 120; //menit

$kondisi =	$cari==""?"":
	" AND (NamaPenerima LIKE '$cari%'
				OR Keterangan LIKE '%$cari%'
				OR NamaPembuat LIKE '%$cari%'
				OR NamaReleaser LIKE '%$cari%')";

$order	=($order=='')?"DESC":$order;
	
$sort_by =($sort_by=='')?"WaktuTransaksi":$sort_by;

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:I2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Berita Acara Biaya Non Operasional Cari:'.$cari);

$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Penerima');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Waktu BA');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D3', 'BA Oleh');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Jenis Biaya');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F3', 'Jumlah');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Keterangan');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Releaser');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'Waktu Released');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J3', 'Status');

$sql	=
	"SELECT *,IF(TIME_TO_SEC(TIMEDIFF(WaktuTransaksi,NOW()))/60>=-$expired_time OR WaktuTransaksi IS NULL,0,1) AS IsExpired
	FROM tbl_ba_bnop
	$kondisi
	ORDER BY $sort_by $order;";

if(!$result = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

$i=1;

$idx=0;

while ($row = $db->sql_fetchrow($result)){
	$idx++;
	$idx_row=$idx+3;
	
	if($row['IsRelease']==1){
		$status= "RELEASED";
	}
	elseif($row['IsExpired']==0){
		$status= "BELUM RELEASED";
	}
	else{
		$status= "EXPIRED";
	}

	$LIST_JENIS_BIAYA=array(
		"BELI AQUA",
		"LISTRIK",
		"LAINNYA");
	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $row['NamaPenerima']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, dateparse(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])));
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $row['NamaPembuat']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $LIST_JENIS_BIAYA[$row['JenisBiaya']]);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $row['Jumlah']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $row['Keterangan']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $row['NamaReleaser']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $row['WaktuRelease']==""?"":dateparse(FormatMySQLDateToTglWithTime($row['WaktuRelease'])));
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$idx_row, $status);
	
}
$temp_idx=$idx_row;

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Berita Acara Biaya Non Operasional.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}

  
?>
