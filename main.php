<?php
//
// Menu Utama
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,200);
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$USER_LEVEL_INDEX['SCHEDULER']){
  //redirect('index.'.$phpEx,true); 
}
//#############################################################################
// HEADER

// TEMPLATE
switch($userdata['user_level']){
	case $USER_LEVEL_INDEX['CSO']:
		redirect(append_sid('menu_operasional.'.$phpEx.''),true);
	break;

	case $USER_LEVEL_INDEX['CSO2']:
		redirect(append_sid('reservasi.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX['CSO_PAKET']:
		redirect(append_sid('reservasi_paket.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX['MEKANIK']:
		redirect(append_sid('pengaturan_mobil.'.$phpEx.''),true); 
	break;
	
	case $USER_LEVEL_INDEX['ADMIN']:
		
		if($HTTP_GET_VARS['menu_blokir']==1){
			redirect(append_sid('pengaturan_blokir.'.$phpEx),true); 
		}
		else{
			redirect(append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional',true)); 
		}
	break;
	
	case $USER_LEVEL_INDEX['MANAJEMEN']:
		redirect(append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional',true)); 
	break;
	
	case $USER_LEVEL_INDEX['MANAJER']:
		redirect(append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional',true)); 
	break;
	
	case $USER_LEVEL_INDEX['SPV_RESERVASI'] :
		redirect(append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional',true)); 
	break;

	case $USER_LEVEL_INDEX['SPV_OPERASIONAL'] :
		redirect(append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional',true)); 
	break;
	
	case $USER_LEVEL_INDEX['SCHEDULER'] :
		redirect(append_sid('menu_operasional.'.$phpEx.'?top_menu_dipilih=top_menu_operasional',true)); 
	break;
	
	case $USER_LEVEL_INDEX['KEUANGAN'] :
		redirect(append_sid('menu_lap_keuangan.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan',true)); 
	break;
	
	case $USER_LEVEL_INDEX['CUSTOMER_CARE'] :
		redirect(append_sid('menu_pengelolaan_member.'.$phpEx.'?top_menu_dipilih=top_menu_lap_keuangan',true));
	break;
	
}


$template->assign_vars(array
  ( 'BCRUMP'    				=> '<a href="'.append_sid('main.'.$phpEx) .'">Home',
    'U_PENGATURAN'  		=>append_sid('pengaturan.'.$phpEx.''),
    'U_RESERVASI'   		=>append_sid('reservasi.'.$phpEx.''),
		'U_USER_LOGIN'			=>append_sid('pengaturan_user_list_login.'.$phpEx),
		'U_UBAHPASS'   			=>append_sid('ubah_password.'.$phpEx.''),
		'U_PENGUMUMAN'  		=>append_sid('pengaturan_pengumuman.'.$phpEx.''),
		'U_PENJADWALAN' 		=>append_sid('pengaturan_penjadwalan_kendaraan.'.$phpEx.''),
		'U_BATAL' 					=>append_sid('pembatalan.'.$phpEx.''),
    'U_JENIS_DISCOUNT'	=>append_sid('jenis_discount.'.$phpEx),
    'U_CABANG'					=>append_sid('pengaturan_cabang.'.$phpEx),
    'U_JURUSAN'					=>append_sid('pengaturan_jurusan.'.$phpEx),
    'U_JADWAL'					=>append_sid('pengaturan_jadwal.'.$phpEx),
    'U_SOPIR'						=>append_sid('pengaturan_sopir.'.$phpEx),
    'U_MOBIL'						=>append_sid('pengaturan_mobil.'.$phpEx),
    'U_USER'						=>append_sid('pengaturan_user.'.$phpEx),
		'U_PROMO'						=>append_sid('pengaturan_promo.'.$phpEx),
		'U_MEMBER'					=>append_sid('pengaturan_member.'.$phpEx),
		'U_MEMBER_ULTAH'		=>append_sid('pengaturan_member_ultah.'.$phpEx),
		'U_MEMBER_CALON'		=>append_sid('pengaturan_member_calon_member.'.$phpEx),
		//'U_MEMBER_CALON'		=>append_sid('main.'.$phpEx),
		'U_MEMBER_FREKWENSI'=>append_sid('pengaturan_member_frekwensi.'.$phpEx),
		'U_MEMBER_HAMPIR_EXPIRED'=>append_sid('pengaturan_member_hampir_expired.'.$phpEx),
		'U_PENGATURAN_UMUM'	=>append_sid('pengaturan_umum.'.$phpEx),
		'U_LAPORAN_CSO'			=>append_sid('laporan_penjualan_cso.'.$phpEx),
		'U_LAPORAN_UANG_CSO'=>append_sid('laporan_uang_user.'.$phpEx.'?mode=cso'),
		'U_LAPORAN_CABANG'	=>append_sid('laporan_omzet_cabang.'.$phpEx),
		'U_LAPORAN_JURUSAN'	=>append_sid('laporan_omzet_jurusan.'.$phpEx),
		'U_LAPORAN_JADWAL'	=>append_sid('laporan_omzet_jadwal.'.$phpEx),
		'U_LAPORAN_OMZET'		=>append_sid('laporan_omzet.'.$phpEx),
		'U_LAPORAN_KENDARAAN'=>append_sid('laporan_omzet_kendaraan.'.$phpEx),
		'U_LAPORAN_SOPIR'		=>append_sid('laporan_omzet_sopir.'.$phpEx),
		'U_LAPORAN_UANG_CABANG'=>append_sid('laporan_uang_cabang.'.$phpEx),
		'U_LAPORAN_UANG_CSO'=>append_sid('laporan_keuangan_cso.'.$phpEx),
		'U_LOG_BATAL'				=>append_sid('laporan_pembatalan.'.$phpEx),
		'U_REKAP_UANG_HARIAN'=>append_sid('laporan_keuangan_harian.'.$phpEx),
		'U_REKAP_FEE_TRANSAKSI'=>append_sid('laporan_keuangan_fee.'.$phpEx),
		'U_MASTER_ASURANSI'	=>append_sid('pengaturan_asuransi.'.$phpEx),
		'U_LAPORAN_ASURANSI'	=>append_sid('laporan_asuransi.'.$phpEx),
		'U_MEMO'	=>append_sid('pengaturan_memo.'.$phpEx)
		
  ));

// PARSE
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>