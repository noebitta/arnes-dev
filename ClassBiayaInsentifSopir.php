<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit; 
}

//#############################################################################

class BiayaInsentifSopir{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function BiayaInsentifSopir(){
		$this->ID_FILE="C-BIS";
	}
	
	//BODY
	
	function tambah(

		$kode_jadwal,$kode_kendaraan,$kode_sopir,
		$nama_sopir,$nominal,
		$keterangan=""){
	  
		/*
		ID		: 001
		DESC	: menambahkan data biaya operasional 
		*/

		//kamus
		global $db;
		global $userdata;

		//ambil data penjadwalan_kendaraan
		$sql = "SELECT * FROM tbl_penjadwalan_kendaraan
				WHERE KodeJadwal='$kode_jadwal'
				AND IdJurusan=f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal')
				AND KodeKendaraan='$kode_kendaraan'
				AND KodeDriver='$kode_sopir'
				AND NamaDriver='$nama_sopir'";

		$result = $db->sql_query($sql);
		$row = $db->sql_fetchrow($result);
		$IdPenjadwalan = $row['IdPenjadwalan'];
		$jam = $row['JamBerangkat'];

		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql	=
			"INSERT INTO tbl_biaya_insentif_sopir (
				IdPenjadwalan,JamBerangkat,
				TglBerangkat, IdJurusan, KodeJadwal,
				KodeKendaraan, KodeSopir, NamaSopir,
				IdPembuat, NamaPembuat,
				Keterangan, NominalInsentif,
				WaktuBuat)
			VALUES (
				'$IdPenjadwalan','$jam',
				NOW(), f_jadwal_ambil_id_jurusan_by_kode_jadwal('$kode_jadwal'), '$kode_jadwal',
				'$kode_kendaraan','$kode_sopir', '$nama_sopir',
				'$userdata[user_id]','$userdata[nama]',
				'$keterangan', '$nominal',
				NOW());";

		if (!$db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function ambilBiaya($kode_jadwal,$sopir){
		/*
		ID	:002
		Desc	:Mengembalikan data biaya op sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT 
				IdPenjadwalan, TglBerangkat,
				JamBerangkat, IdJurusan,
				KodeJadwal, KodeKendaraan,
				KodeSopir, NamaSopir,
				Keterangan,IdPembuat,
				NamaPembuat,WaktuBuat,
				IdApprover,NamaApprover,
				WaktuApprove,NominalInsentif,
				IdReleaser,NamaReleaser,
				WaktuRelease,CabangRelease,
				IsRelease
			FROM tbl_biaya_insentif_sopir
			WHERE KodeJadwal='$kode_jadwal'
			AND KodeSopir='$sopir';";

		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$my_error=$db->sql_error();
			die_error("Err: $this->ID_FILE".__LINE__ .$my_error['message']);
		}
		
	}//  END ambilBiayaOpByJurusan
	
}
?>