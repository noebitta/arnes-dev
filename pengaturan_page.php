<?php
/**
 * Created by PhpStorm.
 * User: assus
 * Date: 12/31/2015
 * Time: 13:50 PM
 */
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassUser.php');

$userdata = session_pagestart($user_ip,251);  // Master : 200
init_userprefs($userdata);

$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode'];
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;
$Cabang	= new Cabang();
$User		= new User();

    function setComboPage($page_dipilih){
        //SET COMBO cabang
        global $db;

        $sql = "SELECT *
                FROM tbl_page
                ORDER BY page_id ASC";

        $result=$db->sql_query($sql);
        $opt_page="";

        if($result){
            while ($row = $db->sql_fetchrow($result)){
                $selected	=($page_dipilih!=$row['page_id'])?"":"selected";
                $opt_page .="<option value='$row[page_id]' $selected>$row[nama_page]</option>";
            }
        }
        else{
            echo("Error :".__LINE__);exit;
        }
        return $opt_page;
        //END SET COMBO CABANG
    }

    function level($data){
        $USER_LEVEL= array(
            '0.0'=>"Admin",
            '1.0'=>"Manajemen",
            '1.2'=>"Manajer",
            '1.3'=>"Spv.Reservasi",
            '1.4'=>"Spv.Operasional",
            '2.0'=>"CSO",
            '2.1'=>"CSO Paket",
            '2.2'=>"CSO2",
            '3.0'=>"Scheduler",
            '4.0'=>"Kasir",
            '5.0'=>"Keuangan",
            '6.0'=>"Customer Care",
            '7.0'=>"Mekanik",
            '8.0'=>"Checker",
            '9.0'=>"Picker");

        $ExplodeData = explode(',',$data);

        $level = [];
        foreach($ExplodeData as $key=>$a){
            array_push($level,$USER_LEVEL[$a]);
        }

        foreach($level as $key=>$val){
            if($val != end($level)){
                $access .= $val.', ';
            }else{
                $access .= $val;
            }
        }
        return $access;
    }

    if ($mode=='assign'){

        $pesan = $HTTP_GET_VARS['pesan'];

        if($pesan==1){
            $pesan="<font color='green' size=3>Data Berhasil Ditambahkan!</font>";
            $bgcolor_pesan="98e46f";
        }

        $option = "<select id='opt_user_level' name='opt_user_level'>".setComboUserLevel(2)."</select>";

        $template->set_filenames(array('body' => 'page/assign_body.tpl'));
        $template->assign_vars(array(
                'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=assign").'">Assign Permission</a> ',
                'JUDUL'		=>'Assign Permission',
                'MODE'   	=> 'PostAssign',
                'OPT_USER_LEVEL'	=>$option,
                'PAGE_ID'	=>setComboPage(""),
                'PESAN'						=> $pesan,
                'BGCOLOR_PESAN'		=> $bgcolor_pesan,
                'U_ASSIGN_USER_ACT'	=> append_sid('pengaturan_page.'.$phpEx)
            )
        );
    }
    elseif($mode=='PostAssign'){
        $user_level				= $HTTP_POST_VARS['opt_user_level'];
        $page_id				= $HTTP_POST_VARS['page_id'];

        $terjadi_error=false;

        if($submode=='permission'){

            $judul="Edit Permission";
            $path	='<a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=assign").'">Edit Permission</a> ';

            $sql = "
                      SELECT user_level FROM tbl_permission
                      WHERE page_id=$page_id";

            $result = $db->sql_query($sql);
            $sql = array();

            $no = 0;
            while($row=$db->sql_fetchrow($result)){
                $sql =
                    "UPDATE tbl_permission
                SET
                    page_id=$page_id,
                    user_level=$user_level[$no]
                WHERE page_id=$page_id
                AND user_level=$row[user_level]";

                $db->sql_query($sql);

                $no++;
            }

            redirect(append_sid('pengaturan_page.'.$phpEx.'?mode=edit&id='.$page_id.'&pesan=1',true));

        }elseif($submode==0){
            $judul="Assign Permission";
            $path	='<a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=assign").'">Assign Permission</a> ';

            $sql = "INSERT INTO tbl_permission (page_id, user_level)
                            VALUES ($page_id, '$user_level')";

            if($db->sql_query($sql)){
                redirect(append_sid('pengaturan_page.'.$phpEx.'?mode=assign&pesan=1',true));
            }else{
                die('Error query database');
            }
        }
    }
    else if($mode=='page'){
        $pesan = $HTTP_GET_VARS['pesan'];

        if($pesan==1){
            $pesan="<font color='green' size=3>Data Berhasil Ditambahkan!</font>";
            $bgcolor_pesan="98e46f";
        }

        $template->set_filenames(array('body' => 'page/add_page_body.tpl'));
        $template->assign_vars(array(
                'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=assign").'">Tambah Data Page</a> ',
                'JUDUL'		=>'Tambah Page Data',
                'MODE'   	=> 'PostPage',
                'PESAN'						=> $pesan,
                'BGCOLOR_PESAN'		=> $bgcolor_pesan,
                'PAGE_ADD'	=> append_sid('pengaturan_page.'.$phpEx)
            )
        );
    }
    elseif($mode=='PostPage'){
        $nama   					= $HTTP_POST_VARS['nama_page'];
        $page   					= $HTTP_POST_VARS['page_id'];

        $terjadi_error=false;

        $sql = "SELECT *
                FROM tbl_page
                WHERE page_id=$page";

        if($db->sql_numrows($sql) > 0){
            $pesan="<font color='white' size=3>Page yang dimasukkan sudah terdaftar dalam sistem!</font>";
            $bgcolor_pesan="red";
            $terjadi_error=true;
        }
        else{
            if($submode=='page'){
                $judul="Ubah Data Page";
                $path	='<a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=page").'">Ubah Data Page</a> ';

                $sql =
                    "UPDATE tbl_page
                    SET
                        page_id='$page',
                        nama_page='$nama'
                    WHERE page_id=$page;";

                if($db->sql_query($sql)){
                    redirect(append_sid('pengaturan_page.'.$phpEx.'?mode=edit&submode=page&id='.$page.'&pesan=1',true));
                }

            }else{
                $judul="Tambah Data Page";
                $path	='<a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=page").'">Tambah Data Page</a> ';

                $sql = "INSERT INTO tbl_page (page_id, nama_page)
                            VALUES ($page, '$nama')";

                if($db->sql_query($sql)){
                    redirect(append_sid('pengaturan_page.'.$phpEx.'?mode=page&pesan=1',true));
                }
            }

        }
    }elseif ($mode=='edit'){
        $pesan = $HTTP_GET_VARS['pesan'];

        if($pesan=='1'){
            $pesan="<font color='green' size=3>Data Berhasil Ditambahkan!</font>";
            $bgcolor_pesan="98e46f";
        }
        // edit

        $id = $HTTP_GET_VARS['id'];

        if($submode == 'page'){
            $id = $HTTP_GET_VARS['id'];

            $sql = "SELECT page_id,nama_page
                FROM tbl_page
                WHERE page_id=$id";

            $result = $db->sql_query($sql);
            $row=$db->sql_fetchrow($result);

            $template->set_filenames(array('body' => 'page/add_page_body.tpl'));
            $template->assign_vars(array(
                    'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx.'?mode=detail_page').'">Detail Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=edit&id=$id").'">Ubah Data Page</a> ',
                    'JUDUL'		=>'Ubah Data Page',
                    'MODE'   	=> 'PostPage',
                    'SUB'    	=> 'page',
                    'PESAN'						=> $pesan,
                    'PAGE_ID' 	=> $row['page_id'],
                    'NAMA_PAGE'    	=> $row['nama_page'],
                    'PAGE_ADD'	=> append_sid('pengaturan_page.'.$phpEx)
                )
            );
        }else{
            $sql = "SELECT page_id,GROUP_CONCAT(tbl_permission.user_level) AS access
                FROM tbl_permission
                WHERE page_id=$id
                GROUP BY page_id";

            $result = $db->sql_query($sql);
            $row=$db->sql_fetchrow($result);

            $template->set_filenames(array('body' => 'page/assign_body.tpl'));

            $explode = explode(',',$row['access']);

            $SelectOption = "";
            foreach($explode as $key => $value) {
                $SelectOption .= "
                    <select id='opt_user_level' name='opt_user_level[]'>".
                    setComboUserLevel($value)
                    ."</select><br/>
				";

            }

            $template->assign_vars(array(
                    'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=edit&id=$id").'">Ubah Data Permission</a> ',
                    'JUDUL'		=>'Ubah Data Permission',
                    'MODE'   	=> 'PostAssign',
                    'SUB'       => 'permission',
                    'PAGE_ID'   => setComboPage($row['page_id']),
                    'OPT_USER_LEVEL' => $SelectOption,
                    'U_ASSIGN_USER_ACT'=>append_sid('pengaturan_page.'.$phpEx)
                )
            );
        }
    }elseif ($mode=='delete'){
        global $db;
        $list_page = str_replace("\'","'",$HTTP_GET_VARS['list_page']);

        if($HTTP_GET_VARS['page'] == 'detail'){
            $sql =
                "DELETE FROM tbl_page
                WHERE page_id IN($list_page);";
        }else{
            $sql =
                "DELETE FROM tbl_permission
                WHERE page_id IN($list_page);";
        }

        if (!$db->sql_query($sql)){
            die_error("Err: $this->ID_FILE".__LINE__);
        }

        exit;
    }
    elseif($mode=='detail_page') {
        if ($submode=='delete'){
            global $db;
            $list_page = str_replace("\'","'",$HTTP_GET_VARS['list_page']);

            $sql =
                "DELETE FROM tbl_page
        WHERE page_id IN($list_page);";

            if (!$db->sql_query($sql)){
                die_error("Err: $this->ID_FILE".__LINE__);
            }

            exit;
        }
        // LIST
        $template->set_filenames(array('body' => 'page/detail_page_body.tpl'));

        if($HTTP_POST_VARS["txt_cari"]!=""){
            $cari=$HTTP_POST_VARS["txt_cari"];
        }
        else{
            $cari=$HTTP_GET_VARS["cari"];
        }

        $temp_cari=str_replace("nama_page=","",$cari);
        if($temp_cari==$cari){
            $kondisi_cabang =
                "nama_page LIKE '%$cari%'
                OR page_id LIKE '%$cari%'";
        }
        else{
            $kondisi_cabang = "nama_page LIKE '%$temp_cari%' ";
            $cari=$temp_cari;
        }

        $kondisi	=($cari=="")?"WHERE 1":
            " WHERE  $kondisi_cabang";

        $kondisi_user	= $userdata['user_level']!=$USER_LEVEL_INDEX['ADMIN']?" AND user_level!='".$USER_LEVEL_INDEX['ADMIN']."'":"";

        $sort_by	= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
        $order		= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

        $order	=($order=='')?"ASC":$order;

        $sort_by =($sort_by=='')?"page_id":$sort_by;

        //PAGING======================================================
        $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
        $paging		= pagingData($idx_page,"page_id","tbl_page","&cari=$cari&sort_by=$sort_by&order=$order",$kondisi.$kondisi_user,"pengaturan_page.php?mode=detail_page",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
        //END PAGING======================================================

        $sql ="SELECT page_id,nama_page
                FROM tbl_page
                ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

        $idx_check=0;

        if ($result = $db->sql_query($sql)){
            $i = $idx_page*$VIEW_PER_PAGE+1;
            while ($row = $db->sql_fetchrow($result)){
                $odd ='odd';

                if (($i % 2)==0){
                    $odd = 'even';
                }

                $idx_check++;

                $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";

                $act 	="<a href='".append_sid('pengaturan_page.'.$phpEx.'?mode=edit&submode=page&id='.$row[0])."'>Edit</a> + ";
                $act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
                $template->
                assign_block_vars(
                    'ROW',
                    array(
                        'odd'=>$odd,
                        'check'=>$check,
                        'no'=>$i,
                        'page_id'=>$row['page_id'],
                        'nama_page'=>$row['nama_page'],
                        'action'=>$act
                    )
                );

                $i++;
            }
        }
        else{
            echo("Error:".__LINE__);exit;
        }

        $page_title	= "Pengaturan Page";

        //paramter sorting
        $order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
        $parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

        $template->assign_vars(array(
                'BCRUMP'    		=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx.'?mode=detail_page').'">Detail Page</a>',
                'U_PAGE'		=> append_sid('pengaturan_page.'.$phpEx.'?mode=detail_page'),
                'U_USER_ADD'		=> append_sid('pengaturan_page.'.$phpEx.'?mode=page'),
                'ACTION_CARI'		=> append_sid('pengaturan_page.'.$phpEx),
                'TXT_CARI'			=> $cari,
                'PAGING'				=> $paging,
                'A_SORT_1'			=> append_sid('pengaturan_page.'.$phpEx.'?sort_by=page_id'.$parameter_sorting),
                'A_SORT_2'			=> append_sid('pengaturan_page.'.$phpEx.'?sort_by=nama_page'.$parameter_sorting)
            )
        );

    }elseif ($mode=='edit'){
        // edit

        $id = $HTTP_GET_VARS['id'];

        $sql = "SELECT page_id,nama_page
            FROM tbl_permission
            WHERE page_id=$id";

        $result = $db->sql_query($sql);
        $row=$db->sql_fetchrow($result);

        $template->set_filenames(array('body' => 'page/add_page_body.tpl'));

        $SelectOption = "";
        foreach($explode as $key => $value) {
            $SelectOption .= "
                    <select id='opt_user_level' name='opt_user_level'>".
                        setComboUserLevel($value)
                    ."</select><br/>
				";

        }

        $template->assign_vars(array(
                'BCRUMP'		=>'<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan').'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx."?mode=edit&id=$id").'">Ubah Data Page</a> ',
                'JUDUL'		=>'Ubah Data Page',
                'MODE'   	=> 'PostPage',
                'SUB'       => '1',
                'PAGE_ID'   => setComboPage($row['page_id']),
                'OPT_USER_LEVEL' => setComboUserLevel($row['access']),
                'U_USER_ADD_ACT'=>append_sid('pengaturan_page.'.$phpEx)
            )
        );

    }elseif($mode=='PostPage'){
        $nama   					= $HTTP_POST_VARS['nama_page'];
        $page   					= $HTTP_POST_VARS['page_id'];

        $terjadi_error=false;

        $sql = "SELECT *
            FROM tbl_page
            WHERE page_id=$page";

        if($db->sql_numrows($sql) > 0){
            $pesan="<font color='white' size=3>Page yang dimasukkan sudah terdaftar dalam sistem!</font>";
            $bgcolor_pesan="red";
            $terjadi_error=true;
        }
    }
    else {
        // LIST
        $template->set_filenames(array('body' => 'page/page_body.tpl'));

        if($HTTP_POST_VARS["txt_cari"]!=""){
            $cari=$HTTP_POST_VARS["txt_cari"];
        }
        else{
            $cari=$HTTP_GET_VARS["cari"];
        }

        $temp_cari=str_replace("nama_page=","",$cari);
        if($temp_cari==$cari){
            $kondisi_cabang =
                "nrp nama_page '%$cari%'
                    OR page_id LIKE '%$cari%'";
        }
        else{
            $kondisi_cabang = "nama_page LIKE '%$temp_cari%' ";
            $cari=$temp_cari;
        }

        $kondisi	=($cari=="")?"WHERE 1":
            " WHERE  $kondisi_cabang";

        $kondisi_user	= $userdata['user_level']!=$USER_LEVEL_INDEX['ADMIN']?" AND user_level!='".$USER_LEVEL_INDEX['ADMIN']."'":"";

        $sort_by	= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
        $order		= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

        $order	=($order=='')?"ASC":$order;

        $sort_by =($sort_by=='')?"page_id":$sort_by;

        //PAGING======================================================
        $idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
        $paging		= pagingData($idx_page,"page_id","tbl_permission","&cari=$cari&sort_by=$sort_by&order=$order",$kondisi.$kondisi_user,"pengaturan_page.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
        //END PAGING======================================================

        $sql ="SELECT tbl_permission.page_id,tbl_page.nama_page,GROUP_CONCAT(tbl_permission.user_level) AS access
                    FROM tbl_permission
                    JOIN tbl_page ON tbl_permission.page_id=tbl_page.page_id
                    GROUP BY tbl_permission.page_id
                    ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE";

        $idx_check=0;

        if ($result = $db->sql_query($sql)){
            $i = $idx_page*$VIEW_PER_PAGE+1;
            while ($row = $db->sql_fetchrow($result)){
                $odd ='odd';

                if (($i % 2)==0){
                    $odd = 'even';
                }

                $idx_check++;

                $check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";

                $act 	="<a href='".append_sid('pengaturan_page.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
                $act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
                $template->
                assign_block_vars(
                    'ROW',
                    array(
                        'odd'=>$odd,
                        'check'=>$check,
                        'no'=>$i,
                        'page_id'=>$row['page_id'],
                        'nama_page'=>$row['nama_page'],
                        'level'=>level($row['access']),
                        'action'=>$act
                    )
                );

                $i++;
            }
        }
        else{
            echo("Error:".__LINE__);exit;
        }

        $page_title	= "Pengaturan Page";

        //paramter sorting
        $order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
        $parameter_sorting	= "&page=$idx_page&cari=$cari&order=$order_invert";

        $template->assign_vars(array(
                'BCRUMP'    		=> '<a href="'.append_sid('menu_pengaturan.'.$phpEx.'?top_menu_dipilih=top_menu_pengaturan') .'">Home</a> | <a href="'.append_sid('pengaturan_page.'.$phpEx).'">Page</a>',
                'U_PAGE'		=> append_sid('pengaturan_page.'.$phpEx.'?mode=detail_page'),
                'U_USER_ADD'		=> append_sid('pengaturan_page.'.$phpEx.'?mode=assign'),
                'ACTION_CARI'		=> append_sid('pengaturan_page.'.$phpEx),
                'TXT_CARI'			=> $cari,
                'PAGING'				=> $paging,
                'A_SORT_1'			=> append_sid('pengaturan_page.'.$phpEx.'?sort_by=page_id'.$parameter_sorting),
                'A_SORT_2'			=> append_sid('pengaturan_page.'.$phpEx.'?sort_by=nama_page'.$parameter_sorting),
                'A_SORT_3'			=> append_sid('pengaturan_page.'.$phpEx.'?sort_by=access'.$parameter_sorting)
            )
        );

    }

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');