<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern

if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true);
	exit;
}

//#############################################################################

class BeritaAcaraBNOP{

	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function BeritaAcaraBNOP(){
		$this->ID_FILE="C-BABNOP";
	}
	
	//BODY
	
	function generateKodeBA(){
		global $db;
		global $userdata;
		
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		
		
		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$kode_ba	= "BOP".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s.$rnd3;
		
			$sql = "SELECT COUNT(1) FROM tbl_ba_bnop WHERE KodeBA='$kode_ba'";
						
			if (!$result = $db->sql_query($sql)){
				echo("Err: $this->ID_FILE ".__LINE__);exit;
			}
			
			$row=$db->sql_fetchrow($result);
		
		}while($row[0]>0);
			
		
		
		return $kode_ba;
	}
	
	function tambah(
		$KodeBA,$JenisBiaya,$Jumlah,
		$Penerima,$Keterangan){

		//kamus
		global $db;
		global $userdata;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"INSERT INTO tbl_ba_bnop(
				KodeBA,WaktuTransaksi,JenisBiaya,
				Jumlah,NamaPenerima,
				Keterangan,IdPembuat,NamaPembuat,
				IsRelease)
			VALUES(
				'$KodeBA',NOW(),'$JenisBiaya',
				'$Jumlah','$Penerima',
				'$Keterangan','$userdata[user_id]','$userdata[nama]',
				0);";

		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}
	
	function ubah(
		$IdBA, $Penerima,$JenisBiaya,$Jumlah,
		$Keterangan){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"UPDATE tbl_ba_bnop
			SET
				NamaPenerima='$Penerima',
				JenisBiaya='$JenisBiaya',
				Jumlah='$Jumlah',
				Keterangan='$Keterangan'
				WHERE Id='$IdBA'";

		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}
	
	function hapus($IdBA){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"DELETE FROM tbl_ba_bop
			WHERE Id='$IdBA'";
								
		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}
	
	function bayar(
		$IdBA, $IdReleaser, $NamaReleaser){
	  
		//kamus
		global $db;
		
		$expired_time = 120; //menit
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"UPDATE tbl_ba_bnop
			SET
				IdReleaser='$IdReleaser',
				NamaReleaser='$NamaReleaser',
				WaktuRelease=NOW(),
				IsRelease=1
			WHERE Id='$IdBA'
				AND TIME_TO_SEC(TIMEDIFF(WaktuTransaksi,NOW()))/60>=-$expired_time";

		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}
	
	function ambilDetail($IdBA){

		//kamus
		global $db;
		
		//MENGAMBIL DATA
		$sql = 
			"SELECT * FROM tbl_ba_bnop WHERE Id='$IdBA'";
								
		if (!$result=$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		$row	= $db->sql_fetchrow($result);
		
		return $row;
	}
	
	function ambilDetailSPJ($NoSPJ){
	  
		//kamus
		global $db;
		
		//MENGAMBIL DATA
		$sql = 
			"SELECT * FROM tbl_spj WHERE KodeBA='$NoSPJ'";
								
		if (!$result=$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		$row	= $db->sql_fetchrow($result);
		
		return $row;
	}
	
}
?>