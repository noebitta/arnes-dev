<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] )
{  
  redirect(append_sid('index.'.$phpEx),true); 
}

// PARAMETER
$mode    = $HTTP_GET_VARS['mode'];
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$mode = ($mode != '') ? $mode : 'load_awal';

switch($mode){
	
case 'load_awal':
	//menampilkan data pelanggan yang ditemukan berdasarkan no telepon

	$no_telp   = $HTTP_GET_VARS['no_telp'];
		
	$sql = "SELECT DISTINCT TOP 10 nama0,alamat0,telp0,hp0
					FROM	tbreservasi 
					WHERE (telp0='$no_telp' AND telp0<>'-') OR (hp0='$no_telp' AND hp0<>'-')";
	
	if (!$result = $db->sql_query($sql)){
		//die_error('Cannot LOAD DATA',__FILE__,__LINE__,$sql);
		die_error('Cannot LOAD DATA');
	}
	else {
		$i = 0;
		
		echo("
			<table width='100%' class='border'>
				<tr>
					<th>#</th><th>Nama</th><th>Alamat</th><th>Telp</th><th>HP</th>
				</tr>");
	
		while ($row=$db->sql_fetchrow($result)){ 
			$i++;
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			echo("
				<tr bgcolor='D0D0D0'>
	       <td class=$odd>$i</td>
	       <td class=$odd><a href='#' onClick=\"PilihPelanggan('$row[nama0]','$row[alamat0]','$row[telp0]','$row[hp0]');\">$row[nama0]</a></td>
	       <td class=$odd>$row[alamat0]</td>
	       <td class=$odd>$row[telp0]</td>
	       <td class=$odd>$row[hp0]</td>
	     </tr>");
		 
		}
		
		echo("</table>");
	}
	
	if($i<=0) echo("<font color='ffffff'>Data pelanggan dengan no telepon $no_telp tidak ditemukan!</font>");
	
exit;

case 'jadwal_pelanggan':
	//menampilkan data pelanggan yang ditemukan berdasarkan no telepon

	$no_telp   = $HTTP_GET_VARS['no_telp'];
		
	$sql = "SELECT DISTINCT nama0,alamat0,CONVERT(varchar(10),TGLBerangkat,105) as TGLBerangkat,asal,tujuan,CONVERT(varchar(5),jamberangkat,114) as jamberangkat
					FROM	tbreservasi 
					WHERE telp0='$no_telp' OR hp0='$no_telp' or NoUrut LIKE '%$no_telp' and nospj=''";
	
	if (!$result = $db->sql_query($sql)){
		die_error('Cannot LOAD DATA',__FILE__,__LINE__,$sql);
		//die_error('Cannot LOAD DATA');
	}
	else {
		$i = 0;
		
		echo("
			<table width='100%' class='border'>
				<tr>
					<th>#</th><th>Nama</th><th>Alamat</th><th>Tgl.Berangkat</th><th>asal</th><th>tujuan</th><th>jam Berangkat</th>
				</tr>");
	
		while ($row=$db->sql_fetchrow($result)){ 
			$i++;
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			echo("
				<tr bgcolor='D0D0D0'>
	       <td class=$odd>$i</td>
	       <td class=$odd>$row[nama0]</td>
	       <td class=$odd>$row[alamat0]</td>
	       <td class=$odd>$row[TGLBerangkat]</td>
	       <td class=$odd>$row[asal]</td>
				 <td class=$odd>$row[tujuan]</td>
				 <td class=$odd>$row[jamberangkat]</td>
	     </tr>");
		 
		}
		
		echo("</table>");
	}
	
	if($i<=0) echo("<font color='ffffff'>Pelanggan dengan no telepon/tiket $no_telp tidak memiliki jadwal keberangkatan!</font>");
	
exit;


} //switch mode
?>