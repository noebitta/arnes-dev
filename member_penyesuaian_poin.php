<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']!=$LEVEL_ADMIN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;

$data_member;
$body		= "";
$valid	= true;

$TransaksiMember= new TransaksiMember();
$Member					= new Member();

switch ($mode){
	case 'blank':
		
		$id_member	= $HTTP_GET_VARS['id_member'];
		
		$body=
			"
			<table width='80%'>
				<tr>
					<td colspan=3 align='center'>Silahkan masukkan jumlah poin yang sudah disesuaikan</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input' name='frm_input' action=\"".append_sid('member_penyesuaian_poin.'.$phpEx)."&mode=konfirmasi_proses\" method='post'>
							<input type='hidden' id='hdn_id_member' name='hdn_id_member' value='$id_member' />
							<input type='text' id='jumlah_poin' name='jumlah_poin' /> <input type='submit' value='&nbsp;&nbsp;UBAH&nbsp;&nbsp;' />
						</form>
					</td>
				</tr>
			</table>
			<input type='button' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;' onClick='window.close();'/>";
		
	break;
	
	case 'konfirmasi_proses':
		
		$id_member		= $HTTP_POST_VARS['hdn_id_member'];
		$jumlah_poin	= $HTTP_POST_VARS['jumlah_poin'];
		
		$data_member=$Member->ambilDataDetail($id_member);
		
		$body=
			"
			<table width='80%'>
				<tr>
					<td colspan=3 align='left'><h3>Data yang anda masukkan adalah</h3></td>
				</tr>
				<tr>
					<td>ID Member</td><td>:</td><td>$data_member[id_member]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
				<tr>
					<td>poin sekarang</td><td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
				</tr>
				<tr>
					<td>poin Koreksi</td><td>:</td><td><strong>".number_format($jumlah_poin,0,",",".")."</strong></td>
				</tr>
			</table>
			<input type='button' value='&nbsp;&nbsp;Lakukan proses koreksi&nbsp;&nbsp;' onClick='konfirmPassword();'/>";
			
			$parameter="
				<input type='hidden' id='id_member' name='id_member' value='$id_member' />
				<input type='hidden' id='jumlah_poin' name='jumlah_poin' value='$jumlah_poin' />";
			
	break;
	
	//TRANSAKSI  =============================================================================================
	case 'proses_transaksi':
				
		$id_member		= trim($HTTP_POST_VARS['id_member']);		
		$jumlah_poin	= trim($HTTP_POST_VARS['jumlah_poin']);		
		$password			= trim($HTTP_POST_VARS['password']);		
		
		$useraktif	= $userdata['username'];
		
		//memeriksa otorisasi password
		$sql = 
			"SELECT user_password FROM tbCustomer WHERE username='$useraktif';";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			$password_user=$row['user_password'];
		} 
		else{
			die_error("Gagal melakukan otorisasi password ");
		}
		
		if($password_user==md5($password)){// MEMERIKSA OTORISASI USER
			//JIKA PASSWORD BENAR
			
			// koreksi deposit
			$return=$TransaksiMember->koreksiPoint($id_member,$jumlah_poin);
			if($return!="false") {
				$body	=	
					"<h2>KOREKSI POIN BERHASIL DILAKUKAN!</h2>
					<a href='javascript:window.close()'>Tutup window</a>";
			}
			else{
				$body	=	
					"<h2><font color='red'>Gagal melakukan proses koreksi $return $id_member</font></h2><br>
					<a href='javascript:history.back()'>Kembali</a><br>";
			}
		}
		else{
			$body	=	
				"<h2><font color='red'>Password anda tidak benar!</font></h2><br>
				<a href='javascript:history.back()'>Kembali</a><br>";
		}
		
		
		
		

	break;
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_penyesuaian_poin.tpl')); 
$template->assign_vars (
	array(
		'BODY'			=> $body,
		'PARAMETER'	=> $parameter
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>