<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

//$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tgl_awal_mysql  		= FormatTglToMySQLDate($HTTP_GET_VARS['tgl_awal']);
$tgl_akhir_mysql  		= FormatTglToMySQLDate($HTTP_GET_VARS['tgl_akhir']);
$cari  				= $HTTP_GET_VARS['cari'];
$cabang_tujuan			= $HTTP_GET_VARS['opt_tujuan'];
$status				= $HTTP_GET_VARS['status'];

//PARAMETER KONDISI
$tbl_pencarian  		= "tbl_paket";
$kondisi			= " WHERE (TglBerangkat BETWEEN '$tgl_awal_mysql' AND '$tgl_akhir_mysql') AND FlagBatal!=1 AND IdJurusan AND CetakTiket=1  AND StatusDiambil=0";

//Jika ada parameter pencarian
$kondisi        	       .= ($cari=="")?"":" AND (NoTiket LIKE '%$cari%' OR NamaPengirim LIKE '%$cari%' OR AlamatPengirim LIKE '%$cari%' OR TelpPengirim LIKE '%$cari%' OR NamaPenerima LIKE '%$cari%' OR AlamatPenerima LIKE '%$cari%' OR TelpPenerima LIKE '%$cari%' OR KodeJadwal LIKE '$cari%')";

//Jika ada parameter cabang tujuan paket
if($cabang_tujuan!=""){
        $kondisi               .= " AND f_jurusan_get_kode_cabang_tujuan_by_jurusan(IdJurusan) LIKE '$cabang_tujuan' ";
}

//Query paket belum diambil
$sql 				= 
				  "SELECT *
               			   FROM $tbl_pencarian
                                   $kondisi 
                                   ORDER BY TglBerangkat,JamBerangkat,KodeJadwal";

 
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//Isi array temp laporan
$temp_array=array();
$idx=0;

while ($row = $db->sql_fetchrow($result)){

	$temp_array[$idx]['NoTiket']		= $row['NoTiket'];
	$temp_array[$idx]['KodeCabang']		= $row['KodeCabang'];
	$temp_array[$idx]['KodeJadwal']		= $row['KodeJadwal'];
	$temp_array[$idx]['JenisLayanan']	= $row['JenisLayanan'];
	$temp_array[$idx]['TglBerangkat']	= $row['TglBerangkat'];
	$temp_array[$idx]['JamBerangkat']	= $row['JamBerangkat'];
        $temp_array[$idx]['NamaPengirim']       = $row['NamaPengirim'];
        $temp_array[$idx]['AlamatPengirim']     = $row['AlamatPengirim'];
        $temp_array[$idx]['TelpPengirim']       = $row['TelpPengirim'];
        $temp_array[$idx]['WaktuPesan']         = $row['WaktuPesan'];
        $temp_array[$idx]['NamaPenerima']       = $row['NamaPenerima'];
        $temp_array[$idx]['AlamatPenerima']     = $row['AlamatPenerima'];
        $temp_array[$idx]['TelpPenerima']       = $row['TelpPenerima'];
        $temp_array[$idx]['HargaPaket']         = $row['HargaPaket'];
	$temp_array[$idx]['Diskon']             = $row['Diskon'];
        $temp_array[$idx]['TotalBayar']         = $row['TotalBayar'];
        $temp_array[$idx]['Dimensi']            = $row['Dimensi'];
        $temp_array[$idx]['JumlahKoli']         = $row['JumlahKoli'];
        $temp_array[$idx]['Berat']              = $row['Berat'];
        $temp_array[$idx]['JenisPembayaran']    = $row['JenisPembayaran']==0?"TUNAI":"LANGGANAN";
     
	$idx++;
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Paket Belum Diambil');
$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
$objPHPExcel->getActiveSheet()->mergeCells('A4:B4');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
$objPHPExcel->getActiveSheet()->mergeCells('A5:B5');
$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Cabang Penerima');
$objPHPExcel->getActiveSheet()->mergeCells('A6:B6');
$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Keyword Pencarian');
//Data header
$objPHPExcel->getActiveSheet()->setCellValue('C3', $tgl_awal_mysql);
$objPHPExcel->getActiveSheet()->setCellValue('C4', $tgl_akhir_mysql);
$objPHPExcel->getActiveSheet()->setCellValue('C5', $cabang_tujuan!=""?$cabang_tujuan:"ALL");
$objPHPExcel->getActiveSheet()->setCellValue('C6', $cari==""?"-":$cari);
//Header tabel
$objPHPExcel->getActiveSheet()->mergeCells('A8:A9');
$objPHPExcel->getActiveSheet()->setCellValue('A8', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('B8:B9');
$objPHPExcel->getActiveSheet()->setCellValue('B8', 'Resi');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('C8:C9');
$objPHPExcel->getActiveSheet()->setCellValue('C8', 'Cabang Asal');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('D8:D9');
$objPHPExcel->getActiveSheet()->setCellValue('D8', 'Jadwal');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('E8:E9');
$objPHPExcel->getActiveSheet()->setCellValue('E8', 'Waktu Berangkat');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('F8:F9');
$objPHPExcel->getActiveSheet()->setCellValue('F8', 'Layanan');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('G8:G9');
$objPHPExcel->getActiveSheet()->setCellValue('G8', 'Jenis Bayar');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('H8:J8');
$objPHPExcel->getActiveSheet()->setCellValue('H8', 'Pengirim');
$objPHPExcel->getActiveSheet()->setCellValue('H9', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('I9', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('J9', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('K8:M8');
$objPHPExcel->getActiveSheet()->setCellValue('K8', 'Penerima');
$objPHPExcel->getActiveSheet()->setCellValue('K9', 'Nama');
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('L9', 'Alamat');
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('M9', 'Telp');
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('N8:N9');
$objPHPExcel->getActiveSheet()->setCellValue('N8', 'Berat');
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('O8:O9');
$objPHPExcel->getActiveSheet()->setCellValue('O8', 'Koli');
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('P8:P9');
$objPHPExcel->getActiveSheet()->setCellValue('P8', 'Dimensi');
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('Q8:Q9');
$objPHPExcel->getActiveSheet()->setCellValue('Q8', 'Harga');
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('R8:R9');
$objPHPExcel->getActiveSheet()->setCellValue('R8', 'Diskon');
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('S8:S9');
$objPHPExcel->getActiveSheet()->setCellValue('S8', 'Berat');
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);

$idx=0;
$idx_row=10;

while ($idx<count($temp_array)){
	
		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['NoTiket']);
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['KodeCabang']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['KodeJadwal']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['TglBerangkat'].' '.$temp_array[$idx]['JamBerangkat']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['JenisLayanan']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['JenisPembayaran']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $temp_array[$idx]['NamaPengirim']);
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$idx_row, $temp_array[$idx]['AlamatPengirim']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$idx_row, $temp_array[$idx]['TelpPengirim'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$idx_row, $temp_array[$idx]['NamaPenerima']);
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$idx_row, $temp_array[$idx]['AlamatPenerima']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$idx_row, $temp_array[$idx]['TelpPenerima'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('N'.$idx_row, $temp_array[$idx]['Berat']);
        $objPHPExcel->getActiveSheet()->setCellValue('O'.$idx_row, $temp_array[$idx]['JumlahKoli']);
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$idx_row, $temp_array[$idx]['Dimensi']);
        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$idx_row, $temp_array[$idx]['HargaPaket']);
        $objPHPExcel->getActiveSheet()->setCellValue('R'.$idx_row, $temp_array[$idx]['Diskon']);
        $objPHPExcel->getActiveSheet()->setCellValue('S'.$idx_row, $temp_array[$idx]['TotalBayar']);
       	
	$idx_row++;
	$idx++;
}
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Paket Belum Diambil '.$tgl_awal_mysql.' sd '.$tgl_akhir_mysql.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
