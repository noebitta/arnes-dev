<?php
//
// LAPORAN
//
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMember.php');
include($adp_root_path . 'ClassMemberTransaksi.php');
include($adp_root_path . 'ClassMemberPromo.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode	= $HTTP_GET_VARS['mode'];
$mode	=($mode=='')?"blank":$mode;

$data_member;
$body		= "";
$valid	= true;

$TransaksiMember= new TransaksiMember();
$Member					= new Member();
$Promo					= new Promo();

$tgl_berangkat	="";
$kode_jadwal		="";
$layout_kursi		=0;
$id_kartu				="";
$id_member			="";
$jam_berangkat			="";
$masterkota 				="";
$asal 							="";
$tujuan 						="";
$nopol 							="";
$merek 							="";
$namasupir 					="";
$jum_kursi_tersedia =0;
$biayasopir 				=0;
$harga_tiket				=0;
$discount						=0;
$jumlah_point				=0;
$total_transaksi		=0;

switch ($mode){
	case 'blank':
		
		$kode_jadwal	= $HTTP_GET_VARS['kode_jadwal'];
		$tgl_berangkat= FormatMySQLDateToTgl($HTTP_GET_VARS['tgl_berangkat']);
		$layout_kursi	= $HTTP_GET_VARS['layout_kursi'];
		$list_tiket		= trim($HTTP_GET_VARS['list_tiket']);
		
		$list_tiket=str_replace("\'","'",$list_tiket);
		
		$body=
			"
			<table width='80%'>
				<tr>
					<td colspan=3 align='center'>Silahkan gesekkan kartu member ke alat pembaca</td>
				</tr>
				<tr>
					<td colspan=3 align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action=\"".append_sid('member_transaksi.'.$phpEx)."&mode=konfirmasi_transaksi&sub_mode=0&tgl_berangkat=$tgl_berangkat&kode_jadwal=$kode_jadwal&layout_kursi=$layout_kursi&list_tiket=$list_tiket\" method='post'>
							<input type='hidden' id='hdn_id_member' name='hdn_id_member' value='$id_member' />
							<input type='password' id='id_kartu' name='id_kartu' />
						</form>
					</td>
				</tr>
				<tr>
					<td colspan='3' align='center'> ATAU <BR> Silahkan masukkan nomor seri kartu member lalu kemudian tekan enter pada kolom isian dibawah ini</td>
				</tr>
				<tr>
					<td colspan='3' align='center'>
						<form id='frm_input_kartu' name='frm_input_kartu' action=\"".append_sid('member_transaksi.'.$phpEx)."&mode=konfirmasi_transaksi&sub_mode=1&tgl_berangkat=$tgl_berangkat&kode_jadwal=$kode_jadwal&layout_kursi=$layout_kursi&list_tiket=$list_tiket\" method='post'>
							<input type='text' id='no_seri_kartu' name='no_seri_kartu' />
						</form>
					</td>
				</tr>
			</table>
			<input type='button' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;' onClick='window.close();'/>&nbsp;&nbsp;";
		
	break;
	
	//MEMILIH TIKET YANG AKAN DIBAYAR==========================================================================
	case 'pilih_bayar':
		
		$kode_book		= trim($HTTP_GET_VARS['kode_book']);		
		$tgl_berangkat= trim($HTTP_GET_VARS['tgl_berangkat']);		
		$kode_jadwal	= trim($HTTP_GET_VARS['kode_jadwal']);	
		$layout_kursi	= $HTTP_GET_VARS['layout_kursi'];
		
		$sql = 
			"SELECT 
				NoUrut,Nama0,Alamat0,Telp0,HP0,HargaTiket,TOTAL,NomorKursi
			FROM TbReservasi
			WHERE kode0='$kode_book' 
			AND CetakTiket=0
			ORDER BY NomorKursi";
				
		if (!$result = $db->sql_query($sql)){
			die_error('Cannot Load data',__LINE__,__FILE__,$sql);
		} 
		
		$body=
			"
			<input type='hidden' id='tgl_berangkat' value='$tgl_berangkat' />
			<input type='hidden' id='kode_jadwal' value='$kode_jadwal' />
			<input type='hidden' id='layout_kursi' value='$layout_kursi' />
			<table>
				<tr bgcolor='D0D0D0'>
					<td width='2%' align='center'>&nbsp;</td>
					<td width='3%' align='center'>No Kursi</td>
					<td width='10%' align='center'>No.Tiket</td>
					<td width='15%' align='center'>Nama</td>
					<td width='15%' align='center'>Alamat</td>
					<td width='10%' align='center'>Telp.</td>
					<td width='10%' align='center'>HP</td>
					<td width='10%' align='center'>Harga Tiket</td>
					<td width='10%' align='center'>Total</td>
				</tr>
			";
		$i=1;
		while ($row = $db->sql_fetchrow($result)){
			
			$odd ='odd';
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$body	.=
					"<tr class='$odd'>
						<td>
							<input type='checkbox' checked id='chk_tiket_$i' value=\"'$row[NoUrut]'\"	></input>
						</td>
						<td>
							$row[NomorKursi]
						</td>
						<td>
							$row[NoUrut]
						</td>
						<td>
							$row[Nama0]
						</td>
						<td>
							$row[Alamat0]
						</td>
						<td>
							$row[Telp0]
						</td>
						<td>
							$row[HP0]
						</td>
						<td align='right'>".
							number_format($row[HargaTiket],0,",",".")
						."</td>
						<td align='right'>".
							number_format($row[TOTAL],0,",",".")
						."</td>
					</tr>";
			$i++;
		}
		
		$body .="
			</table>
			<input type='button' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;' onClick='window.close();'/>&nbsp;&nbsp;
			<input type='button' onClick='bayarTiketDipilih();' value='Lakukan Autodebit' />";
	break;
	
	//KONFIRMASI TRANSAKSI  =============================================================================================
	case 'konfirmasi_transaksi':
		
		$sub_mode			= $HTTP_GET_VARS['sub_mode'];		
		$id_member		= trim($HTTP_GET_VARS['hdn_id_member']);		
		$id_kartu			= md5(trim($HTTP_POST_VARS['id_kartu']));		
		$no_seri_kartu= trim($HTTP_POST_VARS['no_seri_kartu']);		
		$tgl_berangkat= trim($HTTP_GET_VARS['tgl_berangkat']);		
		$kode_jadwal	= trim($HTTP_GET_VARS['kode_jadwal']);		
		$layout_kursi	= trim($HTTP_GET_VARS['layout_kursi']);
		$list_tiket		= trim($HTTP_GET_VARS['list_tiket']);
		$list_tiket=str_replace("\'","'",$list_tiket);
		
		$useraktif	= $userdata['username'];

		if($sub_mode==0){
			//jika pencarian data dilakukan berdasarkan ID KARTU
			$data_member=$Member->ambilDataByIdKartu($id_kartu);
		}
		else{
			//jika pencarian data dilakukan berdasarkan NO SERI KARTU
			$data_member=$Member->ambilDataByNoSeriKartu($no_seri_kartu);
			$id_kartu=$data_member['id_kartu'];
		}
		//die_error($no_seri_kartu);
		if($data_member['id_member']==''){ //MEMERIKSA APAKAH ID KARTU TERSEBUT DITEMUKAN DI DATABASE ATAU TIDAK
			//JIKA DATA TIDAK DITEMUKAN
			$body = 
				"<h2><font color='red'>KARTU TIDAK TERDAFTAR</font></h2>
				<a href='#' onClick='history.back();'><< kembali ke proses sebelumnya</a>";
		}		
		else if(!$data_member['status_member']){ //MEMERIKSA STATUS AKTIF MEMBER
			//JIKA MEMBER TIDAK AKTIF
			$body = "<h2><font color='red'>STATUS MEMBER SEDANG DIBLOKIR</font></h2>";
		}
		else if($TransaksiMember->ambilDeposit($data_member['id_member'])=='false'){
			//JIKA DEPOSIT MEMBER TIDAK VALID, DIKARENAKAN ADANYA PERUBAHAN SALDO SECARA ILEGAL
			$body = "<h2><font color='red'>SALDO TIDAK VALID, SILAHKAN MENGHUBUNGI ADMIN ANDA</font></h2>";
		}
		else{
			
			if($list_tiket==''){
				//JIKA KURSI BELUM DIPESAN
				//mengambil jumlah kursi yang dipilih		
				if($layout_kursi<=10){
					//jika bukan bis
					$sql = 
						"SELECT 
							K1,K2,K3,K4,K5,K6,K7,K8,K9,K10,
							sess1,sess2,sess3,sess4,sess5,sess6,sess7,sess8,sess9,sess10,id
						FROM TbPosisi
							WHERE (KodeJadwal = '$kode_jadwal') AND (CONVERT(CHAR(20), TGLBerangkat, 105) = '$tgl_berangkat')";
										
					if ($result = $db->sql_query($sql)){
								
						while ($row = $db->sql_fetchrow($result)){
							$kursi=array($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7],$row[8],$row[9]);				
							$jum_kursi_terisi = $jum_kursi_terisi+$row[0]+$row[1]+$row[2]+$row[3]+$row[4]+$row[5]+$row[6]+$row[7]+$row[8]+$row[9];
							$session_kursi=array($row[10],$row[11],$row[12],$row[13],$row[14],$row[15],$row[16],$row[17],$row[18],$row[19]);
						}
					} 
					else{      
						die_error('GAGAL MENGAMBIL DATA KURSI 1');//,__LINE__,__FILE__,$sql);
					}
				}
				else{
					//jika bis
					//mengupdate tbl_layout_detail
					$sql = 
						"SELECT id_layout
						FROM tbl_layout_header
						WHERE 
							(kode_jadwal = '$kode_jadwal') 
							AND (CONVERT(VARCHAR(20),tgl_berangkat,105)='$tgl_berangkat')";
								
					if ($result = $db->sql_query($sql)){
						$row = $db->sql_fetchrow($result);
						$id_layout=$row['id_layout'];
					}
					else{
						die_error('Gagal mengambil data');
					} 
							
					$sql = 
						"SELECT 
							nomor_kursi,status_kursi,sess
						FROM tbl_layout_detail
						WHERE 
							(id_layout = '$id_layout')
							ORDER BY nomor_kursi";
						
					if ($result = $db->sql_query($sql)){
						while ($row = $db->sql_fetchrow($result)){
							$no_kursi									= trim($row['nomor_kursi'])-1;
							$kursi[$no_kursi]					= $row['status_kursi'];
							$session_kursi[$no_kursi]	= $row['sess'];
						} 

					}
					else{	  
						die_error('GAGAL MENGAMBIL DATA KURSI 2');//,__LINE__,__FILE__,$sql);
					}
				}
					
				$jum_kursi_dipesan=0;
						
				//MEMERIKSA JUMLAH KURSI YANG DIPESAN
				for($idx=0;$idx<=$layout_kursi-1;$idx++){
								
					if(($kursi[$idx]==1) && ($session_kursi[$idx]==$useraktif)){
						//jika memang benar kursi dipilih oleh pengguna aktif
						$jum_kursi_dipesan ++;
					}
				}
			}
			else{
			//JIKA KURSI SUDAH DIPESAN TERLEBIH DAHULU
				//mengambil data tiket
				$sql = 
					"SELECT count(NoUrut) as jum_kursi_dipesan
					FROM TbReservasi
						WHERE NoUrut IN($list_tiket)";
				
				if (!$result = $db->sql_query($sql)){
					die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
			  } 
				$row = $db->sql_fetchrow($result);
				$jum_kursi_dipesan	= $row['jum_kursi_dipesan'];
			}
					
			//jika ada kursi yang dipesan
			if($jum_kursi_dipesan>0){
				//MENGAMBIL HARGA TIKET
				$sql = 
					"SELECT harga 
					FROM TBMDJadwal
					WHERE 
						kode='$kode_jadwal'";
								
				if ($result = $db->sql_query($sql)){
					while ($row = $db->sql_fetchrow($result)){
						$harga_tiket	= $row['harga'];
					}
				} 
				else{
					die_error("Gagal 01");
				}
						
				//MENGAMBIL DISCOUNT BERDASARKAN PROMO
				//ket: discount hanya diberikan untuk 1 tiket pertama dalam bentuk persentase
				//		 jumlah point juga hanya diberikan per transaksi saja
				$data_promo	= $Promo->ambilDiscountDanPoint($kode_jadwal,$tgl_berangkat);
						
				$discount_persen		= $data_promo['discount_persen']/100;
				$discount						= $harga_tiket*$discount_persen;
				$jumlah_point				= $data_promo['point'];
				
				$sub_total_transaksi= $harga_tiket*$jum_kursi_dipesan;
				$total_transaksi		= $sub_total_transaksi-$discount;
								
				//MEMERIKSA APAKAH NILAI DEPOSIT MENCUKUPI ATAU TIDAK
				//mengambil nilai deposit
				$deposit	= $TransaksiMember->ambilDeposit($data_member['id_member']);
				
				if($total_transaksi<=$deposit){
					$keterangan=
					"	<tr><td>Jumlah dipesan</td><td>:</td><td align='right'>$jum_kursi_dipesan</td></tr>
						<tr><td>Harga tiket</td><td>:</td><td align='right'>Rp.".number_format($harga_tiket,0,",",".")."</td></tr>
						<tr><td colspan=3 bgcolor=red height=1></td></tr>
						<tr><td>Sub Total</td><td>:</td><td align='right'><b>Rp.".number_format($sub_total_transaksi,0,",",".")."</b></td></tr>
						<tr><td>Discount</td><td>:</td><td align='right'>Rp.".number_format($discount,0,",",".")."</td></tr>
						<tr><td>Total bayar</td><td>:</td><td align='right'><b>Rp.".number_format($total_transaksi,0,",",".")."</b></td></tr>
						<tr><td>point</td><td>:</td><td align='right'><b>".number_format($jumlah_point,0,",",".")."</b></td></tr>
						";
							
					//$list_tiket=str_replace("'","\'",$list_tiket);
					
					//$pesan=
						//"<input type='button' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;' onClick='window.close();'/>&nbsp;&nbsp;
						//<input type='button' value='Konfirmasi' onClick=\"konfirmPassword('$tgl_berangkat','$kode_jadwal','$layout_kursi','".trim($data_member['id_member'])."','$id_kartu','$list_tiket');\" />";
					
					$pesan=
						"<input type='button' value='&nbsp;&nbsp;Batal&nbsp;&nbsp;' onClick='window.close();'/>&nbsp;&nbsp;
						<input type='button' value='Konfirmasi' onClick=\"konfirmPassword();\" />";

				}
				else{
					$pesan = "<h2><font color='red'>DEPOSIT TIDAK MENCUKUPI</font></h2>";
				}
			}
			else{
				$pesan = "<h2><font color='red'>ANDA BELUM MEMILIH KURSI YANG DIPESAN</font></h2>";
			}
			
			$parameter="
				<input type='hidden' id='tgl_berangkat' name='tgl_berangkat' value='$tgl_berangkat' />
				<input type='hidden' id='kode_jadwal' name='kode_jadwal' value='$kode_jadwal' />
				<input type='hidden' id='layout_kursi' name='layout_kursi' value='$layout_kursi' />
				<input type='hidden' id='id_member' name='id_member' value='".trim($data_member['id_member'])."' />	
				<input type='hidden' id='id_kartu' name='id_kartu' value='$id_kartu' />
				<input type='hidden' id='list_tiket' name='list_tiket' value=\"$list_tiket\" />";
	
			$body =
			"<table width='70%'>
				<tr>
					<td width='30%'>ID MEMBER</td><td width='5%'>:</td><td width='65%'>$data_member[id_member]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
					<tr>
					<td>Telp</td><td>:</td><td>$data_member[telp_rumah] / $data_member[handphone]</td>
				</tr>
				<tr>
					<td>Deposit</td><td>:</td><td>Rp. ".number_format($data_member['saldo'],0,",",".")."</td>
				</tr>
				<tr>
					<td>Point</td><td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
				</tr>
				<tr><td colspan=3 bgcolor='d0d0d0'></td></tr>
				$keterangan
				<tr><td colspan=3 align='center'>$pesan</td></tr>
			</table>";
					
			
		}
	break;
	
	//TRANSAKSI  =============================================================================================
	case 'proses_transaksi':
				
		$id_member		= trim($HTTP_POST_VARS['id_member']);		
		$id_kartu			= trim($HTTP_POST_VARS['id_kartu']);		
		$tgl_berangkat= trim($HTTP_POST_VARS['tgl_berangkat']);		
		$kode_jadwal	= trim($HTTP_POST_VARS['kode_jadwal']);		
		$layout_kursi	= trim($HTTP_POST_VARS['layout_kursi']);
		$password			= trim($HTTP_POST_VARS['password']);
		$list_tiket		= trim($HTTP_POST_VARS['list_tiket']);
		$list_tiket		= str_replace("\'","'",$list_tiket);
		
		$useraktif	= $userdata['username'];
		
		$data_member=$Member->ambilDataByIdKartu($id_kartu);
		
		//memriksa otorisasi password
		$otorisasi_password = $Member->verifikasiPassword($id_member,$password);
		
		if(!$otorisasi_password){// MEMERIKSA OTORISASI MEMBER
			//JIKA PASSWORD SALAH
			$pesan = 7;
			$valid	=false;
			break;
		}
		
		//MEMERIKSA APAKAH ID KARTU TERSEBUT DITEMUKAN DI DATABASE ATAU TIDAK
		if($data_member['id_member']==''){
			//JIKA DATA TIDAK DITEMUKAN
			$pesan = 2;
			$valid	=false;
			break;
		}
		
		if(!$data_member['status_member']){ //MEMERIKSA STATUS AKTIF MEMBER
			//JIKA MEMBER TIDAK AKTIF
			$pesan = 3;
			$valid	=false;
			break;
		}
				
		$deposit	= $TransaksiMember->ambilDeposit($data_member['id_member']);
		if($deposit=='false'){//MEMERIKSA KODE VALIDASI
			//JIKA DEPOSIT MEMBER TIDAK VALID, DIKARENAKAN ADANYA PERUBAHAN SALDO SECARA ILEGAL
			$pesan = 4;
			$valid	=false;
			break;
		}
		
		//memgambil data rute dan jadwal
		$sql = 
			"SELECT jam,masterkota,asal,tujuan,nopol,Merek,namasopir,kursi,biayasopir
			FROM Tbmdjadwal
			WHERE (Kode LIKE '$kode_jadwal')";
							  
		if ($result = $db->sql_query($sql)){
			while ($rowidp = $db->sql_fetchrow($result)){	  
				$jam_berangkat			= $rowidp[0];
				$masterkota 				= $rowidp[1];
				$asal 							= $rowidp[2];
				$tujuan 						= $rowidp[3];
				$nopol 							= $rowidp[4];
				$merek 							= $rowidp[5];
				$namasupir 					= $rowidp[6];
				$jum_kursi_tersedia = $rowidp[7];
				$biayasopir 				= ($rowidp[8]=='')?0:$rowidp[8];
												
				$jam_berangkat=substr($jam_berangkat,11);
			}    
		}
		else{  
			die_error('GAGAL MENGAMBIL RUTE');//,__LINE__,__FILE__,$sql);
		}
			
		if($list_tiket==""){
		//JIKA KURSI BELUM DIBOOK
			//mengambil jumlah kursi yang dipilih
					
			if($layout_kursi<=10){
				//jika bukan bis
				$sql = 
					"SELECT 
						K1,K2,K3,K4,K5,K6,K7,K8,K9,K10,
						sess1,sess2,sess3,sess4,sess5,sess6,sess7,sess8,sess9,sess10,id
					FROM TbPosisi
						WHERE (KodeJadwal = '$kode_jadwal') AND (CONVERT(CHAR(20), TGLBerangkat, 105) = '$tgl_berangkat')";
									
				if ($result = $db->sql_query($sql)){
							
					while ($row = $db->sql_fetchrow($result)){
						$kursi=array($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7],$row[8],$row[9]);				
						$jum_kursi_terisi = $jum_kursi_terisi+$row[0]+$row[1]+$row[2]+$row[3]+$row[4]+$row[5]+$row[6]+$row[7]+$row[8]+$row[9];
						$session_kursi=array($row[10],$row[11],$row[12],$row[13],$row[14],$row[15],$row[16],$row[17],$row[18],$row[19]);
					}
				} 
				else{      
					die_error('GAGAL MENGAMBIL DATA KURSI 1');//,__LINE__,__FILE__,$sqlid);
				}
			}
			else{
				//jika bis
				//mengupdate tbl_layout_detail
				$sql = 
					"SELECT id_layout
					FROM tbl_layout_header
					WHERE 
						(kode_jadwal = '$kode_jadwal') 
						AND (CONVERT(VARCHAR(20),tgl_berangkat,105)='$tgl_berangkat')";
							
				if ($result = $db->sql_query($sql)){
					$row = $db->sql_fetchrow($result);
					$id_layout=$row['id_layout'];
				}
				else{
					die_error('Gagal mengambil data');
				} 
						
				$sql = 
					"SELECT 
						nomor_kursi,status_kursi,sess
					FROM tbl_layout_detail
					WHERE 
						(id_layout = '$id_layout')
						ORDER BY nomor_kursi";
					
				if ($result = $db->sql_query($sql)){
					while ($row = $db->sql_fetchrow($result)){
						$no_kursi									= trim($row['nomor_kursi'])-1;
						$kursi[$no_kursi]					= $row['status_kursi'];
						$session_kursi[$no_kursi]	= $row['sess'];
					} 

				}
				else{	  
					die_error('GAGAL MENGAMBIL DATA KURSI 2');//,__LINE__,__FILE__,$sql);
				}
			}
				
			$jum_kursi_dipesan=0;
					
			//MEMERIKSA JUMLAH KURSI YANG DIPESAN
			for($idx=0;$idx<=$layout_kursi-1;$idx++){
							
				if(($kursi[$idx]==1) && ($session_kursi[$idx]==$useraktif)){
					//jika memang benar kursi dipilih oleh pengguna aktif
					$jum_kursi_dipesan ++;
				}
			}
		}
		else{
		//JIKA KURSI SUDAH DIPESAN TERLEBIH DAHULU
			//mengambil data tiket
			$sql = 
				"SELECT count(NoUrut) as jum_kursi_dipesan
				FROM TbReservasi
				WHERE NoUrut IN($list_tiket)";
				
			if (!$result = $db->sql_query($sql)){
				die_error('GAGAL MENGAMBIL DATA',__LINE__,__FILE__,$sql);
			} 
			$row = $db->sql_fetchrow($result);
			$jum_kursi_dipesan	= $row['jum_kursi_dipesan'];
		}
		
		//jika tidak ada kursi yang dipesan
		if($jum_kursi_dipesan==0){
			$pesan = 5;
			$valid=false;
			break;
		}
		//--------------------------------------------------------------------------------
				
		//MENGAMBIL HARGA TIKET
		$sql = 
			"SELECT harga 
			FROM TBMDJadwal
			WHERE 
				kode='$kode_jadwal'";
						
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$harga_tiket	= $row['harga'];
			}
		} 
		else{
			die_error("Gagal 01");
		}
				
		//MENGAMBIL DISCOUNT BERDASARKAN PROMO
		//ket: discount hanya diberikan untuk 1 tiket pertama dalam bentuk persentase
		//		 jumlah point juga hanya diberikan per transaksi saja
		$data_promo	= $Promo->ambilDiscountDanPoint($kode_jadwal,$tgl_berangkat);
						
		$discount_persen= $data_promo['discount_persen']/100;
		$discount						= $harga_tiket*$discount_persen;
		$jumlah_point				= $data_promo['point'];
				
		$sub_total_transaksi= $harga_tiket*$jum_kursi_dipesan;
		$total_transaksi		= $sub_total_transaksi-$discount;
				
		//MEMERIKSA APAKAH NILAI DEPOSIT MENCUKUPI ATAU TIDAK
		//mengambil nilai deposit
		$deposit	= $TransaksiMember->ambilDeposit($data_member['id_member']);
		if($total_transaksi>$deposit){
			$pesan = 6;
			$valid=false;
			break;
		}
		else{
			$valid=true;
		}

	break;
	
	//SELESAI======================================================================================================
	case 'selesai':
		
		$id_member	= trim($HTTP_GET_VARS['id_member']);		
		$pesan			= trim($HTTP_GET_VARS['pesan']);		
		$list_tiket	= trim($HTTP_GET_VARS['list_tiket']);		
		
		if($pesan==1){
					
			$text_pesan	= "<font color=green><h2>TRANSAKSI BERHASIL</h2></font>";
			
			$no_book = trim($HTTP_GET_VARS['no_book']);	
			
			if($list_tiket==""){
				//MENGAMBIL DETAIL TIKET
				$sql = 
					"SELECT NoUrut 
					FROM TbReservasi
					WHERE 
						kode0='$no_book'";

				$list_tiket="";
				
				if ($result = $db->sql_query($sql)){
					while ($row = $db->sql_fetchrow($result)){
						$list_tiket	= "\'$row[NoUrut]\',".$list_tiket;
					}
				} 
				else{
					die_error("Gagal 02");
				}
				
				$list_tiket	= substr($list_tiket,0,-1);
			}
			
			$tombol_cetak_tiket = "<tr><td colspan=3 bgcolor='d0d0d0' align='center'><input type='button' id='cetak_tiket' onClick=\"cetakTiket('$list_tiket');\" value='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CETAK TIKET&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'/></td></tr>";
		}
		else if($pesan==2){
			$text_pesan	= "<font color=red><h2>KARTU BELUM TERDAFTAR</h2></font>";
		}
		else if($pesan==3){
			$text_pesan	="<font color=red><h2>MEMBER DIBLOKIR</h2></font>";
		}
		else if($pesan==4){
			$text_pesan	="<font color=red><h2>NILAI DEPOSIT TIDAK VALID, SILAHKAN HUBUNGI ADMIN ANDA</h2></font>";
		}
		else if($pesan==5){
			$text_pesan	="<font color=red><h2>ANDA BELUM MEMILIH KURSI YANG DIPESAN</h2></font>";
		}
		else if($pesan==6){
			$text_pesan	="<font color=red><h2>NILAI DEPOSIT ANDA TIDAK MENCUKUPI!</h2></font>";
		}
		else if($pesan==7){
			$text_pesan	="<font color=red><h2>PASSWORD OTORISASI MEMBER SALAH!</h2> <a href='#' onClick='history.back();'><-kembali ke proses sebelumnya </a></font>";
		}
		
		$data_member	= $Member->ambilDataDetail($id_member);
	
		$body =
			"<table width='80%'>
				<tr>
					<td width='20%'>ID MEMBER</td><td width='5%'>:</td><td width='75%'>$data_member[id_member]</td>
				</tr>
				<tr>
					<td>Nama</td><td>:</td><td>$data_member[nama]</td>
				</tr>
				<tr>
					<td>Alamat</td><td>:</td><td>$data_member[alamat] $data_member[kota]</td>
				</tr>
					<tr>
					<td>Telp</td><td>:</td><td>$data_member[telp_rumah] / $data_member[handphone]</td>
				</tr>
				<tr>
					<td>Deposit</td><td>:</td><td>Rp. ".number_format($data_member['saldo'],0,",",".")."</td>
				</tr>
				<tr>
					<td>Point</td><td>:</td><td>".number_format($data_member['point'],0,",",".")."</td>
				</tr>
				<tr><td colspan=3 bgcolor='d0d0d0'></td></tr>
				$tombol_cetak_tiket
				<tr><td colspan=3>$text_pesan </td></tr>
			</table>";
	break;
}

if($mode=='proses_transaksi'){
	
	$id_member=$data_member['id_member'];
	
	if($valid){
		
		$noinvoice 	= generateKodeBooking($kode_jadwal);
		$nama			= trim($data_member['nama']);
		$add			= trim($data_member['alamat']);
		$telepon	= trim($data_member['telp_rumah']);
		$handphone= trim($data_member['handphone']);

		$TransaksiMember->mutasiPembelianTiket(
			$data_member['id_member'],$noinvoice,
			"Transaksi pembelian tiket ID Member:$data_member[id_member] $jum_kursi_dipesan penumpang $kode_jadwal $tgl_berangkat",$total_transaksi,$jumlah_point);
		
		$flag_kursi_pertama=true;
		
		//JIKA KURSI SUDAH DIBOOKING TERLEBIH DAHULU
		if($list_tiket!=""){
			
			if($layout_kursi<=10){
				$sql = 
					"SELECT NomorKursi
					FROM TbReservasi
					WHERE 
						NoUrut IN ($list_tiket)";
				
				if (!$result = $db->sql_query($sql)){
					die_error("Gagal");
				}
				
				$field_diupdate="";
				
				while ($row = $db->sql_fetchrow($result)){
					$field_diupdate	.= "bayar$row[NomorKursi]=1,";
				}
				
				$field_diupdate = substr($field_diupdate,0,-1);
				
				//mengupdate tabel posisi
				$sql = 
						"UPDATE tbposisi 
						SET $field_diupdate
						WHERE TGLBerangkat = CONVERT(DATETIME,'$tgl_berangkat',103) AND 
							kodejadwal='$kode_jadwal';";
			}
			else{
										
				$sql = 
					"UPDATE tbl_layout_detail 
					SET status_bayar=1
					WHERE no_tiket IN($list_tiket);";
			}

			//memberi discount pada kursi pertama
			$arr_no_tiket=explode(",",$list_tiket);
			
			$sql .= 
					"UPDATE tbReservasi 
					SET CetakTiket=1,cso='$useraktif'
					WHERE NoUrut IN($list_tiket);";
			
			$sql .= 
					"UPDATE tbReservasi 
					SET discount=CONVERT(money,$discount),total=subtotal-CONVERT(money,$discount)
					WHERE NoUrut = $arr_no_tiket[0];";
					
			if (!$result = $db->sql_query($sql)){
					die_error("GAGAL MENYIMPAN TRANSAKSI");// $sql");
				}
			
		}
		else{
		//JIKA TIKET BELUM DIBOOKING (GOSHOW)
		
			for($idx=0;$idx<=$layout_kursi-1;$idx++){
				
				if(($kursi[$idx]==1) && ($session_kursi[$idx]==$useraktif)){
					//jika memang benar kursi dipilih oleh pengguna aktif
					
					//Mengenerate no tiket
					$fix = generateNoTiket($kode_jadwal);
									
					$nomor_kursi=$idx+1; //nomor kursi adalah idx + 1
									
					$jum_kursi_dipesan=1;
									
					//sisa kursi
					$sisa_kursi=$jum_kursi_tersedia-1;
												
					//mengambil nilai  hari dan waktu sekarang temp:sisakursi = '$sisa_kursi'
					$hari_pesan 		= tanggaltoDate(date('d-m-Y'));
					$tgl_berangkat	= tanggaltoDate($tgl_berangkat);
					$jam_pesan			= (date('H')-1).":".date('i').":".date('s');
									
					//KET: yang mendapat diskon hanya kursi pertama
					if(!$flag_kursi_pertama) {
						$discount=0;
					}
					else{
						$flag_kursi_pertama=false;
					}
					
					$sub_total			=$harga_tiket-$discount;
					
					if($layout_kursi<=10){
						//mengupdate tabel posisi
						$sql = 
							"UPDATE tbposisi 
							SET K$nomor_kursi = '1', nama$nomor_kursi='$nama', t$nomor_kursi='$fix', sess$nomor_kursi='', bayar$nomor_kursi=1
							WHERE TGLBerangkat = CONVERT(DATETIME,'$tgl_berangkat',103) AND 
								kodejadwal='$kode_jadwal';";
					}
					else{
											
						$sql = 
							"UPDATE tbl_layout_detail 
							SET status_kursi = '1', nama='$nama', no_tiket='$fix', sess='',status_bayar=1
							WHERE id_layout='$id_layout'
								AND nomor_kursi='$nomor_kursi';";
					}
						
					//mengisi ke tabel reservasi    
					$sql .= 
						"INSERT INTO tbreservasi 
							(nourut,kode0,nama0,alamat0,telp0,hp0,
							tglpesan,jampesan,
							tglberangkat,jamberangkat,hargatiket,
							kodejadwal,asal,tujuan,nopol,jumlahkursi,pesankursi,subtotal,
							discount,total,komisisopir,
							nomorkursi,merek,operator,IsRecordChange,cabang,siap,posting,
							kodecabang,namacabang,kodearea,namaarea,nojurnal,noSPJ,
							idMember,cso,cetakTiket) 
						VALUES(
							'$fix','$noinvoice','$nama','$add','$telepon','$handphone',
							CONVERT(datetime,'$hari_pesan',103),'1899-12-30 $jam_pesan',
							CONVERT(datetime,'$tgl_berangkat',103),'1899-12-30 $jam_berangkat',CONVERT(money,'$harga_tiket'),
							'$kode_jadwal','$asal','$tujuan','$nopol','$jum_kursi_tersedia','$jum_kursi_dipesan',CONVERT(money,$sub_total),
							CONVERT(money,$discount),CONVERT(money,$sub_total),CONVERT(money,$biayasopir),
							'$nomor_kursi','$merek','$useraktif',1,'$asal',1,0,
							'$asal','$asal','$asal','$asal','---','$no_spj',
							'$id_member','$useraktif',1);";

					if (!$db->sql_query($sql)){
						die_error('GAGAL MENYIMPAN TRANSAKSI 1');//,__LINE__,__FILE__,$sql);
					}
					
				}
			}
		}
		
		$pesan = 1;
	}
	
	redirect(append_sid('member_transaksi.'.$phpEx)."&mode=selesai&id_member=".trim($id_member)."&pesan=$pesan&no_book=$noinvoice&list_tiket=$list_tiket",true); 
	
}

include($adp_root_path . 'includes/page_header.php');
$template->set_filenames(array('body' => 'member_transaksi.tpl')); 
$template->assign_vars (
	array(
		'BODY'			=> $body,
		'PARAMETER'	=> $parameter
	)
);
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');

?>