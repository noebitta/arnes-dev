<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraBNOP.php');
include($adp_root_path . 'ClassBiayaOperasional.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassSopir.php');
include($adp_root_path . 'ClassKota.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$cari 			= isset($HTTP_GET_VARS['cari'])? $HTTP_GET_VARS['cari'] : $HTTP_POST_VARS['txt_cari'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

$expired_time = 120; //menit
// LIST
$template->set_filenames(array('body' => 'beritaacara.bnop/index.tpl'));

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql = FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql = FormatTglToMySQLDate($tanggal_akhir);

$BeritaAcaraBOP = new BeritaAcaraBNOP();

$mode	= $mode==""?"explore":$mode;

switch($mode){
	case 'explore':
		$kondisi =	$cari==""?"":
			" AND (NamaPenerima LIKE '$cari%'
				OR Keterangan LIKE '%$cari%' 
				OR NamaPembuat LIKE '%$cari%'
				OR NamaReleaser LIKE '%$cari%')";
		
		$order	=($order=='')?"DESC":$order;
			
		$sort_by =($sort_by=='')?"WaktuTransaksi":$sort_by;
		
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging		= pagingData($idx_page,"1","tbl_ba_bnop",
		"&asal=$asal&tujuan=$tujuan&cari=$kondisi_cari&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&sort_by=$sort_by&order=$order",
		"WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') $kondisi" ,"beritaacara.bop.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql	=
			"SELECT *,IF(TIME_TO_SEC(TIMEDIFF(WaktuTransaksi,NOW()))/60>=-$expired_time OR WaktuTransaksi IS NULL,0,1) AS IsExpired
			FROM tbl_ba_bnop
			WHERE (DATE(WaktuTransaksi) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
			$kondisi
			ORDER BY $sort_by $order LIMIT $idx_awal_record,$VIEW_PER_PAGE;";
			
		if(!$result = $db->sql_query($sql)){
			die_error("Gagal eksekusi query!",__LINE__, $this->ID_FILE,"");
		}
		
		$i=1;
		
		while ($row = $db->sql_fetchrow($result)){
			$odd ='odd';
				
			if (($i % 2)==0){
				$odd = 'even';
			}
			
			$show_header	= ($i%$config['repeatshowheader']!=0 && $i!=1)?"none":"";
			
			if($row['IsRelease']==1){
				$odd="green";
				$act= "RELEASED";
			}
			elseif($row['IsExpired']==0){
				$odd="yellow";
				$act= in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['KEUANGAN'])) && $row['IsRelease']==0?
					"<span class='b_edit' onClick=\"false;setDataBA('".$row['Id']."');\" title='ubah' >&nbsp;</span> | <span class='b_delete' onClick=\"false;hapus('".$row['Id']."');\" title='hapus' >&nbsp;</span>":"";
			}
			else{
				$odd="red";
				$act= "EXPIRED";
			}
			
			
			$template->
				assign_block_vars(
					'ROW',
					array(
						'odd'=>$odd,
						'no'=>$i+$idx_page*$VIEW_PER_PAGE,
						'penerima'=>$row['NamaPenerima'],
						'waktubuat'=>dateparse(FormatMySQLDateToTglWithTime($row['WaktuTransaksi'])),
						'dibuatoleh'=>$row['NamaPembuat'],
						'jenisbiaya'=>$LIST_JENIS_BIAYA[$row['JenisBiaya']],
						'jumlah'=>"Rp.".number_format($row['Jumlah'],0,",","."),
						'keterangan'=>$row['Keterangan'],
						'releaser'=>$row['NamaReleaser']==""?"Belum Released":$row['NamaReleaser'],
						'waktureleased'=>$row['WaktuRelease']==""?"Belum Released":dateparse(FormatMySQLDateToTglWithTime($row['WaktuRelease'])),
						'act'=>$act,
					)
				);
			$i++;
		}
		
		if($i-1<=0){
			$template->assign_block_vars('ROW',array('showheader'=>''));
			$no_data	=	"<div style='width:100%;' class='yellow' align='center'><font size=3><b>data tidak ditemukan</b></font></div>";
		}
		
		//KOMPONEN UNTUK EXPORT
		$parameter_cetak	= "&sort_by=$sort_by&order=$order&cari=".$cari;
		$script_cetak_excel="Start('beritaacara.bnop.cetakexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
		//--END KOMPONEN UNTUK EXPORT
		
		//paramter sorting
		$order_invert	= ($order=='ASC' || $order=='')?'DESC':'ASC';
		$parameter_sorting	= "&page=$idx_page&kota=$kota&asal=$asal&tujuan=$tujuan&tanggal_mulai=$tanggal_mulai&tanggal_akhir=$tanggal_akhir&order=$order_invert";
		
		$array_sort	= 
			"'".append_sid('beritaacara.bop.php?sort_by=TglBerangkat'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NoPolisi'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NamaSopir'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=KodeJadwal'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=JamBerangkat'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NoSPJ'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=WaktuTransaksi'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NamaPembuat'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=JenisBiaya'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=Jumlah'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=Keterangan'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=NamaReleaser'.$parameter_sorting)."',".
			"'".append_sid('beritaacara.bop.php?sort_by=WaktuRelease'.$parameter_sorting)."'";

    $Kota = new Kota();

		$LIST_JENIS_BIAYA=array(
			"BELI AQUA",
			"LISTRIK",
			"LAINNYA");

		$FLAG_BIAYA_BELI_AQUA	= 1;
		$FLAG_BIAYA_LISTRIK		= 2;
		$FLAG_BIAYA_LAINNYA	= 3;

		$page_title	= "Berita Acara Biaya Non Operasional";
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('beritaacara.bnop.'.$phpEx).'">Berita Acara Biaya Non Operasional</a>',
			'ACTION_CARI'		=> append_sid('beritaacara.bop.'.$phpEx),
			'BIAYA_PENGELUARAN'	=> append_sid('beritaacara.bnop.'.$phpEx),
			'PAGING'				=> $paging,
			'CETAK_XL'			=> $script_cetak_excel,
			'TGL_AWAL'			=> $tanggal_mulai,
			'TGL_AKHIR'			=> $tanggal_akhir,
			'OPT_KOTA'			=> $Kota->setComboKota($kota),
			'TXT_CARI'			=> $cari,
			'KOTA'					=> $kota,
			'ASAL'					=> $asal,
			'TUJUAN'				=> $tujuan,
			'ARRAY_SORT'		=> $array_sort,
			'NO_DATA'				=> $no_data,
			'JENIS_BIAYA_AQUA'=>$FLAG_BIAYA_BELI_AQUA,
			'JENIS_BIAYA_LISTRIK'=>$FLAG_BIAYA_LISTRIK,
			'JENIS_BIAYA_LAINNYA'=>$FLAG_BIAYA_LAINNYA
			)
		);
						
		
		include($adp_root_path . 'includes/page_header.php');
		$template->pparse('body');
		include($adp_root_path . 'includes/page_tail.php');
		
		exit;
	case 'getasal':
		$Cabang	= new Cabang();
		
		echo "
			<select name='asal' id='asal' onChange='getUpdateTujuan(this.value);'>
				".$Cabang->setInterfaceComboCabangByKota($kota,$asal,"")."
			</select>";
		
	exit;
		
	case 'gettujuan':
		$Cabang	= new Cabang();
		
		echo "
			<select name='tujuan' id='tujuan' >
				".$Cabang->setInterfaceComboCabangTujuan($asal,$tujuan)."
			</select>";
	exit;
	
	case "showdialog":
		
		$kode_ba	=  $BeritaAcaraBOP->generateKodeBA();

		echo("
			proses_ok	= true;
			detailmanifest.style.display='block';
			showtombol1.style.display='none';
			showkodeba.innerHTML	= '$kode_ba';
			kodeba.value					= '$kode_ba';
			jumlah.value					= '';
			keterangan.value			= '';
			dlg_ba.show();
		");
	exit;
	
	case "getdatamanifest":
		// aksi menambah member
		
		//membandingkan waktu keberangkatan dengan waktu cetak sekarang
		$waktu_sekarang	= date("Y-m-d H:i:s");
		$waktu_sekarang	= strtotime($waktu_sekarang);
		
		$waktu_berangkat= strtotime(substr($data_manifest['TglBerangkat'],0,10)." ".$data_manifest['JamBerangkat'].":00");
		$selisih  			= $waktu_sekarang - $waktu_berangkat;
		$selisih_menit	= round($selisih / 60); //menit
		
		if($selisih_menit>120){
			echo("proses_ok=true;alert('Transaksi tidak dapat dibuat 2 jam setelah waktu keberangkatan!');");exit;
		}
		
		$return_val	=
			"proses_ok=true;
			nospj.style.display='none';
			shownospj.style.display='inline-block';
			shownospj.innerHTML='".$data_manifest['NoSPJ']."';
			detailmanifest.style.display='block';
			tglberangkat.innerHTML='".FormatMySQLDateToTgl($data_manifest['TglBerangkat'])."';
			kodejadwal.innerHTML='".$data_manifest['KodeJadwal']."';
			jamberangkat.innerHTML='".$data_manifest['JamBerangkat']."';
			kodebody.innerHTML='".$data_manifest['NoPolisi']."';
			sopir.innerHTML='".$data_manifest['Driver']."';
			jumlah.value='';
			keterangan.value='';
			showtombol1.style.display='none';
			dlg_ba.show();";
		
		echo($return_val);
	exit;
	
	case 'simpan':
		
		//MENYIMPAN BERITA ACARA
		$id_ba			= $HTTP_POST_VARS["idba"];
		$kode_ba		= $HTTP_POST_VARS["kodeba"];
		$penerima		= $HTTP_POST_VARS["penerima"];
		$jenis_biaya= $HTTP_POST_VARS["jenisbiaya"];
		$jumlah			= $HTTP_POST_VARS["jumlah"];
		$keterangan	= $HTTP_POST_VARS["keterangan"];

		if($id_ba==""){
			//berita acara baru
			
			if($BeritaAcaraBOP->tambah($kode_ba,$jenis_biaya,$jumlah,
				$penerima,$keterangan)){
				
				echo("proses_ok=true;location.reload();");
			}
			else{
				echo("proses_ok=false;");
			}
		}
		else{
			$data_ba	= $BeritaAcaraBOP->ambilDetail($id_ba);
			
			if(in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['KEUANGAN']))){
				//berita acara sudah disimpan hanya level tertentu bisa mengedit
				if($BeritaAcaraBOP->ubah(
					$id_ba, $penerima,$jenis_biaya,$jumlah,
					$keterangan)){
					
					echo("proses_ok=true;location.reload();");	
				}
				else{
					echo("proses_ok=false;");
				}
			}
		}
		
	exit;
	
	case 'hapus':
		
		//MENYIMPAN BERITA ACARA
		$id_ba	= $HTTP_POST_VARS["idba"];
		
		$data_ba	= $BeritaAcaraBOP->ambilDetail($id_ba);
		
		if($data_ba["IsRelease"]==1){
			echo("proses_ok=true;alert('Biaya sudah direlease');");exit;
		}
			
		if($BeritaAcaraBOP->hapus($id_ba)){
			echo("proses_ok=true;location.reload();");	
		}
		else{
			echo("proses_ok=false;");
		}
		
	exit;
	
	case "getdataba":
		$id_ba			= $HTTP_POST_VARS["id"];

		$data_ba	=  $BeritaAcaraBOP->ambilDetail($id_ba);
		
		$return_val	=
			"proses_ok=true;
			showkodeba.innerHTML='".$data_ba['KodeBA']."';
			detailmanifest.style.display='block';
			penerima.value='".$data_ba['NamaPenerima']."';
			jumlah.value='".$data_ba['Jumlah']."';
			keterangan.value='".$data_ba['Keterangan']."';
			setJenisBiaya('".$data_ba['JenisBiaya']."');
			showtombol1.style.display='none';
			dlg_ba.show();";

		echo($return_val);
		
	exit;
}
?>