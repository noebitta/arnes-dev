<?php
//
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassBeritaAcaraBOP.php');
include($adp_root_path . 'ClassBiayaOperasional.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$id	 				= isset($HTTP_GET_VARS['id'])? $HTTP_GET_VARS['id'] : $HTTP_POST_VARS['id'];

// LIST
$BeritaAcaraBOP= new BeritaAcaraBOP();

$data_ba	= $BeritaAcaraBOP->ambilDetail($id);

if($data_ba["Id"]==""){
	die_error("Data tidak ditemukan!","NULL","Error code","");
}

$template->set_filenames(array('body' => 'beritaacara.bop/release.cetak.tpl')); 

$template->
	assign_block_vars(
		'ROW',
		array(
      'NAMA_PERUSAHAAN'=>$config['nama_perusahaan'],
			'JENIS_BIAYA'		=>$LIST_JENIS_BIAYA[$data_ba["JenisBiaya"]],
			'NO_SPJ'				=>$data_ba["NoSPJ"],
			'TGL_BERANGKAT'	=>$data_ba["TglBerangkat"],
			'JAM_BERANGKAT'	=>substr($data_ba["JamBerangkat"],0,5),
			'KODE_JADWAL'		=>$data_ba["KodeJadwal"],
			'KODE_UNIT'			=>$data_ba["KodeKendaraan"],
			'NAMA_SOPIR'		=>$data_ba["NamaSopir"],
			'NIK'						=>$data_ba["KodeSopir"],
			'JUMLAH'				=>"Rp.".number_format($data_ba["Jumlah"],0,",","."),
			'KETERANGAN'		=>$data_ba["Keterangan"],
			'CSO'						=>$data_ba["NamaReleaser"],
			'WAKTU_CETAK'		=>dateparseWithTime(FormatMySQLDateToTglWithTime($data_ba['WaktuRelease'])),
			'PEMBUAT_BA'		=>$data_ba["NamaPembuat"]
		)
	);

$template->pparse('body');	
?>