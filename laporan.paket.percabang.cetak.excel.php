<?php
//
// LAPORAN
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

require_once dirname(__FILE__) . '/classes/PHPExcel.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/RichText.php';

require_once dirname(__FILE__) . '/classes/PHPExcel/IOFactory.php'; 

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX';      // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0; // buat pagination      

//$is_today  			= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai_mysql  = $HTTP_GET_VARS['tglawal'];
$tanggal_akhir_mysql  = $HTTP_GET_VARS['tglakhir'];
$cari  								= $HTTP_GET_VARS['cari'];
$sort_by							= $HTTP_GET_VARS['sortby'];
$order								= $HTTP_GET_VARS['order'];

//INISIALISASI
//$tbl_reservasi	= $is_today=="1" || $is_today==""?"tbl_reservasi":"tbl_reservasi_olap";

$kondisi_cari	=($cari=="")?"WHERE 1 ":
	" WHERE (KodeCabang LIKE '$cari%'
		OR Nama LIKE '%$cari%'
		OR Kota LIKE '%$cari%'
		OR Telp LIKE '$cari%'
		OR Alamat LIKE '%$cari%')";

if(in_array($userdata['user_level'],array($LEVEL_SUPERVISOR))){
	$kondisi_cabang		= " AND KodeCabang='$userdata[KodeCabang]'";	
	$kondisi_cabang_2	= " AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";	
}			

$kondisi_cari	.= $kondisi_cabang;

$sql=
	"SELECT 
		KodeCabang,Nama,Alamat,Kota,Telp,Fax
	FROM tbl_md_cabang
	$kondisi_cari";

if (!$result_laporan = $db->sql_query($sql)){
	echo("Err:".__LINE__);exit;
}

//DATA PENJUALAN PAKET
$sql	= 
	"SELECT 
		f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) AS KodeCabang,
		IS_NULL(COUNT(IF(JenisLayanan='P2',1,NULL)),0) AS TotalPaketP2,
		IS_NULL(COUNT(IF(JenisLayanan='D2',1,NULL)),0) AS TotalPaketD2,
		IS_NULL(SUM(IF(JenisLayanan='P2',HargaPaket,0)),0) AS OmzPaketP2,
		IS_NULL(SUM(IF(JenisLayanan='D2',HargaPaket,0)),0) AS OmzPaketD2,
		IS_NULL(SUM(IF(JenisPembayaran=0,TotalBayar,0)),0) AS OmzTunai,
		IS_NULL(SUM(IF(JenisPembayaran!=0,TotalBayar,0)),0) AS OmzLangganan,
		IS_NULL(SUM(TotalBayar),0) AS TotalPenjualanPaket
	FROM tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
		AND CetakTiket=1 AND FlagBatal!=1 $kondisi_cabang_2
	GROUP BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan) ORDER BY f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)";
		
if (!$result = $db->sql_query($sql)){
	echo("Err: ".__LINE__);exit;
}

//debug
//echo($sql);exit;

while ($row = $db->sql_fetchrow($result)){
	$data_paket_total[$row['KodeCabang']]	= $row;
}

//isi array temp laporan

$temp_array=array();

$idx=0;

while ($row = $db->sql_fetchrow($result_laporan)){

	$temp_array[$idx]['KodeCabang']					= $row['KodeCabang'];
	$temp_array[$idx]['Nama']					= $row['Nama'];
	$temp_array[$idx]['Alamat']					= $row['Alamat'];
	$temp_array[$idx]['Kota']					= $row['Kota'];
	$temp_array[$idx]['Telp']					= $row['Telp'];
	$temp_array[$idx]['Fax']					= $row['Fax'];
	$temp_array[$idx]['hp']						= $row['hp'];
	$temp_array[$idx]['TotalPaketP2']				= $data_paket_total[$row['KodeCabang']]['TotalPaketP2']!=""?$data_paket_total[$row['KodeCabang']]['TotalPaketP2']:0;
	$temp_array[$idx]['TotalPaketD2']				= $data_paket_total[$row['KodeCabang']]['TotalPaketD2']!=""?$data_paket_total[$row['KodeCabang']]['TotalPaketD2']:0;
	$temp_array[$idx]['OmzPaketP2']					= $data_paket_total[$row['KodeCabang']]['OmzPaketP2']!=""?$data_paket_total[$row['KodeCabang']]['OmzPaketP2']:0;
	$temp_array[$idx]['OmzPaketD2']					= $data_paket_total[$row['KodeCabang']]['OmzPaketD2']!=""?$data_paket_total[$row['KodeCabang']]['OmzPaketD2']:0;
	$temp_array[$idx]['OmzTunai']					= $data_paket_total[$row['KodeCabang']]['OmzTunai']!=""?$data_paket_total[$row['KodeCabang']]['OmzTunai']:0;
	$temp_array[$idx]['OmzLangganan']				= $data_paket_total[$row['KodeCabang']]['OmzLangganan']!=""?$data_paket_total[$row['KodeCabang']]['OmzLangganan']:0;
	$temp_array[$idx]['OmzTotal']					= $data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket']!=""?$data_paket_total[$row['KodeCabang']]['TotalPenjualanPaket']:0;
		
	$idx++;
}

if($order=='ASC'){
	//$temp_array = multiSortArray($temp_array, array($sort_by=>1));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_ASC);
}
else{
	//$temp_array = multiSortArray($temp_array, array($sort_by=>0));
	$temp_array = array_orderby($temp_array, $sort_by,SORT_DESC);
}

$objPHPExcel = new PHPExcel();          
$objPHPExcel->setActiveSheetIndex(0);  
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:G2');

//HEADER
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Laporan Omzet Paket per Cabang Tanggal '.$tanggal_mulai_mysql.' s/d '.$tanggal_akhir_mysql);
$objPHPExcel->getActiveSheet()->mergeCells('A3:A4');
$objPHPExcel->getActiveSheet()->setCellValue('A3', 'No.');
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('B3:B4');
$objPHPExcel->getActiveSheet()->setCellValue('B3', 'Cabang');
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('C3:D3');
$objPHPExcel->getActiveSheet()->setCellValue('C3', 'Total Paket (qty)');
$objPHPExcel->getActiveSheet()->setCellValue('C4', 'P2');
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('D4', 'D2');
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('E3:F3');
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'Total Paket (Rp.)');
$objPHPExcel->getActiveSheet()->setCellValue('E4', 'P2');
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('F4', 'D2');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->mergeCells('G3:H3');
$objPHPExcel->getActiveSheet()->setCellValue('G3', 'Omzet');
$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Tunai');
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Total');
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

$idx=0;
$idx_row=5;

while ($idx<count($temp_array)){
	
		
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, $idx+1);
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$idx_row, $temp_array[$idx]['Nama']." (".$temp_array[$idx]['KodeCabang'].")");
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row, $temp_array[$idx]['TotalPaketP2']);
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row, $temp_array[$idx]['TotalPaketD2']);
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row, $temp_array[$idx]['OmzPaketP2']);
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row, $temp_array[$idx]['OmzPaketD2']);
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row, $temp_array[$idx]['OmzTunai']);
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row, $temp_array[$idx]['OmzTotal']);	
	
	$idx_row++;
	$idx++;
}

$objPHPExcel->getActiveSheet()->mergeCells('A'.$idx_row.':B'.$idx_row);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$idx_row, "TOTAL");
$objPHPExcel->getActiveSheet()->setCellValue('C'.$idx_row,"=SUM(C5:C".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('D'.$idx_row,"=SUM(D5:D".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('E'.$idx_row,"=SUM(E5:E".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('F'.$idx_row,"=SUM(F5:F".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('G'.$idx_row,"=SUM(G5:G".($idx_row-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('H'.$idx_row,"=SUM(H5:H".($idx_row-1).")");
	
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); 

if ($idx>0){
	header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Laporan Omzet Paket per Cabang Tanggal '.$tanggal_mulai_mysql.' sd '.$tanggal_akhir_mysql.'.xls"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output'); 
}
  
?>
