<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemo.php');
include($adp_root_path . 'ClassCabang.php');
include($adp_root_path . 'ClassJurusan.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassPenjadwalanKendaraan.php');
include($adp_root_path . 'ClassReservasi.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($LEVEL_ADMIN,$LEVEL_MANAJEMEN,$LEVEL_MANAJER,$LEVEL_SUPERVISOR,$LEVEL_SCHEDULER))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

// membuat option Asal
function OptionAsal($kota,$asal_dipilih=""){
	global $db;
	
	$Cabang	= new Cabang();
	
	$result	= $Cabang->setComboCabang($kota);
	
  $opt = "";
	
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
      $selected	= ($asal_dipilih!=$row[0])?"":"selected";
			$opt .= "<option value='$row[0]' $selected >$row[1]</option>";
    }    
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
			
  return $opt;
}

// membuat option jurusan
function OptionJurusan($asal,$tujuan_dipilih=""){
	
	global $db;
	
	$Jurusan	= new Jurusan();
	
	$result = $Jurusan->setComboJurusan($asal);
	
  $opt = "";
  if ($result){
		$opt = "<option value=''>(none)</option>" . $opt;
		
		while ($row = $db->sql_fetchrow($result)){
			$selected	= ($tujuan_dipilih!=$row[0])?"":"selected";
			$opt .= "<option value='$row[0]' $selected>$row[1] ($row[2])</option>";
    }  
	} 
	else{
		$opt .="<option selected=selected>Error</option>";
  }
	
  return $opt;
}

// membuat option jam
function OptionJadwal($tgl,$id_jurusan,$jadwal_dipilih=""){
  global $db;
	
  if ($id_jurusan!=''){
		
		$Jadwal	= new Jadwal();
		
		$result	= $Jadwal->setComboJadwal($tgl,$id_jurusan);
		
	  $opt = "";
		
	  if ($result){
			$opt = "<option>(none)</option>" . $opt;  
			while ($row = $db->sql_fetchrow($result)){
				$selected	= ($jadwal_dipilih!=$row[0])?"":"selected";
				$opt .= "<option value='$row[0]' $selected>$row[0]-$row[1]</option>";
			}    
	  } 
		else{
			$opt .="<option selected=selected>Error</option>";
	  }
  }  
  
  return $opt;
}

$Memo	= new Memo();

	//Asal  ===========================================================================================================================================================================================
	if($mode=="asal"){
    // membuat nilai awal..
		$kota	 = $HTTP_GET_VARS['kota_asal'];
		$asal	 = $HTTP_GET_VARS['asal'];
		
		$disabled	= ($asal=="")?"":"disabled";
		
		$oasal = OptionAsal($kota,$asal);
		 
		echo("<select onchange='getUpdateTujuan(this.value);' id='asal' name='asal' $disabled>$oasal</select><span id='progress_asal' style='display:none;'><img src='./templates/images/loading.gif' /></span>");

		exit;
	}
	//TGL ===========================================================================================================================================================================================
	elseif($mode=="tujuan"){
    // membuat nilai awal..
		$asal		= $HTTP_GET_VARS['asal'];
		$tujuan	= $HTTP_GET_VARS['tujuan'];
		
		$disabled	= ($tujuan=="")?"":"disabled";
		
		$ojur = OptionJurusan($asal,$tujuan);
		  
		echo("<select onchange='getUpdateJadwal(this.value);' id='tujuan' name='tujuan' $disabled>$ojur</select><span id='progress_tujuan' style='display:none;'><img src='./templates/images/loading.gif' /></span>");
		exit;
	}
	// JAM ===========================================================================================================================================================================================
	elseif($mode=="jam"){
	  // memilih jam yang tersedia sesuai dengan rute yang dimasukan
	  $tgl 				= $HTTP_GET_VARS['tgl'];
		$id_jurusan = $HTTP_GET_VARS['id_jurusan'];
		$jadwal 		= $HTTP_GET_VARS['jadwal'];
	  $ojam 			= OptionJadwal($tgl,$id_jurusan,$jadwal);
		
		$disabled	= ($jadwal=="")?"":"disabled";
		
	  echo("<select onchange='getMemo();' name='jadwal' id='jadwal' $disabled>$ojam</select><span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span></td></tr>");   
	  	
	  exit;
	}
	elseif ($mode=='add'){
		// add 
		
		$pesan = $HTTP_GET_VARS['pesan'];
		
		if($pesan==1){
			$pesan="<font color='green' size=3>Data Berhasil Disimpan!</font>";
			$bgcolor_pesan="98e46f";
		}
		
		$template->set_filenames(array('body' => 'memo/add_body.tpl')); 
		$template->assign_vars(array(
		 'BCRUMP'		=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_memo.'.$phpEx).'">Memo</a> | <a href="'.append_sid('pengaturan_memo.'.$phpEx."?mode=add").'">Tambah Memo</a> ',
		 'JUDUL'		=>'Tambah Memo',
		 'MODE'   	=> 'save',
		 'SUB'    	=> '0',
		 'PESAN'		=> $pesan,
		 'BGCOLOR_PESAN'		=> $bgcolor_pesan,
		 'OPT_KOTA'			=> setComboKota("BANDUNG"),
		 'KOTA_ASAL'		=> "BANDUNG",
		 'TGL_BERANGKAT'=> dateD_M_Y(),
		 'U_MEMO_ADD_ACT'	=> append_sid('pengaturan_memo.'.$phpEx)
		 )
		);
	} 
	else if ($mode=='save'){
		// aksi menambah memo
		$tgl_berangkat				= $HTTP_POST_VARS['p_tgl_user'];
		$tgl_berangkat_mysql	= FormatTglToMySQLDate($tgl_berangkat);
		$kode_jadwal					= $HTTP_POST_VARS['jadwal'];
		$memo   							= $HTTP_POST_VARS['memo'];
		
		$Reservasi						= new Reservasi();
		$Jadwal								= new Jadwal();
		$PenjadwalanKendaraan	= new PenjadwalanKendaraan();
		
		$terjadi_error=false;
		
		if($Memo->tambahMemo($tgl_berangkat_mysql,$kode_jadwal,$memo,$userdata['user_id'],$Jadwal,$Reservasi,$PenjadwalanKendaraan)){
			redirect(append_sid('pengaturan_memo.'.$phpEx)."&mode=edit&tgl_berangkat=$tgl_berangkat&kode_jadwal=$kode_jadwal",true); 
		}
		
	} 
	else if ($mode=='edit'){
		// edit
		
		$id 						= $HTTP_GET_VARS['id'];
		$tgl_berangkat 	= FormatTglToMysqlDate($HTTP_GET_VARS['tgl_berangkat']);
		$kode_jadwal 		= $HTTP_GET_VARS['kode_jadwal'];
		
		if($id!=""){
			$row	= $Memo->ambilDataDetailById($id);
		}
		else{
			$row						= $Memo->ambilDataDetail($tgl_berangkat,$kode_jadwal);
			$pesan					= "<font size=3 color='green'>Proses penyimpanan berhasil!</font>";
			$bgcolor_pesan	= "98e46f";
			$id							= $row['ID'];
		}
		
		$Jadwal	= new Jadwal();
		$Cabang	= new Cabang();
		
		$data_jadwal	= $Jadwal->ambilDataDetail($row['KodeJadwal']);
		$data_cabang	= $Cabang->ambilDataDetail($data_jadwal['Asal']);
		
		$template->set_filenames(array('body' => 'memo/add_body.tpl')); 
		$template->assign_vars(array(
			 'BCRUMP'					=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_memo.'.$phpEx).'">Memo</a> | <a href="'.append_sid('pengaturan_memo.'.$phpEx."?mode=edit&id=$id").'">Ubah Memo</a> ',
			 'JUDUL'					=>'Ubah Memo',
			 'MODE'   				=> 'save',
			 'SUB'    				=> '1',
			 'TGL_BERANGKAT'	=> FormatMysqlDateToTgl($row['TglBerangkat']),
			 'OPT_KOTA'				=> setComboKota($data_cabang['Kota']),
			 'KOTA_ASAL'			=> $data_cabang['Kota'],
			 'ASAL'						=> $data_jadwal['Asal'],
			 'TUJUAN'					=> $data_jadwal['IdJurusan'],
			 'JADWAL'					=> $data_jadwal['KodeJadwal'],
			 'MEMO'						=> $row['Memo'],
			 'PEMBUAT'				=> $row['NamaPembuat'],
			 'WAKTU_BUAT'			=> dateparse(FormatMySQLDateToTglWithTime($row['WaktuBuatMemo'])),
			 'DISABLED'				=> "disabled",
			 'PESAN'					=> $pesan,
			 'BGCOLOR_PESAN'	=> $bgcolor_pesan,
			 'U_MEMO_ADD_ACT'	=>append_sid('pengaturan_memo.'.$phpEx)
			 )
		);
	} 
	else if ($mode=='getmemo'){
		// edit
		
		$tgl_berangkat 	= FormatTglToMysqlDate($HTTP_GET_VARS['tgl_berangkat']);
		$kode_jadwal 		= $HTTP_GET_VARS['kode_jadwal'];
		
		$row				= $Memo->ambilDataDetail($tgl_berangkat,$kode_jadwal);
		$flag_memo	= $row['FlagMemo'];
		
		if($flag_memo!=""){
			echo("
				document.getElementById('memo').value='".$row['Memo']."';
				document.getElementById('nama_pembuat').innerHTML	='".$row['NamaPembuat']."';
				document.getElementById('waktu_pembuat').innerHTML	='".FormatMySQLDateToTglWithTime($row['WaktuBuatMemo'])."';
			");
		}
		else{
			echo("
				document.getElementById('memo').value='';
				document.getElementById('nama_pembuat').innerHTML	='';
				document.getElementById('waktu_pembuat').innerHTML	='';
			");
		}
		
		exit;
	} 
	else if ($mode=='delete'){
		// aksi hapus memo
		$list_memo = str_replace("\'","'",$HTTP_GET_VARS['list_memo']);
		//echo($list_memo. " asli :".$HTTP_GET_VARS['list_memo']);
		$Memo->hapus($list_memo);
		
		exit;
	} 
	else {
		// LIST
		$template->set_filenames(array('body' => 'memo/memo_body.tpl')); 
		
		if($HTTP_POST_VARS["txt_cari"]!=""){
			$cari=$HTTP_POST_VARS["txt_cari"];
		}
		else{
			$cari=$HTTP_GET_VARS["cari"];
		}
		
		$kondisi	=($cari=="")?" WHERE 1 ":
			" WHERE KodeMemo LIKE '%$cari%' 
				OR JudulMemo LIKE '%$cari%' 
				OR Memo LIKE '%$cari%'";
		
		$kondisi	= $kondisi. " AND Memo!=''";
		
		//PAGING======================================================
		$idx_page = ($HTTP_GET_VARS['page']!='')?$HTTP_GET_VARS['page']:0;
		$paging=pagingData($idx_page,"ID","tbl_posisi","&cari=$cari",$kondisi,"pengaturan_memo.php",$VIEW_PER_PAGE,$PAGE_PER_SECTION,$idx_awal_record);
		//END PAGING======================================================
		
		$sql = 
			"SELECT ID,Memo,f_user_get_nama_by_userid(PembuatMemo) AS PembuatMemo,WaktuBuatMemo,JamBerangkat,KodeJadwal,TglBerangkat
			FROM tbl_posisi $kondisi 
			ORDER BY TglBerangkat DESC,JamBerangkat DESC LIMIT $idx_awal_record,$VIEW_PER_PAGE";
		
		$idx_check=0;
		
		
		if ($result = $db->sql_query($sql)){
			$i = $idx_page*$VIEW_PER_PAGE+1;
		  while ($row = $db->sql_fetchrow($result)){
				$odd ='odd';
				
				if (($i % 2)==0){
					$odd = 'even';
				}
				
				$idx_check++;
				
				$check="<input type='checkbox' id='checked_$idx_check' name='checked_$idx_check' value=\"'$row[0]'\"/>";
				
				$act 	="<a href='".append_sid('pengaturan_memo.'.$phpEx.'?mode=edit&id='.$row[0])."'>Edit</a> + ";
				$act .="<a  href='' onclick='return hapusData(\"$row[0]\");'>Delete</a>";
				$template->
					assign_block_vars(
						'ROW',
						array(
							'odd'			=>$odd,
							'check'		=>$check,
							'no'			=>$i,
							'tanggal'	=>dateparse(FormatMySQLDateToTgl($row['TglBerangkat'])),
							'kode_jadwal'	=>$row['KodeJadwal'],
							'jam_berangkat'	=>substr($row['JamBerangkat'],0,5),
							'memo'=>$row['Memo'],
							'pembuat'	=>$row['PembuatMemo']."@".dateparse(FormatMySQLDateToTglWithTime($row['WaktuBuatMemo'])),
							'action'=>$act
						)
					);
				
				$i++;
		  }
			
			if($i-1<=0){
				$no_data	=	"<tr><td colspan=9 class='yellow' align='center'><font size=3><b>Tidak ada data ditemukan</b></font></td></tr>";
			}
		} 
		else{
			//die_error('Cannot Load memo',__FILE__,__LINE__,$sql);
			echo("Error :".__LINE__);exit;
		} 
		
		$template->assign_vars(array(
			'BCRUMP'    		=> '<a href="'.append_sid('main.'.$phpEx) .'">Home</a> | <a href="'.append_sid('pengaturan_memo.'.$phpEx).'">Memo</a>',
			'U_ADD'		=> append_sid('pengaturan_memo.'.$phpEx.'?mode=add'),
			'ACTION_CARI'		=> append_sid('pengaturan_memo.'.$phpEx),
			'TXT_CARI'			=> $cari,
			'NO_DATA'				=> $no_data,
			'PAGING'				=> $paging
			)
		);
		
	}      

include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>