<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX["ADMIN"],$USER_LEVEL_INDEX["MANAJEMEN"],$USER_LEVEL_INDEX["MANAJER"],$USER_LEVEL_INDEX["KEUANGAN"]))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

$tanggal_mulai  = isset($HTTP_GET_VARS['tanggal_mulai'])? $HTTP_GET_VARS['tanggal_mulai'] : $HTTP_POST_VARS['tanggal_mulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tanggal_akhir'])? $HTTP_GET_VARS['tanggal_akhir'] : $HTTP_POST_VARS['tanggal_akhir'];
$kota_dipilih 	= isset($HTTP_GET_VARS['kota'])? $HTTP_GET_VARS['kota'] : $HTTP_POST_VARS['kota'];
$sort_by				= isset($HTTP_GET_VARS['sort_by'])? $HTTP_GET_VARS['sort_by'] : $HTTP_POST_VARS['sort_by'];
$order					= isset($HTTP_GET_VARS['order'])? $HTTP_GET_VARS['order'] : $HTTP_POST_VARS['order'];

if($HTTP_POST_VARS["txt_cari"]!=""){
	$cari=$HTTP_POST_VARS["txt_cari"];
}
else{
	$cari=$HTTP_GET_VARS["cari"];
}

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();
$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);
$kota_dipilih	= ($kota_dipilih!='')?$kota_dipilih:"JAKARTA";
		
$kondisi_cari	=($cari=="")?
	"":
	" AND (f_sopir_get_nama_by_id(tbo.KodeSopir) LIKE '$cari%'
					OR f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) LIKE '%$cari%'
					OR tbo.NoSPJ LIKE '%$cari%'
					OR f_user_get_nama_by_userid(IdPetugas) LIKE '%$cari%'
					OR CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) LIKE  '%$cari%'
				)";
	
$order	=($order=='')?"ASC":$order;
	
$sort_by =($sort_by=='')?"tbo.TglTransaksi":$sort_by;

$sum_total	= 0;

//QUERY
$sql	=
	"SELECT
		tbo.TglTransaksi,
		CONCAT(f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan)),'-',f_cabang_get_name_by_kode(f_jurusan_get_kode_cabang_tujuan_by_jurusan(tbo.IdJurusan))) AS Jurusan,
		ts.JamBerangkat,
		tbo.NoSPJ,
		f_kendaraan_ambil_nopol_by_kode(tbo.NoPolisi) AS NoKendaraan,
		f_sopir_get_nama_by_id(tbo.KodeSopir) AS NamaSopir,
		Jumlah,
		f_user_get_nama_by_userid(IdPetugas) AS Pencetak
	FROM (tbl_biaya_op tbo INNER JOIN tbl_spj ts ON tbo.NoSPJ=ts.NoSPJ)
	WHERE FlagJenisBiaya=$FLAG_BIAYA_VOUCHER_BBM
		AND (tbo.TglTransaksi BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql')
		AND (SELECT Kota FROM tbl_md_cabang WHERE KodeCabang = f_jurusan_get_kode_cabang_asal_by_jurusan(tbo.IdJurusan))='$kota_dipilih'
		$kondisi_cari
	ORDER BY $sort_by $order";
	
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
#$pdf=new PDF('P','mm','A4');
$pdf=new PDF('L','mm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(10,10,10,10);
$pdf->SetFont('courier','',10);

$tgl_cetak	=	date("d-m-Y");

//HEADER 
$pdf->Image('templates/images/logo_small.png',10,10,40);
$pdf->Ln(15);
$pdf->SetFont('courier','B',20);
$pdf->Cell(40,8,'Laporan Biaya BBM menggunakan Voucher '.$kota_dipilih,'',0,'L');$pdf->Ln();
$pdf->SetFont('courier','',10);
$pdf->Cell(20,4,'Periode','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(35,4,dateparseD_Y_M($tanggal_mulai).' s/d ','',0,'');$pdf->Cell(40,4,dateparseD_Y_M($tanggal_akhir),'',0,'');$pdf->Ln();
$pdf->Cell(20,4,'Tgl Cetak','',0,'L');$pdf->Cell(5,4,':','',0,'');$pdf->Cell(15,4,dateparseD_Y_M($tgl_cetak),'',0,'');$pdf->Ln();
$pdf->Ln(4);

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(5,5,'#','B',0,'C',1);
$pdf->Cell(25,5,'Tgl','B',0,'C',1);
$pdf->Cell(60,5,'Jrsn','B',0,'C',1);
$pdf->Cell(15,5,'Jam','B',0,'C',1);
$pdf->Cell(40,5,'#SPJ','B',0,'C',1);
$pdf->Cell(20,5,'Kndrn','B',0,'C',1);
$pdf->Cell(50,5,'Sopir','B',0,'C',1);
$pdf->Cell(20,5,'Jumlah','B',0,'C',1);
$pdf->Cell(50,5,'Pencetak','B',0,'C',1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('courier','',10);
$pdf->SetTextColor(0);
//CONTENT

if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$pdf->Cell(5,5,$i,'',0,'C');
		$pdf->Cell(25,5,dateparseD_Y_M(FormatMySQLDateToTgl($row['TglTransaksi'])),'',0,'L');
		$pdf->Cell(60,5,$row['Jurusan'],'',0,'C');
		$pdf->Cell(15,5,substr($row['JamBerangkat'],0,-3),'',0,'C');
		$pdf->Cell(40,5,$row['NoSPJ'],'',0,'L');
		$pdf->Cell(20,5,$row['NoKendaraan'],'',0,'L');
		$pdf->Cell(50,5,$row['NamaSopir'],'',0,'L');
		$pdf->Cell(20,5,number_format($row['Jumlah'],0,",","."),'',0,'R');
		$pdf->Cell(50,5,$row['Pencetak'],'',0,'C');
		$pdf->Ln();
		$pdf->Cell(285,1,'','B',0,'');
		$pdf->Ln(1);
		$i++;
		
		$sum_total	+=$row['Jumlah'];
  }
} 
else{
	//die_error('Cannot Load laporan_omzet_cabang',__FILE__,__LINE__,$sql);
	echo("Error:".__LINE__);exit;
} 

$pdf->SetFont('courier','B',10);
$pdf->SetTextColor(255);
$pdf->Cell(215,5,'Total ','B',0,'R',1);
$pdf->Cell(20,5,number_format($sum_total,0,",","."),'B',0,'R',1);
$pdf->Cell(50,5,'','B',0,'R',1);

										
$pdf->Output();
						
?>