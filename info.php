<?php
//
// Info Page
//

// STANDAR
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION 
$userdata = session_pagestart($user_ip,200);
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] )
{  
  redirect(append_sid('index.'.$phpEx),true); 
}

// HEADER
include($adp_root_path . 'includes/page_header.php');

$mode = $HTTP_GET_VARS['mode'];

if ($mode=='about')
{	
	$template->set_filenames(array('body' => 'about_body.tpl')); // about 
	$template->assign_vars  (array('BCRUMP' =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('info.'.$phpEx.'?mode=about').'">About</a>'));
} else
{	
	$template->set_filenames(array('body' => 'info_body.tpl')); // info (ato help kali ya?)
	$template->assign_vars  (array('BCRUMP' =>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('info.'.$phpEx.'?mode=info').'">Info</a>'));
}	

// VARS
// $template->assign_vars(array('USERNAME'  =>$userdata['username']));

// PARSE
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>