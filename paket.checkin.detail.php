<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$mode = $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode

if($mode=="docheckin"){
	$list_no_tiket	= str_replace("\'","'",$HTTP_POST_VARS['listnotiket']);
	
	include($adp_root_path . 'ClassPaketEkspedisi.php');
	$Paket = new Paket();
	
	$Paket->checkInPaket($list_no_tiket);
	exit;
}

$no_spj						= $HTTP_GET_VARS['no_spj'];
$kode_driver			= $HTTP_GET_VARS['kode_driver'];
$driver						= $HTTP_GET_VARS['driver'];
$no_polisi				= $HTTP_GET_VARS['no_polisi'];
$tgl_berangkat		= $HTTP_GET_VARS['tgl_berangkat'];
$kode_jadwal			= $HTTP_GET_VARS['kode_jadwal'];
$waktu_berangkat	= $HTTP_GET_VARS['waktu_berangkat'];

//$is_today  		= isset($HTTP_GET_VARS['is_today'])? $HTTP_GET_VARS['is_today'] : $HTTP_POST_VARS['is_today'];
$tanggal_mulai  = isset($HTTP_GET_VARS['tglmulai'])? $HTTP_GET_VARS['tglmulai'] : $HTTP_POST_VARS['tglmulai'];
$param_filter  	= isset($HTTP_GET_VARS['paramfilter'])? $HTTP_GET_VARS['paramfilter'] : $HTTP_POST_VARS['paramfilter'];
$jenis_laporan	= isset($HTTP_GET_VARS['tipe'])? $HTTP_GET_VARS['tipe'] : $HTTP_POST_VARS['tipe'];
$keterangan			= isset($HTTP_GET_VARS['ket'])? $HTTP_GET_VARS['ket'] : $HTTP_POST_VARS['ket'];

// LIST
$template->set_filenames(array('body' => 'paket.checkin/paket.checkin.detail.tpl')); 

//$tbl_reservasi	= $is_today=="1"?"tbl_reservasi":"tbl_reservasi_olap";

$sql	= 
	"SELECT
		NoTiket,NamaPengirim,AlamatPengirim,TelpPengirim,
		NamaPenerima,AlamatPenerima,TelpPenerima,
		Berat,HargaPaket,Diskon,TotalBayar,
		Dimensi,JenisLayanan,JenisPembayaran,IsSampai,WaktuSampai,Nama
	FROM tbl_paket LEFT JOIN tbl_user ON UserCheckIn=user_id
	WHERE DATE(TglBerangkat)=DATE('$tgl_berangkat') AND KodeJadwal='$kode_jadwal' AND CetakTiket=1 AND FlagBatal!=1";
	


if(!$result = $db->sql_query($sql)){
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
}

$i = 1;

while ($row = $db->sql_fetchrow($result)){
	$odd ='odd';
	
	if (($i % 2)==0){
		$odd = 'even';
	}
	
	if($row['IsSampai']!=1){
		$check			= "style='display:block;'";
		$img_check	= "";
		$remark			= "";
	}
	else{
		$check			= "style='display:none;'";
		$img_check	= "<img src='./templates/images/check.gif' />";
		$remark			= "Dicatat tiba pada:".FormatMySQLDateToTglWithTime($row['WaktuSampai'])."<br>
							Dicatat Oleh:".$row['Nama'];
	}
	
	
	$template->
		assign_block_vars(
			'ROW',
			array(
				'odd'							=>$odd,
				'check'						=>$img_check."<input $check type='checkbox' id='checked_$i' name='checked_$i' value=\"'".$row['NoTiket']."'\"/>",
				'no_tiket'				=>$row['NoTiket'],
				'nama_pengirim'		=>$row['NamaPengirim'],
				'alamat_pengirim'	=>$row['AlamatPengirim'],
				'telp_pengirim'		=>$row['TelpPengirim'],
				'nama_penerima'		=>$row['NamaPenerima'],
				'alamat_penerima'	=>$row['AlamatPenerima'],
				'telp_penerima'		=>$row['TelpPenerima'],
				'berat'						=>$row['Berat'],
				'harga'						=>number_format($row['HargaPaket'],0,",","."),
				'diskon'					=>number_format($row['Diskon'],0,",","."),
				'bayar'						=>number_format($row['TotalBayar'],0,",","."),
				'layanan'					=>$row['Layanan'],
				'jenis_bayar'			=>$row['JenisPembayaran']==0?"TUNAI":"LANGGANAN",
				'remark'					=> $remark
			)
		);
	
	$i++;
}



//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&p0=".$tanggal_mulai."&p1=".$tanggal_akhir."&p2=".$pencari."&p3=".$jenis_laporan."";
	
$script_cetak_pdf="Start('laporan_rekap_tiket_detail_cetak_pdf.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
												
$script_cetak_excel="Start('laporan_rekap_tiket_detail_cetak_excel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'KETERANGAN'		=> $jenis_laporan." ".$keterangan,
	'NO_MANIFEST'	=> $no_spj,
	'BERANGKAT'		=> $waktu_berangkat,
	'KODE_JADWAL'	=> $kode_jadwal,
	'DRIVER'			=> $driver,
	'NO_POLISI'		=> $no_polisi,
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>