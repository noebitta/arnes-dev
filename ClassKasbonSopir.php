<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
	exit;
}
//#############################################################################

class KasbonSopir{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function KasbonSopir(){
		$this->ID_FILE="C-KBS";
	}
	
	//BODY
	
	function tambah($TglTransaksi,$KodeSopir,$Jumlah,$IdKasir,$DiberikanDiCabang){
	  
		/*
		ID	: tambah
		IS	: data kasbon belum tersimpan dalam database
		FS	:Data kasbon baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"INSERT INTO 
				tbl_kasbon_sopir(TglTransaksi,KodeSopir,Jumlah,IdKasir,DiberikanDiCabang,WaktuCatatTransaksi) 
			VALUES(
				'$TglTransaksi','$KodeSopir','$Jumlah','$IdKasir','$DiberikanDiCabang',NOW())";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function batalkan($id){
	  
		/*
		ID	: batalkan
		IS	: data kasbon sudah ada dalam database
		FS	:Flag IsBatal diubah
		*/
		
		//kamus
		global $db,$userdata;
		
		//MENGUBAH DATA KEDALAM DATABASE
		$sql =
			"UPDATE tbl_kasbon_sopir SET
				IsBatal=1, WaktuBatal=NOW(),Pembatal=".$userdata['user_id']." 
			WHERE ID=$id;";
								
		if (!$db->sql_query($sql)){
			//return false;
			die_error("Err:$this->ID_FILE $sql".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		ID	:003
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *
			FROM tbl_kasbon_sopir
			WHERE 
				KodeCabang LIKE '$pencari' 
				OR Nama LIKE '%$pencari%' 
				OR Alamat LIKE '%$pencari%'
				OR Telp LIKE '$pencari'
				OR Kota LIKE '$pencari'
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			//die_error("Gagal $this->ID_FILE 003");
			echo("Err:". __LINE__);
		}
		
	}//  END ambilData
	
	function hapus($list_id){
	  
		/*
		ID	: 005
		IS	: data Cabang sudah ada dalam database
		FS	:Data Cabang dihapus
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"DELETE FROM tbl_kasbon
			WHERE ID IN($list_id);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($kode=""){
		
		/*
		ID	:007
		Desc	:Mengembalikan data member sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT *
			FROM tbl_kasbon_sopir
			WHERE Id='$kode';";
		
		if ($result = $db->sql_query($sql,TRUE)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			$error	= $db->sql_error();
			die_error("Err:$this->ID_FILE ".__LINE__);
		}
		
	}//  END ambilData
	
}
?>