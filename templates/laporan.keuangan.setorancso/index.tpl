<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<!-- HEADER -->
<div class="banner">
	<div class="bannerjudul">&nbsp;Laporan Setoran CSO</div>
	<div class="bannercari">
		<form action="{ACTION_CARI}" method="post">
			Status:&nbsp;<select id="status" name="status"><option value="" {SEL_STATUS}>-semua-</option><option value="0" {SEL_STATUS0}>Belum Setor</option><option value="1" {SEL_STATUS1}>Sudah Setor</option></select>
			&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
			&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
			&nbsp;Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" placeholder="nama cso,no resi"/>&nbsp;	
			<input type="submit" value="cari" />&nbsp;
		</form>
	</div>
</div>
<br>
<!--<center><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></center>-->
<br>
<!-- HEADER END -->

<table width="100%" cellspacing="1" cellpadding="1">
	<tr>
		<th width="50" rowspan="2">No.</th>
		<th width="150" rowspan="2">CSO</th>
		<th width="150" rowspan="2">Waktu</th>
		<th width="200" rowspan="2">#Resi</th>
		<th class="thintop" colspan="3">Penumpang</th>
		<th class="thintop" colspan="2">Paket</th>
		<th width="150" rowspan="2">Biaya Op.</th>
		<th width="150" rowspan="2">Total Setor</th>
		<th width="200" rowspan="2">Act</th>
	</tr>
	<tr>
		<th class="thintop" width="100">Qty</th>
		<th class="thintop" width="100">Diskon</th>
		<th class="thintop" width="100">Setoran</th>
		<th class="thintop" width="100">Qty</th>
		<th class="thintop" width="100">Setoran</th>
  </tr>
  <!-- BEGIN ROW -->
  <tr class="{ROW.odd}">
		<td align="center">{ROW.no}</td>
		<td align="center">{ROW.cso}</td>
		<td align="center">{ROW.waktu_setor}</td>
		<td align="center">{ROW.no_resi}</td>
		<td align="right">{ROW.jum_pnp}</div></td>
		<td align="right">{ROW.disc_pnp}</div></td>
		<td align="right">{ROW.omz_pnp}</div></td>
		<td align="right">{ROW.jum_pkt}</div></td>
		<td align="right">{ROW.omz_pkt}</div></td>
		<td align="right">{ROW.biaya}</div></td>
		<td align="right">{ROW.total_setor}</div></td>
		<td align="center">{ROW.act}</div></td>
  </tr>
  <!-- END ROW -->
</table>
<!-- BEGIN NO_DATA -->
<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
<!-- END NO_DATA -->