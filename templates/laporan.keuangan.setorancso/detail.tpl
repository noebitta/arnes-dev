<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript">
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<div class="banner"><div class="bannerjudul">&nbsp;Detail Rekap Transaksi</div></div>
<br>
<h2>Detail Tiket</h2>
<table width="100%" cellspacing="1" cellpadding="1">
	<tr>
		<th width=30>No</th>
		<th width=200>Waktu Pesan</th>
		<th width=200>No.Tiket</th>
		<th width=200>Waktu Berangkat</th>
		<th width=100>Kode Jadwal</th>
		<th width=200>Nama</th>
		<th width=100>Telp</th>
		<th width=50>Kursi</th>
		<th width=100>Harga Tiket</th>
		<th width=100>Discount</th>
		<th width=70>Total</th>
		<th width=100>Tipe Disc.</th>
		<th width=200>CSO</th>
		<th width=100>Status</th>
		<th width=100>Ket.</th>
	</tr>
	<!-- BEGIN ROW -->
	<tr class="{ROW.odd}">
		<td align="center">{ROW.no}</td>
		<td align="center">{ROW.waktu_pesan}</td>
		<td align="center">{ROW.no_tiket}</td>
		<td align="center">{ROW.waktu_berangkat}</td>
		<td align="center">{ROW.kode_jadwal}</td>
		<td align="center">{ROW.nama}</td>
		<td align="center">{ROW.telp}</td>
		<td align="center">{ROW.no_kursi}</td>
		<td align="right">{ROW.harga_tiket}</td>
		<td align="right">{ROW.discount}</td>
		<td align="right">{ROW.total}</td>
		<td align="center">{ROW.tipe_discount}</td>
		<td align="center">{ROW.cso}</td>
		<td align="center">{ROW.status}</td>
		<td align="center">{ROW.ket}</td>
	</tr>  
	<!-- END ROW -->
</table>
<!-- BEGIN NO_DATA_TIKET -->
<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
<!-- END NO_DATA_TIKET -->
<hr>

<h2>Detail Paket</h2>
<table width="100%" cellspacing="1" cellpadding="1">
	<tr>
		<th width=30>No</th>
		<th width=200>Waktu Berangkat</th>
		<th width=200>No.Tiket</th>
		<th width=100>Kode Jadwal</th>
		<th width=200>Dari</th>
		<th width=200>Untuk</th>
		<th width=100>Harga Paket</th>
		<th width=200>CSO</th>
		<th width=100>Status</th>
		<th width=100>Ket.</th>
  </tr>
  <!-- BEGIN ROWPAKET -->
	<tr class="{ROWPAKET.odd}">
		<td align="center">{ROWPAKET.no}</td>
		<td align="center">{ROWPAKET.waktu_berangkat}</td>
		<td align="center">{ROWPAKET.no_tiket}</td>
		<td align="center">{ROWPAKET.kode_jadwal}</td>
		<td align="center">{ROWPAKET.dari}</td>
		<td align="center">{ROWPAKET.untuk}</td>
		<td align="right">{ROWPAKET.harga_paket}</td>
		<td align="center">{ROWPAKET.cso}</td>
		<td align="center">{ROWPAKET.status}</td>
		<td align="center">{ROWPAKET.ket}</td>
	</tr>  
  <!-- END ROWPAKET -->
</table>
<!-- BEGIN NO_DATA_PAKET -->
<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
<!-- END NO_DATA_PAKET -->
<hr>
	
<h2>Detail Biaya</h2>
<table width="100%" cellspacing="1" cellpadding="1">
	<tr>
		<th width=30>No</th>
		<th width=200>Waktu Trx</th>
		<th width=150>#Jadwal</th>
		<th width=150>Waktu Berangkat</th>
		<th width=150>#Manifest</th>
		<th width=150>Jenis Biaya</th>
		<th width=200>Sopir</th>
		<th width=200>Unit</th>
		<th width=100>Jumlah</th>
		<th width=200>Ket.</th>
  </tr>
  <!-- BEGIN ROWBIAYA -->
	<tr class="{ROWBIAYA.odd}">
		<td align="center">{ROWBIAYA.no}</td>
		<td align="center">{ROWBIAYA.waktu_transaksi}</td>
		<td align="center">{ROWBIAYA.kode_jadwal}</td>
		<td align="center">{ROWBIAYA.waktu_berangkat}</td>
		<td align="center">{ROWBIAYA.nospj}</td>
		<td align="center">{ROWBIAYA.jenis_biaya}</td>
		<td align="center">{ROWBIAYA.sopir}</td>
		<td align="center">{ROWBIAYA.unit}</td>
		<td align="right">{ROWBIAYA.jumlah}</td>
		<td align="center">{ROWBIAYA.ket}</td>
	</tr>  
  <!-- END ROWBIAYA -->
</table>
<!-- BEGIN NO_DATA_BIAYA -->
<div class="yellow" style="font-size: 16px;">Tidak ada data ditemukan</div>
<!-- END NO_DATA_BIAYA -->
<hr>