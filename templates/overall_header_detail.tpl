<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
</head>  
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/lib/prototype.js"></script>
<script type='text/javascript' src='{ROOT}js/prototype.js'></script>
<script type='text/javascript' src='{ROOT}js/scriptaculous.js?load=effects'></script>
<script type='text/javascript' src='{ROOT}js/prototip.js'></script>
<link rel="stylesheet" type="text/css" href="{ROOT}css/prototip.css" />
<body>
<div align='center'>
	<div class="header_overall_image" style="width: 100%;"></div>
	<div class="header_shadow">&nbsp;</div>
