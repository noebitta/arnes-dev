<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	var idbaglobal;
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function showDialogBA(){
		idbaglobal="";

		new Ajax.Request("beritaacara.bnop.php?sid={SID}",
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=showdialog",
			onLoading: function(request) 
			{
			},
			onComplete: function(request) 
			{
			},
			onSuccess: function(request) 
			{
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
			},
			onFailure: function(request) 
			{
				 alert('Error !!! Cannot Save');        
				 assignError(request.responseText);
			}
		})
	}
	
	function proses(){

		kodeba			= document.getElementById('kodeba');
		penerima			= document.getElementById('penerima');
		jenisbiaya	= document.getElementById('jenisbiaya');
		jumlah			= document.getElementById('jumlah');
		keterangan	= document.getElementById('keterangan');

		valid=true;

		if(jumlah.value=='' || jumlah.value*1<=0){valid=false;jumlah.style.background="red";}
		if(penerima.value=='' || penerima.value*1<=0){valid=false;penerima.style.background="red";}
		if(keterangan.value==''){valid=false;keterangan.style.background="red";}
		
		if(!valid){
			exit;
		}
		
		new Ajax.Request("beritaacara.bnop.php?sid={SID}",
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=simpan"+
				"&kodeba="+kodeba.value+
				"&penerima="+penerima.value+
				"&jenisbiaya="+jenisbiaya.value+
				"&jumlah="+jumlah.value+
				"&keterangan="+keterangan.value+
				"&idba="+idbaglobal,
			onLoading: function(request){
				popup_loading.show();
				dlg_ba.hide();
			},
			onComplete: function(request){
				
			},
			onSuccess: function(request) 
			{
				
				popup_loading.hide();
				
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		})
	}
	
	function hapus(idba){
		
		
		if(!confirm("Apakah anda yakin akan menghapus data ini?")){
			exit;
		}
		
		new Ajax.Request("beritaacara.bop.php?sid={SID}", 
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=hapus"+
				"&idba="+idba,
			onLoading: function(request){
				popup_loading.show();
			},
			onComplete: function(request){
				
			},
			onSuccess: function(request) 
			{
				
				popup_loading.hide();
				
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		})
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","daftar_manifest.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","daftar_manifest.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	function setDataBA(idba){
		
		idbaglobal=idba;

		penerima			= document.getElementById('penerima');
		jenisbiaya	= document.getElementById('jenisbiaya');
		jumlah			= document.getElementById('jumlah');
		keterangan	= document.getElementById('keterangan');

		penerima.style.background="white";
		jumlah.style.background="white";
		keterangan.style.background="white";
		
		valid=true;
		
		new Ajax.Request("beritaacara.bnop.php?sid={SID}",
		{
			asynchronous: true,
			method: "post",
			parameters: "mode=getdataba&id="+idbaglobal,
			onLoading: function(request) 
			{
				popup_loading.show();
			},
			onComplete: function(request) 
			{
				popup_loading.hide();
			},
			onSuccess: function(request) 
			{
				var proses_ok=false;
				
				eval(request.responseText);
				
				if(!proses_ok){
					alert("Terjadi kegagalan!");
				}
				
			},
			onFailure: function(request) 
			{
				popup_loading.hide();
				alert('Error !!! Cannot Save');        
				assignError(request.responseText);
			}
		});
		
	}
	
	function setJenisBiaya(jenisbiaya){
		
		objjenisbiaya = document.getElementById("jenisbiaya");
		
		jumjenisbiaya=objjenisbiaya.length;
		jenisbiayas=objjenisbiaya.options;
			
		i=0;
		ketemu=false;
			
		while(i<jumjenisbiaya && !ketemu){
			if(jenisbiayas[i].value!=jenisbiaya){
				i++;
			}
			else{
				ketemu=true;
			}
		}
		
		objjenisbiaya.selectedIndex=i;
		
	}
	
	function init(e){
		
		//control dialog daftar sopir
		dlg_ba				= dojo.widget.byId("dlgberitaacara");
		
		popup_loading	= dojo.widget.byId("popuploading");
		
		getUpdateAsal("{KOTA}");
		getUpdateTujuan("{ASAL}");
		setSortId();
		
	}
	
	dojo.addOnLoad(init);
</script>

<!--BEGIN dialog buat BA-->
<div dojoType="dialog" id="dlgberitaacara" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
	<div class="dlgwrapper">
		<h3 style="padding-left: 10px;">BERITA ACARA BIAYA NON OPERASIONAL</h3>
		<div class="dlgcontent">
			<input type="hidden" id="kodeba">
			<span class="dlglabel"><b>Kode BA</b></span><span class="dlgfield"><b>: <span id="showkodeba"></span></b></span></br>
			<br>
			<span id="showtombol1" style="text-align: center;display: block;">
			</span><br/>
			
			<span id="detailmanifest" style="display: none;">
				<b>DATA BIAYA</b><br>
				<span class="dlglabel">Penerima</span><span class="dlgfield">: <input id="penerima" type="text" size="20" maxlength="10" onfocus="this.style.background='white';"/></span></br>
				<span class="dlglabel">Jenis Biaya</span><span class="dlgfield">: <select id="jenisbiaya"><option value="{JENIS_BIAYA_AQUA}">Biaya Aqua</option><option value="{JENIS_BIAYA_LISTRIK}">Biaya Listrik</option><option value="{JENIS_BIAYA_LAINNYA}">Biaya Lainnya</option></select></span></br>
				<span class="dlglabel">Jumlah (Rp.)</span><span class="dlgfield">: <input id="jumlah" type="text" size="20" maxlength="7" onfocus="this.style.background='white';" onkeypress="validasiAngka(event);" style="text-align: right;"/></span></br>
				<span class="dlglabel">Keterangan</span><span class="dlgfield">: <textarea id='keterangan' name="keterangan" onfocus="this.style.background='white';" cols="40" rows="3"></textarea></span></br>
				<br>
				<br><br>
				<span style="text-align: center;display: inline-block; width: 400px;">
					<input type="button" onclick="dlg_ba.hide();" value="BATAL" style="width: 100px;">
					&nbsp;&nbsp;&nbsp;<input type="button" onClick="proses();" value="PROSES" style="width: 100px;"><br><br>
				</span>
			</span>
		</div>
	</div>
<br>
</div>
<!--END dialog buat BA-->


<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;BA Biaya Non Operasional</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2 align='center' valign='middle'>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<div style="float: left;"><a href="#" onClick="showDialogBA();return false;"> + Buat Berita Acara</a></div>
					<div style="float: right;">{PAGING}</div>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table width='100%'>
			<tr style="display:{ROW.showheader};">
				<th class="thin" colspan="8">1:Berita Acara</th>
				<th class="thin" colspan="3">2:Release</th>
			</tr>
			<tr style="display:{ROW.showheader};">
				<th width=30>No</th>
				<th width=100><a class="th" id="sort1" href='#'>Penerima</a></th>
				<th width=100><a class="th" id="sort2" href='#'>Waktu</a></th>
				<th width=100><a class="th" id="sort3" href='#'>Oleh</a></th>
				<th width=100><a class="th" id="sort4" href='#'>Jenis Biaya</a></th>
				<th width=100><a class="th" id="sort5" href='#'>Jumlah</a></th>
				<th width=100><a class="th" id="sort6" href='#'>Keterangan</a></th>
				<th width=100><a class="th" id="sort7" href='#'>Releaser</a></th>
				<th width=100><a class="th" id="sort8" href='#'>Released</a></th>
				<th width=100><a class="th" href='#'>Act.</a></th>
			</tr>
			<!-- BEGIN ROW -->
			<tr class="{ROW.odd}">
				<td align="center">{ROW.no}</td>
				<td align="center">{ROW.penerima}</td>
				<td align="center">{ROW.waktubuat}</td>
				<td align="center">{ROW.dibuatoleh}</td>
				<td align="center">{ROW.jenisbiaya}</td>
				<td align="right">{ROW.jumlah}</td>
				<td align="center">{ROW.keterangan}</td>
				<td align="center">{ROW.releaser}</td>
				<td align="center">{ROW.waktureleased}</td>
				<td align="center">{ROW.act}</td>
     </tr>
     <!-- END ROW -->
    </table>
		{NO_DATA}
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>