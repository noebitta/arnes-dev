<script type="text/javascript" src="{TPL}js/main.js"></script>
<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "width=800,toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","reservasi.releaseinsentifsopir.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","reservasi.releaseinsentifsopir.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}	
	
	function bayarBO(id){
		
		isBayar = confirm("Anda akan melakukan pembayaran biaya operasional tambahan! Silahkan tekan OK untuk melanjutkan proses ini.");
		
		if(!isBayar) exit;
		
		new Ajax.Request("reservasi.releasebabnop.php?sid={SID}",{
    asynchronous: true,
    method: "post",
    parameters:
			"mode=bayar"+
			"&id="+id,
    onLoading: function(request){
			popup_loading.show();
    },
    onComplete: function(request){
			popup_loading.hide();
		},
    onSuccess: function(request) {
			var proses_ok=false;
				
			eval(request.responseText);
				
			if(!proses_ok){
				alert("Terjadi kegagalan!");
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
	}
	
	function init(e){	
		popup_loading	= dojo.widget.byId("popuploading");
		
		getUpdateAsal("{KOTA}");
		getUpdateTujuan("{ASAL}");
	}
	
	dojo.addOnLoad(init);
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;BA Biaya OP</td>
				<td align='right' valign='middle'>
					<table>
						<tr>
							<td class='bannernormal'>Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" /></td>	
							<td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>								
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan=2>
					<div style="float: right;">{PAGING}</div>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
		<table width='100%'>
			<tr style="display:{ROW.showheader};">
				<th class="thin" colspan="8">1:Berita Acara</th>
				<th class="thin" colspan="3">2:Release</th>
			</tr>
			<tr style="display:{ROW.showheader};">
				<th width=30>No</th>
				<th width=100><a class="th" id="sort1" href='#'>Penerima</a></th>
				<th width=100><a class="th" id="sort2" href='#'>Waktu</a></th>
				<th width=100><a class="th" id="sort3" href='#'>Oleh</a></th>
				<th width=100><a class="th" id="sort4" href='#'>Jenis Biaya</a></th>
				<th width=100><a class="th" id="sort5" href='#'>Jumlah</a></th>
				<th width=100><a class="th" id="sort6" href='#'>Keterangan</a></th>
				<th width=100><a class="th" id="sort7" href='#'>Releaser</a></th>
				<th width=100><a class="th" id="sort8" href='#'>Released</a></th>
				<th width=100><a class="th" href='#'>Act.</a></th>
			</tr>
			<!-- BEGIN ROW -->
			<tr class="{ROW.odd}">
				<td align="center">{ROW.no}</td>
				<td align="center">{ROW.penerima}</td>
				<td align="center">{ROW.waktubuat}</td>
				<td align="center">{ROW.dibuatoleh}</td>
				<td align="center">{ROW.jenisbiaya}</td>
				<td align="right">{ROW.jumlah}</td>
				<td align="center">{ROW.keterangan}</td>
				<td align="center">{ROW.releaser}</td>
				<td align="center">{ROW.waktureleased}</td>
				<td align="center">{ROW.act}</td>
     </tr>
     <!-- END ROW -->
    </table>
		{NO_DATA}
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>