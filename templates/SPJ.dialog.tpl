<input type="hidden" id="mobildipilih" name="mobildipilih" value="{MOBIL_DIPILIH}" />
<input type="hidden" id="sopirdipilih" name="sopirdipilih" value="{SOPIR_DIPILIH}" />
<input type="hidden" id="biayabbm" name="biayabbm" value="{BIAYA_BBM}" />
<input type="hidden" id="biayatol" name="biayatol" value="{BIAYA_TOL}"  />
<input type="hidden" id="biayasopir" name="biayasopir" value="{BIAYA_SOPIR}" />
<input type="hidden" id="biayaparkir" name="biayaparkir" value="{BIAYA_PARKIR}" />

<table bgcolor='white' width='100%'>
	<tr><td>Unit berangkat</td>
		<td>
			<input type="text" id="manflistmobil" name="manflistmobil" style="text-transform: uppercase;" size="30" onfocus="this.style.background='white';" onblur="setMobilManifest(this);{EVENT_HITUNG_BBM}"/>
			<span id="manflistmobilloading" style="display: none"><img src="./templates/images/loading.gif" alt="Working..." /></span>
			<div id="manflistmobilchoices" class="autocomplete"></div>
		</td>
	</tr>
	<tr><td>Sopir berangkat</td>
		<td>
			<input type="text" id="manflistsopir" name="manflistsopir" style="text-transform: uppercase;" size="30" onfocus="this.style.background='white';" onblur="setSopirManifest(this);checkBiaya();" />
			<span id="manflistsopirloading" style="display: none"><img src="./templates/images/loading.gif" alt="Working..." /></span>
			<div id="manflistsopirchoices" class="autocomplete"></div>
		</td>
	</tr>
	<!-- BEGIN show_tombol_cetak_voucher_bbm -->
	<tr><td>&nbsp;</td></tr>
	<tr align="center">
		<td colspan="2"><input type="button" value="Cetak Voucher BBM" onclick="cetakVoucherBBM('{NO_SPJ}');"></td>
	</tr>
	<!-- END show_tombol_cetak_voucher_bbm -->
	<tr><td colspan=2><br><h2>Biaya-biaya</h2></td></tr>
	<tr>
		<td>Biaya BBM</td>
		<td align='right'><span id="showbiayabbm">Rp. {BIAYA_BBM_SHOW}</span><span id='loadingbiayabbm' style='display:none;'><img src='./templates/images/loading.gif' />&nbsp;loading...</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>Biaya Tol</td>
		<td align='right'><span id="showbiayatol">Rp. {BIAYA_TOL_SHOW} </span><span id='loadingbiayatol' style='display:none;'><img src='./templates/images/loading.gif' />&nbsp;loading...</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>Biaya Sopir</td>
		<td align='right'><span id="showbiayasopir">Rp. {BIAYA_SOPIR_SHOW}</span><span id='loadingbiayasopir' style='display:none;'><img src='./templates/images/loading.gif' />&nbsp;loading...</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td>Biaya Parkir</td>
		<td align='right'>Rp. {BIAYA_PARKIR_SHOW}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<!-- BEGIN SHOW_VOUCHER_BBM -->
	<tr>
		<td colspan=2 align='center' height="20">
			<span id='loadingvoucherbbm' style='display:none;'><img src='./templates/images/loading.gif' />&nbsp;loading...</span>
			<span id="rewritevoucherbbm"></span>
		</td>
	</tr>
	<!-- END SHOW_VOUCHER_BBM -->
	<tr><td colspan=2 height=1 bgcolor='red'></td></tr>
	<tr>
		<td>Total Biaya</td>
		<td align='right'>Rp. <span id="biayatotal">{TOTAL_BIAYA}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br>
			<input type="button" id="dialog_SPJ_btn_Cancel" value="&nbsp;Cancel&nbsp;" onClick="dialog_SPJ.hide();">
			<input type="button" onclick="CetakSPJ();" id="btnok_dialogcetakulangvoucherbbm" value="&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;" {CETAK_DISABLED}>
		</td>
	</tr>
</table>