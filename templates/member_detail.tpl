<script type="text/javascript"> 
  djConfig = { isDebug: false };   // tidak memakai debug 
</script>

<script language="JavaScript">

function SetBlank(){
			id_member.value		='';
			nama.value				='';
			tempat_lahir.value='';
			no_ktp.value			='';
			alamat.value			='';
			kota.value				='';
			kodepos.value			='';
			telp_rumah.value	='';
			handphone.value		='';	
			email.value				='';	
			password.value		='';	
			konf_password.value	='';	
			tgl_register.value='{TANGGAL_REGISTRASI}';
			tgl_lahir.value		='';
			
}

function ValidasiAngka(objek){
	temp_nilai=objek.value*0;
	nama_objek=objek.name;
	
	if(temp_nilai!=0){
		alert(nama_objek+" harus angka!");
		objek.setFocus;exit;
	}
	
}

function SimpanMember(){
	
	//memeriksa apakah field nama  valid
	if(nama.value==''){
		alert("Nama tidak boleh kosong!");
		document.forms.f_data_anggota.nama.focus();
		return false;
	}
	
	//memeriksa apakah field no ktp valid
	if(no_ktp.value==''){
		alert("No KTP tidak boleh kosong!");
		document.forms.f_data_anggota.no_ktp.focus();
		return false;
	}
	
	//memeriksa apakah biaya sudah valid(berisi angka)
	
  if(aksi.value=='Penambahan'){
		//jika id_member kosong, berarti adalah proses penambahan data member baru
		mode="tambah_member";
		//memeriksa apakah field password  valid
		
		if(password.value==''){
			alert("password tidak boleh kosong!");
			document.forms.f_data_anggota.password.focus();
			return false;
		}
		
		//memeriksa apakah field konf password  valid
		if(konf_password.value==''){
			alert("Konfirmasi password tidak boleh kosong!");
			document.forms.f_data_anggota.konf_password.focus();
			return false;
		}
	}
	else{
		//jika id_member tidak kosong, berarti adalah proses pengubahan data member
		mode="ubah_member";
	}
	
	//memeriksa apakah field nama  valid
	if(konf_password.value!=password.value){
		alert("Konfirmasi password tidak sama dengan password!");
		document.forms.f_data_anggota.password.focus();
		password.value="";
		konf_password.value="";
		return false;
	}
	
		
	new Ajax.Request("member_detail.php?sid={SID}", 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"&tgl_register="+tgl_register.value+
			"&id_member_old="+hdn_id_member.value+
			"&nama="+nama.value+
			"&jenis_kelamin="+jenis_kelamin.value+
			"&kategori_member="+kategori_member.value+
			"&tempat_lahir="+tempat_lahir.value+
			"&tgl_lahir="+tgl_lahir.value+
			"&no_ktp="+no_ktp.value+
			"&alamat="+alamat.value+
			"&kota="+kota.value+
			"&kodepos="+kodepos.value+
			"&telp_rumah="+telp_rumah.value+
			"&handphone="+handphone.value+
			"&password="+password.value+
			"&konf_password="+konf_password.value+
			"&email="+email.value+
			"&mode="+mode,
    onLoading: function(request) 
    {
    },
    onComplete: function(request) 
    {
		},
    onSuccess: function(request) 
    {
      
			if(request.responseText=='berhasil'){
				if(aksi.value=='Penambahan'){
					SetBlank();
					alert("Data Member berhasil ditambahkan!");
				 //alert(request.responseText);
				}
				else{
					//kembali ke halaman member
					alert("Data Member berhasil disimpan!");
					document.location="member.php?sid="+sid.value;
				}
			}
			else if(request.responseText=='duplikasi'){
				alert("ID Member yang anda masukkan sudah digunakan oleh member lain!");
				document.forms.f_data_anggota.id_member.focus();
			}
			else if(request.responseText=='false password'){
				alert("Password yang anda masukkan tidak sesuai dengan konfirmasi password");
				document.forms.f_data_anggota.password.focus();
			}
			else{
				alert("Isian penulisan tanggal anda salah, silahkan periksa kembali");
				document.forms.f_data_anggota.tgl_lahir.focus();
			}

		},
    onFailure: function(request) 
    {
       
    }
  })      
}

function showDialogUbahSaldo(){
	this.open("member_penyesuaian_saldo.php?sid={SID}&id_member="+hdn_id_member.value,"CtrlWindow","top=200,left=200,width=600,height=400,scrollbars=1,resizable=0");	
}

function showDialogUbahPoin(){
	this.open("member_penyesuaian_poin.php?sid={SID}&id_member="+hdn_id_member.value,"CtrlWindow","top=200,left=200,width=600,height=400,scrollbars=1,resizable=0");	
}

// global
var DataMemberBtnSimpan,DataMemberBtnNo,tgl_sekarang;

function init(e) {
  // inisialisasi variabel
	//dialog data member_________________________
	DataMemberBtnSimpan = document.getElementById("DataMemberBtnSimpan");
	DataMemberBtnNo = document.getElementById("DataMemberBtnNo");
	
  aksi									=document.getElementById("aksi");
	tgl_register					=document.getElementById("tgl_register");
	hdn_id_member					=document.getElementById("hdn_id_member");
	nama									=document.getElementById("nama");
	jenis_kelamin					=document.getElementById("jenis_kelamin");
	kategori_member				=document.getElementById("kategori_member");
	tempat_lahir					=document.getElementById("tempat_lahir");
	tgl_lahir							=document.getElementById("tgl_lahir");
	no_ktp								=document.getElementById("no_ktp");
	alamat								=document.getElementById("alamat");
	kota									=document.getElementById("kota");
	kodepos								=document.getElementById("kodepos");
	telp_rumah						=document.getElementById("telp_rumah");
	handphone							=document.getElementById("handphone");
	email									=document.getElementById("email");
	password							=document.getElementById("password");
	konf_password					=document.getElementById("konf_password");
	sid										=document.getElementById("sid");
	
}

dojo.addOnLoad(init);



</script>
<input type='hidden' name='sid' id='sid' value='{SID}'>
<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer" >{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
		<!-- BODY -->
		<font><h2><u>{PESAN} Data Member</u></h2></font>
		
		<input type='hidden' name='aksi' id='aksi' value='{PESAN}'>
		<input type='hidden' name='hdn_id_member' id='hdn_id_member' value='{ID_MEMBER}'>
		<form id='f_data_anggota' name='f_data_anggota' method="post">		
		<table bgcolor='FFFFFF' width='100%'> <!-- table 2 kolom -->
			<tr>
				<td valign='top'align='center'>
					<table> <!-- tabel kolom 1-->
						<tr>
						  <td><strong>ID Member</strong></td>
							<td>:</td>
							<td>{ID_MEMBER}</td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Tgl Registrasi</td>
							<td>:</td>
							<td>
								<input type='text' id='tgl_register' value='{TANGGAL_REGISTRASI}' maxlength='10'/> (dd/mm/yyyy)
							</td>
						</tr>
						<tr>
						  <td class='kolomwajib'>Nama</td>
							<td>:</td>
							<td><input  name="nama" id="nama" type="text" maxlength='25' value='{NAMA}'></td>
						</tr>
						<tr>
						  <td>Jenis kelamin</td>
							<td>:</td>
						  <td>
								<select name="jenis_kelamin" id="jenis_kelamin">
									{OPT_JENIS_KELAMIN}
								</select>
							</td>
						</tr>
						<tr>
						  <td>Tempat lahir</td>
							<td>:</td>
							<td><input name="tempat_lahir" id="tempat_lahir" type="text" maxlength='20' value='{TEMPAT_LAHIR}'></td>
						</tr>
						<tr>
						  <td>Tgl Lahir</td>
						  <td>:</td>
						  <td>
								<input type='text' id='tgl_lahir' value='{TANGGAL_LAHIR}' maxlength='10'/> (dd/mm/yyyy)
							</td>
						</tr>
						<tr>
						  <td>Kategori Member</td>
						  <td>:</td>
						  <td>
								<select name="kategori_member" id="kategori_member">
									{OPT_KATEGORI_MEMBER}
								</select>
							</td>
						</tr>
						<tr>
						  <td colspan='3' height='20'></td>
						</tr>
						
					</table> <!--END tabel kolom 2-->
				</td>
				<td bgcolor='D0D0D0'><!--garis--></td>
				<td valign='top' align='center'>
					<table> <!--tabel kolom 2-->
						<tr>
						  <td><strong><u>No. KTP</u></strong></td>
							<td>:</td>
							<td><input name="no_ktp" id="no_ktp" type="text" maxlength='40' value='{NO_KTP}'></td>
						</tr>
						<tr>
						  <td>Alamat</td>
							<td>:</td>
							<td><textarea name='alamat' id='alamat' cols='30' rows='2' >{ALAMAT}</textarea></td>
						</tr>
						<tr>
						  <td>Kota</td>
							<td>:</td>
							<td><input name="kota" id="kota" type="text" maxlength='20' value='{KOTA}'></td>
						</tr>
						<tr>
						  <td>Kodepos</td>
							<td>:</td>
							<td><input name="kodepos" id="kodepos" type="text" maxlength='6' value='{KODEPOS}'></td>
						</tr>
						<tr>
						  <td>Telp rumah</td>
							<td>:</td>
							<td><input name="telp_rumah" id="telp_rumah" type="text" maxlength='20' value='{TELP_RUMAH}'></td>
						</tr>
						<tr>
						  <td>Handphone</td>
							<td>:</td>
							<td><input name="handphone" id="handphone" type="text" maxlength='20' value='{HANDPHONE}'></td>
						</tr>
						<tr>
						  <td>Email</td>
							<td>:</td>
							<td><input name="email" id="email" type="text" maxlength='50' value='{EMAIL}'></td>
						</tr>
						<tr>
						  <td colspan='3' height='20'></td>
						</tr>
						<tr>
						  <td class='red'>Password</td>
							<td>:</td>
							<td><input name="password" id="password" type="password" maxlength='6'></td>
						</tr>
						<tr>
						  <td class='red'>Konfirm Password</td>
							<td>:</td>
							<td><input name="konf_password" id="konf_password" type="password" maxlength='6'></td>
						</tr>
					</table> <!--END tabel kolom 2-->
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
						  <td width='25%'>Deposit</td>
							<td width='1%'>:</td>
							<td width='74%'><label>Rp. {DEPOSIT}</label></td>
						</tr>
						<tr>
						  <td>Point</td>
							<td>:</td>
							<td><label>{POINT}</label></td>
						</tr>
						<tr>
						  <td>Data diubah pd tanggal</td>
							<td>:</td>
							<td><label>{TGL_DIUBAH}</label></td>
						</tr>
						<tr>
						  <td>Data diubah oleh</td>
							<td>:</td>
							<td><label>{DIUBAH_OLEH}</label></td>
						</tr>
						{KOLOM_TOMBOL_UBAH_SALDO_POIN}
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan='3' bgcolor='C0C0C0' valign='middle' height='40'>
					<input type="button" id="DataMemberBtnSimpan" onClick="SimpanMember();" value="&nbsp;Simpan&nbsp">
					<input type="button" onClick="javascript:document.location='member.php?sid={SID}';" id="DataMemberBtnNo" value="&nbsp;Batal&nbsp;">
				</td>
			</tr>
		</table><!--END tabel 2 kolom-->
		</form>
	</td>
</tr>
</table>