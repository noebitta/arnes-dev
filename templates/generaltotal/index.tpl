<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<table width="100%" cellspacing="0" cellpadding="0" >
<tr>
 <td class="whiter" valign="middle" align="center">		
	<form action="{ACTION_CARI}" method="post">
		<!--HEADER-->
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;General Total</td>
				<td align='left' valign='middle'>
					<table>
						<tr>
              <td class='bannernormal' colspan=2>
                Kota:&nbsp;<select onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
                &nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
              </td>
              <td class='bannernormal'><input name="btn_cari" type="submit" value="cari" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END HEADER-->
	</form>
	</br>
	<!--BODY-->
	<table>
		<tr class="generaltotalrow"><td class="generaltotalkolom">Tanggal</td><td class="generaltotaldata"><b>{TANGGAL}</b></td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">open trip</td><td class="generaltotaldata">{OPEN_TRIP}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">trip isi</td><td class="generaltotaldata">{TRIP_ISI}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">booking</td><td class="generaltotaldata">{BOOKING}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">cancel/noshow</td><td class="generaltotaldata">{CANCEL}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">jumlah penumpang</td><td class="generaltotaldata">{PNP}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">Jumlah pnp.online</td><td class="generaltotaldata">{PNP_OL}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">jumlah paket</td><td class="generaltotaldata">{PKT}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">omzet penumpang</td><td class="generaltotaldata">Rp. {OMZ_PNP}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">omzet penumpang online</td><td class="generaltotaldata">Rp. {OMZ_OL}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">omzet paket</td><td class="generaltotaldata">Rp. {OMZ_PKT}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">unit aktif</td><td class="generaltotaldata">{UNIT_AKTIF}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">unit tidak aktif</td><td class="generaltotaldata">{UNIT_NONAKTIF}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">supir aktif</td><td class="generaltotaldata">{SUPIR_AKTIF}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">supir tidak aktif</td><td class="generaltotaldata">{SUPIR_NONAKTIF}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">member aktif</td><td class="generaltotaldata">{MEMBER_AKTIF}</td></tr>
		<tr class="generaltotalrow"><td class="generaltotalkolom">member nonaktif</td><td class="generaltotaldata">{MEMBER_NONAKTIF}</td></tr>
	</table>
 </td>
</tr>
</table>