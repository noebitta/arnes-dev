<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
</script>	 

<body class='tiket'>
<div style="font-size:14px;width:400px;height: 470px;">
	<span style="font-size:22px;">
		REKAP SETORAN<br>
		{NAMA_PERUSAHAAN}<br>
	</span>
	-----------------------------------------<br>
	#RESI:{RESI}<br>
	CSO:{NAMA_CSO}<br>
	Tgl:{TANGGAL_TRANSAKSI}<br>
	-----------------------------------------<br>
	PENJUALAN TIKET<br>
	-----------------------------------------<br>
	<!-- BEGIN TIKET -->
	{TIKET.SHOWTANGGAL}
	{TIKET.JURUSAN}
	Jam:{TIKET.JAM}<br>
	{TIKET.KET_SPJ}<br>
	Pnp:{TIKET.LIST_PENUMPANG}<br>
	Tunai:Rp.{TIKET.TUNAI}<br>
	{TIKET.DEBIT}
	{TIKET.KREDIT}
	-----------------------------------------<br>
	<!-- END TIKET -->
	{SHOW_SUB_TOTAL_TIKET}
	-----------------------------------------<br>
	<!-- BEGIN PAKET -->
	PENJUALAN PAKET<br>
	-----------------------------------------<br>
	Jum.Trx: {PAKET.JUMLAH_TRANSAKSI}<br>
	Jum.Pax: {PAKET.JUMLAH_PAX}<br>
	Jum.Kilo:{PAKET.JUMLAH_KILO} Kg.<br>
	Tunai:Rp.{PAKET.TUNAI}<br>
	{PAKET.DEBIT}
	{PAKET.KREDIT}
	Penj.Paket:Rp.{PAKET.TOTAL_PENJUALAN}
	-----------------------------------------<br>
	<!-- END PAKET -->
	REKAP BIAYA<br>
	-----------------------------------------<br>
	Sopir: Rp.{BIAYA_SOPIR}<br>
	Ins.Sopir: Rp.{BIAYA_INSENTIF_SOPIR}<br>
	Parkir: Rp.{BIAYA_PARKIR}<br>
	Tol: Rp.{BIAYA_TOL}<br>
	<!-- BEGIN ketbiayatambahan -->
	<br>
	BIAYA TAMBAHAN<br>
	<!-- END ketbiayatambahan -->
	<!-- BEGIN showtambahanbbm -->
	BBM: Rp.{showtambahanbbm.JUMLAH}<br>
	<!-- END showtambahanbbm -->
	<!-- BEGIN showtambahantol -->
	TOL: Rp.{showtambahantol.JUMLAH}<br>
	<!-- END showtambahantol -->
	<!-- BEGIN showtambahanlain -->
	LAIN: Rp.{showtambahanlain.JUMLAH}<br>
	<!-- END showtambahanlain -->
	<br>
	Total Biaya: Rp.{TOTAL_BIAYA}<br>
	-----------------------------------------<br>
	SETORAN TUNAI: Rp.{TOTAL_SETORAN}<br>
	{SHOW_TOTAL_SETORAN}
	<br><br>
	<span style="font-size:22px;">
		REKAP MANIFEST<br>
	</span>
	-----------------------------------------<br>
	<!-- BEGIN SPJ_TIKET -->
	<b>{SPJ_TIKET.NO_SPJ}</b><br>
	Tgl:{SPJ_TIKET.TGL_BERANGKAT}<br>
	{SPJ_TIKET.JURUSAN}
	Jam:{SPJ_TIKET.JAM}|{SPJ_TIKET.KENDARAAN}<br>
	({SPJ_TIKET.KODE_JADWAL})<br>
	{SPJ_TIKET.SOPIR}<br>
	Pnp:{SPJ_TIKET.LIST_PENUMPANG}<br>
	Omz.Pnp:Rp.{SPJ_TIKET.OMZET_PNP}<br>
	Pkt:{SPJ_TIKET.PAX_PAKET} pax<br>
	Omz.Pkt:Rp.{SPJ_TIKET.OMZET_PKT}<br>
	Biaya:Rp.{SPJ_TIKET.BIAYA}<br>
	-----------------------------------------<br>
	{SPJ_TIKET.SHOW_SUB_TOTAL_TIKET_JURUSAN}
	<!-- END TIKET -->
	{SPJ_SHOW_SUB_TOTAL}
	-----------------------------------------<br>
	{SPJ_SHOW_TOTAL}
	-----------------------------------------<br>
	Waktu Cetak {WAKTU_CETAK}<br>
</div>
</body>

<script language="javascript">
	printWindow();
	window.close();
	
	if (!{CETAK_ULANG}) {
		window.opener.location.reload(false);
	}
</script>
</html>
