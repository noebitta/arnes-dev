<SCRIPT>

function printWindow() {
	bV = parseInt(navigator.appVersion);
	if (bV >= 4) window.print();
}

window.load=printWindow();

</SCRIPT>
<font size='1' face='Courier New'>
	<table width='400' cellspacing=0 cellpadding=0>
		<tr><td colspan=3><h3>Bukti Setoran CSO</h3></td></tr>
		<tr><td width='40%'>Tgl. Setor</td><td width=3>:</td><td>{TGL_SETOR}</td>
		<tr>
		<tr><td>Nama</td><td>:</td><td>{NAMA}</td><tr>
		<tr><td>NRP</td><td>:</td><td>{NRP}</td><tr>
		<tr><td>Cabang</td><td>:</td><td>{CABANG}</td><tr>
		<tr><td>Telp</td><td>:</td><td>{TELP}</td><tr>
		<tr><td>Tgl.Trx</td><td>:</td><td>{TGL_TRANSAKSI}</td><tr>
		<tr><td colspan=3><strong>Setoran</strong></td></tr>
		<tr><td>Jum.Tiket</td><td>:</td><td align='left'>{TIKET}</td><tr>
		<tr><td>Ttl.Omzet</td><td>:</td><td align='right'>Rp.{TOTAL_OMZET}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><tr>
		<tr><td>Ttl.Discount</td><td>:</td><td align='right'>Rp.-{TOTAL_DISCOUNT}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><tr>
		<tr><td colspan=3 bgcolor='D0D0D0' height=1></td><tr>
		<tr><td>Ttl.Setor</td><td>:</td><td align='right'><b>Rp.{TOTAL_SETOR}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><tr>
		<tr><td colspan=3>
			<table width='100%' border=1>
				<tr>
					<td>&nbsp;</td>
					<td align='center'><strong>Tunai</strong></td>
					<td align='center'><strong>Debit</strong></td>
					<td align='center'><strong>Kredit</strong></td>
					<td align='center'><strong>Cpgt Card</strong></td>
				</tr>
				<tr>
					<td><strong>Go Show</strong></td>
					<td><div align='right'>{TOTAL_TUNAI_GOSHOW} </div></td>
					<td><div align='right'>{TOTAL_DEBIT_GOSHOW} </div></td>
					<td><div align='right'>{TOTAL_KREDIT_GOSHOW}</div></td>
					<td><div align='right'>{TOTAL_CPGTC_GOSHOW} </div></td>
				</tr>
				<tr>
					<td><strong>Pesanan</strong></td>
					<td><div align='right'>{TOTAL_TUNAI_PESANAN} </div></td>
				  <td><div align='right'>{TOTAL_DEBIT_PESANAN} </div></td>
				  <td><div align='right'>{TOTAL_KREDIT_PESANAN}</div></td>
				  <td><div align='right'>{TOTAL_CPGTC_PESANAN} </div></td>
				</tr>                  
				<tr>
					<td><strong>Total</strong></td>
					<td><div align='right'>{TOTAL_TUNAI} </div></td>
					<td><div align='right'>{TOTAL_DEBIT} </div></td>
					<td><div align='right'>{TOTAL_KREDIT}</div></td>
					<td><div align='right'>{TOTAL_CPGTC} </div></td>
				</tr>                  
			</table>
		</td></tr>
		<tr><td>Cabang Setor</td><td>:</td><td align='left'>{CABANG_SETOR}</td><tr>
		<tr><td colspan=3>
			<table width='100%' border=1>
				<tr><td align='center' width='50%'>TTD CSO</td><td align='center' width='50%'>TTD Kasir</td></tr>
				<tr><td align='center' valign='bottom' width='50%' height=100>{CSO}</td><td align='center' valign='bottom' width='50%'>&nbsp;</td></tr>
			</table>
		</td></tr>
	</table>
</font>