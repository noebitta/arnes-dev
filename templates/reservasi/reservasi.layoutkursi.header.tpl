<style type="text/css">
  .headertitle{
    display: inline-block;
    font-weight: bold;
    width: 120px;
  }
  
  .headerfield{
    display: inline-block;
    width: 280px;
  }
</style>

<input type='hidden' value='{JAM_BERANGKAT}' id='jam_berangkat_aktif' />
<input type='hidden' readonly='yes' value='{NO_SPJ}' name='txt_spj' id='txt_spj' />
<input type='hidden' readonly='yes' value='{NO_POLISI}' name='plat' id='plat' />
<input type='hidden' value='{NO_UNIT}' name='hide_mobil_sekarang' id='hide_mobil_sekarang'></input>
<input type='hidden' value='{JUMLAH_KURSI}' name='hide_layout_kursi' id='hide_layout_kursi'></input>
<input type='hidden' name='hide_nama_sopir_sekarang' id='hide_nama_sopir_sekarang' value='{KODE_SOPIR}'>
  
<div style="font-size: 13px; width:400px;height:65px; text-align: left;padding-left: 10px;">
  <span class="headertitle">#Manifest</span><span class="headerfield">:{NO_SPJ}</span><br>
  <span class="headertitle">#Kendaraan</span><span class="headerfield">:{DATA_UNIT}</span><br>
  <span class="headertitle">Harga</span><span class="headerfield">:<span class="{CLASS_HARGATIKET}">Rp.{HARGA_TIKET}</span>&nbsp;{HARGA_TIKET_PROMO}</span>
  <!--<span class="headertitle">Daftar Tunggu</span><span class="headerfield">:<a style="color: red;font-size: 12px;" href="" onClick="{ACTION_DAFTAR_TUNGGU};return false;">{DAFTAR_TUNGGU} Orang</a></span><br>-->
</div>
<div style="font-size: 13px; text-align: center;width:400px;">
  <span style="display: inline-block;height: 20px;width: 400px;background: #d0d0d0;text-align: left;">
    <span style="color:#606060; font-weight: bold;font-size: 15px;padding-left: 150px;">LAYOUT KURSI</span>
    <img id='progress_kursi' style="display:none;" src='./templates/images/loading.gif' />
  </span><br>
  <div style="text-align: center;width:400px;vertical-align: top;background-color: #ffffff;height: 75px;">
    <!-- BEGIN TOMBOL_MANIFEST -->
    <div style="float: left; padding-top: 5px;height: 50px;">&nbsp;<span class="b_print_manifest" onclick='setDialogSPJ();' title="mencetak manifest"></span></div>
    <!-- END TOMBOL_MANIFEST -->
    <!-- BEGIN TOMBOL_VOUCHER_BBM -->
    <div style="float: left; padding-top: 5px;height: 50px;">&nbsp;<span class="b_print_voucher_bbm" onclick="cetakVoucherBBM('{NO_SPJ}');" title="mencetak voucher bbm"></span></div>
    <!-- END TOMBOL_VOUCHER_BBM -->
    <div style="float:right; padding-top: 5px;height: 50px;"><span class="b_reload_layout" onclick='getUpdateMobil();' title="reload layout kursi"></span>&nbsp;</div>
    <div style="width: 200px; padding-left: 100px; padding-top: 10px;">
      <strong>
        {TGL_BERANGKAT}<br>
        {KODE_JADWAL}
      </strong>
    </div>
  </div>