<script type="text/javascript">	
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%'>
			<tr>
				<td align='center' valign='middle' class="bannerjudul">Laporan Biaya BBM</td>
				<td colspan=2 align='right' class="bannercari" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						SPBU:&nbsp;<select id='opt_spbu' name='opt_spbu'>{OPT_SPBU}</select>&nbsp;	<br><br>
						&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
						&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
						<input type="submit" value="cari" />&nbsp;		
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<td class='border'>
								<a href='#' onClick="{CETAK_PDF}"> <img src="{TPL}/images/icon_adobe.png">&nbsp;Cetak ke PDF</a> &nbsp;
							</td><td width=1></td>
							<td class='border'>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
				<td colspan=2 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table class="border">
    <tr>
       <th width=30>No</th>
			 <th width=200>Tgl.Voucher</th>
			 <th width=200>Kode</th>
			 <th width=200>No.SPJ</th>
			 <th width=300>Jurusan</th>
			 <th width=200>No.Pol</th>
			 <th width=300>Sopir</th>
			 <th width=200>Jenis BBM</th>
			 <th width=200>Jumlah</th>
			 <th width=200>KM</th>
			 <th width=200>Jumlah Liter</th>
			 <th width=200>SPBU</th>
			 <th width=100>Kasir</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.tgl_voucher}</div></td>
			 <td><div align="left">{ROW.kode}</div></td>
			 <td><div align="left">{ROW.no_spj}</div></td>
			 <td><div align="left">{ROW.jurusan}</div></td>
			 <td><div align="left">{ROW.no_pol}</div></td>
       <td><div align="left">{ROW.sopir}</div></td>
			 <td><div align="left">{ROW.jenis_bbm}</div></td>
			 <td><div align="right">{ROW.jumlah}</div></td>
			 <td><div align="right">{ROW.km}</div></td>
			 <td><div align="right">{ROW.jumlah_liter}</div></td>
			 <td><div align="left">{ROW.spbu}</div></td>
			 <td><div align="left">{ROW.kasir}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>