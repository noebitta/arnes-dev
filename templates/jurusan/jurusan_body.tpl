<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}

		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_jurusan="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatusAktif(id){
	
		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatusaktif&id="+id,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function ubahStatusTuslah(id,tuslah){
		
		new Ajax.Request("pengaturan_jurusan.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatustuslah&id="+id+"&tuslah="+tuslah,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}


</script>
<!-- HEADER -->
<div class="banner">
  <div class="bannerjudul">&nbsp;Master Jurusan</div>
  <div class="bannercari">
    <form action="{ACTION_CARI}" method="post">
      &nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}"/>&nbsp;
      <input type="submit" value="cari" />&nbsp;
    </form>
  </div>
</div>
<br>
<!--<center><a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a></center>-->
<br>
<!-- HEADER END -->
<div style="width: 100%;padding-bottom: 20px;">
  <div style="float:left">
    <a href="{U_JURUSAN_ADD}">[+]Tambah Jurusan</a>&nbsp;|&nbsp;
    <a href="" onClick="return hapusData('');">[-]Hapus Jurusan</a>
  </div>
  <div style="float:right;">
    <a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
    <a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
  </div>
</div>
<table width='100%' class="border">
  <th rowspan='2' width=10></th>
    <th rowspan='2' width=20>No</th>
    <th rowspan='2' width=70>#Jurusan</th>
    <th rowspan='2' width=150>Asal</th>
    <th rowspan='2' width=150>Tujuan</th>
    <th rowspan='2' width=130>Jenis</th>
    <th rowspan='2' width=80>Harga Tiket (Rp.)</th>
    <th rowspan='2' width=80>Harga Tiket Tuslah (Rp.)</th>
    <th colspan='2' class='thintop'>Harga Kirim Paket</th>
    <th rowspan='2' width=100>Biaya</th>
    <th rowspan='2' width=70>Aktif</th>
    <th rowspan='2' width=100>Action</th>
  </tr>
  <tr>
    <th width='70' class='thinbottom'>Dok</th>
    <th width='70' class='thinbottom'>Barang</th>
    <!--<th width='70' class='thinbottom'>S</th>
    <th width='70' class='thinbottom'>M</th>
    <th width='70' class='thinbottom'>L</th>
    <th width='70' class='thinbottom'>XL</th>-->
  </tr>
  <!-- BEGIN ROW -->
  <tr class="{ROW.odd}">
    <td align="center">{ROW.check}</td>
    <td align="center">{ROW.no}</td>
    <td align="center">{ROW.kode}</td>
    <td align="center">{ROW.asal}</td>
    <td align="center">{ROW.tujuan}</td>
    <td align="center" class="{ROW.class_op_jurusan}" title="{ROW.titlejenis}">{ROW.jenis}</td>
    <td align="right">{ROW.harga_tiket}</td>
    <td align="right">{ROW.harga_tiket_tuslah}</td>
    <td align="right">{ROW.harga_paket_1}</td>
    <td align="right">{ROW.harga_paket_2}</td>
    <td align="right">Tol:Rp.{ROW.biayatol}<br>Parkir:Rp.{ROW.biayaparkir}<br>Sopir:Rp.{ROW.biayasopir}</td>
    <!--<td><div align="right">{ROW.harga_paket_3}</div></td>
    <td><div align="right">{ROW.harga_paket_4}</div></td>
    <td><div align="right">{ROW.harga_paket_5}</div></td>
    <td><div align="right">{ROW.harga_paket_6}</div></td>-->
    <td align="center">{ROW.status_aktif}</td>
    <td align="center">{ROW.action}</td>
  </tr>
  <!-- END ROW -->
  {NO_DATA}
</table>
<div style="width: 100%;padding-top: 5px;">
  <div style="float:left">
    <a href="{U_JURUSAN_ADD}">[+]Tambah Jurusan</a>&nbsp;|&nbsp;
    <a href="" onClick="return hapusData('');">[-]Hapus Jurusan</a>
  </div>
  <div style="float:right;">
    <a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
    <a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
  </div>
</div>
