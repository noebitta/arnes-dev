<div class="menuwrapper" style="height: {HEIGHT_WRAPPER}px">
	<br/>
	<h2 style="background: #d0d0d0">-- PAKET --</h2>
	
	<div class="menuicon">
		<a href="{U_LAPORAN_DATA_PAKET}">
			<img src="{TPL}images/icon_laporan_cabang.png" /><br/>
			Laporan Data Paket
		</a>
	</div>
	
	<div class="menuicon">
		<a href="{U_LAPORAN_PAKET_BELUM_DIAMBIL}">
			<img src="{TPL}images/icon_laporan_cabang.png" /><br/>
			Laporan Paket<br>Belum Diambil
		</a>
	</div>
	
	<div class="menuicon">
		<a href="{U_LAPORAN_OMZET_PAKET_CABANG}">
			<img src="{TPL}images/icon_laporan_omzet.png" /><br/>
			Laporan Omzet Paket<br>Per Cabang
		</a>
	</div>
	
	<div class="menuicon">
		<a href="{U_LAPORAN_OMZET_PAKET_JURUSAN}">
			<img src="{TPL}images/icon_laporan_omzet.png" /><br/>
			Laporan Omzet Paket<br>Per Jurusan
		</a>
	</div>
	
	<!-- BEGIN menu_cargo -->
	<div style="height: 100px;"></div>
	<h2 style="background: #d0d0d0">-- CARGO --</h2>
	<!-- END menu_cargo -->
	
	<!-- BEGIN menu_cargo -->
	<div class="menuicon">
		<a href="{U_CARGO}">
			<img src="{TPL}images/icon_cargo.png" /><br/>
			Cargo
		</a>
	</div>
	<!-- END menu_cargo -->
</div>
