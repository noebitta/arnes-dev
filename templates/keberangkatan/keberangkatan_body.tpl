<script language="JavaScript">

function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
    
		new Ajax.Updater("rewritetujuan","pengaturan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&mode=keberangkatan&submode=tujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}

function gambarHasil(){
    
		document.getElementById('rewritehasil').innerHTML="<h3>SILAHKAN TUNGGU</h3>";
		asal  = document.getElementById('asal').value;
    tujuan  = document.getElementById('tujuan').value;
		
		new Ajax.Updater("rewritehasil","pengaturan.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan=" + tujuan + "&mode=keberangkatan&submode=gambarhasil",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
}
</script>

<table width="100%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
		<h1>Pengelolaan Jadwal Keberangkatan</h1>
		<div align="left">
			<table border=0>
				<tr>
					<td>Asal:&nbsp;<select id="asal" name="asal" onchange="getUpdateTujuan(this.value)">{O_ASAL}</select></td>
					<td><div id="rewritetujuan"></div></td>
					<td><input name="filter" type="button" onclick="gambarHasil()" value="&nbsp;Filter&nbsp;"></input></td>
				</tr>
			</table>
			<div align='center'><a href="{U_JURUSAN_ADD}">Tambah Jurusan</a></div>
		</div>
		<div id="rewritehasil"></div>
    <br />
    <a href="{U_JURUSAN_ADD}">Tambah Jurusan</a>
 </td>
</tr>
</table>