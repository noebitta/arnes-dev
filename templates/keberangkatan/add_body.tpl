<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="center">
    <form action="{U_JURUSAN_ADD_ACT}" method="post">
    <table width='80%'>     
     <tr>
       <input type="hidden" name="platold" value="{PLATOLD}">
			 <td>Kode</td><td><input type="text" name="kode" value="{KODE}"></td>
     </tr>
		 <tr>
       <td>Point Asal</td><td><input type="text" name="asal" value="{ASAL}"></td>
     </tr>
		 <tr>
       <td>Point Tujuan</td><td><input type="text" name="tujuan" value="{TUJUAN}"></td>
     </tr>
		 <tr>
       <td>Tahun</td><td><input type="text" name="tahun" value="{TAHUN}"></td>
     </tr>
		 <tr>
       <td>Warna</td><td><input type="text" name="warna" value="{WARNA}"></td>
     </tr>
		 <tr>
       <td>Jum. Seat</td><td><input type="text" name="seat" value="{SEAT}"></td>
     </tr>
		 <tr>
       <td>Sopir 1</td><td><input type="text" name="sopir1" value="{SOPIR}"></td>
     </tr>
		 <tr>
       <td>Sopir 2</td><td><input type="text" name="sopir2" value="{SOPIR1}"></td>
     </tr>
		 <tr>
       <td>Tanggal</td><td><input readonly="yes" id="tanggal" onclick="javascript:cal1.popup();" name="tanggal" value="{TANGGAL}" type="Text"></td>
     </tr>
		 <tr>
       <td>Kondisi</td><td><input type="text" name="status" value="{STATUS}"></td>
     </tr>
		 <tr>
       <td>Keterangan</td><td><textarea name="keterangan" id="keterangan" cols="30" rows="3">{KETERANGAN}</textarea></td>
     </tr>
		 <tr>
       <td>No STNK</td><td><input type="text" name="nostnk" value="{NOSTNK}"></td>
     </tr>
		 <tr>
       <td>STNK Valid Hingga</td><td><input readonly="yes" id="tanggalstnk" onclick="javascript:cal2.popup();" name="tanggalstnk" value="{VALID}" type="Text"></td>
     </tr>
		 <tr>
       <td>No BPKB</td><td><input type="text" name="nobpkb" value="{NOBPKB}"></td>
     </tr>
		 <tr>
       <td>KIR Valid Hingga</td><td><input readonly="yes" id="tanggalkir" onclick="javascript:cal3.popup();" name="tanggalkir" value="{VALIDKIR}" type="Text"></td>
     </tr>
		 <tr>
       <td>IBM Valid Hingga</td><td><input readonly="yes" id="tanggalibm" onclick="javascript:cal4.popup();" name="tanggalibm" value="{VALIBM}" type="Text"></td>
     </tr>
		 <tr>
       <td>KM</td><td><input type="text" name="km" value="{KM}"></td>
     </tr>
		 <tr>
       <td>No Mesin</td><td><input type="text" name="nomesin" value="{NOMESIN}"></td>
     </tr>
		 <tr>
       <td>Tgl Service</td><td><input readonly="yes" id="tanggalservice" onclick="javascript:cal5.popup();" name="tanggalservice" value="{TGLSERVICE}" type="Text"></td>
     </tr>
		 <tr>
       <td>Status</td>
				<td>
					<select id="aktif" name="aktif">
						<option value=1 {AKTIF1}>AKTIF</option>
						<option value=0 {AKTIF2}>TIDAK AKTIF</option>
					</select>
				</td>
     </tr>
     <tr>
       <td></td>
       <td>
         <input type="hidden" name="mode" value="{MODE}">
         <input type="hidden" name="submode" value="{SUB}">
         <input type="submit" name="submit" value="Save">
       </td>
     </tr>            
    </table>
    </form>
 </td>
</tr>
</table>