<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
</script>	 

<body class='tiket'>
	
<!-- BEGIN ROW -->
<div style="font-size:14px;width:400px;height: 470px;">
	<span style="font-size:22px;">{ROW.NAMA_PERUSAHAAN}</span><br>
	INSENTIF SOPIR<br>
	-slip untuk SOPIR-<br>
	Kepada:{ROW.NAMA_SOPIR}<br>
	NIK:{ROW.NIK}<br>
	Sebesar:{ROW.NOMINAL_INSENTIF}<br>
	Oleh:{ROW.CSO}<br>
	Pada:{ROW.WAKTU_BAYAR}<br>
	BA Oleh:{ROW.APPROVER}<br>
	TTD CSO<br>
	<br>
	<br>
	<br>
	{ROW.CSO}<br>
	<div style="text-align: center;width:400px;"">-- Terima Kasih --</div><br>
	<br>
	----------potong disini------------------------<br><br>
	<span style="font-size:22px;">{ROW.NAMA_PERUSAHAAN}</span><br>
	INSENTIF SOPIR<br>
	-slip untuk CSO-<br>
	Kepada:{ROW.NAMA_SOPIR}<br>
	NIK:{ROW.NIK}<br>
	Sebesar:{ROW.NOMINAL_INSENTIF}<br>
	Oleh:{ROW.CSO}<br>
	Pada:{ROW.WAKTU_BAYAR}<br>
	BA Oleh:{ROW.APPROVER}<br>
	TTD SOPIR<br>
	<br>
	<br>
	<br>
	{ROW.NAMA_SOPIR}<br>
	<div style="text-align: center;width:400px;"">-- Terima Kasih --</div><br>
</div>
<!-- END ROW -->
</body>

<script language="javascript">
	printWindow();
	window.close();
</script>
</html>
