<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validasiInputanAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
			|| [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39 || [evt.keyCode||evt.which]==116)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function validateInput(){
	
	valid=true;
	
	Element.hide('nama_plan_invalid');
	Element.hide('besar_premi_invalid');
	
	id_plan_asuransi	= document.getElementById('id_plan_asuransi');
	nama_plan					= document.getElementById('nama_plan');
	besar_premi				= document.getElementById('besar_premi');
	keterangan				= document.getElementById('keterangan');
	
	if(nama_plan.value==''){
		valid=false;
		Element.show('nama_plan_invalid');
	}
	
	if(!cekValue(besar_premi.value)){	
		valid=false;
		Element.show('besar_premi_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data" action="{U_ASURANSI_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<input type="hidden" name="id_plan_asuransi" value="{ID_PLAN_ASURANSI}">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Asuransi</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='800'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<tr>
			<td align='center' valign='top' width='800'>
				<table width='400'>   
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
			      <td><u>Nama plan asuransi</u></td><td>:</td>
						<td>
							<input type="text" id="nama_plan" name="nama_plan" value="{NAMA_PLAN}" maxlength=50 onChange="Element.hide('nama_plan_invalid');">
							<span id='nama_plan_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td>Besar premi (Rp.)</td><td>:</td>
						<td>
							<input type="text" id="besar_premi" name="besar_premi" value="{BESAR_PREMI}" maxlength=9 onkeypress='validasiInputanAngka(event);' onChange="Element.hide('besar_premi_invalid');">
							<span id='besar_premi_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
			    </tr>
					<tr>
			      <td valign='top'>Keterangan</td><td  valign='top'>:</td><td><textarea name="keterangan" id="keterangan" cols="30" rows="3"  maxlength=300>{KETERANGAN}</textarea></td>
			    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>            
	</table>
	</td>
</tr>
</table>
</form>