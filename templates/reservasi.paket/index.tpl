<input type="hidden" value="{HARGA_MINIMUM_PAKET}" id="hdn_harga_minimum_paket">
<input type="hidden" value="{SID}" id="hdn_SID">

<input type="hidden" value=0 id="ismodemutasion">
<input type="hidden" value=0 id="notiketdicetak">	
<input type="hidden" value='' id="id_jurusan_aktif">

<!-- calender European format dd-mm-yyyy -->
<script language="JavaScript" src="calendar/calendar1.js"></script><!-- Date only with year scrolling -->

<script language="JavaScript"  src="{TPL}/js/reservasi_paket.js"></script>

<!--dialog Paket-->
<div dojoType="dialog" id="dialog_cari_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='900'>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<table>
			<tr><td><h2>Cari Paket</h2></td></tr>
			<tr><td><div id="rewrite_cari_paket"></div></td></tr>
		</table>
		<span id='progress_cari_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" id="dialog_cari_paket_btn_Cancel" value="&nbsp;Cancel&nbsp;"> 
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog Paket-->

<!--dialog ambil paket-->
<div dojoType="dialog" id="dialog_ambil_paket" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table width='800'>
<tr><td><h1>Paket</h1></td></tr>
<tr>
	<td bgcolor='ffffff' height=300 valign='top' align='center'>
		<div id="rewrite_ambil_paket"></div>
		<span id='progress_ambil_paket' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
	</td>
</tr>
<tr>
   <td colspan="2" align="center">  
		<br>
		<input type="button" onClick="dialog_ambil_paket.hide();" id="dlg_ambil_paket_button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
		<input type="button" onclick="prosesAmbilPaket();" id="dlg_ambil_paket_button_ok" value="&nbsp;&nbsp;&nbsp;Ambil&nbsp;&nbsp;&nbsp;">
	 </td>
</tr>
</table>
</form>
</div>
<!--END dialog ambil paket-->

<!--dialog Pilih Pembayaran-->
<div dojoType="dialog" id="dialog_pembayaran" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr><td><h2>Pilih Jenis Pembayaran</h2></td></tr>
<tr>
	<td align='center'>
		<table bgcolor='white' width='100%'>
			<tr height=30><td>Silahkan pilih jenis pembayaran<td></tr>
			<tr><td>
				<input type='hidden' id='kode_booking_go_show' value='' />
				<table width='100%'>
					<tr>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakResi(0);return false;"><img src="{TPL}images/icon_tunai.png" /></a>  
							<br />
							<a href="" onClick="CetakResi(0);return false;"><span class="genmed">Tunai</span></a>    
						</td>
						<td width='50%' align='center' valign='middle'>
							<a href="" onClick="CetakResi(1);return false;"><img src="{TPL}images/icon_debitcard.png" /></a>  
							<br />
							<a href="" onClick="CetakResi(1);return false;"><span class="genmed">Debit Card</span></a> 
						</td>
					</tr>
					<tr>
						<td align='center' valign='middle'>
							<a href="" onClick="CetakResi(2);return false;"><img src="{TPL}images/icon_mastercard.png" /></a>  
							<br />
							<a href="" onClick="CetakResi(2);return false;"><span class="genmed">Credit Card</span></a>    
						</td>
						<!--<td align='center' valign='middle'>
							<a href="" onClick="CetakTiket(3);return false;"><img src="{TPL}images/icon_voucher.png" /></a>  
							<br />
							<a href="" onClick="CetakTiket(3);return false;"><span class="genmed">Voucher</span></a>    
						</td>-->
					</tr>
				</table>
			</td></tr>
		</table>
	</td>
</tr>
<tr>
  <td align="center">   
		<input type="reset" id="dialog_pembayaran_btn_Cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;">
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog Pilih Pembayaran-->

<!--dialog SPJ-->
<div dojoType="dialog" id="dialog_SPJ" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;">
<form onsubmit="return false;">
<table>
<tr>
	<td bgcolor='ffffff'>
		<table>
			<tr><td><h2>Cetak Manifest</h2></td></tr>
			<tr><td><div id="rewrite_list_mobil"></div></td></tr>
			<tr><td><div id="rewrite_list_sopir"></div></td></tr>
		</table>
		<span id='progress_dialog_spj' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=2 color='ffffff'>sedang memproses...</font></span>
		<br>
	</td>
</tr>
</table>
</form>
</div>
<!--END dialog SPJ-->

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
<td class="whiter" valign="middle" align="center">
 
<table width="100%" cellspacing="0" cellpadding="4" border="0">
<tr>
	<td height='80' width='100%'>
		<table width='100%' height='100%' cellspacing=0 cellpadding=0 class='header_reservasi'>
			<tr>	
				<td align='left' valign='middle' width='7%' background='./templates/images/icon_cari.png' STYLE='background-repeat: no-repeat;background-position: left top;'></td>
				<td align='left' valign='middle' width='23%'>
					<font color='505050'><b>Cari paket</b></font><br>
					<input name="txt_cari_paket" id="txt_cari_paket" type="text">
					<input class='tombol' name="btn_periksapaket" id="btn_periksapaket" value="Cari" type="button" onClick="periksaPaket(txt_cari_paket.value)"><br>	
					<font size=1 color='505050'>(masukkan no resi paket/ no.telp pelanggan )</font>
				</td>
				<td align='right' width='55%' valign='top'>
					<table>
						<tr>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_CHECKIN_PAKET};return false"><img src='./templates/images/icon_checkin_paket.png' /><br>Check-in Paket</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_LAPORAN_UANG};return false"><img src='./templates/images/icon_penjualan_uang.png' /><br>Laporan Uang</a></td>
							<td width='80' align='center' valign='top'><a class='menu' href="#;" onClick="{U_LAPORAN_PENJUALAN};return false"><img src='./templates/images/icon_penjualan.png' /><br>Laporan Paket</a></td>
							<td width='80' align='center' valign='top' class='notifikasi_pengumuman'><div id="rewritepengumuman"></div></td>
							<td width='80' align='center' valign='top'><a class='menu' href={U_UBAH_PASSWORD}><img src='./templates/images/icon_password.png' /><br>Ubah Password</a></td>
						</tr>
					</table>
			</tr>
		</table>
	</td>
</tr>
<tr>
 <td valign="middle" align="left">    
  <table width="100%" class='reservasi_background' cellpadding='0' cellspacing='0'>
		<tr>
			<td width="230" valign="top">       
				<table border="0" width="100%" cellpadding='0' cellspacing='0'>
					<tr><td colspan=2 class='formHeader'><strong>1. Jadwal</strong></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<b>Tanggal keberangkatan</b>
						</td>
					</tr>
					<tr>
						<td colspan=2 align='center'>
							<!--  kolom kalender -->
							<iframe width=174 height=189 name="gToday:normal:agenda.js" id="gToday:normal:agenda.js" src="./calendar/iflateng.htm" scrolling="no" frameborder="0">
							</iframe>
		
							<form name="formTanggal">
								<input type="hidden" id="p_tgl_user" name="p_tgl_user" value="{TGL_SEKARANG}" />			
							</form>
							<!--end kolom kalender-->
							
							<input name="update" onclick="getUpdateAsal(kota_asal.value,1)" type="button" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Proses&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"></input>
							
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table width='100%'> 
							  <tr><td>KOTA KEBERANGKATAN<br></td></tr>
						    <tr>
									<td>
										<select onchange='getUpdateAsal(this.value,0);' id='kota_asal' name='kota_asal'>{OPT_KOTA}</select>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
						<div id="rewritetype"></div>
						</td>
					</tr>
					<!-- Bagian ini akan kita rewite saat requestForUpdate dipanggil -->
					<tr>
						<td colspan="2" height=150 valign='top'>
							<div id="rewriteall">
								<div id="rewriteasal"></div>
								<div id="rewritetujuan"></div>
								<div id="rewritejadwal"></div>
								<!--<span id='progress_jam' style='display:none;'><img src='./templates/images/loading.gif' /></span>-->
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width=1 bgcolor='e0e0e0'></td>
			<td valign='top' width='400'>
				<!--LIST PAKET-->
				<table width='100%' cellpadding='0' cellspacing='0'>
					<tr>
						<td width='100%' valign='top' align='center'>
							<div align='left' class='formHeader'><strong>2. List Paket</strong></div>
							<br>
							<div id='loadingrefresh' class="resvcargoloadingcirc"></div>
							<div id="rewritepaket"><!-- LAYOUT PAKET--></div>
						</td>
					</tr>
				</table>
			</td>       
			<td width=1 bgcolor='e0e0e0'></td>
			<!--LAYOUT DATA ISIAN PENUMPANG-->
			<td valign='top' width='370'>
				<div align='left' class='formHeader'><strong>3. Data Paket</strong></div>
				<br>							
				<!--data pelanggan-->
				<div id='loadingdetail' class="resvcargoloadingcirc"></div>
				<div id="showdatadetail" align='left'></div>
				
				<!--data isian pengiriman paket-->
				<form onsubmit="return false;">
          <div style="margin-left: 5px;">
            <h2>Data Pengirim</h2>
            <span class="resvcargobooklabel">Telp Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input id='telp_pengirim' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp4Paket(this.value,1)" onfocus="this.style.background='';"/></span></br>
            <span class="resvcargobooklabel">Nama Pengirim <span id='loadingcaritelppengirim' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=1 color="777777">&nbsp;mencari</font></span></span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input id='nama_pengirim' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>
            <span class="resvcargobooklabel">Alamat Pengirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input id='alamat_pengirim' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>

            <h2>Data Penerima</h2>
            <span class="resvcargobooklabel">Telp Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input id='telp_penerima' type='text' maxlength='15' onkeypress='validasiNoTelp(event);' onBlur="cariDataPelangganByTelp4Paket(this.value,0)" onfocus="this.style.background='';"/></span></br>
            <span class="resvcargobooklabel">Nama Penerima <span id='loadingcaritelppenerima' style='display:none;'><img src='{TPL}images/loading.gif' /><font size=1 color="777777">&nbsp;mencari</font></span></span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input id='nama_penerima' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>
            <span class="resvcargobooklabel">Alamat Penerima</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input id='alamat_penerima' type='text' maxlength='100' onfocus="this.style.background='';"/></span></br>

            <h2>Data Barang</h2>
            <input id='harga_kg_pertama_1' type='hidden' value='0'/>
            <input id='harga_kg_pertama_2' type='hidden' value='0'/>
            <input id='harga_kg_pertama_3' type='hidden' value='0'/>
            <input id='harga_kg_pertama_4' type='hidden' value='0'/>
            <input id='harga_kg_pertama_5' type='hidden' value='0'/>
            <input id='harga_kg_pertama_6' type='hidden' value='0'/>
            <input id='harga_kg_berikutnya_1' type='hidden' value='0'/>
            <input id='harga_kg_berikutnya_2' type='hidden' value='0'/>
            <input id='harga_kg_berikutnya_3' type='hidden' value='0'/>
            <input id='harga_kg_berikutnya_4' type='hidden' value='0'/>
            <input id='harga_kg_berikutnya_5' type='hidden' value='0'/>
            <input id='harga_kg_berikutnya_6' type='hidden' value='0'/>
            <span class="resvcargobooklabel">Jenis Barang</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select id='dimensi' onChange="setJenisBarang(this.value);"><option value="1">Dokumen</option><option value="2">Barang</option><option value="3">Bagasi</option></select></span></br>
            <span class="resvcargobooklabel">Koli</span><span class="resvcargobookfield">:</span>&nbsp;<span class="resvcargobookfield"><input id='jumlah_koli' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onBlur="hitungHargaPaket();" onfocus="this.style.background='';" />&nbsp;Pax</span></br>
            <span class="resvcargobooklabel">Perhitungan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield"><input type="radio" name="isvolumetric" id="isvolumetric0" onclick="setFormVolumetric();" selected/>Weight &nbsp; <input type="radio" name="isvolumetric" id="isvolumetric1" onclick="setFormVolumetric();"/>Volumetric (PxLxT)</span></br>

						<span id="showinputvolumetric" style="display: none;">
							<span class="resvcargobooklabel">Dimensi P x L x T</span><span class="resvcargobookfield">:</span>&nbsp;
							<span class="resvcargobookfield"><input id='dimensipanjang' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="P" onblur="hitungHargaPaket();"/>&nbsp;cm x</span>
							<span class="resvcargobookfield"><input id='dimensilebar' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="L" onblur="hitungHargaPaket();"/>&nbsp;cm x</span>
							<span class="resvcargobookfield"><input id='dimensitinggi' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" placeholder="T" onblur="hitungHargaPaket();"/>&nbsp;cm</span></br>
              <span class="resvcargobooklabel">Berat</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<span id="beratvolumetric"></span>&nbsp;Kg</span>
						</span>

            <span id="showinputweight" style="display: none;">
							<span class="resvcargobooklabel">Berat</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<input id='berat' type='text' style='text-align: right;' size='5' maxlength='3' onkeypress='validasiInputanAngka(event);' onfocus="this.style.background='';" onblur="hitungHargaPaket();"/>&nbsp;Kg</span>
						</span>

             <span id="showinputsizebagasi" style="display: none;">
							<span class="resvcargobooklabel">Ukuran</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select id='ukuranbagasi' onChange="hitungHargaPaket();"><option value="1">Kecil</option><option value="2">Sedang</option><option value="3">Besar</option></select></span>
						</span>
            <br>
            <span class="resvcargobooklabel">Biaya Kirim</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;Rp.<span id="rewrite_harga_paket_show"></span></span></br>
            <span class="resvcargobooklabel">Layanan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select id='jenislayanan'><option value='P2P'>Port to Port</option><option value='P2D'>Port to Door</option></select></span></br>
            <span class="resvcargobooklabel">Pembayaran</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<select id='cara_bayar'><option value="0">Langsung</option><!--<option value="1">Langganan</option><option value="2">Bayar di Tujuan</option>--></select></span></br>
            <span class="resvcargobooklabel">Keterangan</span><span class="resvcargobookfield">:</span><span class="resvcargobookfield">&nbsp;<textarea id='keterangan' rows='3' cols='30' onfocus="this.style.background='';" ></textarea></span></br>
            <br>
            <center>
              <input type="button" onClick="resetIsianPaket();" id="button_cancel" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"> &nbsp;
              <input type="button" onclick="pesanPaket();" id="button_ok" value="&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;">
            </center>
          </div>
				</form>

			</td>
    </tr>        
  </table>   
 </td>
</tr>
</table>

</td>
</tr>
</table>