<script type="text/javascript">
filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<table width="100%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%'>
			<tr>
				<td align='center' valign='middle' class="bannerjudul">Laporan Penjualan {NAMA}</td>
				<td colspan=2 align='right' class="bannercari" valign='middle'>
					<br>
					<form action="{ACTION_CARI}" method="post">
						&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
						&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
						&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
						<input type="submit" value="cari" />&nbsp;		
					</form>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
			<tr>
				<td colspan=3 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table class="border">
    <tr>
       <th width=30>No</th>
			 <th width=200>Waktu Pesan</th>
			 <th width=200>No.Tiket</th>
			 <th width=200>Waktu Berangkat</th>
			 <th width=100>Kode Jadwal</th>
			 <th width=200>Nama</th>
			 <th width=100>No.Kursi</th>
			 <th width=100>Harga Tiket</th>
			 <th width=100>Discount</th>
			 <th width=70>Total</th>
			 <th width=100>Tipe Discount</th>
			 <th width=100>Pesanan</th>
			 <th width=100>Pembayaran</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="right">{ROW.no}</div></td>
       <td><div align="left">{ROW.waktu_pesan}</div></td>
			 <td><div align="left">{ROW.no_tiket}</div></td>
       <td><div align="left">{ROW.waktu_berangkat}</div></td>
       <td><div align="left">{ROW.kode_jadwal}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			 <td><div align="center">{ROW.no_kursi}</div></td>
			 <td><div align="right">{ROW.harga_tiket}</div></td>
			 <td><div align="right">{ROW.discount}</div></td>
			 <td><div align="right">{ROW.total}</div></td>
			 <td><div align="left">{ROW.tipe_discount}</div></td>
			 <td><div align="left">{ROW.pesanan}</div></td>
       <td><div align="center">{ROW.pembayaran}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>