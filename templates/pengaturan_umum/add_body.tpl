<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validasiHarga(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
			|| [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39 || [evt.keyCode||evt.which]==116)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
}

function validateInput(){
	
	Element.hide('telp_invalid');
	
	telp	= document.getElementById('telp');
	
	valid=true;
	
	if(!cekValue(telp.value)){	
		valid=false;
		Element.show('telp_invalid');
	}
	
	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data" action="{U_ADD_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Pengaturan Umum</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr>
			<td align='center'>
				<table width='800'>
					<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
					<tr>
						<td colspan=3><h2>{JUDUL}</h2></td>
					</tr>
					<tr>
					  <td valign='top' width='200'>Pesan di Tiket</td><td  valign='top' width='5'>:</td>
						<td>
							<textarea name="pesan_di_tiket" id="pesan_di_tiket" cols="30" rows="3"  maxlength=300>{PESAN_DI_TIKET}</textarea>
						</td>
					</tr>
					<tr>
					  <td valign='top'>Alamat Perusahaan</td><td  valign='top'>:</td>
						<td>
							<textarea name="alamat" id="alamat" cols="30" rows="3"  maxlength=300>{ALAMAT}</textarea>
						</td>
					</tr>
					<tr>
					  <td>Telp. Perusahaan</td><td>:</td>
						<td>
							<input type="text" id="telp" name="telp" value="{TELP}" maxlength=50 onChange="Element.hide('telp_invalid');">
							<span id='telp_invalid' style='display:none;'><font color=red>&nbsp;<b>(X)</b></font></span>
						</td>
					</tr>
					<tr>
					  <td>Email Perusahaan</td><td>:</td>
						<td>
							<input type="text" id="email" name="email" value="{EMAIL}" maxlength=50 />
						</td>
					</tr>
					<tr>
					  <td>Website Perusahaan</td><td>:</td>
						<td>
							<input type="text" id="website" name="website" value="{WEBSITE}" maxlength=50>
						</td>
					</tr>
					<tr>
						<td>Tanggal Mulai Tuslah</td><td>:</td>
						<td><input readonly="yes"  id="tanggal_mulai_tuslah" name="tanggal_mulai_tuslah" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_MULAI_TUSLAH}" size=10></td>
					</tr>
					<tr>
						<td>Tanggal Berakhir Tuslah</td><td>:</td>
						<td><input readonly="yes"  id="tanggal_akhir_tuslah" name="tanggal_akhir_tuslah" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR_TUSLAH}" size=10></td>
					</tr>
					<!--<tr><td colspan=3><br><br><h3>Pengaturan Harga Paket</h3></td></tr>
					<tr><td>PLATINUM</td><td>:</td><td>1 Kilo Pertama Rp. <input type='text' id='harga_paket_pertama_p' name='harga_paket_pertama_p' value='{PAKET_HARGA_PERTAMA_P}' maxlength=7 onkeypress='validasiHarga(event);'/>&nbsp;&nbsp; Per Kilo Selanjutnya Rp. <input type='text' id='harga_paket_selanjutnya_p' name='harga_paket_selanjutnya_p' value='{PAKET_HARGA_SELANJUTNYA_P}' maxlength=7 onkeypress='validasiHarga(event);'/></td></tr>
					<tr><td>GOLD ANTAR</td><td>:</td><td>1 Kilo Pertama Rp. <input type='text' id='harga_paket_pertama_ga' name='harga_paket_pertama_ga' value='{PAKET_HARGA_PERTAMA_GA}' maxlength=7 onkeypress='validasiHarga(event);'/>&nbsp;&nbsp; Per Kilo Selanjutnya Rp. <input type='text' id='harga_paket_selanjutnya_ga' name='harga_paket_selanjutnya_ga' value='{PAKET_HARGA_SELANJUTNYA_GA}' maxlength=7 onkeypress='validasiHarga(event);'/></td></tr>
					<tr><td>GOLD DIAMBIL</td><td>:</td><td>1 Kilo Pertama Rp. <input type='text' id='harga_paket_pertama_gd' name='harga_paket_pertama_gd' value='{PAKET_HARGA_PERTAMA_GD}' maxlength=7 onkeypress='validasiHarga(event);'/>&nbsp;&nbsp; Per Kilo Selanjutnya Rp. <input type='text' id='harga_paket_selanjutnya_gd' name='harga_paket_selanjutnya_gd' value='{PAKET_HARGA_SELANJUTNYA_GD}' maxlength=7 onkeypress='validasiHarga(event);'/></td></tr>
					<tr><td>SILVER</td><td>:</td><td>1 Kilo Pertama Rp. <input type='text' id='harga_paket_pertama_s' name='harga_paket_pertama_s' value='{PAKET_HARGA_PERTAMA_S}' maxlength=7 onkeypress='validasiHarga(event);'/>&nbsp;&nbsp; Per Kilo Selanjutnya Rp. <input type='text' id='harga_paket_selanjutnya_s' name='harga_paket_selanjutnya_s' value='{PAKET_HARGA_SELANJUTNYA_S}' maxlength=7 onkeypress='validasiHarga(event);'/></td></tr>
					-->
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr> 
	</table>
	</td>
</tr>
</table>
</form>