<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
</script>

<table width="95%" class="border" cellspacing="1" cellpadding="4" border="0">
<tr>
 <th align='left'>Welcome {USERNAME},</th>
</tr>
<tr>
 <td align='left' class="indexer">{BCRUMP}</td>
</tr>
<tr>
 <td class="whiter" valign="middle" align="left">		
	<form action="{ACTION_CARI}" method="post">
		<table width='1000'>
			<tr>
				<td align='center' valign='middle' class="bannerjudul" height=50>Laporan Penerimaan Kasir</td>
				<td colspan=2 align='right' class="bannercari" valign='middle'>
						<table>
							<tr><td>
								&nbsp;Periode:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
								&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
								&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
								<input type="submit" value="cari" />&nbsp;								
							</td></tr>
						</table>
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
			<tr>
				<td>Urutkan berdasarkan:&nbsp;<select id='sort_by' name='sort_by'>{OPT_SORT}</select>&nbsp;
					<select id='order' name='order'>{OPT_ORDER}</select>&nbsp;<input type='submit' value='Urutkan' />
				</td>
				<td colspan=2 align='right' valign='bottom'>
					{PAGING}
				</td>
			</tr>
		</table>
		<table class="border">
    <tr>
       <th width=30>No</th>
			 <th width=200>Nama</th>
			 <th width=100>NRP</th>
			 <th width=100>Username</th>
			 <th width=100>Cabang</th>
			 <th width=200>Telp</th>
			 <th width=400 >Detail</th>
			 <th width=100>Discount</th>
			 <th width=100>Total</th>
			 <th width=100>Tiket</th>
			 <th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td valign='top'><div align="right">{ROW.no}</div></td>
       <td valign='top'><div align="left"><font size=5>{ROW.nama}</font></div></td>
			 <td valign='top'><div align="left">{ROW.nrp}</div></td>
       <td valign='top'><div align="left">{ROW.username}</div></td>
       <td valign='top'><div align="left">{ROW.cabang}</div></td>
			 <td valign='top'><div align="left">{ROW.telp}</div></td>
			 <td align='center'>
				<table width='100%'>
					<tr>
						<td>&nbsp;</td>
						<td align='center'><strong>Tunai</strong></td>
						<td align='center'><strong>Debit</strong></td>
						<td align='center'><strong>Kredit</strong></td>
						<td align='center'><strong>Cpgt Card</strong></td>
					</tr>
					<tr>
						<td><strong>Go Show</strong></td>
						<td><div align="right">{ROW.total_goshow_tunai}</div></td>
						<td><div align="right">{ROW.total_goshow_debit}</div></td>
						<td><div align="right">{ROW.total_goshow_kredit}</div></td>
						<td><div align="right">{ROW.total_goshow_cpgtc}</div></td>
					</tr>
					<tr>
						<td><strong>Pesanan</strong></td>
						<td><div align="right">{ROW.total_pesanan_tunai}</div></td>
					  <td><div align="right">{ROW.total_pesanan_debit}</div></td>
					  <td><div align="right">{ROW.total_pesanan_kredit}</div></td>
					  <td><div align="right">{ROW.total_pesanan_cpgtc}</div></td>
					</tr>
					<tr><td bgcolor='red' height=1 colspan=5></td></tr>
					<tr>
						<td><strong>Total</strong></td>
						<td><div align="right">{ROW.total_tunai}</div></td>
						<td><div align="right">{ROW.total_debit}</div></td>
						<td><div align="right">{ROW.total_kredit}</div></td>
						<td><div align="right">{ROW.total_cpgtc}</div></td>
					</tr>
					<tr>
				</table>
			 </td>
			 <td valign='top'><div align="right">{ROW.discount}</div></td>
			 <td valign='top'><div align="right">{ROW.total}</div></td>
			 <td valign='top'><div align="right">{ROW.tiket}</div></td>
       <td valign='top'><div align="center">{ROW.act}</div></td>
     </tr>
     <!-- END ROW -->
    </table>
		<table width='100%'>
			<tr>
				<td align='right' width='100%'>
					{PAGING}
				</td>
			</tr>
			<tr>
				<td align='left' valign='bottom' colspan=3>
				{SUMMARY}
				</td>
			</tr>
		</table>
	</form>
 </td>
</tr>
</table>