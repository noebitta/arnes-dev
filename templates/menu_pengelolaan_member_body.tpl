<div class="menuwrapper" style="height: {HEIGT_WRAPPER}px">
	<br/>
	<!-- BEGIN menu_data_member -->
	<div class="menuicon">
		<a href="{U_MEMBER}">
			<img src="{TPL}images/icon_master_member.png" /><br/>
			Data Member
		</a>
	</div>
	<div style="height: {menu_data_member.br};"></div>
	<!-- END menu_data_member -->
	
	<!-- BEGIN menu_member_ultah -->
	<div class="menuicon">
		<a href="{U_MEMBER_ULTAH}">
			<img src="{TPL}images/icon_member_ultah.png" /><br />
			Member <br>Berulang Tahun
		</a>
	</div>
	<div style="height: {menu_member_ultah.br};"></div>
	<!-- END menu_member_ultah -->
	
	<!-- BEGIN menu_potensial_jadi_member -->
	<div class="menuicon">
		<a href="{U_MEMBER_CALON}">
			<img src="{TPL}images/icon_member_calon.png" /><br />
			Pelanggan Potensi<br>Jadi Member
		</a>
	</div>
	<div style="height: {menu_potensial_jadi_member.br};"></div>
	<!-- END menu_potensial_jadi_member -->
	
	<!-- BEGIN menu_frekwensi_member -->
	<div class="menuicon">
		<a href="{U_MEMBER_FREKWENSI}">
			<img src="{TPL}images/icon_laporan_cso.png" /><br />
			Frekwensi Berangkat
		</a>
	</div>
	<div style="height: {menu_frekwensi_member.br};"></div>
	<!-- END menu_frekwensi_member -->
	
	<!-- BEGIN menu_member_hampir_expired -->
	<div class="menuicon">
		<a href="{U_MEMBER_HAMPIR_EXPIRED}">
			<img src="{TPL}images/icon_member_hampir_expired.png" /><br />
			Member Hampir<br>Expired
		</a>
	</div>
	<div style="height: {menu_member_hampir_expired.br};"></div>
	<!-- END menu_member_hampir_expired -->
	
</div>