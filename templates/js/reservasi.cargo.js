var SID;
var harga_minimum_paket;

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}

function cekValue(nilai){
	cek_value=nilai*0;
	
	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function cekValueNoTelp(no_telp){
	//cek 1
	jum_digit=no_telp.length;
	
	if(jum_digit<8){
		//alert(1);
		return false;
	}
	
	//cek 2
	kode_inisial=no_telp.substring(0,1);
	
	if(kode_inisial!=0){
		//alert(2);
		return false;
	}
	
	//cek 3
	kode_area=no_telp.substring(1,2)*1;
	
	if(kode_area<=1){
		//alert(3);
		return false;
	}
	
	return true;
	
}

function validasiNoTelp(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13 || 
		[evt.keyCode||evt.which]==46 || [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
	
	return true;
}


function ShowDialogKode(jenis){
	document.getElementById('jenis_pembatalan').value=jenis;
	//document.getElementById('kode').value='';
	dlg_TanyaHapus.show();
	//HapusBerdasarkanJenis();
}

function Right(str, n){
	/*
      IN: str - the string we are RIGHTing
      n - the number of characters we want to return

      RETVAL: n characters from the right side of the string
       */
  if (n <= 0)     // Invalid bound, return blank string
		return "";
  else if (n > String(str).length)   // Invalid bound, return
		return str;                     // entire string
  else { // Valid bound, return appropriate substring
		var iLen = String(str).length;
    return String(str).substring(iLen, iLen - n);
  }
}

Number.prototype.formatMoney = function(c, d, t){
	var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function ValidasiAngka(objek,kolom){
	temp_nilai=objek.value*0;
	
	if(temp_nilai!=0){
		alert(kolom+" harus angka!");
		objek.setFocus;exit;
	}
	
	if(objek.value<0){
		alert(kolom+" tidak boleh kurang dari 0!");
		objek.setFocus;exit;
	}
	
}

function validasiInputanAngka(evt){
	var theEvent = evt || window.event;
	
	var key = theEvent.keyCode || theEvent.which;
	
	key = String.fromCharCode(key);
	
	var regex = /[0-9]/;
	
	if ([evt.keyCode||evt.which]==8 || [evt.keyCode||evt.which]==9 || [evt.keyCode||evt.which]==13
			|| [evt.keyCode||evt.which]==37 || [evt.keyCode||evt.which]==39 || [evt.keyCode||evt.which]==116)  return true;  
	
	if( !regex.test(key) ) {
		theEvent.returnValue = false;
		theEvent.preventDefault();
	}
	
	return true;
}

function FormatUang(uang,separator){
	len_uang = String(uang).length;
	return_val='';
	for (i=len_uang;i>=0;i--){
		if ((len_uang-i)%3==0 && len_uang-i!=0 && i!=0) return_val =separator+return_val;

		return_val =String(uang).substring(i,i-1)+return_val;
	}
	
	return return_val;
}

function getUpdateAsal(kotapickup,mode){
	
	pointpickup=document.getElementById('pointpickup');
	
	if(!pointpickup || mode==0){
		
		new Ajax.Updater("showpointpickup","reservasi.cargo.php?sid="+SID, {
			asynchronous: true,
			method: "get",

			parameters: "mode=setpointpickup&kotapickup="+kotapickup,
			onLoading: function(request){
				Element.show('progress_asal');
			},
			onComplete: function(request){
				Element.show('showpointpickup');
				Element.hide('progress_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});
		
	}
	else{
		getUpdatePaket();
		getPengumuman();
	}	
}

function setKotaAsal(prov){
  // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
 
	new Ajax.Updater("showoptkotaasal","reservasi.cargo.php?sid="+SID, 
  {
      asynchronous: true,
      method: "get",
      parameters: "mode=setkotaasal&prov=" + prov,
      onLoading: function(request){
      },
      onComplete: function(request) {
      },
      onFailure: function(request) 
      { 
        assignError(request.responseText); 
      }
  });   
}

function setKotaTujuan(prov){
  // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
 
	new Ajax.Updater("showoptkotatujuan","reservasi.cargo.php?sid="+SID, 
  {
      asynchronous: true,
      method: "get",
      parameters: "mode=setkotatujuan&prov=" + prov,
      onLoading: function(request){
      },
      onComplete: function(request) {
      },
      onFailure: function(request) 
      { 
        assignError(request.responseText); 
      }
  });   
}

function setFormVolumetric(){
	
	if(isvolumetric0.checked) {
		showinputvolumetric.style.display="none";
		showinputweight.style.display="inline-block";
	}
	else{
		showinputvolumetric.style.display="inline-block";
		showinputweight.style.display="none";
	}
	
	hitungHargaPaket();
	
}

function setFormAsuransi(){
	
	if(isasuransi0.checked) {
		showinputisasuransi.style.display="none";
	}
	else{
		showinputisasuransi.style.display="inline-block";
	}
	
	hitungHargaPaket();
	
}

function setHarga(){
	
	if (kotaasal.value=='') {
		kotaasal.style.background="red";
		return;
	}
	
	if (kotatujuan.value=='') {
		kotatujuan.style.background="red";
		return;
	}
	
	new Ajax.Request("reservasi.cargo.php?sid="+SID,{
		asynchronous: true,
		method: "get",
		parameters: "mode=setharga"+
			"&kotaasal="+kotaasal.value+
			"&kotatujuan="+kotatujuan.value,
		onLoading: function(request) {
			
		},
		onComplete: function(request) {
		},
		onSuccess: function(request) {			
			var prosesok=false;
			
			eval(request.responseText);
		},
		onFailure: function(request) 
		{
		}
	});  
}

function hitungHargaPaket(){
	
	if (isvolumetric0.checked) {
		hargapaket	= hargaperkg.value * berat.value;
	}
	else{
		showberat		= Math.ceil((dimensipanjang.value*dimensilebar.value*dimensitinggi.value)/4000);
		hargapaket	= hargaperkg.value * showberat;
		beratvolumetric.innerHTML	=  FormatUang(showberat,'.');
	}
	
	if (isasuransi0.checked) {
		tempbiayaasuransi=0
		biayaasuransi.innerHTML="";
	}
	else{
		tempbiayaasuransi	= 0.002*hargadiakui.value;
		biayaasuransi.innerHTML=FormatUang(tempbiayaasuransi,'.');
	}
	
	temptotalbiaya	= hargapaket+(biayatambahan.value*1)+(biayapacking.value*1)+tempbiayaasuransi;
	
	biayakirim.innerHTML	= FormatUang(hargapaket,'.');
	totalbiaya.innerHTML	= FormatUang(temptotalbiaya,'.');
	
}

function getPengumuman(){

		new Ajax.Updater("rewritepengumuman","reservasi.cargo.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters: "mode=pengumuman",
        onLoading: function(request) 
        {
            
        },
        onComplete: function(request) 
        {   
					
        },
        onFailure: function(request) 
        { 
            assignError(request.responseText); 
        }
    });       
}

function getUpdatePaket(){
		//mereset timer waktu refresh
		resetTimerRefresh();
		
		new Ajax.Updater("showlistpaket","reservasi.cargo.php?sid="+SID, 
    {
        asynchronous: true,
        method: "get",
        parameters:
					"mode=paketlayout"+
					"&tglditerima=" + document.getElementById("p_tgl_user").value+
					"&poinpickedup="+ document.getElementById("pointpickup").value+
					"&namapoinpickedup="+ document.getElementById("pointpickup").options[document.getElementById("pointpickup").selectedIndex].text+
					"&modemutasion=" + document.getElementById("ismodemutasion").value,
        onLoading: function(request) 
        {
					document.getElementById("loadingrefresh").style.display="block"; 
        },
        onComplete: function(request) 
        {   	    
					document.getElementById("loadingrefresh").style.display="none"; 
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });       
}

function showPaket(id){
	document.getElementById('ismodemutasion').value=0; 
	
	new Ajax.Updater("showdatadetail","reservasi.cargo.php?sid="+SID,{
    asynchronous: true,
    method: "get",
    parameters: "id="+id+"&mode=paketdetail",
    onLoading: function(request){
			document.getElementById("loadingdetail").style.display="block"; 
    },
    onComplete: function(request){              
			document.getElementById("loadingdetail").style.display="none"; 
    },
    onFailure: function(request){ 
        assignError(request.responseText); 
    }
  });  
}

function batalPaket(id){
	
	if(confirm("Apakah anda yakin akan membatalkan cargo ini?")){
	
		new Ajax.Request("reservasi.cargo.php?sid="+SID,{
			asynchronous: true,
			method: "get",
			parameters: "mode=paketbatal&id="+id,
			onLoading: function(request) {
				document.getElementById("loadingrefresh").style.display="block"; 
			},
			onComplete: function(request) {
			
			},
			onSuccess: function(request) {			
				var prosesok=true;
				
				eval(request.responseText);
				
				if(prosesok){
					document.getElementById('showdatadetail').innerHTML='';
					getUpdatePaket();
				}
				
				document.getElementById("loadingrefresh").style.display="none"; 
			},
			onFailure: function(request){
			}
		});  
	}
	
	return false;
		
}

function simpanPaket(){
  
	valid=true;
	
	if (document.getElementById('txt_spj')){
		no_spj	= document.getElementById('txt_spj').value;	
	}
	else{
		no_spj='';
	}
  
	if (telppengirim.value=='' || !cekValueNoTelp(telppengirim.value)){
		telppengirim.style.background="red";
		valid=false;
  }
	
	if (namapengirim.value==''){
		namapengirim.style.background="red";
		valid=false;
	}
	
	if (alamatpengirim.value==''){
		alamatpengirim.style.background="red";
		valid=false;
	}
  
	if (telppenerima.value=='' || !cekValueNoTelp(telppenerima.value)){
		telppenerima.style.background="red";
		valid=false;
  }
	
	if (namapenerima.value==''){
		namapenerima.style.background="red";
		valid=false;
	}
	
	if (alamatpenerima.value==''){
		alamatpenerima.style.background="red";
		valid=false;
	}
	
	if (kotaasal.value==''){
		kotaasal.style.background="red";
		valid=false;
	}
	
	if (kotatujuan.value==''){
		kotatujuan.style.background="red";
		valid=false;
	}
	
	if (koli.value*1<=0){
		koli.style.background="red";
		valid=false;
  }
	
	if (isvolumetric0.checked){
		if (berat.value*1<=0){
			berat.style.background="red";
			valid=false;
		}
	}
	else{
		if (dimensipanjang.value*1<=0){
			dimensipanjang.style.background="red";
			valid=false;
		}
		
		if (dimensilebar.value*1<=0){
			dimensilebar.style.background="red";
			valid=false;
		}
		
		if (dimensitinggi.value*1<=0){
			dimensitinggi.style.background="red";
			valid=false;
		}
	}
	
	if (isasuransi1.checked){
		if (hargadiakui.value*1<=0){
			hargadiakui.style.background="red";
			valid=false;
		}
	}
	
	if (deskripsibarang.value==''){
		deskripsibarang.style.background="red";
		valid=false;
  }
	
  if(!valid){
		return;
	}
	
	optkotaasal		= document.getElementById("kotaasal");
	optkotatujuan = document.getElementById("kotatujuan");
	optpickedup		= document.getElementById("pointpickup");
	
  new Ajax.Request("reservasi.cargo.php?sid="+SID,{
    asynchronous: true,
    method: "post",
    parameters: 
			"mode=pakettambah"+
			"&telppengirim="+telppengirim.value+
			"&namapengirim="+namapengirim.value+
			"&alamatpengirim="+alamatpengirim.value+
			"&telppenerima="+telppenerima.value+
			"&namapenerima="+namapenerima.value+
			"&alamatpenerima="+alamatpenerima.value+
			"&tglberangkat="+p_tgl_user.value+
			"&kodeasal="+kotaasal.value+
			"&asal="+optkotaasal.options[optkotaasal.selectedIndex].text+
			"&kodetujuan="+kotatujuan.value+
			"&tujuan="+optkotatujuan.options[optkotatujuan.selectedIndex].text+
			"&jenisbarang="+jenis.value+
			"&via="+via.value+
			"&layanan="+service.value+
			"&koli="+koli.value+
			"&isvolumetric=" +(isvolumetric0.checked?0:1)+
			"&dimensipanjang="+dimensipanjang.value+
			"&dimensilebar="+dimensilebar.value+
			"&dimensitinggi="+dimensitinggi.value+
			"&berat="+berat.value+
			"&biayatambahan="+biayatambahan.value+
			"&biayapacking="+biayapacking.value+
			"&isasuransi="+(isasuransi0.checked?0:1)+
			"&hargadiakui="+hargadiakui.value+
			"&istunai="+istunai.value+
			"&deskripsibarang="+deskripsibarang.value+
			"&poinpickedup="+optpickedup.value+
			"&namapoinpickedup="+optpickedup.options[optpickedup.selectedIndex].text+
			"&nospj="+document.getElementById("nospj").value,
			
    onLoading: function(request) 
    {
			document.getElementById("loadingrefresh").style.display="block"; 
    },
    onComplete: function(request) 
    {
			
    },
    onSuccess: function(request) 
    {
			
			var prosesok=false;
			var noawb;
			
			eval(request.responseText);
			
			if (prosesok) {
				resetIsianPaket();
				getUpdatePaket();
			}
			else{
				alert("Terjadi Kegagalan!");
			}
			
			document.getElementById("loadingrefresh").style.display="none"; 
		},
    onFailure: function(request) 
    {
       alert('Gagal menyimpan');        
       assignError(request.responseText);
    }
  });
}

function resetIsianPaket(){
	
	isvolumetric0.checked=true;
	setFormVolumetric();
	
	biayakirim.innerHTML=0;
	leadtime.innerHTML=0;
	
	isasuransi0.checked=true;
	biayaasuransi.innerHTML=0;
	setFormAsuransi();
	
	hargaperkg.value="";
	
	telppengirim.style.background="";
	namapengirim.style.background="";
	alamatpengirim.style.background="";
	telppenerima.style.background="";
	namapenerima.style.background="";
	alamatpenerima.style.background="";
	
	if (document.getElementById("kotaasal")) {
		kotaasal.style.background="";
	}
	
	if (document.getElementById("kotatujuan")) {
		kotatujuan.style.background="";
	}
	
	koli.style.background="";
	berat.style.background="";
	dimensipanjang.style.background="";
	dimensilebar.style.background="";
	dimensitinggi.style.background="";
	hargadiakui.style.background="";
	deskripsibarang.style.background="";
	
	telppengirim.value="";
	namapengirim.value="";
	alamatpengirim.value="";
	telppenerima.value="";
	namapenerima.value="";
	alamatpenerima.value="";
	koli.value="";
	berat.value="";
	biayatambahan.value="";
	biayapacking.value="";
	dimensipanjang.value="";
	dimensilebar.value="";
	dimensitinggi.value="";
	hargadiakui.value="";
	deskripsibarang.value="";
	
}

function ambilDataPaket(no_resi){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		dialog_cari_paket.hide();
		
		document.getElementById('rewrite_ambil_paket').innerHTML="";
		
		if(no_resi==""){
			alert("Anda belum memasukkan no resi paket yang akan dicari!");
			return;
		}
		
		dialog_ambil_paket.show();
		
		new Ajax.Updater("rewrite_ambil_paket","reservasi.cargo.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "no_tiket=" +no_resi+"&mode=paketdetail&submode=ambil",
	        onLoading: function(request){
						Element.show('progress_ambil_paket');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_ambil_paket');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
		
}

function cetakAWB(awb){
  //mencetak resi
  Start("reservasi.cargo.resi.php?sid="+SID+"&noawb="+awb);
}

function cariDataPelangganByTelp(objek){
	
	if(objek.value==""){
		return;
	}
	
	if(!cekValueNoTelp(objek.value)){	
		alert("Nomor telepon yang anda masukkan tidak benar!");
		return;
	}
	
	new Ajax.Request("reservasi.cargo.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: "telp="+objek.value+
								"&mode=periksanotelp&obj="+objek.id,
    onLoading: function(request) 
    {
			Element.show("loadingcari"+objek.id);
    },
    onComplete: function(request) 
    {
			Element.hide("loadingcari"+objek.id);
    },
    onSuccess: function(request) 
    {		
			var prosesok=false;
			
			eval(request.responseText);
			
			if(!prosesok) {
				alert("Terjadi kegagalan");
			}
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
			
}

function periksaPaket(no_resi){
	// Mendapatkan dan menampilkan data tiket dengan no_tiket tertentu
		
		
		document.getElementById('rewrite_cari_paket').innerHTML="";
		
		if(no_resi==""){
			alert("Anda belum memasukkan no resi paket yang akan dicari!");
			return;
		}
		
		dialog_cari_paket.show();
		
		new Ajax.Updater("rewrite_cari_paket","reservasi.cargo.php?sid="+SID, 
	  {
	        asynchronous: true,
	        method: "get",
	        parameters: "mode=caripaket&noresi=" +no_resi+"&",
	        onLoading: function(request){
						Element.show('progress_cari_paket');
	        },
	        onComplete: function(request) {
	        },
	        onSuccess: function(request){
						Element.hide('progress_cari_paket');
	        },
	        onFailure: function(request) 
	        {
	           assignError(request.responseText);
	        }
	  }) 
}

function setFlagMutasiPaket(){
	
	if(document.getElementById('ismodemutasion').value!=1){
		if(confirm("Anda akan mengaktifkan mode mutasi, " + 
			"ketika anda memilih menekan tombol 'Mutasikan Kesini' pada 'Daftar Barang', maka secara otomatis barang di data lama akan dimutasikan ke data keberangkatan yang baru."+
			"Klik OK untuk melanjutkan proses mutasi atau klik CANCEL untuk keluar dari mode mutasi!")){
			lanjut = true;
		}
		else{
			lanjut = false;
		}
	}
	else{
		lanjut=true;
	}
	
	if(lanjut){
		document.getElementById('ismodemutasion').value	= 1-document.getElementById('ismodemutasion').value;
		
		if(document.getElementById('ismodemutasion').value==1){
			document.getElementById('tombolmutasipaket').value="B A T A L K A N   M U T A S I";
		}
		else{
			document.getElementById('tombolmutasipaket').value="M U T A S I";
		}
		
		getUpdatePaket();
	}
}

function mutasiPaket(){
  
	if(!document.getElementById('pointpickup')){
		alert("Anda belum memilih point pick up!");
		return;
	}
	
	tgl  					= document.getElementById('p_tgl_user').value;
  pointpickup  	= document.getElementById('pointpickup').value;  
	id						= document.getElementById('idpaket').value;
	
	if(!confirm("Apakah anda yakin akan memindahkan barang ke keberangkatan ini?")){
		exit;
	}
	
	optpickedup		= document.getElementById("pointpickup");
	
  new Ajax.Request("reservasi.cargo.php?sid="+SID, 
  {
    asynchronous: true,
    method: "get",
    parameters: 
			"mode=mutasipaket"+
			"&id="+id+
			"&tanggal=" +tgl+
			"&pointpickedup="+pointpickup+
			"&namapoinpickedup="+optpickedup.options[optpickedup.selectedIndex].text,
			
    onLoading: function(request){
			document.getElementById("loadingrefresh").style.display="block"; 
    },
    onComplete: function(request){
			
    },
    onSuccess: function(request) 
    {			
			var prosesok=true;
			
			eval(request.responseText);
			
			if(prosesok){
				document.getElementById('ismodemutasion').value=0;
				document.getElementById('showdatadetail').innerHTML='';
				
				getUpdatePaket();
				
			}
			else{
				alert("Terjadi kegagalan!");
			}
			
			document.getElementById("loadingrefresh").style.display="none"; 
		},
    onFailure: function(request) 
    {
       alert('Error !!! Cannot Save');        
       assignError(request.responseText);
    }
  })  
}

function setDialogSPJ(){
  //menampilkan dialog spj
	
	nospj  = document.getElementById('nospj').value;
	
	popup_loading.show();
	
	new Ajax.Updater("showlistpembawa","reservasi.cargo.php?sid="+SID,{
    asynchronous: true,
    method: "get",
    parameters:
			"mode=setdialogpembawa"+
			"&nospj=" +nospj,
    onLoading: function(request) {
			
    },
    onComplete: function(request){
    },                
    onFailure: function(request) 
    { 
      assignError(request.responseText);
    }
  });
	
	popup_loading.hide();
	
	dialog_SPJ.show();
}

function cetakSPJ(){
  //mencetak SPJ
	
	valid=true;
	
	if (optkurir.value==''){
		optkurir.style.background="red";
		valid=false;
	}
	
	if(!valid) {
		exit;
	}
	
	optpickedup					= document.getElementById("pointpickup");
	optpetugaspickedup	= document.getElementById("optkurir");
	
	Start("reservasi.cargo.spj.php?sid="+SID+
		"&tglberangkat="+p_tgl_user.value+
		"&poinpickedup="+optpickedup.value+
		"&namapoinpickedup="+optpickedup.options[optpickedup.selectedIndex].text+
		"&kurir="+optpetugaspickedup.value+
		"&namakurir="+optpetugaspickedup.options[optpetugaspickedup.selectedIndex].text+
		"&nospj="+document.getElementById("nospj").value);
	
	dialog_SPJ.hide();
}

// global
var dialog_SPJ,dialog_ubah_tiket,btn_SPJ_OK,btn_SPJ_Cancel,msg;

function init(e) {
  // inisialisasi variabel
	SID =document.getElementById("hdn_SID").value;
	//harga_minimum_paket = document.getElementById("hdn_harga_minimum_paket").value;
	
	popup_loading	= dojo.widget.byId("popuploading");
	
	setKotaAsal(provasal.value);
	setKotaTujuan(provtujuan.value);
	
	resetIsianPaket();
	
	//control dialog cari paket
	dialog_cari_paket = dojo.widget.byId("dialog_cari_paket");
  btn_cari_paket_Cancel = document.getElementById("dialog_cari_paket_btn_Cancel");
  dialog_cari_paket.setCloseControl(btn_cari_paket_Cancel);
	
	//control dialog ambil paket
	dialog_ambil_paket 						= dojo.widget.byId("dialog_ambil_paket");
  dlg_ambil_paket_button_ok 		= document.getElementById("dlg_ambil_paket_button_ok");
  dlg_ambil_paket_button_cancel = document.getElementById("dlg_ambil_paket_button_cancel");

	//control dialog pembayaran
	dialog_pembayaran = dojo.widget.byId("dialog_pembayaran");
  btn_pembayaran_OK = document.getElementById("dialog_pembayaran_btn_OK");
  btn_pembayaran_Cancel = document.getElementById("dialog_pembayaran_btn_Cancel");
  dialog_pembayaran.setCloseControl(btn_pembayaran_Cancel);
	
	//control dialog box SPJ
	dialog_SPJ = dojo.widget.byId("dialog_SPJ");
  btn_SPJ_OK = document.getElementById("dialog_SPJ_btn_OK");
  btn_SPJ_Cancel = document.getElementById("dialog_SPJ_btn_Cancel");
  //dialog_SPJ.setCloseControl(btn_SPJ_OK);
  dialog_SPJ.setCloseControl(btn_SPJ_Cancel);
	
	getPengumuman();

}

dojo.addOnLoad(init);

var jdwl,tgl;
var waktu_refresh=10;//detik
var temp_waktu=waktu_refresh;
var g_flag_refresh=1;

function beginrefresh(){

	if (temp_waktu==1) {
		if(g_flag_refresh==1){
			
			temp_waktu=waktu_refresh;
			
			beginrefresh();
			
			//merefresh pengumumman
			getPengumuman();
			getUpdatePaket();
			
		}
	}
	else {
		if(g_flag_refresh==1){
			//get the minutes, seconds remaining till next refersh
			temp_waktu-=1;
			setTimeout("beginrefresh()",1000);
		}
	}
	
}

function resetTimerRefresh(){
	temp_waktu=waktu_refresh;
}

function setAutoRefreshOff(){
	g_flag_refresh=0;
}

function setAutoRefreshOn(){
	g_flag_refresh=1;
	beginrefresh();
}

//call the function beginrefresh
window.onload=beginrefresh();
//-->