<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function Start(page) {
		OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
	}
	
	function getUpdateAsal(kota){
		
		new Ajax.Updater("rewrite_asal","laporan.keuangan.biayaop.php?sid={SID}", {
			asynchronous: true,
			method: "get",
		
			parameters: "mode=getasal&kota="+kota+"&asal={ASAL}",
			onLoading: function(request){
			},
			onComplete: function(request){
				Element.show('rewrite_asal');
			},
			onFailure: function(request){ 
				assignError(request.responseText); 
			}
		});				
	}
	
	function getUpdateTujuan(asal){
    // fungsi ini mengubah tujuan menjadi isi yang sesuai konteks :P saat pengguna melakukan click [GO]   
		
		new Ajax.Updater("rewrite_tujuan","laporan.keuangan.biayaop.php?sid={SID}", 
    {
        asynchronous: true,
        method: "get",
        parameters: "asal=" + asal + "&tujuan={TUJUAN}&mode=gettujuan",
        onLoading: function(request) 
        {
        },
        onComplete: function(request) 
        {	
        },
        onFailure: function(request) 
        { 
          assignError(request.responseText); 
        }
    });   
	}
	
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
	}
	
	function init(e){
			
		getUpdateAsal("{KOTA}");
		getUpdateTujuan("{ASAL}");
		setSortId();
	}
	
	dojo.addOnLoad(init);
	
</script>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
 <td class="whiter" valign="middle" align="left">		
	<!--HEADER-->
	<table width='100%' cellspacing="0">
		<tr class='banner' height=40>
			<td align='center' valign='middle' class="bannerjudul">&nbsp;Laporan Keuangan Biaya Operasional</td>
		</tr>
		<tr class='banner'>
			<td align='right' valign='middle' class="bannernormal">
				<form action="{ACTION_CARI}" method="post">
					Filter biaya:&nbsp;
					<input id="filbiaya1" name="filbiaya1" type="checkbox" {CHKBIAYA1}>Tol&nbsp;
					<input id="filbiaya8" name="filbiaya8" type="checkbox" {CHKBIAYA8}>Tambahan Tol&nbsp;
					<input id="filbiaya2" name="filbiaya2" type="checkbox" {CHKBIAYA2}>Sopir&nbsp;
					<input id="filbiaya5" name="filbiaya5" type="checkbox" {CHKBIAYA5}>Insentif Sopir&nbsp;
					<input id="filbiaya3" name="filbiaya3" type="checkbox" {CHKBIAYA3}>BBM&nbsp;
					<input id="filbiaya7" name="filbiaya7" type="checkbox" {CHKBIAYA7}>Tambahan BBM&nbsp;
					<input id="filbiaya4" name="filbiaya4" type="checkbox" {CHKBIAYA4}>Parkir&nbsp;
					<input id="filbiaya9" name="filbiaya9" type="checkbox" {CHKBIAYA9}>Lainnya&nbsp;
					</br></br>
					Kota:&nbsp;<select onchange='getUpdateAsal(this.value);' id='kota' name='kota'><option value=''>-semua kota-</option>{OPT_KOTA}</select>
					Asal:&nbsp;<span id='rewrite_asal'></span>
					&nbsp;Tujuan:&nbsp;<span id='rewrite_tujuan'></span>
					&nbsp;Tgl:&nbsp;<input readonly="yes"  id="tanggal_mulai" name="tanggal_mulai" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}">
					&nbsp; s/d &nbsp;<input readonly="yes"  id="tanggal_akhir" name="tanggal_akhir" type="text" size=10 onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}">
					Cari:<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />
					<input name="btn_cari" type="submit" value="cari" />				
				</form>
			</td>
		</tr>
		<tr>
			<td colspan=2 align='center' valign='middle'>
				<table>
					<tr>
						<td>
							<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align='left' valign='bottom' colspan=3>
			{SUMMARY}
			</td>
		</tr>
		<tr><td colspan=2 align='right'>{PAGING}</td></tr>
	</table>
	<!-- END HEADER-->
	<table class="border" width='100%' >
		<!-- BEGIN ROW -->
		<tr style="display:{ROW.showheader};">
			 <th width=30>No</th>
			 <th width=60><a class="th" id="sort1" href='#' >Tanggal</a></th>
			 <th width=100><a class="th" id="sort2" href='#'>Jurusan</a></th>
			 <th width=50><a class="th" id="sort3" href='#' >Jam</a></th>
			 <th width=100><a class="th" id="sort4" href='#'>Jenis Biaya</a></th>
			 <th width=100><a class="th" id="sort5" href='#'>No.SPJ</a></th>
			 <th width=70><a class="th" id="sort6" href='#'>Kendaraan</a></th>
			 <th width=100><a class="th" id="sort7" href='#'>Sopir</a></th>
			 <th width=70><a class="th" id="sort8" href='#'>Jumlah</a></th>
			 <th width=100><a class="th" id="sort11" href='#'>Remark</a></th>
			 <th width=100><a class="th" id="sort9" href='#'>Releaser</a></th>
			 <th width=100><a class="th" id="sort10" href='#'>Released</a></th>
		 </tr>
		 <tr class="{ROW.odd}">
			 <td align="right">{ROW.no}</td>
			 <td align="center">{ROW.tanggal}</td>
			 <td align="center">{ROW.jurusan}</td>
			 <td align="center">{ROW.jam}</td>
			 <td align="center">{ROW.jenisbiaya}</td>
			 <td align="center">{ROW.no_spj}</td>
			 <td align="center">{ROW.kendaraan}</td>
			 <td align="center">{ROW.sopir}</td>
			 <td align="right">{ROW.jumlah}</td>
			 <td align="center">{ROW.remark}</td>
			 <td align="center">{ROW.releaser}</td>
			 <td align="center">{ROW.released}</td>
		 </tr>
		 <!-- END ROW -->
  </table>
	{NO_DATA}
	<table width='100%'>
		<tr>
			<td align='right' width='100%'>
				{PAGING}
			</td>
		</tr>
		<tr>
			<td align='left' valign='bottom' colspan=3>
			{SUMMARY}
			</td>
		</tr>
	</table>
 </td>
</tr>
</table>