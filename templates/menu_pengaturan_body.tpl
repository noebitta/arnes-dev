<div class="menuwrapper" style="height: {HEIGT_WRAPPER}px">
	<br/>
  <!-- BEGIN menu_pengaturan_kota -->
  <div class="menuicon">
    <a href="{U_KOTA}">
      <img src="{TPL}images/icon_master_kota.png" /><br/>
      Kota
    </a>
  </div>
  <div style="height: {menu_pengaturan_kota.br};"></div>
  <!-- END menu_pengaturan_kota -->

	<!-- BEGIN menu_pengaturan_cabang -->
	<div class="menuicon">
		<a href="{U_CABANG}">
			<img src="{TPL}images/icon_master_cabang.png" /><br/>
			Cabang
		</a>
	</div>
	<div style="height: {menu_pengaturan_cabang.br};"></div>
	<!-- END menu_pengaturan_cabang -->
	
	<!-- BEGIN menu_pengaturan_jurusan -->
	<div class="menuicon">
		<a href="{U_JURUSAN}">
			<img src="{TPL}images/icon_master_jurusan.png" /><br />
			Jurusan
		</a>
	</div>
	<div style="height: {menu_pengaturan_jurusan.br};"></div>
	<!-- END menu_pengaturan_jurusan -->
	
	<!-- BEGIN menu_pengaturan_jadwal -->
	<div class="menuicon">
		<a href="{U_JADWAL}">
			<img src="{TPL}images/icon_master_jadwal.png" /><br />
			Jadwal
		</a>
	</div>
	<div style="height: {menu_pengaturan_jadwal.br};"></div>
	<!-- END menu_pengaturan_jadwal -->
	
	<!-- BEGIN menu_pengaturan_umum -->
	<div class="menuicon">
		<a href="{U_PENGATURAN_UMUM}">
			<img src="{TPL}images/icon_pengaturan_umum.png" /><br />
			Pengaturan Umum
		</a>
	</div>
	<div style="height: {menu_pengaturan_umum.br};"></div>
	<!-- END menu_pengaturan_umum -->
	
	<!-- BEGIN menu_pengaturan_sopir -->
	<div class="menuicon">
		<a href="{U_SOPIR}">
			<img src="{TPL}images/icon_master_sopir.png" /><br />
			Sopir
		</a>
	</div>
	<div style="height: {menu_pengaturan_sopir.br};"></div>
	<!-- END menu_pengaturan_sopir -->
	
	<!-- BEGIN menu_pengaturan_mobil -->
	<div class="menuicon">
		<a href="{U_MOBIL}">
			<img src="{TPL}images/icon_master_mobil.png" /><br />
			Mobil
		</a>
	</div>
	<div style="height: {menu_pengaturan_mobil.br};"></div>
	<!-- END menu_pengaturan_mobil -->
	
	<!-- BEGIN menu_pengaturan_macaddress -->
	<div class="menuicon">
		<a href="{U_MACADDRESS}">
			<img src="{TPL}images/icon_pengaturan_mac.png" /><br />
			Daftar Komputer
		</a>
	</div>
	<div style="height: {menu_pengaturan_macaddress.br};"></div>
	<!-- END menu_pengaturan_macaddress -->
	
	<!-- BEGIN menu_pengaturan_user -->
	<div class="menuicon">
		<a href="{U_USER}">
			<img src="{TPL}images/icon_master_user.png" /><br />
			User
		</a>
	</div>
	<div style="height: {menu_pengaturan_user.br};"></div>
	<!-- END menu_pengaturan_user -->

	<!-- BEGIN menu_pengaturan_page -->
	<div class="menuicon">
		<a href="{U_PAGE}">
			<img src="{TPL}images/icon_menu_password.png" /><br />
			Pengaturan Page
		</a>
	</div>
	<div style="height: {menu_pengaturan_kota.br};"></div>
	<!-- END menu_pengaturan_page -->

</div>