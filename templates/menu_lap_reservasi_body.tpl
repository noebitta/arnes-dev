<div class="menuwrapper" style="height: {HEIGT_WRAPPER}px">
	<br/>
	<!-- BEGIN menu_lap_reservasi_cso -->
	<div class="menuicon">
		<a href="{U_LAPORAN_CSO}">
			<img src="{TPL}images/icon_laporan_cso.png" /><br/>
			CSO
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_cso.br};"></div>
	<!-- END menu_lap_reservasi_cso -->
	
	<!-- BEGIN menu_lap_reservasi_cabang -->
	<div class="menuicon">
		<a href="{U_LAPORAN_CABANG}">
			<img src="{TPL}images/icon_laporan_cabang.png" /><br/>
			Cabang
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_cabang.br};"></div>
	<!-- END menu_lap_reservasi_cabang -->
	
	<!-- BEGIN menu_lap_reservasi_jurusan -->
	<div class="menuicon">
		<a href="{U_LAPORAN_JURUSAN}">
			<img src="{TPL}images/icon_laporan_jurusan.png" /><br/>
			Jurusan
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_jurusan.br};"></div>
	<!-- END menu_lap_reservasi_jurusan -->
	
	<!-- BEGIN menu_lap_reservasi_jam -->
	<div class="menuicon">
		<a href="{U_LAPORAN_JADWAL}">
			<img src="{TPL}images/icon_laporan_jadwal.png" /><br/>
			Berdasarkan<br>Jam
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_jam.br};"></div>
	<!-- END menu_lap_reservasi_jam -->
	
	<!-- BEGIN menu_lap_reservasi_seluruh -->
	<div class="menuicon">
		<a href="{U_LAPORAN_OMZET}">
			<img src="{TPL}images/icon_laporan_omzet.png" /><br/>
			Keseluruhan
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_seluruh.br};"></div>
	<!-- END menu_lap_reservasi_seluruh -->
	
	<!-- BEGIN menu_lap_reservasi_kendaraan -->
	<div class="menuicon">
		<a href="{U_LAPORAN_KENDARAAN}">
			<img src="{TPL}images/icon_laporan_kendaraan.png" /><br/>
			Kendaraan
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_kendaraan.br};"></div>
	<!-- END menu_lap_reservasi_kendaraan -->
	
	<!-- BEGIN menu_lap_reservasi_sopir -->
	<div class="menuicon">
		<a href="{U_LAPORAN_SOPIR}">
			<img src="{TPL}images/icon_laporan_sopir.png" /><br/>
			Sopir
		</a>
	</div>
	<div style="height: {menu_lap_reservasi_sopir.br};"></div>
	<!-- END menu_lap_reservasi_sopir -->
	
	<!-- BEGIN menu_lap_voucher -->
	<div class="menuicon">
		<a href="{U_LAPORAN_VOUCHER}">
			<img src="{TPL}images/icon_laporan_voucher.png" /><br/>
			Voucher
		</a>
	</div>
	<div style="height: {menu_lap_voucher.br};"></div>
	<!-- END menu_lap_voucher -->
	
</div>
