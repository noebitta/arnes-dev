<div class="menuwrapper" style="height: {HEIGHT_WRAPPER}px">
	<br/>
	<!-- BEGIN menu_rsv_reg -->
	<div class="menuicon">
		<a href="{U_RSV_REG}">
			<img src="{TPL}images/icon_booking.png" /><br/>
			Rsv. Reguler
		</a>
	</div>
	<div style="height: {menu_rsv_reg.br};"></div>
	<!-- END menu_rsv_reg -->

  <!-- BEGIN menu_rsv_nonreg
  <div class="menuicon">
    <a href="{U_RSV_NONREG}">
      <img src="{TPL}images/icon_booking.png" /><br/>
      Rsv. Non-Reguler
    </a>
  </div>
  <div style="height: {menu_rsv_nonreg.br};"></div>
  END menu_rsv_nonreg -->
	
	<!-- BEGIN menu_penjadwalan -->
	<div class="menuicon">
		<a href="{U_PENJADWALAN}">
			<img src="{TPL}images/icon_penjadwalan.png" /><br />
			Penjadwalan<br>Kendaraan
		</a>
	</div>
	<div style="height: {menu_penjadwalan.br};"></div>
	<!-- END menu_penjadwalan -->
	
	<!-- BEGIN menu_pengumuman -->
	<div class="menuicon">
		<a href="{U_PENGUMUMAN}">
			<img src="{TPL}images/icon_pengumuman.png" /><br />
			Pengumuman
		</a>
	</div>
	<div style="height: {menu_pengumuman.br};"></div>
	<!-- END menu_pengumuman -->
	
	<!-- BEGIN menu_jenis_discount -->
	<div class="menuicon">
		<a href="{U_JENIS_DISCOUNT}">
			<img src="{TPL}images/icon_jenis_diskon.png" /><br />
			Jenis Discount
		</a>
	</div>
	<div style="height: {menu_jenis_discount.br};"></div>
	<!-- END menu_jenis_discount -->
	
	<!-- BEGIN menu_ubah_password -->
	<div class="menuicon">
		<a href="{U_UBAHPASS}">
			<img src="{TPL}images/icon_menu_password.png" /><br />
			Ubah Password
		</a>
	</div>
	<div style="height: {menu_ubah_password.br};"></div>
	<!-- END menu_ubah_password -->
	
	<!-- BEGIN menu_status_online -->
	<div class="menuicon">
		<a href="{U_USER_LOGIN}">
			<img src="{TPL}images/icon_user_online.png" /><br />
			Status On-Line<br>User
		</a>
	</div>
	<div style="height: {menu_status_online.br};"></div>
	<!-- END menu_status_online -->
	
	<!-- BEGIN menu_promo -->
	<div class="menuicon">
		<a href="{U_PROMO}">
			<img src="{TPL}images/icon_promo.png" /><br />
			Promo
		</a>
	</div>
	<div style="height: {menu_promo.br};"></div>
	<!-- END menu_promo -->
	
	<!-- BEGIN daftar_manifest -->
	<div class="menuicon">
		<a href="{U_DAFTAR_MANIFEST}">
			<img src="{TPL}images/icon_laporan_kendaraan.png" /><br />
			Daftar Manifest
		</a>
	</div>
	<div style="height: {daftar_manifest.br};"></div>
	<!-- END daftar_manifest -->
	
	<!-- BEGIN menu_pembatalan -->
	<div class="menuicon">
		<a href="{U_BATAL}">
			<img src="{TPL}images/icon_batal.png" /><br />
			Pembatalan
		</a>
	</div>
	<div style="height: {menu_pembatalan.br};"></div>
	<!-- END menu_pembatalan -->
	
	<!-- BEGIN menu_laporan_pembatalan -->
	<div class="menuicon">
		<a href="{U_LOG_BATAL}">
			<img src="{TPL}images/icon_laporan_tiket_batal.png" /><br />
			Laporan<br>Pembatalan
		</a>
	</div>
	<div style="height: {menu_laporan_pembatalan.br};"></div>
	<!-- END menu_laporan_pembatalan -->
	
	<!-- BEGIN menu_laporan_koreksi_disc -->
	<div class="menuicon">
		<a href="{U_LOG_KOREKSI_DISC}">
			<img src="{TPL}images/icon_jenis_diskon.png" /><br />
			Laporan<br>Koreksi Discount
		</a>
	</div>
	<div style="height: {menu_laporan_koreksi_disc.br};"></div>
	<!-- END menu_laporan_koreksi_disc -->
	
	<!-- BEGIN menu_dashboard -->
	<div class="menuicon">
		<a href="{U_DASHBOARD}">
			<img src="{TPL}images/icon_dashboard.png" /><br />
			Dashboard
		</a>
	</div>
	<div style="height: {menu_dashboard.br};"></div>
	<!-- END menu_dashboard -->
	
	<!-- BEGIN menu_general_total -->
	<div class="menuicon">
		<a href="{U_GT}">
			<img src="{TPL}images/icon_gt.png" /><br />
			General Total
		</a>
	</div>
	<div style="height: {menu_general_total.br};"></div>
	<!-- END menu_general_total -->
	
	<!-- BEGIN menu_berita_acara_insentif_sopir -->
	<div class="menuicon">
		<a href="{U_BERITA_ACARA}">
			<img src="{TPL}images/icon_ba.png" /><br />
			Berita Acara<br>Insentif Sopir
		</a>
	</div>
	<div style="height: {menu_berita_acara_insentif_sopir.br};"></div>
	<!-- END menu_berita_acara_insentif_sopir -->
	
	<!-- BEGIN menu_berita_acara_biayaoperasional -->
	<div class="menuicon">
		<a href="{U_BERITA_ACARA_BOP}">
			<img src="{TPL}images/icon_ba.png" /><br />
			Berita Acara<br>Biaya Operasional
		</a>
	</div>
	<div style="height: {menu_berita_acara_insentif_sopir.br};"></div>
	<!-- END menu_berita_acara_biayaoperasional -->
	
</div>