<html>
<head>
	<link rel="stylesheet" href="./templates/trav.css" type="text/css" />
</head>
<script type="text/javascript">
	function printWindow() {
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
</script>	 

<body class='tiket'>
<div style="font-size:14px;width:300px;">
	<!-- BEGIN RESI -->
	<span style="font-size:22px;">{NAMA_PERUSAHAAN}</span><br>
	<span style="font-size: 18px;font-weight: bold;text-transform: uppercase;">
		AWB: {AWB}<br>
		[{LAYANAN}] {VIA}<br>
		[-{KOTA_TUJUAN}-]
	</span><br>
	{RESI.peruntunkan}<br>
	-------------------------------<br>
	[DATA PENGIRIM]<br>
	{TELP_PENGIRIM} {NAMA_PENGIRIM}</br>	
	{ALAMAT_PENGIRIM}</br>
	-------------------------------<br>
	[DATA PENERIMA]<br>
	{TELP_PENERIMA} {NAMA_PENERIMA}</br>
	{ALAMAT_PENERIMA}</br>	
	-------------------------------<br>
	[DATA BARANG]<br>
	{KOTA_ASAL}-{KOTA_TUJUAN}</br>
	{LAYANAN}|{JENIS_BARANG}|{VIA}<br>
	{KOLI} Pax | {BERAT} Kg</br>
	<span class="resicargolabel">Lead Time</span><span class="resicargofield">:</span><span class="resicargofield">{LEAD_TIME} Hari</span></br>
	<span class="resicargolabel">Asuransi</span><span class="resicargofield">:</span><span class="resicargofield">{IS_ASURANSI}</span><br>
	<!-- BEGIN POLIS_ASURANSI -->
	<span class="resicargolabel">No.Polis</span><span class="resicargofield">:</span><span class="resicargofield">{NO_POLIS}</span><br>
	<!-- END POLIS_ASURANSI -->
	<span class="resicargolabel">Total Biaya</span><span class="resicargofield">:</span><span class="resicargofield">Rp. {TOTAL_BIAYA}</span></br>
	<span class="resicargolabel">Pembayaran</span><span class="resicargofield">:</span><span class="resicargofield">{CARA_BAYAR}</span></br>
	-------------------------------<br>						
	<span class="resicargolabel">Isi Barang</span><span class="resicargofield">:</span><span class="resicargofield">{DESKRIPSI_BARANG}</span></br>
	-------------------------------<br>
	<span class="resicargolabel">CSO</span><span class="resicargofield">:</span><span class="resicargofield">{CSO}</span></br>
	<span class="resicargolabel">Waktu Terima</span><span class="resicargofield">:</span><span class="resicargofield">{WAKTU_TERIMA}</span></br>
	<br><br>
	{RESI.ttd_penerima}
	<!-- END RESI -->
	
	<!-- BEGIN LABEL_PAKET -->
	----------potong disini--------<br>
	<span style="font-size:22px;">{NAMA_PERUSAHAAN}</span><br>
	<span style="font-size: 18px;font-weight: bold;text-transform: uppercase;">
		AWB: {AWB}<br>
		[{LAYANAN}] {VIA}<br>
		[-{KOTA_TUJUAN}-]
	</span><br>
	-------------------------------<br>
	UNTUK DI TEMPEL DI BARANG<br>
	<b>KOLI KE-{LABEL_PAKET.i_koli} DARI {KOLI}</b><br>
	[DATA PENGIRIM]<br>
	<span class="resicargolabel">Telp</span><span class="resicargofield">:</span><span class="resicargofield">{TELP_PENGIRIM}</span></br>	
	<span class="resicargolabel">Nama</span><span class="resicargofield">:</span><span class="resicargofield">{NAMA_PENGIRIM}</span></br>
	<span class="resicargolabel">Alamat</span><span class="resicargofield">:</span><span class="resicargofield">{ALAMAT_PENGIRIM}</span></br>
	-------------------------------<br>
	[DATA PENERIMA]<br>
	<span class="resicargolabel">Telp</span><span class="resicargofield">:</span><span class="resicargofield">{TELP_PENERIMA}</span></br>	
	<span class="resicargolabel">Nama</span><span class="resicargofield">:</span><span class="resicargofield">{NAMA_PENERIMA}</span></br>
	<span class="resicargolabel">Alamat</span><span class="resicargofield">:</span><span class="resicargofield">{ALAMAT_PENERIMA}</span></br>
	<br>
	<!-- END LABEL_PAKET -->
</div>
</body>


<script language="javascript">
	printWindow();
	window.close();
</script>
</html>