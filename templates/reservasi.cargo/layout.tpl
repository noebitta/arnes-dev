<input type='hidden' value='{NO_SPJ}' name='nospj' id='nospj' />

<div id="cargolayoutwrapper">
	<!-- BEGIN TOMBOL_MUTASIKAN -->
	<center><input type="button" value="Mutasikan Kesini" style="width:150px;height: 30px;" onclick="mutasiPaket();"></center>
	<!-- END TOMBOL_MUTASIKAN -->
	
	<span class="resvcargolayoutlabel">Tanggal</span><span class="resvcargolayoutfield">: {TANGGAL_BERANGKAT_DIPILIH}</span><br>
	<span class="resvcargolayoutlabel">Poin Pick Up</span><span class="resvcargolayoutfield">: {POIN_PICKEDUP_DIPILIH}</span><br>
	
	<!-- BEGIN DATA_MANIFEST -->
	<span class="resvcargolayoutlabel">#Manifest</span><span class="resvcargolayoutfield">: <b>{DATA_MANIFEST.no_spj}</b></span><br>
	<span class="resvcargolayoutlabel">Waktu Cetak</span><span class="resvcargolayoutfield">: {DATA_MANIFEST.waktu_cetak}</span><br>
	<span class="resvcargolayoutlabel">Pencetak</span><span class="resvcargolayoutfield">: {DATA_MANIFEST.petugas_pencetak}</span><br>
	<!-- END DATA_MANIFEST -->
	
	<!-- BEGIN BELUM_PICKUP -->
	<center><h2 style="color: red;">BELUM DI PICKED UP</h2></center>
	<!-- END BELUM_PICKUP -->
	
	<!-- BEGIN DATA_PICKUP -->
	<span class="resvcargolayoutlabel">Waktu Picked Up</span><span class="resvcargolayoutfield">: {DATA_PICKUP.waktu_pickedup}</span><br>
	<span class="resvcargolayoutlabel">Pemberi</span><span class="resvcargolayoutfield">: {DATA_PICKUP.petugas_pemberi}</span><br>
	<span class="resvcargolayoutlabel">Penerima</span><span class="resvcargolayoutfield">: {DATA_PICKUP.petugas_penerima}</span><br>
	<!-- END DATA_PICKUP -->
	<br>
	<center>
		<!-- BEGIN TOMBOL_CETAK_MANIFEST -->
		<input type='button' value='{TOMBOL_CETAK_MANIFEST.value}' onclick='setDialogSPJ();' style="width: 150px; height: 30px;margin-right: 5px;" />
		<!-- END TOMBOL_CETAK_MANIFEST -->
		<input type='button' value='REFRESH' onclick='getUpdatePaket();getPengumuman();' style="width: 150px; height: 30px;margin-left: 5px;"/>
		<hr>
		Trx:<b>{TOTAL_TRANSAKSI}</b> | Pax:<b>{TOTAL_PAX}</b> | Omzet:<b>Rp. {TOTAL_OMZET}</b>
		<hr>
	</center>
	<!-- BEGIN DAFTAR_CARGO -->
	<div class='{DAFTAR_CARGO.odd}' onclick="showPaket('{DAFTAR_CARGO.id}')" style="cursor: pointer;">
		<h3 style="float:left;font-size: 20px;padding-left: 5px;">{DAFTAR_CARGO.no}</h3>
		<div class="resvcargolistawb">AWB: {DAFTAR_CARGO.awb}</div>
		<div class="resvcargolisttitle">{DAFTAR_CARGO.jenis_barang} <span class="resvcargolistserviceurg">[{DAFTAR_CARGO.layanan}]</span></div>
		<div class="resvcargolistrute">{DAFTAR_CARGO.rute}</div>
		<div class="resvcargolistdata">Pengirim: {DAFTAR_CARGO.nama_pengirim}</div>
		<div class="resvcargolistdata">Penerima: {DAFTAR_CARGO.nama_penerima}</div>
		<div class="resvcargolistpax">Koli: {DAFTAR_CARGO.koli} Pax / Berat: {DAFTAR_CARGO.berat} Kg</div>
		<hr>
	</div>
	<!-- END DAFTAR_CARGO -->
	
	<!-- BEGIN NO_DATA -->
	<div style="background: #ffcc00;height: 50px;text-align: center;padding-top: 1px;"><h2>Daftar Barang Kosong</h2></div>
	<!-- END NO_DATA -->
</div>