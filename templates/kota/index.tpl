<script type="text/javascript" src="{ROOT}/ajax/dojo.js"></script>
<script language="JavaScript">
		// komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
	
	function selectAll(){
			
			i=1;
			loop=true;
			record_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					chk.checked=true;
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
	}
	
	function deselectAll(){
			
			i=1;
			loop=true;
			record_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					chk.checked=false;
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
			
	}
	
	function hapusData(kode){
		
		if(confirm("Apakah anda yakin akan menghapus data ini?")){
			
			if(kode!=''){
				list_dipilih="'"+kode+"'";
			}
			else{
				i=1;
				loop=true;
				list_dipilih="";
				do{
					str_var='checked_'+i;
					if(chk=document.getElementById(str_var)){
						if(chk.checked){
							if(list_dipilih==""){
								list_dipilih +=chk.value;
							}
							else{
								list_dipilih +=","+chk.value;
							}
						}
					}
					else{
						loop=false;
					}
					i++;
				}while(loop);
			}
				
			new Ajax.Request("pengaturan_kota.php?sid={SID}",{
			 asynchronous: true,
			 method: "get",
			 parameters: "mode=delete&list="+list_dipilih,
			 onLoading: function(request) 
			 {
			 },
			 onComplete: function(request) 
			 {
				
			 },
			 onSuccess: function(request) 
			 {			
				window.location.reload();
				deselectAll();
			},
			 onFailure: function(request) 
			 {
			 }
			})  
		}
		
		return false;
			
	}
		
	function setSortId(){
		listHrefSort = [{ARRAY_SORT}];
		
		for (i=0;i<listHrefSort.length;i++){
			document.getElementById("sort"+(i+1)).href=listHrefSort[i];
		}
		
	}
	
	function init(e){
		setSortId();
	}
	
	dojo.addOnLoad(init);
	
</script>
<div class="banner" style="vertical-align: middle;">
  <div class="bannerjudul" style="margin-left: 10px;">Master Kota</div>
  <div class="bannernormal" style="float: right;padding-top: 10px;">
    <form action="{ACTION_CARI}" method="post">
      Cari:&nbsp;<input type="text" id="cari" name="cari" value="{CARI}" size=50 />&nbsp;<input type="submit" value="cari" />&nbsp;
    </form>
  </div>
</div>
<br>
<div style="width: 800px;padding-bottom: 10px; ">
  <div style="float: left;">
    <input type="button" onclick="window.open('{U_ADD}','_self');" value="Tambah" style="width:80px;" />
    <input type="button" onClick="return hapusData('');" value="Hapus" style="width:80px;"/>
  </div>
  <div style="float: right;">
    <a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
    <a href="" onClick="deselectAll();return false;">Uncheck All</a>
  </div>
</div><br>
<table width='800' class="border">
  <tr>
    <th class='title' width=30></th>
    <th width="30">No</th>
		<th width="100"><a class="th" id="sort1" href='#'>Kode Kota</a></th>
		<th width="570"><a class="th" id="sort2" href='#'>Nama Kota</a></th>
		<th width="100">Action</th>
  </tr>
  <!-- BEGIN ROW -->
  <tr class="{ROW.odd}">
    <td align="center">{ROW.check}</td>
    <td align="center">{ROW.no}</td>
    <td align="center">{ROW.kodekota}</td>
		<td align="center">{ROW.namakota}</td>
    <td align="center">{ROW.action}</td>
  </tr>
  <!-- END ROW -->
</table>

<!-- BEGIN NO_DATA -->
<div style="font-size: 18px;background-color: yellow;text-align: center;width: 800px;">Tidak ada data ditemukan</div>
<!-- END NO_DATA -->

<div style="width: 800px;padding-top: 5px;">
  <div style="float: left;">
    <input type="button" onclick="window.open('{U_ADD}','_self');" value="Tambah" style="width:80px;" />
    <input type="button" onClick="return hapusData('');" value="Hapus" style="width:80px;"/>
  </div>
  <div style="float: right;">
    <a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
    <a href="" onClick="deselectAll();return false;">Uncheck All</a>
  </div>
</div>
