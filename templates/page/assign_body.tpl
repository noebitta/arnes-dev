<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">

var kode;

function cekValue(nilai){
	cek_value=nilai*0;

	if(cek_value==0){
		return true;
	}
	else{
		return false;
	}
}

function validateInput(){

	valid=true;

	Element.hide('nrp_invalid');
	Element.hide('nama_invalid');
	Element.hide('username_invalid');
	Element.hide('userpassword_invalid');
	Element.hide('konfirm_password_invalid');
	Element.hide('telp_invalid');
	Element.hide('hp_invalid');

	nrp			= document.getElementById('nrp');
	nama			= document.getElementById('nama');
	user_name	= document.getElementById('user_name');
	user_password	= document.getElementById('userpassword');
	kofirm_password	= document.getElementById('kofirm_password');
	submode		= document.getElementById('submode');
	telp			= document.getElementById('telp');
	hp				= document.getElementById('hp');

	if(nrp.value==''){
		valid=false;
		Element.show('nrp_invalid');
	}

	if(nama.value==''){
		valid=false;
		Element.show('nama_invalid');
	}

	if(user_name.value==''){
		valid=false;
		Element.show('username_invalid');
	}

	if(submode.value==0){
		if(user_password.value==''){
			valid=false;
			Element.show('userpassword_invalid');
		}
		else{
			if(user_password.value!=kofirm_password.value){
				valid=false;
				Element.show('konfirm_password_invalid');
			}
		}
	}
	else{
		if(user_password.value!=''){
			if(user_password.value!=kofirm_password.value){
				valid=false;
				Element.show('konfirm_password_invalid');
			}
		}
	}

	if(!cekValue(hp.value)){
		valid=false;
		Element.show('hp_invalid');
	}

	if(!cekValue(telp.value)){
		valid=false;
		Element.show('telp_invalid');
	}

	if(valid){
		return true;
	}
	else{
		return false;
	}
}

</script>

<form name="frm_data" action="{U_ASSIGN_USER_ACT}" method="post" onSubmit='return validateInput();'>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr class='banner' height=40>
	<td align='center' valign='middle' class="bannerjudul">&nbsp;Assign Permission</td>
</tr>
<tr>
	<td class="whiter" valign="middle" align="center">
	<table width='1000'>
		<tr><td colspan=3 bgcolor='{BGCOLOR_PESAN}' align='center'>{PESAN}</td></tr>
		<br>
		<tr><h2>{JUDUL}</h2></tr>
		<tr>
			<td align='center' valign='top'>
				<table width=500>
					<tr><td><h2>&nbsp;</h2></td></tr>
					<tr><td colspan=3><h3><u>Assign Permission</u></h3></td></tr>
					<tr>
			      		<td>Page</td><td>:</td>
						<td>
							<select id='page_id' name='page_id'>
								{PAGE_ID}
							</select>
						</td>
			    	</tr>
					<tr>
			      		<td valign='top'>User Level</td><td  valign='top'>:</td>
						<td>
							{OPT_USER_LEVEL}
						</td>
			    	</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=3 align='center' valign='middle' height=40>
				<input type="hidden" name="mode" value="{MODE}">
			  <input type="hidden" id='submode' name="submode" value="{SUB}">
				<input type="button" onClick="javascript: history.back();" value="&nbsp;&nbsp;&nbsp;KEMBALI&nbsp;&nbsp;&nbsp;">&nbsp;&nbsp;&nbsp;
			  <input type="submit" name="submit" value="&nbsp;&nbsp;&nbsp;SIMPAN&nbsp;&nbsp;&nbsp;">
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
</form>