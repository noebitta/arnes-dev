<script type="text/javascript">
	filePath = '{TPL}js/dropdowncalendar/images/';
</script>
<script src="{TPL}js/dropdowncalendar/calendar.js" type="text/javascript"></script>
<script language="JavaScript">
	
function selectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=true;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function deselectAll(){
		
		i=1;
		loop=true;
		record_dipilih="";
		do{
			str_var='checked_'+i;
			if(chk=document.getElementById(str_var)){
				chk.checked=false;
			}
			else{
				loop=false;
			}
			i++;
		}while(loop);
		
}

function hapusData(kode){
	
	if(confirm("Apakah anda yakin akan menghapus data ini?")){
		
		if(kode!=''){
			list_dipilih="'"+kode+"'";
		}
		else{
			i=1;
			loop=true;
			list_dipilih="";
			do{
				str_var='checked_'+i;
				if(chk=document.getElementById(str_var)){
					if(chk.checked){
						if(list_dipilih==""){
							list_dipilih +=chk.value;
						}
						else{
							list_dipilih +=","+chk.value;
						}
					}
				}
				else{
					loop=false;
				}
				i++;
			}while(loop);
		}
		
		new Ajax.Request("pengaturan_member.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=delete&list_member="+list_dipilih,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
			deselectAll();
		},
	   onFailure: function(request) 
	   {
	   }
	  })  
	}
	
	return false;
		
}

function ubahStatus(kode){
	
		new Ajax.Request("pengaturan_member.php?sid={SID}",{
	   asynchronous: true,
	   method: "get",
	   parameters: "mode=ubahstatus&id_member="+kode,
	   onLoading: function(request) 
	   {
	   },
	   onComplete: function(request) 
	   {
			
	   },
	   onSuccess: function(request) 
	   {			
			window.location.reload();
		},
	   onFailure: function(request) 
	   {
	   }
	  });  
	
	return false;
		
}

function Start(page) {
	OpenWin = this.open(page, "CtrlWindow", "toolbar=no,menubar=yes,location=yes,scrollbars=yes,resizable=yes");
}


</script>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
 <td class="whiter" valign="middle" align="center">		
		<table width='100%' cellspacing="0">
			<tr class='banner' height=40>
				<td align='center' valign='middle' class="bannerjudul">&nbsp;Master Member</td>
				<td colspan=2 align='right' class="bannernormal" valign='middle'>
					<form action="{ACTION_CARI}" method="post">
						<table>
							<tr><td class='bannernormal'>
								&nbsp;Tgl.Registrasi:&nbsp;<input id="tanggal_mulai" name="tanggal_mulai" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AWAL}" size=10>
								&nbsp; s/d &nbsp;<input id="tanggal_akhir" name="tanggal_akhir" type="text" onfocus="this.select();lcs(this)" onclick="event.cancelBubble=true;this.select();lcs(this)" value="{TGL_AKHIR}" size=10>
								&nbsp;Cari:&nbsp;<input type="text" id="txt_cari" name="txt_cari" value="{TXT_CARI}" />&nbsp;	
								<input type="submit" value="cari" />&nbsp;								
							</td></tr>
						</table>
					</form>
				</td>
			</tr>
			<tr>
				<td align='center' colspan=2>
					<table>
						<tr>
							<td>
								<a href='#' onClick="{CETAK_XL}"> <img src="{TPL}/images/icon_msexcel.png">&nbsp;Cetak ke MS EXCEL</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_MEMBER_ADD}">[+]Tambah Member</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Member</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
		<table width='100%' class="border">
    <tr>
       <th width=30></th>
       <th width=30>No</th>
			 <th width=200><a class="th" href='{A_SORT_BY_NAMA}'>Nama Member</a></th>
			 <th width=50><a class="th" href='{A_SORT_BY_KODE}'>Kode</a></th>
			 <th width=300><a class="th" href='{A_SORT_BY_ALAMAT}'>Alamat</a></th>
			 <th width=70><a 	class="th" href='{A_SORT_BY_HP}'>HP</a></th>
			 <th width=50><a 	class="th" href='{A_SORT_BY_EMAIL}'>Email</a></th>
			 <th width=100><a class="th" href='{A_SORT_BY_PEKERJAAN}'>Pekerjaan</a></th>
			 <th width=100><a class="th" href='{A_SORT_TGL_DAFTAR}'>Tgl.Daftar</a></th>
			 <th width=50><a class="th" href='{A_SORT_BY_FREKWENSI}'>Freq.</a></th>
			 <th width=100><a class="th" href='{A_SORT_BY_STATUS}'>Aktif</th>
			 <th width=100>Action</th>
     </tr>
     <!-- BEGIN ROW -->
     <tr class="{ROW.odd}">
       <td><div align="center">{ROW.check}</div></td>
       <td><div align="right">{ROW.no}</div></td>
			 <td><div align="left">{ROW.nama}</div></td>
			  <td><div align="left">{ROW.id_member}</div></td>
       <td><div align="left">{ROW.alamat}</div></td>
			 <td><div align="left">{ROW.hp}</div></td>
			 <td><div align="left">{ROW.email}</div></td>
			 <td><div align="left">{ROW.pekerjaan}</div></td>
			 <td><div align="center">{ROW.tgl_daftar}</div></td>
			 <td><div align="right">{ROW.frekwensi}</div></td>
			 <td><div align="center">{ROW.aktif}</div></td>
       <td><div align="center">{ROW.action}</div></td>
     </tr>  
     <!-- END ROW -->
    </table>
		{NO_DATA}
    <table width='100%'>
			<tr>
				<td width='30%' align='left'>
					<a href="{U_MEMBER_ADD}">[+]Tambah Member</a>&nbsp;|&nbsp;
					<a href="" onClick="return hapusData('');">[-]Hapus Member</a></td>
				<td width='70%' align='right'>
					<a href="" onClick="selectAll();return false;">Check All</a>&nbsp;|&nbsp;
					<a href="" onClick="deselectAll();return false;">Uncheck All</a>&nbsp;|&nbsp;{PAGING}
				</td>
			</tr>
		</table>
 </td>
</tr>
</table>