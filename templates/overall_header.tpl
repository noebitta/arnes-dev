<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
  <meta http-equiv="Content-Style-Type" content="text/css">
	<link rel="icon" type="image/ico" href="favicon.ico">
  {META}
  <title>{SITENAME} :: {PAGE_TITLE}</title>
  <link rel="stylesheet" href="{TPL}trav.css" type="text/css" />
  <script language="javaScript" type="text/javascript" src="includes/calendar.js"></script>
  <link href="includes/calendar.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

  /*var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25961668-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

</script>
</head>  
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/prototype.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/prototip.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/effects.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/controls.js"></script>
<script language="JavaScript" type="text/javascript" src="{ROOT}ajax/src/scriptaculous.js?load=effects"></script>
<!--<script type='text/javascript' src='{ROOT}js/scriptaculous.js?load=effects'></script>-->
<link rel="stylesheet" type="text/css" href="{ROOT}css/prototip.css" />

<script type="text/javascript" src="{ROOT}ajax/dojo.js"></script>

<script type="text/javascript" src="{TPL}js/main.js"></script>

<script type="text/javascript">
  // komponen khusus dojo 
  dojo.require("dojo.widget.Dialog");
</script> 

<body>

<!--BEGIN popuploading -->
<div dojoType="dialog" id="popuploading" bgColor="black" bgOpacity="0.7" toggle="fade" toggleDuration="500" style="display: none;" align="center">
<table width="300" cellpadding="0" cellspacing="0" >
	<tr><td align="center" valign="middle" style="padding-top: 10px;">
		<font style="color: white;font-size: 12px;">sedang mengerjakan</font><br>
		<img src="{TPL}/images/loading_bar.gif" />
	</td></tr>
</table>
<br>
</div>
<!--END popuploading-->
	
<!-- BEGIN if_login -->
<div align='center'>
<table width="100%" cellpadding=0 cellspacing=0>
<tr><td>
<table width='100%' cellpadding=0 cellspacing=0>
<tr>
	<!--<td class="banner" background='./templates/images/bg_tombol_header.png' STYLE='background-repeat: no-repeat;background-position: right bottom;'>-->
	<td>
		<table width='100%' cellpadding=0 cellspacing=0>
			<tr>
				<td class='header_overall_image'></td>
				<td class='header_overall'>www.tiketux.com</td>
				<td class='header_overall_separator'>{USERNAME}&nbsp;&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width='100%' class='header_shadow'>
			<tr>
				<td>{BCRUMP}</td>
				<td align='right' valign='bottom'>
					<a>Cabang Login: {CABANG_LOGIN}</a>|
					<a href="{U_UBAH_PASSWORD}">Ubah Password</a> |      
					<a href="{U_LOGOUT}">Logout</a>
				</td>
			</tr>
			<!--<tr><td colspan='2' height='1' bgcolor='e0e0e0'></td></tr>-->
		</table>
	</td>
</tr>
</table>
<!-- END if_login -->

<!-- MENU -->
<div align='center'>
<!-- BEGIN if_menu_utama --> 
<form name='form_pilih_menu' method='post'>
<!-- END if_menu_utama -->
<input id='menu_dipilih' name='menu_dipilih' type='hidden' value='{MENU_DIPILIH}' />
<input id='top_menu_dipilih' name='top_menu_dipilih' type='hidden' value='{TOP_MENU_DIPILIH}' />
<input id='hdn_SID' name='hdn_SID' type='hidden' value='{SID}' />

<table cellspacing='0' cellpadding='1'>
	<tr valign='bottom' height='35'>
		<!-- BEGIN menu_operasional -->
		<td>
			<div id='top_menu_operasional' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >&nbsp;Operasional&nbsp;</div>
		</td>
		<!-- END menu_operasional -->
		
		<!-- BEGIN menu_laporan_reservasi -->
		<td>
			<div id='top_menu_lap_reservasi' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;" >&nbsp;Sales&nbsp;</div>
		</td>
		<!-- END menu_laporan_reservasi -->
		
		<!-- BEGIN menu_laporan_paket -->
		<td>
			<div id='top_menu_lap_paket' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  style="font-size: 12px;">&nbsp;Paket & Cargo&nbsp;</div>
			</td>
		<!-- END menu_laporan_paket -->
		
		<!-- BEGIN menu_laporan_keuangan -->
		<td>
			<div id='top_menu_lap_keuangan' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >&nbsp;Keuangan&nbsp;</div>
			</td>
		<!-- END menu_laporan_keuangan -->
		
		<!-- BEGIN menu_tiketux -->
		<td>
			<div id='top_menu_tiketux' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >&nbsp;Tiketux&nbsp;</div>
			</td>
		<!-- END menu_tiketux -->
	
		<!-- BEGIN menu_pengelolaan_member -->
		<td>
			<div id='top_menu_pengelolaan_member' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >&nbsp;Member&nbsp;</div>
			</td>
		<!-- END menu_pengelolaan_member -->
		
		<!-- BEGIN menu_pengaturan -->
		<td>
			<div id='top_menu_pengaturan' class='top_menu_lost_focus' onClick="submitPilihanMenu(this.id);return false;"  >&nbsp;Pengaturan&nbsp;</div>
			</td>
		<!-- END menu_pengaturan -->
	</tr>
	
</table>