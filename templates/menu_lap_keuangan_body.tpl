<div class="menuwrapper" style="height: {HEIGT_WRAPPER}px">
	<br/>
	<!-- BEGIN menu_lap_keuangan_cso -->
	<div class="menuicon">
		<a href="{U_LAPORAN_UANG_CSO}">
			<img src="{TPL}images/icon_laporan_cso.png" /><br/>
			Setoran CSO
		</a>
	</div>
	<div style="height: {menu_lap_keuangan_cso.br};"></div>
	<!-- END menu_lap_keuangan_cso -->
	
	<!-- BEGIN menu_lap_keuangan_cabang -->
	<div class="menuicon">
		<a href="{U_LAPORAN_UANG_CABANG}">
			<img src="{TPL}images/icon_laporan_cabang.png" /><br/>
			Cabang
		</a>
	</div>
	<div style="height: {menu_lap_keuangan_cabang.br};"></div>
	<!-- END menu_lap_keuangan_cabang -->
	
	<!-- BEGIN menu_lap_harian -->
	<div class="menuicon">
		<a href="{U_REKAP_UANG_HARIAN}">
			<img src="{TPL}images/icon_laporan_omzet.png" /><br/>
			Harian
		</a>
	</div>
	<div style="height: {menu_lap_harian.br};"></div>
	<!-- END menu_lap_harian -->
	
	<!-- BEGIN menu_lap_fee_rekon -->
	<div class="menuicon">
		<a href="{U_LAPORAN_FEE}">
			<img src="{TPL}images/icon_rekonsiliasi.png" /><br/>
			Laporan<br>Rekon Fee
		</a>
	</div>
	<div style="height: {menu_lap_fee_rekon.br};"></div>
	<!-- END menu_lap_fee_rekon -->
	
	<!-- BEGIN menu_lap_pembayaran_sopir -->
	<div class="menuicon">
		<a href="{U_LAPORAN_PEMB_SOPIR}">
			<img src="{TPL}images/icon_pembayaran_sopir.png" /><br/>
			Biaya Insentif Sopir<br>
		</a>
	</div>
	<div style="height: {menu_lap_pembayaran_sopir.br};"></div>
	<!-- END menu_lap_pembayaran_sopir -->
	
	<!-- BEGIN menu_lap_voucher_bbm -->
	<div class="menuicon">
		<a href="{U_LAPORAN_VOUCHER_BBM}">
			<img src="{TPL}images/icon_voucher_bbm.png" /><br/>
			Laporan<br>Voucher BBM
		</a>
	</div>
	<div style="height: {menu_lap_voucher_bbm.br};"></div>
	<!-- END menu_lap_voucher_bbm -->
 
	<!-- BEGIN menu_lap_biaya_op -->
	<div class="menuicon">
		<a href="{U_LAPORAN_BIAYA_OP}">
			<img src="{TPL}images/icon_laporan_biaya_op.png" /><br/>
			Laporan<br>Biaya Operasional
		</a>
	</div>
	<div style="height: {menu_lap_biaya_op.br};"></div>
	<!-- END menu_lap_biaya_op -->
 
</div>