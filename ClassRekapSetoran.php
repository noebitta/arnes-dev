<?php
// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern

if(!$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true);
	exit;
}

//#############################################################################

class RekapSetoran{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function RekapSetoran(){
		$this->ID_FILE="C-RKS";
	}
	
	//BODY
	
	function generateKodeSetoran(){
		global $db;
		
		$temp	= array("0",
			"1","2","3","4","5","6","7","8","9",
			"A","B","C","D","E","F","G","H","I","J",
			"K","L","M","N","O","P","Q","R","S","T",
			"U","V","W","X","Y","Z",
			"A1","B1","C1","D1","E1","F1","G1","H1","I1","J1",
			"K1","L1","M1","N1","O1","P1","Q1","R1","S1","T1",
			"U1","V1","W1","X1","Y1","Z1");
		
		$y		= $temp[date("y")*1];
		$m		= $temp[date("m")*1];
		$d		=	$temp[date("d")*1];
		$j		= $temp[date("j")*1];
		$mn		= $temp[date("i")*1];
		$s		= $temp[date("s")*1];
		
		
		do{
			$rnd1	= $temp[rand(1,61)];
			$rnd2	= $temp[rand(1,61)];
			$rnd3	= $temp[rand(1,61)];
			
			$id_setoran	= "RKS".$y.$rnd1.$m.$rnd2.$d.$j.$mn.$s.$rnd3;
		
			$sql = "SELECT COUNT(1) FROM tbl_user_setoran WHERE IdSetoran='$id_setoran'";
						
			if (!$result = $db->sql_query($sql)){
				echo("Err: $this->ID_FILE ".__LINE__);exit;
			}
			
			$row=$db->sql_fetchrow($result);
		
		}while($row[0]>0);
			
		
		
		return $id_setoran;
	}
	
	function prosesSetoran(
		$IdSetoran,$IdUser,$NamaUser,
		$JumlahTiketUmum,$JumlahTiketDiskon,$OmzetPenumpangTunai,
		$OmzetPenumpangDebit,$OmzetPenumpangKredit,$TotalDiskon,
		$TotalPaket,$OmzetPaket,$TotalBiaya){
	  
		//kamus
		global $db;
		global $userdata;
		global $FLAG_BIAYA_BBM,$FLAG_BIAYA_VOUCHER_BBM;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"INSERT INTO tbl_user_setoran(
				IdSetoran,WaktuSetoran,IdUser,NamaUser,
				JumlahTiketUmum,JumlahTiketDiskon,OmzetPenumpangTunai,
				OmzetPenumpangDebit,OmzetPenumpangKredit,TotalDiskon,
				TotalPaket,OmzetPaket,TotalBiaya)
			VALUES(
				'$IdSetoran',NOW(),'$IdUser','$NamaUser',
				'$JumlahTiketUmum','$JumlahTiketDiskon','$OmzetPenumpangTunai',
				'$OmzetPenumpangDebit','$OmzetPenumpangKredit','$TotalDiskon',
				'$TotalPaket','$OmzetPaket','$TotalBiaya');";
								
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		//UPDATE IDSETORAN TIKET
		$sql = 
			"UPDATE tbl_reservasi SET IdSetoran='$IdSetoran',WaktuSetor=NOW()
				WHERE
					(IdSetoran='' OR IdSetoran IS NULL)
					AND CetakTiket=1 
					AND FlagBatal!=1 
					AND PetugasCetakTiket=$IdUser;";
								
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		//UPDATE IDSETORAN PAKET
		$sql = 
			"UPDATE tbl_paket SET IdSetoran='$IdSetoran',WaktuSetor=NOW()
				WHERE
					(IdSetoran='' OR IdSetoran IS NULL)
					AND CetakTiket=1 
					AND FlagBatal!=1
					AND PetugasPenjual=$IdUser;";
								
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		//UPDATE IDSETORAN CARGO
		$sql = 
			"UPDATE tbl_paket_cargo SET IdSetoran='$IdSetoran',WaktuSetor=NOW()
				WHERE
					(IdSetoran='' OR IdSetoran IS NULL)
					AND (DicatatOleh!='' || DicatatOleh IS NOT NULL)
					AND IsBatal!=1
					AND DicatatOleh=$IdUser;";
								
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		//UPDATE IDSETORAN BIAYA
		$sql = 
			"UPDATE tbl_biaya_op SET IdSetoran='$IdSetoran'
				WHERE
					(IdSetoran='' OR IdSetoran IS NULL)
					AND IdPetugas='$IdUser'
					AND NOT FlagJenisBiaya IN($FLAG_BIAYA_BBM,$FLAG_BIAYA_VOUCHER_BBM);";
								
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		//UPDATE IDSETORAN SPJ
		$sql = 
			"UPDATE tbl_spj SET IdSetoran='$IdSetoran'
				WHERE
					(IdSetoran='' OR IdSetoran IS NULL)
					AND CSO='$IdUser';";
								
		if (!$db->sql_query($sql)){
			echo("Err: $this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}
	
	function hapus($IdSetoran){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"DELETE FROM tbl_user_setoran
			WHERE IdSetoran='$IdSetoran'";
								
		if (!$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		return true;
	}
	
	function ambilDataDetail($IdSetoran){
	  
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"SELECT * FROM tbl_user_setoran
			WHERE IdSetoran='$IdSetoran'";
								
		if (!$result=$db->sql_query($sql)){
			echo("Err:$this->ID_FILE".__LINE__);exit;
		}
		
		$data	= $db->sql_fetchrow($result);
		
		return $data;
	}
}
?>