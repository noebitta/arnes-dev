<?php
//
// LAPORAN
//
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberSetoranCso.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['user_level']==$LEVEL_SCHEDULER){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER

$cso	= $userdata['nama']." ($userdata[username])";
					
//EXPORT KE PDF
class PDF extends FPDF {
	function Footer() {
		$this->SetY(-1.5);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,1,'',0,0,'R');
	}
}
					
//set kertas & file
$pdf=new PDF('P','cm','A4');
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Setmargins(1,0,0,0);
$pdf->SetFont('courier','',10);
$i=0;
		
$kode_posting			= trim($HTTP_GET_VARS['kode_posting']);		

$SetoranTopUpCSO	= new SetoranTopUpCSO();
$data_posting			= $SetoranTopUpCSO->ambilDataPosting($kode_posting);
		
$no_kwitansi			=$kode_posting;
$cso							=trim($data_posting['cso']);
$operator					=trim($data_posting['operator']);
$total_real_setor	=number_format(trim($data_posting['total_real_setor']),0,",",".");
$waktu_setor			=trim($data_posting['waktu_setor']);

$spasi	= 0.5;

// Header
$pdf->Ln();
$pdf->Cell(0,1,'','',0,'');$pdf->Ln();
$pdf->SetFont('courier','B',12);
$pdf->Cell(0,$spasi,"KWITANSI SETORAN TRANSAKSI TOP UP MEMBER CIPAGANTI SHUTTLE SERVICE",'',0,'');
$pdf->Ln();		
$pdf->SetFont('courier','',10);			
$pdf->Cell(0,$spasi,"No Kwitansi: ".$no_kwitansi.'/'.$operator,'',0,'');
$pdf->Ln();
$pdf->Cell(0,$spasi,"Waktu setor: ".$waktu_setor,'',0,'');	
$pdf->Ln();
//content
$pdf->Cell(1.5,1,'',0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Telah diterima dari ".$cso,0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"sebagai setoran dari transaksi top up member",0,0,'');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'');$pdf->Cell(3,$spasi,"Sebesar Rp. ".$total_real_setor,0,0,'');
$pdf->Ln();
$pdf->Cell(17.5,0.1,'',0,0,'');$pdf->Cell(3.5,1.2,'',0,0,'');

//footer
$pdf->Ln();
$pdf->Cell(5,0,'',0,0,'C');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'C');$pdf->Cell(5,$spasi,"Penyetor: ".$cso,0,0,'C');
$pdf->Cell(4,$spasi,'',0,0,'C');$pdf->Cell(5,$spasi,"Penerima: ".$operator,0,0,'C');
$pdf->Ln();
$pdf->Cell(5,2,'',0,0,'C');
$pdf->Ln();
$pdf->Cell(1.5,$spasi,'',0,0,'C');$pdf->Cell(5,$spasi,$cso,0,0,'C');
$pdf->Cell(4,$spasi,'',0,0,'C');$pdf->Cell(5,$spasi,$operator,0,0,'C');
$pdf->Ln();

$pdf->Ln();
$pdf->Cell(0.5,0.8,'','','','');$pdf->Ln();
										
$pdf->Output();
						
?>