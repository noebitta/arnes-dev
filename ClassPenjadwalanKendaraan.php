<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################


class PenjadwalanKendaraan{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	var $TABEL1;
	
	//CONSTRUCTOR
	function PenjadwalanKendaraan(){
		$this->ID_FILE="C-PJK";
	}
	
	//BODY
	
	function tambah(
		$tgl_berangkat,$jam_berangkat,$kode_jadwal,$id_jurusan,$kode_kendaraan,$kode_driver,$status_aktif){
	  
		/*
		ID	: 002
		IS	: data jadwal belum ada dalam database
		FS	:Data jadwal baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		global $LAYOUT_KURSI_DEFAULT;
		
		$status_aktif	= ($status_aktif!='')?$status_aktif:1;
		
		//mengambil layout kursi dari kendaraan
		$sql	=
			"SELECT IF(IdLayout IS NOT NULL,IdLayout ,'') AS LayoutKursi,IF(JumlahKursi IS NOT NULL,JumlahKursi,0) AS JumlahKursi,NoPolisi FROM tbl_md_kendaraan WHERE KodeKendaraan='$kode_kendaraan'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		
		$layout_kursi	= $row['LayoutKursi'];
		$no_polisi		= $row['NoPolisi'];
		
		if($layout_kursi==''){
			$sql	=
			"SELECT IF(IdLayout IS NOT NULL,IdLayout ,'') AS LayoutKursi,IF(JumlahKursi IS NOT NULL,JumlahKursi,0) AS JumlahKursi FROM tbl_md_jadwal WHERE KodeJadwal='$kode_jadwal'";
		
			if (!$result = $db->sql_query($sql)){
				die_error("Err".__LINE__);
			}
			
			$row = $db->sql_fetchrow($result);
			$layout_kursi	=	$row["LayoutKursi"];
			$jumlah_kursi	=	$row["JumlahKursi"];
		}
		
		$sql	=
			"SELECT Nama FROM tbl_md_sopir WHERE KodeSopir='$kode_driver'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		$nama_driver	= $row['Nama'];
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql =
			"INSERT INTO tbl_penjadwalan_kendaraan
				(TglBerangkat,JamBerangkat,
				KodeJadwal,IdJurusan,
				KodeKendaraan,NoPolisi,LayoutKursi,
				JumlahKursi,KodeDriver,NamaDriver,
				StatusAktif)
			VALUES(
				'$tgl_berangkat','$jam_berangkat',
				'$kode_jadwal',$id_jurusan,
				'$kode_kendaraan','$no_polisi','$layout_kursi',
				'$jumlah_kursi','$kode_driver','$nama_driver',
				$status_aktif);";
								
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
		
		//MENCATAT PELAKU DARI PENGUBAHAN PENJADWALAN
		if($status_aktif==0){
			$id	= $db->sql_nextid();
			$this->mencatatPengubahData($id,1-$status_aktif);
		}
		
		return true;
	}
	
	function ubah(
		$id,$kode_kendaraan,$kode_driver){
	  
		/*
		ID	: 004
		IS	: data jadwal sudah ada dalam database
		FS	:Data jadwal diubah 
		*/
		
		//kamus
		global $db;
		
		$sql	=
			"SELECT IF(JumlahKursi IS NOT NULL,JumlahKursi,0) AS LayoutKursi,NoPolisi FROM tbl_md_kendaraan WHERE KodeKendaraan='$kode_kendaraan'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		
		$field_no_polisi	=($kode_kendaraan=="")?"":"KodeKendaraan='$kode_kendaraan',NoPolisi='$row[NoPolisi]',LayoutKursi=$row[LayoutKursi],";
		
		$sql	=
			"SELECT Nama FROM tbl_md_sopir WHERE KodeSopir='$kode_driver'";
		
		if (!$result = $db->sql_query($sql)){
			 die_error("Err:".__LINE__);
		}
		
		$row = $db->sql_fetchrow($result);
		
		$field_kode_sopir	=($kode_driver=="")?"":"KodeDriver='$kode_driver',NamaDriver='$row[Nama]',";
		
		//MENGUBAH DATA KEDALAM DATABASE
		$field_update	=$field_no_polisi.$field_kode_sopir."StatusAktif=1";
		
		$sql =
			"UPDATE tbl_penjadwalan_kendaraan 
					SET $field_update
					WHERE IdPenjadwalan=$id";
								
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
		
		return true;
	}
	
	function ubahStatus($id){
	  
		/*
		ID	: 006
		IS	: data jadwal sudah ada dalam database
		FS	: Status jadwal diubah 
		*/
		
		//kamus
		global $db;
		global $userdata;
		
		//MENGAMBIL DATA PENJADWALAN
		$sql = 
			"SELECT KodeJadwal,TglBerangkat,StatusAktif
			FROM tbl_penjadwalan_kendaraan
			WHERE IdPenjadwalan='$id'";
				
		if (!$result = $db->sql_query($sql)){
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		$data_jadwal=$db->sql_fetchrow($result);
		
		//MEMERIKSA APAKAH JADWAL YANG AKAN DIUBAH MASIH ADA PENUMPANG YANG BELUM DIPINDAHKAN
		if($data_jadwal['StatusAktif']){
			$sql = 
				"SELECT COUNT(1)
				FROM tbl_reservasi
				WHERE KodeJadwal='$data_jadwal[KodeJadwal]'
					AND TglBerangkat='$data_jadwal[TglBerangkat]'
					AND (FlagBatal!=1 OR FlagBatal IS NULL)";
					
			if (!$result = $db->sql_query($sql)){
				die_error("Err: $this->ID_FILE".__LINE__);
			}
			
			$data_penumpang=$db->sql_fetchrow($result);
			
			if($data_penumpang[0]>0){
				//MASIH ADA PENUMPANG DI JADWAL YANG AKAN DINONAKTIFKAN
				return false;
			}
		}
		
		//MENGUBAH STATUS PENJADWALAN
		$sql ="CALL sp_penjadwalan_kendaraan_ubah_status_aktif($id);";
		
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
		
		//MENCATAT PELAKU DARI PENGUBAHAN PENJADWALAN
		$this->mencatatPengubahData($id,$data_jadwal['StatusAktif']);
		
		return true;
	}//end ubahStatus
	
	private function mencatatPengubahData($id,$is_aktif){
		global $db;
		global $userdata;
		
		//MENCATAT PELAKU DARI PENGUBAHAN PENJADWALAN
		$status_aktif	= $is_aktif==1?"Menonaktifkan":"Mengaktifkan";
		$sql ="UPDATE tbl_penjadwalan_kendaraan SET Remark=CONCAT(Remark,'<br>','User: ".$userdata['nama']." ".$status_aktif." pada ".date("d-m-y H:i")."') WHERE IdPenjadwalan=$id;";

		
		if (!$db->sql_query($sql)){
			die_error("Err:".__LINE__);
		}
	}
	
	function ambilDataDetail($tgl,$kode_jadwal){
		
		/*
		Desc	:Mengembalikan data jadwal sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT KodeKendaraan,JamBerangkat,IdJurusan,NoPolisi,LayoutKursi,f_sopir_get_nama_by_id(KodeDriver) AS NamaSopir,KodeDriver,StatusAktif
			FROM tbl_penjadwalan_kendaraan
			WHERE (KodeJadwal LIKE '$kode_jadwal' AND TglBerangkat='$tgl')";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData

  function generateKodeJadwalNonReguler($tgl_berangkat,$id_jurusan){
    global $db;

    $sql =
      "SELECT KodeCabangAsal,KodeCabangTujuan
			FROM tbl_md_jurusan
			WHERE IdJurusan='$id_jurusan'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $row=$db->sql_fetchrow($result);

    do{
      $kode_jadwal  = $row['KodeCabangAsal']."-".$row['KodeCabangTujuan'].'-NON'.rand(111,999);

      $sql="SELECT COUNT(1) FROM tbl_penjadwalan_kendaraan WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl_berangkat'";

      if (!$result = $db->sql_query($sql)){
        die_error("Err: $this->ID_FILE".__LINE__);
      }

      $data_jadwal = $db->sql_fetchrow($result);

    }while($data_jadwal[0]>0);

    return $kode_jadwal;
  }

  function isDuplikasi($tgl_berangkat,$kode_jadwal){
    global $db;

    $sql="SELECT COUNT(1) FROM tbl_penjadwalan_kendaraan WHERE KodeJadwal='$kode_jadwal' AND TglBerangkat='$tgl_berangkat'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $row = $db->sql_fetchrow($result);

    return ($row[0]<=0?false:true);
  }

  function tambahJadwalNonReguler(
    $TglBerangkat,$JamBerangkat,$KodeJadwal,
    $IdJurusan,$LayoutKursi,$JumlahKursi,
    $Remark){
    /*MENGAMBAHKAN DATA JADWAL NON REGULER KE DALAM DATABASE*/

    global $db;

    /*MENAMBAHKAN KE TABEL PENJADWALAN*/
    $sql  =
      "INSERT INTO tbl_penjadwalan_kendaraan(
        TglBerangkat,JamBerangkat,KodeJadwal,
        IdJurusan,LayoutKursi,JumlahKursi,
        StatusAktif,Remark)
      VALUES(
        '$TglBerangkat','$JamBerangkat','$KodeJadwal',
        '$IdJurusan','$LayoutKursi','$JumlahKursi',
        '1','$Remark');";


    if(!$result=$db->sql_query($sql)){
      echo("Err:".__LINE__);exit;
    }

    /*MENAMBAHKAN KE TABEL POSISI*/
    $sql  =
      "INSERT INTO tbl_posisi(
        KodeJadwal,TglBerangkat,IdJurusan,
        JamBerangkat,JumlahKursi,SisaKursi,
        FlagOperasionalJurusan)
      VALUES(
        '$KodeJadwal','$TglBerangkat','$IdJurusan',
        '$JamBerangkat','$JumlahKursi','$JumlahKursi',
         3);"; /*<== 3 adalah kode untuk jenis jurusan non-reguler*/


    if(!$result=$db->sql_query($sql)){
      echo("Err:".__LINE__);exit;
    }

    return true;
  }

  function hapusJadwalNonReguler($id){
    /*MENGHAPUS JADWAL NON REGULER*/

    //kamus
    global $db;

    //MENGAMBIL DATA PENJADWALAN
    $sql =
      "SELECT KodeJadwal,TglBerangkat,StatusAktif
			FROM tbl_penjadwalan_kendaraan
			WHERE IdPenjadwalan='$id'";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $data_jadwal=$db->sql_fetchrow($result);

    //MEMERIKSA APAKAH JADWAL YANG AKAN DIUBAH MASIH ADA PENUMPANG YANG BELUM DIPINDAHKAN
    $sql =
      "SELECT COUNT(1)
				FROM tbl_reservasi
				WHERE KodeJadwal='$data_jadwal[KodeJadwal]'
					AND TglBerangkat='$data_jadwal[TglBerangkat]'
					AND (FlagBatal!=1 OR FlagBatal IS NULL)";

    if (!$result = $db->sql_query($sql)){
      die_error("Err: $this->ID_FILE".__LINE__);
    }

    $data_penumpang=$db->sql_fetchrow($result);

    if($data_penumpang[0]>0){
      //MASIH ADA PENUMPANG DI JADWAL YANG AKAN DIHAPUS
      return false;
    }


    /*MENGHAPUS DARI TABEL PENJADWALAN*/
    $sql ="DELETE FROM tbl_penjadwalan_kendaraan WHERE IdPenjadwalan='$id';";

    if (!$db->sql_query($sql)){
      die_error("Err:".__LINE__);
    }

    /*MENGHAPUS DARI TABEL POSISI*/
    $sql ="DELETE FROM tbl_posisi WHERE TglBerangkat='$data_jadwal[TglBerangkat]' AND KodeJadwal='$data_jadwal[KodeJadwal]';";

    if (!$db->sql_query($sql)){
      die_error("Err:".__LINE__);
    }

    return true;
  }//end hapusJadwalNonReguler
	
}
?>