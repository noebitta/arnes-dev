<?php
//CONTROLLER UNTUK RENDER LAYOUT KENDARAAN

// SECURITY
define('FRAMEWORK', true);

//INCLUDES FILE
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassKota.php');
include($adp_root_path . 'ClassReservasiNonReguler.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){
  echo("SILAHKAN LOGIN KEMBALI");exit;
}


// PARAMETER
//GET PARAMETER DIKIRIM DARI reservasi.nonreguler.js function reloadJadwal()
$tgl_mysql						= $HTTP_POST_VARS['tanggal'];
$kota_dipilih				  = $HTTP_POST_VARS['kotadipilih'];
$kode_jadwal_dipilih  = $HTTP_POST_VARS['kodejadwaldipilih'];

//CONSTRUCT OBJECT
$Kota       = new Kota();
$Reservasi  = new ReservasiNonReguler();

$res_kota = $Kota->ambilData();

while($row=$db->sql_fetchrow($res_kota)){
  $template->assign_block_vars(
    "LISTKOTA",array(
    "namakota"    => $row['NamaKota'],
    "kodekota"    => $row['KodeKota']
  ));

  if($row['KodeKota']==$kota_dipilih){
    $res_jadwal = $Reservasi->ambilListJadwalByKota($tgl_mysql,$row["NamaKota"]);

    if($db->sql_numrows($res_jadwal)){
      while($data_jadwal=$db->sql_fetchrow($res_jadwal)){
        $template->assign_block_vars(
          "LISTKOTA.LISTJADWAL",array(
          "jamberangkat"  => $data_jadwal["JamBerangkat"],
          "kodejadwal"    => "(".$data_jadwal["KodeJadwal"].")",
          "action"        => "kodejadwaldipilih.value='$data_jadwal[KodeJadwal]';reloadJadwal();getUpdateMobil();",
          "class"         => ($kode_jadwal_dipilih!=$data_jadwal["KodeJadwal"]?"rsvnonreglistjadwal":"rsvnonreglistjadwaldipilih")
        ));
      }
    }
    else{
      $template->assign_block_vars(
        "LISTKOTA.LISTJADWAL",array(
        "jamberangkat"  => "tidak ada data"
      ));
    }

  }

}

$template->set_filenames(array('body' => 'reservasi/reservasi.nonreguler.listjadwal.tpl'));
$template->pparse('body');
?>