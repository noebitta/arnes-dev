<?php
//
// PENGATURAN / MASTER
//

// STANDARD
define('FRAMEWORK', true);
//$adp_root_path = './';
include($adp_root_path . 'common.php');

// SESSION
$userdata = session_pagestart($user_ip,200);  // Master : 200
init_userprefs($userdata);



// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO_PAKET'],$USER_LEVEL_INDEX['KEUANGAN']))){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage 		= $config['perpage'];
$mode 			= isset($HTTP_GET_VARS['mode'])? $HTTP_GET_VARS['mode'] : $HTTP_POST_VARS['mode']; // kalo mode kosong, defaultnya EXplorer Mode
$submode		= isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : $HTTP_POST_VARS['submode']; 
$start   		= (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;


$tanggal_mulai  = isset($HTTP_GET_VARS['tglmulai'])? $HTTP_GET_VARS['tglmulai'] : $HTTP_POST_VARS['tglmulai'];
$tanggal_akhir  = isset($HTTP_GET_VARS['tglakhir'])? $HTTP_GET_VARS['tglakhir'] : $HTTP_POST_VARS['tglakhir'];
$kode_cabang		= isset($HTTP_GET_VARS['kodecabang'])? $HTTP_GET_VARS['kodecabang'] : $HTTP_POST_VARS['kodecabang'];

// LIST
$template->set_filenames(array('body' => 'laporan_omzet_cabang/detail.tpl')); 

$tanggal_mulai	= ($tanggal_mulai!='')?$tanggal_mulai:dateD_M_Y();
$tanggal_akhir	= ($tanggal_akhir!='')?$tanggal_akhir:dateD_M_Y();

$tanggal_mulai_mysql	= FormatTglToMySQLDate($tanggal_mulai);
$tanggal_akhir_mysql	= FormatTglToMySQLDate($tanggal_akhir);

$kondisi_cabang	= $userdata['user_level']!=$USER_LEVEL_INDEX["SUPERVISOR"]?"":" AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$userdata[KodeCabang]'";

//QUERY TIKET
$sql=
	"SELECT 
		WaktuCetakTiket,NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,Nama,
		Alamat,Telp,NomorKursi,
		IF(JenisPenumpang!='R',IF(JenisPembayaran!=3,HargaTiket,0),SubTotal) AS HargaTiket,SubTotal,
		IF(JenisPembayaran!=3,Discount,0) AS Discount,IF(JenisPembayaran!=3,Total,0) AS Total,
		IF(JenisPenumpang!='R',JenisDiscount,'RETURN') AS JenisDiscount,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO,
		f_user_get_nama_by_userid(PetugasPembatalan) AS NamaCSOPembatalan
	FROM 
		tbl_reservasi
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' $kondisi_cabang";	


if ($result = $db->sql_query($sql)){
	$i = 1;//$idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$pesanan	= (!$row['FlagPesanan'])?"Go Show":"Pesanan";
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$odd	= "blue";
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$odd	= 'red';
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		}
		
		$template->
			assign_block_vars(
				'ROW',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'waktu_cetak'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['WaktuCetakTiket'])),
					'no_tiket'=>$row['NoTiket'],
					'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
					'kode_jadwal'=>$row['KodeJadwal'],
					'nama'=>$row['Nama'],
					'no_kursi'=>$row['NomorKursi'],
					'harga_tiket'=>number_format($row['HargaTiket'],0,",","."),
					'discount'=>number_format($row['Discount'],0,",","."),
					'plan_asuransi'=>$plan_asuransi,
					'premi_asuransi'=>number_format($besar_premi,0,",","."),
					'total'=>number_format($row['Total']+$besar_premi,0,",","."),
					'tipe_discount'=>$row['JenisDiscount'],
					'cso'=>$row['NamaCSO'],
					'status'=>$status,
					'ket'=>$keterangan
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 


//QUERY PAKET
$sql=
	"SELECT 
		NoTiket,TglBerangkat,KodeJadwal,
		JamBerangkat,WaktuPesan,NamaPengirim,
		NamaPenerima,HargaPaket,JenisPembayaran,
		FlagBatal,CetakTiket,
		f_user_get_nama_by_userid(PetugasPenjual) AS NamaCSO
	FROM 
		tbl_paket
	WHERE (DATE(TglBerangkat) BETWEEN '$tanggal_mulai_mysql' AND '$tanggal_akhir_mysql') 
	AND CetakTiket=1 AND f_jurusan_get_kode_cabang_asal_by_jurusan(IdJurusan)='$kode_cabang' $kondisi_cabang";	


if ($result = $db->sql_query($sql)){
	$i = $idx_page*$VIEW_PER_PAGE+1;
  while ($row = $db->sql_fetchrow($result)){
		$odd ='odd';
		
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		if($row['FlagBatal']!=1){
			if($row['CetakTiket']!=1){
				$odd	= "blue";
				$status	= "Book";
			}
			else{
				$status	= "OK";
			}
			$keterangan="";
		}
		else{
			$odd	= 'red';
			$status	="BATAL";
			$keterangan	= "dibatalkan oleh: $row[NamaCSOPembatalan]";
		}
		
		$template->
			assign_block_vars(
				'ROWPAKET',
				array(
					'odd'=>$odd,
					'no'=>$i,
					'waktu_berangkat'=>dateparseWithTime(FormatMySQLDateToTglWithTime($row['TglBerangkat']." ".$row['JamBerangkat'])),
					'no_tiket'=>$row['NoTiket'],
					'kode_jadwal'=>$row['KodeJadwal'],
					'dari'=>$row['NamaPengirim'],
					'untuk'=>$row['NamaPenerima'],
					'harga_paket'=>number_format($row['HargaPaket'],0,",","."),
					'cso'=>$row['NamaCSO'],
					'status'=>$status,
					'ket'=>$keterangan
				)
			);
		
		$i++;
  }
} 
else{
	//die_error('Cannot Load laporan_penjualan_user',__FILE__,__LINE__,$sql);
	echo("Err:".__LINE__);exit;
} 

//KOMPONEN UNTUK EXPORT
$parameter_cetak	= "&tglmulai=".$tanggal_mulai."&tglakhir=".$tanggal_akhir."&kodecabang=".$kode_cabang;
													
$script_cetak_excel="Start('laporan.reservasi.cabang.detail.cetakexcel.php?sid=".$userdata['session_id'].$parameter_cetak."');return false;";
//--END KOMPONEN UNTUK EXPORT

$template->assign_vars(array(	
	'TGL_AWAL'			=> $tanggal_mulai,
	'TGL_AKHIR'			=> $tanggal_akhir,
	'PENCARI'				=> $pencari,
	'JENIS_LAPORAN'	=> $jenis_laporan,
	'PAGING'				=> $paging,
	'CETAK_XL'			=> $script_cetak_excel
	)
);
	      
include($adp_root_path . 'includes/page_header_detail.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>