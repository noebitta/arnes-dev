<?php
$ip			= $_SERVER['REMOTE_ADDR'];

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if($ip!=="" && !$userdata['session_logged_in']){ 
  //redirect('index.'.$phpEx,true); 
}
//#############################################################################

class LogSMS{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function LogSMS(){
		$this->ID_FILE="C-LOS";
	}
	
	//BODY
	
	function tambah(
		$no_tujuan, $nama_penerima, $flag_tipe_pengiriman, 
		$kode_referensi, $isi_sms){
		
		//kamus
		global $db;
		
		$panjang_karakter	= strlen($isi_sms);
		$jumlah_pesan			= ceil($panjang_karakter/160);
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = 
			"INSERT INTO tbl_log_sms 
				(NoTujuan, NamaPenerima, JumlahPesan, 
				FlagTipePengiriman, KodeReferensi,WaktuKirim)
			VALUES
				('$no_tujuan','$nama_penerima', '$jumlah_pesan',
				'$flag_tipe_pengiriman','$kode_referensi',NOW())
			";
								
		if (!$db->sql_query($sql)){
			die_error("Err:$this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
}
?>