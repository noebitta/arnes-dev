<?php
/**
 * Lorensius W. L. T <lorenz@londatiga.net>
 */

include 'init.php';

$dispatcher = new Dispatcher();

$dispatcher->enableDebug(true);
$dispatcher->dispatch();
?>