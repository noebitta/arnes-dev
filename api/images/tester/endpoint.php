<?php
$endpoints		= array('/h2h/user/login' 	=> array('method'  	=> 'POST',
                                                 'auth'     => 1,
												 'params'	=> array('userid',
																	 'password')
												),
                        '/h2h/user/info' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array()
												),
                        '/h2h/agen' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array()
												),
                        '/h2h/agen/jurusan' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array('kode',
                                                                     'gbk')
												),
                        '/h2h/jadwal' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array('agen',
                                                                     'jurusan',
                                                                     'tanggal')
												),
                        '/h2h/jadwal/kursi' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array('agen',
                                                                     'jadwal',
                                                                     'tanggal')
												),
                        '/h2h/jadwal/layout' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array('agen',
                                                                     'kode')
												),
                        '/h2h/reservasi/book'    => array('method'  	=> 'POST',
                                                     'auth'     => 3,
                                                     'params'	=> array('kode_agen',
                                                                         'kode_jadwal',
                                                                         'tanggal_berangkat',
                                                                         'nama_pemesan',
                                                                         'alamat_pemesan',
                                                                         'telp_pemesan',
                                                                         'email_pemesan',
                                                                         'nomor_kursi',
                                                                         'nama_penumpang',
                                                                         'channel')),
                        '/h2h/reservasi/cek' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
												 'params'	=> array('kode',
                                                                     'nohp')
												),
                        '/user/join' 	=> array('method'  	=> 'POST',
                                                 'auth'     => 1,
												 'params'	=> array('userid',
																	 'email',
																	 'password',
																	 'name',
																	 'address',
																	 'city',
																	 'phone')
												),

						'/user/login' 	=> array('method'  	=> 'POST',
                                                 'auth'     => 1,
												 'params'	=> array('userid',
																	 'password')
												),

						'/user/forgotpassword'	=> array('method'  	=> 'POST',
                                                         'auth'     => 1,
														 'params'	=> array('email')
														),

						'/user/update' 	=> array('method'  	=> 'POST',
                                                 'auth'     => 2,
												 'params'	=> array('email',
																	 'password',
																	 'name',
																	 'address',
																	 'city',
																	 'phone')
												),

						'/user/me' 	=> array('method'  	=> 'GET',
                                             'auth'     => 2,
											 'params'	=> array()
											),

						'/user/review' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
											 	 'params'	=> array('items',
																	 'page')
												),

						'/user/trip' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 2,
											 	 'params'	=> array('items',
																	 'page')
												),

						'/agen' 	    => array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('ticketing')
												),

                        '/agen/info' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('kode')
												),

                        '/agen/cabang' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('gbk',
                                                                     'lat',
                                                                     'lon',
                                                                     'rad')
												),

                        '/agen/jurusan' => array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('kode',
                                                                     'gbk')
												),

                        '/agen/news' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('kode',
                                                                     'items',
                                                                     'page')
												),

                        '/agen/review' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('kode',
                                                                     'items',
                                                                     'page')
												),
                        '/agen/paymentchannel' 	=> array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('kode')
												),

                        '/cabang' 	    => array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('agen',
                                                                     'kode')
												),

                        '/cabang/jadwal'    => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array('agen',
                                                                         'kode')
                                                    ),

                        '/cabang/nearby'    => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array('lat',
                                                                         'lon',
                                                                         'rad')
												),

                        '/kota' 	    => array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array()
												),

                        '/jadwal' 	    => array('method'  	=> 'GET',
                                                 'auth'     => 0,
											 	 'params'	=> array('agen',
                                                                     'jurusan',
                                                                     'tanggal')
												),

                        '/jadwal/search'    => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array('kta',
                                                                         'ktt',
                                                                         'lok',
                                                                         'agen',
                                                                         'lat',
                                                                         'lon',
                                                                         'rad',
                                                                         'items',
                                                                         'page')
												),

                        '/payment/channel'     => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array()
                                                    ),

                        '/payment/admin'     => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array('kode',
                                                                         'agen')
                                                    ),

                        '/kursi/layout'     => array('method'  	=> 'GET',
                                                     'auth'     => 3,
                                                     'params'	=> array('agen',
                                                                         'kode')
                                                    ),

                        '/reservasi'        => array('method'  	=> 'GET',
                                                     'auth'     => 2,
                                                     'params'	=> array('items',
                                                                         'page')
                                                    ),

                        '/reservasi/kursi'  => array('method'  	=> 'GET',
                                                     'auth'     => 3,
                                                     'params'	=> array('agen',
                                                                         'jadwal',
                                                                         'tanggal')
                                                    ),

                        '/reservasi/add'    => array('method'  	=> 'POST',
                                                     'auth'     => 3,
                                                     'params'	=> array('kode_agen',
                                                                         'kode_jadwal',
                                                                         'tanggal_berangkat',
                                                                         'nama_pemesan',
                                                                         'alamat_pemesan',
                                                                         'telp_pemesan',
                                                                         'email_pemesan',
                                                                         'nomor_kursi',
                                                                         'nama_penumpang',
                                                                         'channel',
                                                                         'payment')
                                                    ),
                        '/reservasi/cek'    => array('method'  	=> 'GET',
                                                     'auth'     => 3,
                                                     'params'	=> array('kode',
                                                                         'email')
                                                    ),

                        '/news'             => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array('items',
                                                                         'page')
                                                    ),

                        '/review'           => array('method'  	=> 'GET',
                                                     'auth'     => 0,
                                                     'params'	=> array('items',
                                                                         'page')
                                                    ),

                        '/review/add'       => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('agent',
                                                                         'rating',
                                                                         'comment')
                                                    ),

                        '/review/delete'    => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('id',
                                                                         'all')
                                                    ),

                        '/trip/add'         => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('jurusan',
                                                                         'agen',
                                                                         'tanggal')
                                                    ),

                        '/trip/delete'      => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('id',
                                                                         'all')
                                                    ),

                        '/trip/checkin'     => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('id',
                                                                         'desc')
                                                    ),

                        '/trip/checkout'    => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('id')
                                                    ),
                        '/pengaduan/add'    => array('method'  	=> 'POST',
                                                     'auth'     => 2,
                                                     'params'	=> array('kode_booking', 'pesan')
                                                    ),
                        );
?>