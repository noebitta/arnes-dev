<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class PelangganController extends Controller{

	private $_pelangganModel;

	public function __construct(){

		parent::__construct();

		$this->_pelangganModel = $this->loadModel('Pelanggan');

	}

	public function index(){

	}

	public function cek(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data			= array();
		$result    		= array();
		$status     	= "ZERO_RESULTS";

		$no_telepon		= $this->getParam('no_telepon');

		$pelanggan 		= $this->_pelangganModel->getDetailByTelp($no_telepon);

		if($pelanggan){

			$status	= "OK";
			$member =  $this->_pelangganModel->getMemberDetailByTelp($pelanggan->NoTelp);

			if($member){
				$type = $member->KategoriMember == '0' ? 'K' : 'M';
			}
			else {
				$type = "U";
			}

			$result = array(
						'pelanggan'	=> 'LAMA',
						'data'		=> array(
										'nama' 				=> $pelanggan->Nama,
										'alamat' 			=> $pelanggan->Alamat,
										'id_member' 		=> $member->IdMember,
										'jenis_penumpang' 	=> $type
									)
					);

		}
		else{

			$status	= "OK";
			$result = array(
						'pelanggan'	=> 'BARU',
						'data'		=> array()
					);

		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

}
?>