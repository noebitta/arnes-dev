<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class UserController extends Controller{

	private $_userModel;

	public function __construct(){

		parent::__construct();

		$this->_userModel = $this->loadModel('User');

	}

	public function index(){

	}

	public function login(){

		global $cfg;

		$this->setRequestMethod('POST');
		$this->authenticate(1);

		$data		= array();
		$result    	= array();
		$status     = "ERROR";

		$username 	= $this->postParam('username');
		$password 	= $this->postParam('password');
		$password 	= md5($password);

		$user  		= $this->_userModel->userLogin($username, $password);
		$now 		= date('Y-m-d H:i:s');

		if(empty($user)){
			$this->error('Username atau password salah !');
		}

		if($user->user_active == 0) {
			$this->error('Akses user diblok, silahkan kontak support/admin');
		}

		if($user->user_level != '0.0' && $user->user_level != '1.0' && $user->user_level != '1.1' && $user->user_level != '1.2' && $user->user_level != '1.3' && 
			$user->user_level != '1.4' && $user->user_level != '2.0'){
			$this->error('Akun yang anda gunakan tidak dapat mengakses aplikasi');
		}

		//cek masa berlaku
		$berlaku 	= $user->berlaku;
		$berlaku 	= strtotime($berlaku);
		$now 		= strtotime($now);

		if($now > $berlaku) {
			$this->error('Masa berlaku habis, silahkan kontak support/admin');
		}

		try {

			$this->dbObj->beginTrans();

			$this->dbObj->updateRecord($cfg['sys']['tblPrefix'] . '_user',
									  array("waktu_login = NOW()"),
									  array("user_id = '" . $user->user_id . "'"));

			//delete token sebelumnya
			$values			= array();

			$values[]		= "user_id	 = '" . $user->user_id . "'";
			$values[]		= "client_id = '" . $this->postParam('auth_client_id') . "'";

			$this->dbObj->deleteRecord($cfg['sys']['tblPrefix'] . '_auth_user_token', $values);

			//buat token baru
			$reqSignature 	= new RequestSignature();
			$accessToken	= $reqSignature->createNonce(true);
			$tokenSecret	= $reqSignature->createNonce(true);
			$expire 		= time() + (365*86400);

			$values			= array();

			$values[]		= "access_token 		= '$accessToken'";
			$values[]		= "user_id				= '" . $user->user_id . "'";
			$values[]		= "client_id 			= '" . $this->postParam('auth_client_id') . "'";
			$values[]		= "access_token_secret 	= '$tokenSecret'";
			$values[]		= "access_token_date	= NOW()";
			$values[]		= "access_token_expire	= NOW()";

			$this->dbObj->insertRecord($cfg['sys']['tblPrefix'] . '_auth_user_token', $values);

			$this->dbObj->commitTrans();

			$status	= "OK";

			$result	= array(
							'user_id'				=> $user->user_id,
							'username'				=> $user->username,
							'nama' 					=> $user->nama,
							'tanggal_lahir' 		=> $user->birthday,
							'email' 				=> $user->email,
							'telpon' 				=> $user->telp,
							'hp' 					=> $user->hp,
							'alamat' 				=> $user->address,
							'access_token'			=> $accessToken,
							'access_token_secret' 	=> $tokenSecret,
							'user_level_id' 		=> $user->user_level,
							'user_level_name' 		=> $cfg['userLevel'][$user->user_level]
						);

		} catch (DbException $e) {
			
			$this->dbObj->rollbackTrans();

			Error::store('User', $e->getMessage());

			$this->internalError();
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

	public function logout(){

		
		
	}
	
}
?>