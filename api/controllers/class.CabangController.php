<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class CabangController extends Controller{

	private $_cabangModel;

	public function __construct(){

		parent::__construct();

		$this->_cabangModel = $this->loadModel('Cabang');

	}

	public function index(){

		global $cfg;

		$this->setRequestMethod('GET');
		$this->authenticate(2);

		$data		= array();
		$result    	= array();
		$status     = "ZERO_RESULTS";

		$cabang  	= $this->_cabangModel->getList();

		if($cabang){

			$status	= "OK";

			for ($i = 0; $i < sizeof($cabang); $i++) {

				$result[]	= array(
								'kode_cabang'	=> $cabang[$i]->KodeCabang,
								'nama_cabang' 	=> ucwords(strtolower($cabang[$i]->Nama)),
								'kota' 			=> ucwords(strtolower($cabang[$i]->Kota))
							);
			}
		}

		$data['status']	= $status;	
		$data['result']	= $result;

		$this->sendResponse($data);
	}

}
?>