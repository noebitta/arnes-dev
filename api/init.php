<?php
/**
 * KELIMUTU REST-Like PHP Framework
 *
 * System initialization
 *
 * (c) Copyright 2012 Lorensius W. L. T <lorenz@londatiga.net>
 */

date_default_timezone_set('Asia/Jakarta');

//define constants
define('ROOT_DIR',      str_replace("/usr", "", dirname(__FILE__)));
define('ROOT_URL',      substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(ROOT_DIR))));
define('SERVER_NAME',   $_SERVER['SERVER_NAME']);
define('FILE_NAME',     substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], '/') + 1));
define('LIB_DIR',       ROOT_DIR . '/libraries');
define('CLASS_DIR',     LIB_DIR  . '/classes');
define('FUNCTION_DIR',  LIB_DIR  . '/functions');
define('CONFIG_DIR',    ROOT_DIR . '/configs');
define('MODEL_DIR',     ROOT_DIR . '/models');
define('CONTROLLER_DIR',    ROOT_DIR . '/controllers');

include_once(FUNCTION_DIR . '/lib.php');
include_once(FUNCTION_DIR . '/lib.geocode.php');

include_once CONFIG_DIR . '/config_system.php';
include_once CONFIG_DIR . '/config_db.php';
include_once CONFIG_DIR . '/config_var.php';
include_once CONFIG_DIR . '/config_sms.php';

include_once CLASS_DIR . '/kelimutu/system/class.Registry.php';
include_once CLASS_DIR . '/kelimutu/system/class.Loader.php';
include_once CLASS_DIR . '/kelimutu/system/class.Error.php';
include_once CLASS_DIR . '/kelimutu/database/class.DbConnection.php';
include_once CLASS_DIR . '/kelimutu/controller/class.Dispatcher.php';
include_once CLASS_DIR . '/kelimutu/controller/class.Controller.php';
include_once CLASS_DIR . '/kelimutu/controller/class.Router.php';
include_once CLASS_DIR . '/kelimutu/model/class.Model.php';
include_once CLASS_DIR . '/kelimutu/transport/class.HTTP.php';
include_once CLASS_DIR . '/kelimutu/transport/class.QueryString.php';
include_once CLASS_DIR . '/kelimutu/auth/class.Auth.php';
// include_once CLASS_DIR . '/class.RequestSignature.php';
include_once CLASS_DIR . '/class.SignatureGenerator.php';
include_once CLASS_DIR . '/class.Agen.php';
include_once CLASS_DIR . '/class.Mailer.php';

include ROOT_DIR . '/functions/sendsms.php';
include ROOT_DIR . '/functions/datename.php';

try {
    $dbObj = DbConnection::getInstance('MySQL');

    $dbObj->setConnectionParameters($cfg['db']['host'],
                                    $cfg['db']['user'],
                                    $cfg['db']['password'],
                                    $cfg['db']['name'],
                                    '3306');

    $dbObj->connect();

    Registry::set('db', $dbObj);

    Router::add('/', array('controller' => 'Home'));

    //User
    Router::add('/register',           array('controller' => 'User', 'action' => 'join'));
    Router::add('/login',              array('controller' => 'User', 'action' => 'login'));
    Router::add('/profil',             array('controller' => 'User', 'action' => 'me'));
    

    //Car
    Router::add('/car/brand',           array('controller' => 'Car', 'action' => 'brand'));
    
} catch (KelimutuException $e) { die ($e->getMessage()); }