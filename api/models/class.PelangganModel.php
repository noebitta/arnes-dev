<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class PelangganModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_pelanggan';
		$this->_id 		= '';
	}
	
	public function getDetailByTelp($tlp) {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_pelanggan
				WHERE 
					NoTelp = '$tlp'
				ORDER BY 
					IdPelanggan DESC
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Pelanggan', $e->getMessage()); }

		return $res;
	}

	public function getMemberDetail($id){
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_member
				WHERE 
					IdMember = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Pelanggan', $e->getMessage()); }

		return $res;
	}

	public function getMemberDetailByTelp($telp){
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_member
				WHERE 
					Telp 		= '$telp' OR 
					Handphone 	= '$telp'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Pelanggan', $e->getMessage()); }

		return $res;
	}

	public function getFrekwensiMember($id, $tgl){
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_member_frekwensi_berangkat
				WHERE 
					IdMember 		= '$id' AND 
					TglBerangkat	= '$tgl'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Pelanggan', $e->getMessage()); }

		return $res;
	}

	public function getFrekwensiMember2($id){
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_member_frekwensi_berangkat
				WHERE 
					IdMember = '$id'
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetch();
		} catch (DbException $e) { Error::store('Pelanggan', $e->getMessage()); }

		return $res;
	}

}