<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class AuthClientModel extends Model
{
	/**
	 * Constructor.
	 *
	 */
	public function __construct()
	{
		global $cfg;

		parent::__construct();

        $this->_table = $this->_tblPrefix. '_auth_client';
	}

	/**
	 * Mendapatkan detail client
	 *
	 * @param string $id Client id
	 *
	 * @return object Client detail
	 */
	public function getDetail($id) {
		return $this->find(array('filter' => array("client_id = '$id'")));
	}
}
?>