<?php

/**
 * @author : Yudi Setiadi Permana
 * email   : yudi.setiadi.permana@gmail.com
 */

class CabangModel extends Model {

	public function __construct() {

		parent::__construct();

		$this->_table 	= $this->_tblPrefix .'_md_cabang';
		$this->_id 		= '';
	}
	
	public function getList() {
		
		global $cfg;

		$sql = "SELECT 
					* 
				FROM 
					tbl_md_cabang
				ORDER BY
					Nama ASC
				";

		$res = null;

		try {
			$this->_dbObj->query($sql);

			$res = $this->_dbObj->fetchAll();
		} catch (DbException $e) { Error::store('Cabang', $e->getMessage()); }

		return $res;
	}

}