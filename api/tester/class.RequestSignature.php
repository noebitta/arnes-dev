<?php
class RequestSignature
{
    public function urlencode ($s)
    {
        if ($s === false) {
            return $s;
        } else {
            return str_replace('%7E', '~', rawurlencode($s));
        }
    }

    public function urldecode ($s)
    {
        if ($s === false) {
            return $s;
        } else {
            return rawurldecode($s);
        }
    }

	public function createNonce ( $unique = false )
	{
		$key = md5(uniqid(rand(), true));
		if ($unique)
		{
			list($usec,$sec) = explode(' ',microtime());
			$key .= dechex($usec).dechex($sec);
		}
		return $key;
	}

    public function normalizeParams($params)
    {
        if (empty($params)) return;

        $normalized = array();

        ksort($params);
        foreach ($params as $name => $value) {
            if (is_array($value) && sizeof($value)) {
                sort($value);

                for ($i = 0; $i < sizeof($value); $i++) {
                    $normalized[] = "$name=" . $this->urlencode($value[$i]);
                }
            } else {
                $normalized[] = "$name=" . $this->urlencode($value);
            }
        }

        return implode("&", $normalized);
    }

    public function createSignatureBase($method, $url, $params)
    {
        $params  = (empty($params)) ? '' : $this->normalizeParams($params);
        $sigbase = array($method,
                         $this->urlencode($url),
                         $this->urlencode($params));

        return implode('&', $sigbase);
    }

    public function createSignature($signatureBase, $clientId, $clientSecret )
    {
        $key = $this->urlencode($clientId) . '&' . $this->urlencode($clientSecret);

        if (function_exists('hash_hmac')) {
            $signature = base64_encode(hash_hmac("sha1", $signatureBase, $key, true));
        } else {
            $blocksize	= 64;
            $hashfunc	= 'sha1';

            if (strlen($key) > $blocksize) {
                $key = pack('H*', $hashfunc($key));
            }

            $key	= str_pad($key,$blocksize,chr(0x00));
            $ipad	= str_repeat(chr(0x36),$blocksize);
            $opad	= str_repeat(chr(0x5c),$blocksize);
            $hmac 	= pack(
                        'H*',$hashfunc(
                            ($key^$opad).pack(
                                'H*',$hashfunc(
                                    ($key^$ipad).$signatureBase
                                )
                            )
                        )
                    );

            $signature = base64_encode($hmac);
        }

        return $this->urlencode($signature);
    }
}

$rs = new RequestSignature();

$base="POST&https%3A%2F%2Fapi.tiketux.com%2Fdev1%2Fh2h%2Freservasi%2Fbook.json&alamat_pemesan%3DGandaria+Jakarta%26auth_access_token%3D1763caff280d2ea3f99306bb4d8351a405414c8a9%26auth_client_id%3DMCOIN%26auth_nonce%3D5ea9987200544e8ef859c21c850cb93c24be%26auth_timestamp%3D1410958934587%26channel%3DMCO%26kode_agen%3DDTR%26kode_jadwal%3DCHP-KRT16%26nama_pemesan%3Dmcoin%26nama_penumpang%3Dmcoin%26nomor_kursi%3D5%26tanggal_berangkat%3D2014-09-18%26telp_pemesan%3D085715889626";

//echo $rs->createSignature($base, '1763caff280d2ea3f99306bb4d8351a405414c8a9', 'e06466b4bc8c2ea927a25243e7aa8c3605414c8a9');
//echo "\n";
?>