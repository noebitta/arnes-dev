<?php
function __p($array)
{
    print_r($array);
}

function __vd($vd)
{
    var_dump($vd);
}

function app_shutdown($message) {
    die("BlackCat: $message");
}

function form_error($error)
{
    return "<span style=\"color:red\">$error</span>";
}

function formatLastLogin($time, $host)
{
    if (empty($time) || $time == '0000-00-00 00:00:00')
        return 'Never Logged In';
    else
        return formatDate($time, 'M d, Y H:i:s') . " from $host";
}

function formatDate($date, $format='d/m/Y')
{
    return ($date == null || $date == '0000-00-00 00:00:00' || $date == '0000-00-00') ? '' : date($format, strtotime($date));
}

function zeroExtend($s, $n)
{
    for ($i = 0; $i < $n; $i++) {
        if (strlen($s) == $n) return $s;
        $s = "0".$s;
    }

    return $s;
}

function formatNumber($number)
{
    $number = "$number";
    $number = (strrpos($number, "-")) ? substr($number, 0) : $number;
    $len    = strlen($number);
    $l      = $len;
    $str    = "";

    for ($i = 0; $i < (ceil($len/3)-1); $i++) {
        $str = ".".substr($number, ($l-=3), 3).$str;
    }

    $dotted_number = substr($number, 0, $l).$str;

    return (strrpos($number, "-")) ? "-$dotted_number" : $dotted_number;;
}

function firstCapital($str)
{
    $arrstr = explode(' ', $str);
    for ($i = 0; $i < sizeof($arrstr); $i++)
    {
        if (strlen($arrstr[$i]) > 0) $arrstr[$i][0] = strtoupper($arrstr[$i][0]);
    }
    return implode(' ', $arrstr);
}

function num2String($num)
    {
        $arrNum =
            array(
                '0' => 'nol',
                '1' => 'satu',
                '2' => 'dua',
                '3' => 'tiga',
                '4' => 'empat',
                '5' => 'lima',
                '6' => 'enam',
                '7' => 'tujuh',
                '8' => 'delapan',
                '9' => 'sembilan'
            );

        $res = '';
        $number = $num . '';
        $length = strlen($number);

        for ($i = 0; $i < $length; $i++)
        {
            $residu = $length - $i;
            $reside = $residu % 3;
            $current = $number[$i];
            $arrcur = $arrNum[$current];

            if ($residu == 1)
            {
                $res .= ((($res != '') && ($current == '0')) ? '' : " $arrcur");
            }
            else
            {
                if ($current == '1')
                {
                    if (($reside == 2) && ($number[$i+1] != '0'))
                    {
                        $i++;
                        $residu = $length - $i;
                        $reside = 1;
                        $current = $number[$i];
                        $arrcur = $arrNum[$current];

                        $res .= ($current == '1') ? ' se' : " $arrcur";
                        $res .= 'belas ';
                    }
                    else
                        $res .= ' se';
                }
                elseif ($current == '0')
                {
                    if ($reside != 1) continue;
                    else $res .= ' ';
                }
                else $res .= ' ' . $arrNum[$current] . ' ';

                if (($reside) == 1)
                {
                    if ($residu == 1) continue;
                    do
                    {
                        $reside = $residu % 12;
                        if ($reside == 1) $res .= 'trilyun';
                        elseif ($reside == 10) $res .= 'milyar';
                        elseif ($reside == 7) $res .= 'juta';
                        elseif ($reside == 4) $res .= 'ribu';
                        $residu -= $reside;
                    }
                    while ($residu > 12);
                }
                elseif ($reside == 0) $res .= 'ratus';
                else $res .= 'puluh';
            }
        }

        return $res;
    }

function array_merge_select($input, $val='')
{
    $merge = array();
    if (is_array($input) && sizeof($input)) {
        $merge[''] = $val;
        foreach ($input as $key => $value) {
            $merge[$key] = $value;
        }
    } else {
        $merge[''] = $val;
    }

    return $merge;
}

function boxError($msg)
{
    return "<div class=\"redbox\">$msg</div>";
}

function boxSuccess($msg)
{
    return "<div class=\"greenbox\">$msg</div>";
}

function boxYellow($msg)
{
    return "<div class=\"yellowbox\">$msg</div>";
}

function DegToDMS($coord, $opt='lat')
{
  $isnorth  = $coord>=0;
  $coord    = abs($coord);
  $deg      = floor($coord);
  $coord    = ($coord-$deg)*60;
  $min      = floor($coord);
  $sec      = floor(($coord-$min)*60);

  return sprintf("%d&deg;%d'%d\"%s", $deg, $min, $sec, ($opt = 'lat') ? ($isnorth ? 'N' : 'S') : ($isnorth ? 'W' : 'E'));
}

function formatlist($str)
{
    if (!empty($str)) {
        return preg_replace("/;/", "<br>", $str);
    }
}

function formatyesno($i)
{
    return ($i == 0) ? 'Tidak' : 'Ya';
}

function getPosition($position)
{
    $pos                = array();
    $pos['latitude']    = '';
    $pos['longitude']   = '';

    if (empty($position)) return $pos;

    $position           = preg_replace("/POINT\(|\)/", '', $position);
    $temp               = explode(' ', $position);
    $pos['latitude']    = trim($temp[1]);
    $pos['longitude']   = trim($temp[0]);

    return $pos;
}

function getStarRating($rating)
{
    $rating = round($rating);

    $star   = '';

    for ($i = 1; $i <= 5; $i++) {
        if ($i > $rating) {
            $star .= "<img src='" . ROOT_URL ."/themes/default/images/star_off.png'>";
        } else {
            $star .= "<img src='" . ROOT_URL ."/themes/default/images/star_on.png'>";
        }
    }

    return $star;
}

function formatAktif($aktif)
{
    return ($aktif == 1) ? '' : "background:#ff0000;color:#ffffff";
}

function formatRegister($time, $client)
{
    $time   = formatDate($time, 'd/m/Y H:i');

    $client = strtolower($client);
    $img    = 'web_small.png';
    $tip    = 'Via web browser';

    if ($client == 'android') {
        $img    = 'android_small.png';
        $tip    = 'Via Android App';
    } else if ($client == 'blackberry') {
        $img    = 'blackberry_small.png';
        $tip    = 'Via Blackberry App';
    } else if ($client == 'j2me') {
        $img    = 'j2me_small.png';
        $tip    = 'Via Java (J2ME) App';
    }

    $str = "<a href='javascript:void(0)' class='lytetip' data-tip='$tip'>$time "
         . "<img src='" . ROOT_URL . "/themes/default/images/$img' border='0'></a>";

    return $str;
}

function sendHttpPost($url,$parameter){
		
	$length = strlen($parameter);
		
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $parameter);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_VERBOSE,1);
	$response = curl_exec($ch);
	//die($url.$parameter." Response:".$response);	
	return $response;
}
	
function sendHttpGet($url,$parameter=""){
	
	if($parameter!=""){
		$parameters	= "?".$parameter;
	}
	
  //$start    = time();
  
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url.$parameters);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	//curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_VERBOSE,1);
	$response = curl_exec($ch);
	//echo(curl_errno($ch));
	//die($url.$parameter." Response:".$response);	
	
  //$end  = time();
  
  return $response;
}

function ambilAgenInfo($kode_agen){
		
		$Agen  =  new Agen();
		
		$return_value = $Agen->getAgenInfo($kode_agen);
		
		$decode = json_decode($return_value);
		
        // var_dump($return_value);
		//var_dump($decode);exit;
		
		//cek status
		if($decode->tiketux->status!="OK"){
			$return['status']			= "GAGAL";
			return "";
		}
	
		$return["kode"] 		= $decode->tiketux->results->kode;
		$return["nama"] 		= $decode->tiketux->results->nama;
		$return["alamat"]		= $decode->tiketux->results->alamat;
		$return["kota"] 		= $decode->tiketux->results->kota;
		$return["telp"] 		= $decode->tiketux->results->telp;
		$return["website"] 	= $decode->tiketux->results->website;
		$return["logo"] 		= $decode->tiketux->results->logo->big;	

		return $return;
	}

function getKataBijak(){
	global $cfg;
	
	$id_random	= rand(0,sizeof($cfg['sys']['kataBijak'])-1);
	
	return $cfg['sys']['kataBijak'][$id_random];
}


?>