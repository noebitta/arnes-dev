<?php
/**
 * Lorensius W. L. T
 * lorenz@londatiga.net
 */
class SignatureGenerator
{
	public function genKeyId($authkey)
	{
		return strtoupper(bin2hex($this->str2bin($authkey)));
	}

	public function genSignature($merchant, $kodeBooking, $waktuPesan, $jumlahBayar, $telp, $keyId)
	{
		$tempKey 	= $merchant . $kodeBooking . $waktuPesan . $jumlahBayar . $telp . $keyId;
		$hashKey 	= $this->getHash($tempKey);

		$signature 	= abs($hashKey);

		return $signature;
	}

	private function hex2bin($data)
	{
		$len = strlen($data);
		return pack("H" . $len, $data);
	}

	private function str2bin($data)
	{
		$len = strlen($data);
		return pack("a" . $len, $data);
	}

	private function intval32bits($value)
    {
        if ($value > 2147483647)
            $value = ($value - 4294967296);
		else if ($value < -2147483648)
            $value = ($value + 4294967296);

        return $value;
    }

	private function getHash($value)
	{
		$h = 0;
		for ($i = 0;$i < strlen($value);$i++)
		{
			$h = $this->intval32bits($this->add31T($h) + ord($value{$i}));
		}
		return $h;
	}

	private function add31T($value)
	{
		$result = 0;
		for($i=1;$i <= 31;$i++)
		{
			$result = $this->intval32bits($result + $value);
		}

		return $result;
	}
}

// sample generate signature
/*
$authKey 			= '42ac65cc4fea2bfe1103dc8f0b385b5850b485dd';
$secretKey 			= 'bde2ad1b8ce0708eb492bd8be9ed6b650c63e4b8';

$merchant			= 'RENTAL';
$orderId			= '73EBB4E976';
$orderTime 			= '2014-09-25 14:13:20';
$orderAmount 		= 700000;
$orderUserPhone 	= '08986825936';

$sigGenerator		= new SignatureGenerator();

$auth 				= $authKey. '&' . $secretKey;
$keyId				= $sigGenerator->genKeyId($auth);
$genSignature		= $sigGenerator->genSignature($merchant, $orderId, $orderTime, $orderAmount, $orderUserPhone, $keyId);

echo "Signature: $genSignature\n";
*/
?>