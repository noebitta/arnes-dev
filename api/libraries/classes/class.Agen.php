<?php
class Agen{

	//KAMUS GLOBAL
	var $id_file;
	var $url_class;

	//CONSTRUCTOR
	public function __construct(){
		global $cfg;
		global $sessObj;

		$this->id_file = "AGEN";
	}

	//FINPAY
	public function payFinpay($booking_code, $booking_name, $booking_price, $user_name, $user_phone, $user_email, $merchant, $gensignature, $time){

		global $cfg;
		global $sessObj;

		$reqSignature	= new RequestSignature();
		$params 		= array();

		//Api Parameter
		$params['auth_nonce']	  	= $reqSignature->createNonce(true);
		$params['auth_timestamp'] 	= time();
		$params['auth_client_id']	= $cfg['sys']['client_id'];

		$params['merchant'] 		= $merchant;
		$params['order_id'] 		= $booking_code;
		$params['order_name'] 		= $booking_name;
		$params['order_user_name'] 	= $user_name;
		$params['order_user_phone'] = $user_phone;
		$params['order_user_email'] = $user_email;
		$params['order_time']	 	= $time;
		$params['order_amount']	 	= $booking_price;
		$params['return_url'] 		= $cfg['sys']['server'] ."/adminbooking/payfinnet/"; // Online Live
		// $params['return_url'] 		= "http://localhost/car_rent/booking/done"; // Local Dev
		$params['signature'] 		= $gensignature;
		
		// $accessToken				= $sessObj->getVar("accessToken");
		// $accessTokenSecret			= $sessObj->getVar("accessTokenSecret");

		$accessToken				= "";
		$accessTokenSecret			= "";

		if (!empty($accessToken)) {
			$params['auth_access_token'] = $accessToken;
			$key 	= $accessToken;
			$secret = $accessTokenSecret;
		} else {
			$key 	= $cfg['sys']['client_id'];
			$secret = $cfg['sys']['client_secret'];
		}

		$baseSignature = $reqSignature->createSignatureBase("POST", $cfg['sys']['apiUrl']."paymentmc/finpay.json", $params);
		$signature     = $reqSignature->createSignature($baseSignature, $key, $secret);

		return sendHttpPost($cfg['sys']['apiUrl'] ."paymentmc/finpay.json",$reqSignature->normalizeParams($params).'&auth_signature=' .$signature);
		
	}
	
}
?>