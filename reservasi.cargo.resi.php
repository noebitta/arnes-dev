<?php
//
// RESERVASI
//

// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){  
  if(!isset($HTTP_GET_VARS['mode'])){
		redirect(append_sid('index.'.$phpEx),true); 
	}
	else{
		echo("Silahkan Login Ulang!");exit;
	}
}

include($adp_root_path . 'ClassPengaturanUmum.php');
include($adp_root_path . 'ClassPaketCargo.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// PARAMETER
$perpage = $config['perpage'];
$mode    = isset($HTTP_GET_VARS['mode'])?$HTTP_GET_VARS['mode']:$HTTP_POST_VARS['mode'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$Cargo	= new Cargo();
$PengaturanUmum	= new PengaturanUmum();

$no_awb	= $HTTP_GET_VARS['noawb'];

$data_cargo	=  $Cargo->ambilDataDetailByAWB($no_awb);

$data_polis	= $PengaturanUmum->ambilParameter("POLIS_ASURANSI");

$list_via	= array("DRT"=>"DARAT","UDR"=>"UDARA","LUT"=>"LAUT");

$template->assign_vars(array(
	'NAMA_PERUSAHAAN'	=> $config['nama_perusahaan'],
	'AWB'							=> $data_cargo['NoAWB'],
	'ID'							=> $data_cargo['Id'],
	'TELP_PENGIRIM'		=> $data_cargo['TelpPengirim'],
	'NAMA_PENGIRIM'		=> $data_cargo['NamaPengirim'],
	'ALAMAT_PENGIRIM'	=> $data_cargo['AlamatPengirim'],
	'TELP_PENERIMA'		=> $data_cargo['TelpPenerima'],
	'NAMA_PENERIMA'		=> $data_cargo['NamaPenerima'],
	'ALAMAT_PENERIMA'	=> $data_cargo['AlamatPenerima'],
	'KOTA_ASAL'				=> $data_cargo['Asal'],
	'KOTA_TUJUAN'			=> $data_cargo['Tujuan'],
	'JENIS_BARANG'		=> ($data_cargo['JenisBarang']=="PRC"?"PARCEL":"DOKUMEN"),
	'VIA'							=> $list_via[$data_cargo['Via']],
	'LAYANAN'					=> ($data_cargo['Layanan']=="REG"?"REGULER":"URGENT"),
	'KOLI'						=> number_format($data_cargo['Koli'],0,",","."),
	'PERHITUNGAN'			=> ($data_cargo['IsVolumetric']==0?"BERAT":"VOLUMETRIC"),
	'SHOW_DIMENSI'		=> ($data_cargo['IsVolumetric']==0?"none":"block"),
	'DIMENSI_PANJANG'	=> number_format($data_cargo['DimensiPanjang'],0,",","."),
	'DIMENSI_LEBAR'		=> number_format($data_cargo['DimensiLebar'],0,",","."),
	'DIMENSI_TINGGI'	=> number_format($data_cargo['DimensiTinggi'],0,",","."),
	'BERAT'						=> number_format($data_cargo['Berat'],0,",","."),
	'LEAD_TIME'				=> $data_cargo['LeadTime'],
	'BIAYA_KIRIM'			=> number_format($data_cargo['BiayaKirim'],0,",","."),
	'BIAYA_TAMBAHAN'	=> number_format($data_cargo['BiayaTambahan'],0,",","."),
	'BIAYA_PACKING'		=> number_format($data_cargo['BiayaPacking'],0,",","."),
	'IS_ASURANSI'			=> ($data_cargo['IsAsuransi']==0?"TIDAK":"YA"),
	'SHOW_ASURANSI'		=> ($data_cargo['IsAsuransi']==0?"none":"block"),
	'HARGA_DIAKUI'		=> number_format($data_cargo['HargaDiakui'],0,",","."),
	'BIAYA_ASURANSI'	=> number_format($data_cargo['BiayaAsuransi'],0,",","."),
	'TOTAL_BIAYA'			=> number_format($data_cargo['TotalBiaya'],0,",","."),
	'CARA_BAYAR'			=> ($data_cargo['IsAsuransi']==1?"TUNAI":"KREDIT"),
	'DESKRIPSI_BARANG'=> $data_cargo['DeskripsiBarang'],
	'CSO'							=> $data_cargo['NamaPencatat'],
	'NO_POLIS'				=> $data_polis["NilaiParameter"],
	'WAKTU_TERIMA'		=> dateparseWithTime(FormatMySQLDateToTglWithTime($data_cargo['WaktuCatat'])),
));

for($i=1;$i<=2;$i++){
	$template->assign_block_vars('RESI',array(
		"peruntunkan"=>($i%2!=0?"--==UNTUK PENGIRIM==--":"--==UNTUK PENERIMA==--"),
		"ttd_penerima"=>($i%2!=0?"":"TTD PENERIMA<br><br><br><br><br><br>NAMA:<br>Tgl.Terima:<br><br>")
		)
	);	
}

for($i=1;$i<=$data_cargo['Koli'];$i++){
	$template->assign_block_vars('LABEL_PAKET',array(
		'i_koli'	=> $i
	));	
}

$template->set_filenames(array('body' => 'reservasi.cargo/resi.tpl')); 

$template->pparse('body');
?>