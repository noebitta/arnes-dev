<?php

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in']){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

class Memo{
	
	//KAMUS GLOBAL
	var $ID_FILE; //ID Kelas
	
	//CONSTRUCTOR
	function Memo(){
		$this->ID_FILE="C-MEM";
	}
	
	//BODY
	
	function tambahMemo($tgl_memo,$kode_jadwal,$memo,$pembuat_memo,$Jadwal,$Reservasi,$PenjadwalanKendaraan){
	  
		/*
		IS	: data memo belum bisa ada atau belum dalam database
		FS	:Data memo telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENGUBAH DDATA KEDALAM DATABASE
		$sql = 
			"UPDATE tbl_posisi 
			SET Memo='$memo',WaktuBuatMemo=NOW(),PembuatMemo=$pembuat_memo,FlagMemo=1
			WHERE TglBerangkat='$tgl_memo' AND KodeJadwal='$kode_jadwal'";
		
		
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		if($db->sql_affectedrows()<=0){
			
			$data_jadwal 				= $Jadwal->ambilDataDetail($kode_jadwal);
		  $jam_berangkat			= $data_jadwal['JamBerangkat'];
		  $layout_kursi 			= $data_jadwal['JumlahKursi'];
			$flag_sub_jadwal		= $data_jadwal['FlagSubJadwal'];
			$kode_jadwal_utama	= $data_jadwal['KodeJadwalUtama'];
			
			//memeriksa flag sub jadwal, jika jadwal ini merupakan subjadwal dari jadwal yang lain , maka layoutnya akan mengambil jadwal induknya
			$kode_jadwal_aktual	= ($flag_sub_jadwal!=1)? $kode_jadwal : $kode_jadwal_utama;
			
			$data_penjadwalan	= $PenjadwalanKendaraan->ambilDataDetail($tgl_memo,$kode_jadwal_aktual);
			$kode_kendaraan		= $data_penjadwalan['KodeKendaraan'];
		  $kode_sopir 			= $data_penjadwalan['KodeDriver'];
		  $layout_kursi 		= ($data_penjadwalan['LayoutKursi']=='')?$layout_kursi:$data_penjadwalan['LayoutKursi'];
			
			$Reservasi->tambahPosisi(
				$kode_jadwal, $tgl_memo,$jam_berangkat,
				$layout_kursi, $kode_kendaraan, $kode_sopir);
				
			if (!$db->sql_query($sql)){
				die_error("Err $this->ID_FILE".__LINE__);
			}
								
		}
		
		return true;
	}
	
	function hapus($list_memo){
	  
		/*
		IS	: data memo sudah ada dalam database
		FS	:Data memo dikosongkan
		*/
		
		//kamus
		global $db;
		
		//MENGOSONGKAN ISI MEMO
		$sql = 
			"UPDATE tbl_posisi 
			SET Memo=NULL,WaktuBuatMemo=NULL,PembuatMemo=NULL,FlagMemo=0
			WHERE ID IN($list_memo);";
								
		if (!$db->sql_query($sql)){
			return false;
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return true;
	}//end hapus
	
	function ambilDataDetail($tgl_berangkat,$kode_jadwal){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT ID,Memo,WaktuBuatMemo,PembuatMemo,FlagMemo,f_user_get_nama_by_userid(PembuatMemo) AS NamaPembuat,
				KodeJadwal,TglBerangkat
			FROM tbl_posisi
			WHERE TglBerangkat='$tgl_berangkat' AND KodeJadwal='$kode_jadwal'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilDataDetailById($id){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = 
			"SELECT ID,Memo,WaktuBuatMemo,PembuatMemo,FlagMemo,f_user_get_nama_by_userid(PembuatMemo) AS NamaPembuat,
				KodeJadwal,TglBerangkat
			FROM tbl_posisi
			WHERE ID='$id'";
				
		if ($result = $db->sql_query($sql)){
			$row=$db->sql_fetchrow($result);
			return $row;
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilData($pencari,$order_by,$asc){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$pencari	= ($pencari=='')?'%':$pencari;
		$order		= ($order_by!='')?" ORDER BY $order_by $asc":'';
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PembuatMemo) AS NamaPembuat
			FROM tbl_pengumuman
			WHERE 
				(KodeMemo LIKE '$pencari' 
				OR JudulMemo LIKE '$pencari' 
				OR Memo LIKE '%$pencari%') 
			$order;";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilSemuaMemo($user_id){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql_status_baca	= "SELECT user_id FROM tbl_user_baca_pengumuman tub WHERE tub.IdMemo=tp.IdPenguman";
		
		$sql = 
			"SELECT *,f_user_get_nama_by_userid(PembuatMemo) AS NamaPembuat,
				IF($user_id IN ($sql_status_baca),1,0) AS StatusBaca
			FROM tbl_pengumuman tp
			ORDER BY WaktuPembuatanMemo DESC, KodeMemo ASC";
				
		if ($result = $db->sql_query($sql)){
			return $result;
		} 
		else{
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
	}//  END ambilData
	
	function ambilJumlahMemoBaru($user_id){
		
		/*
		Desc	:Mengembalikan data pengumuman sesuai dengan kriteria yang dicari
		*/
		
		//kamus
		global $db;
		
		$sql = "SELECT IF(f_user_get_jumlah_pengumuman_baru($user_id) IS NULL,0,f_user_get_jumlah_pengumuman_baru($user_id)) AS jumlah";
				
		if ($result = $db->sql_query($sql)){
			while ($row = $db->sql_fetchrow($result)){
				$jumlah	= $row['jumlah'];
			}
		} 
		else{
			die_error("Err: $this->ID_FILE".__LINE__);
		}
		
		return $jumlah;
	}//  END ambilJumlahMemoBaru
	
	function sudahBaca($id_pengumuman,$user_id){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_user_baca_pengumuman_tambah('$id_pengumuman','$user_id')";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function updateMemoBaru(){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_user_update_pengumuman_baru()";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}
	
	function resetMemoBaru($user_id){
	  
		/*
		IS	: data pengumuman sudah ada dalam database
		FS	:Data pengumuman baru telah disimpan dalam database 
		*/
		
		//kamus
		global $db;
		
		//MENAMBAHKAN DATA KEDALAM DATABASE
		$sql = "CALL sp_user_reset_pengumuman_baru($user_id)";
								
		if (!$db->sql_query($sql)){
			die_error("Err $this->ID_FILE".__LINE__);
		}
		
		return true;
	}

	
}
?>