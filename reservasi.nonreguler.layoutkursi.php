<?php
//CONTROLLER UNTUK RENDER LAYOUT KENDARAAN

// SECURITY
define('FRAMEWORK', true);

//INCLUDES FILE
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassJadwal.php');
include($adp_root_path . 'ClassReservasiNonReguler.php');
include($adp_root_path . 'ClassLayoutKendaraan.php');
include($adp_root_path . 'ClassPenjadwalanKendaraan.php');
include($adp_root_path . 'ClassMobil.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// halaman ini hanya bisa diakses mereka yang sudah login (ber-session)
if(!$userdata['session_logged_in'] || !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['SCHEDULER'],$USER_LEVEL_INDEX['CSO'],$USER_LEVEL_INDEX['CSO2'],$USER_LEVEL_INDEX['CSO_PAKET']))){
  if(!isset($HTTP_GET_VARS['mode'])){
    redirect(append_sid('index.'.$phpEx),true);
  }
  else{
    echo("SILAHKAN LOGIN KEMBALI");exit;
  }
}


// PARAMETER
$user_id  	= $userdata['user_id'];
$user_level	= $userdata['user_level'];

if($userdata['user_level']==$USER_LEVEL["CSO_PAKET"]){
  echo("Silahkan login sebagai CSO!");
  exit;
}

//CONSTRUCT OBJECT
$Jadwal								= new Jadwal();
$PenjadwalanKendaraan	= new PenjadwalanKendaraan();
$Reservasi						= new ReservasiNonReguler();
$LayoutKendaraan			= new LayoutKendaraan();
$Mobil                = new Mobil();

//GET PARAMETER DIKIRIM DARI reservasi.js function getUpdateMobil()
$tgl_mysql						= $HTTP_GET_VARS['tanggal'];
$tgl  								= FormatMySQLDateToTgl($tgl_mysql); // tanggal
$kode_jadwal 					= $HTTP_GET_VARS['jadwal'];    // jam
$kode_booking_dipilih	= $HTTP_GET_VARS['kode_booking_dipilih'];    // jam

if($kode_jadwal==""){
  //BELUM MEMILIH JADWAL
  $template->assign_vars(array("PESAN"=>"Anda belum memilih jadwal keberangkatan!"));
  $template->set_filenames(array('bodyalert' => 'reservasi/reservasi.layoutkursi.alert.tpl'));
  $template->pparse('bodyalert');
  exit;
}

//MENGAMBIL DATA JADWAL
$data_jadwal        = $PenjadwalanKendaraan->ambilDataDetail($tgl_mysql,$kode_jadwal);
$jam_berangkat	    = $data_jadwal['JamBerangkat'];
$layout_kursi       = $data_jadwal['LayoutKursi'];
$id_jurusan         = $data_jadwal['IdJurusan'];
$kode_kendaraan	    = $data_jadwal['KodeKendaraan'];
$no_polisi 			    = $data_jadwal['NoPolisi'];
$kode_sopir 		    = $data_jadwal['KodeDriver'];
$nama_sopir 		    = $data_jadwal['NamaSopir'];
$data_layout	      = $Mobil->getArrayLayout();
$jumlah_kursi       = $data_layout[$layout_kursi];

//MEMERIKSA HAK AKSES, JIKA CSO BIASA TIDAK BOLEH MEMESAN PADA WAKTU YANG SUDAH LALU
if(!$Reservasi->periksaHakAkses($tgl_mysql,$jam_berangkat) && $user_level>=$USER_LEVEL["CSO"]){
  $template->assign_vars(array("PESAN"=>"Anda tidak boleh memilih waktu yang sudah lalu!"));
  $template->set_filenames(array('bodyalert' => 'reservasi/reservasi.layoutkursi.alert.tpl'));
  $template->pparse('bodyalert');
  exit;
}

//JIKA LEVEL CSO, AKAN DICEK TANGGAL BERANGKAT, KARENA TIDAK BOLEH MEMBOOKING LEWAT 8 HARI DARI SEKARANG
/*$temp_tgl 	= str_replace("-", "/", $tgl_mysql);
$start_time_stamp = strtotime(date("Y/m/d"));
$end_time_stamp = strtotime($temp_tgl);

$time_diff = abs($end_time_stamp - $start_time_stamp);

$number_days = $time_diff/86400;  // 86400 seconds in one day

// and you might want to convert to integer
$number_days = intval($number_days);

if($number_days>8 && !in_array($userdata['user_level'],array($USER_LEVEL_INDEX['ADMIN'],$USER_LEVEL_INDEX['MANAJEMEN'],$USER_LEVEL_INDEX['MANAJER'],$USER_LEVEL_INDEX['SPV_RESERVASI'],$USER_LEVEL_INDEX['SPV_OPERASIONAL'],$USER_LEVEL_INDEX['CSO2']))){
  //JIKA TANGGAL BOOKING LEBIH DARI 8 HARI, AKSES DITOLAK
  echo("<br><br><br><br><br><br>
    <img src='./templates/images/icon_warning.png' />
    <font color='red'><h3>Anda tidak boleh memilih waktu lebih dari 8 hari dari sekarang!</h3></font>");
  exit;
}*/
//END==TGL UPDATE: 19 NOVEMBER 2013

/*MENGAMBIL HARGA TIKET*/
$harga_tiket=$Reservasi->getHargaTiket($kode_jadwal,$tgl_mysql,$id_jurusan,true);

//LAYOUT KENDARAAN
//mengambil header dari layout kursi
$row				= $Reservasi->ambilDataHeaderLayout($tgl_mysql,$kode_jadwal);
$id 				= $row['ID'];
$no_spj			= $row['NoSPJ'];

if($no_spj!=""){
  $kode_kendaraan	= $row['KodeKendaraan'];
  $no_polisi			= $row['NoPolisi'];
  $kode_sopir 		= $row['KodeSopir'];
  $nama_sopir 		= $row['NamaSopir'];
}

$data_kendaraan	= $kode_kendaraan==""?"<font color='red'>BELUM DIATUR</font>":"<b>$kode_kendaraan</b> | $no_polisi";

//RENDERING LAYOUT KENDARAAN
//RENDER HEADER
$template->destroy();

$tgl_sekarang	= date('Y-m-d');

if(($tgl_sekarang==$tgl_mysql || $userdata['user_level']<=$USER_LEVEL["SPV_OPERASIONAL"]) &&
  (($kode_jadwal==$kode_jadwal_utama) || (($kode_jadwal!=$kode_jadwal_utama) && $no_spj!="") )){
  //Show tombol manifest jika tanggal cetak adalah tanggal hari ini atau user adalah bukan user biasa
  $template->assign_block_vars("TOMBOL_MANIFEST",array());
}

$template->assign_vars (
  array(
    'TGL_BERANGKAT'=>dateParse(FormatMySQLDateToTgl($tgl_mysql)),
    'KODE_JADWAL'  =>$kode_jadwal,
    'JAM_BERANGKAT'=>$jam_berangkat,
    'NO_SPJ'=>$no_spj,
    'NO_POLISI'=>$no_polisi,
    'NO_UNIT'=>$kode_kendaraan,
    'DATA_UNIT'=>$data_kendaraan,
    'CLASS_HARGATIKET'=>"$class_harga_tiket",
    'HARGA_TIKET_PROMO'=>$harga_tiket_promo,
    'HARGA_TIKET'=>number_format($harga_tiket,0,",","."),
    'JUMLAH_KURSI'=>$layout_kursi,
    'KODE_SOPIR'=>$kode_sopir,
    'DAFTAR_TUNGGU'=>$jumlah_waiting_list,
    'ACTION_DAFTAR_TUNGGU'=>"window.open('".append_sid("reservasi.waitinglist.php?tglberangkat=".$tgl_mysql."&kodejadwal=".$kode_jadwal)."', 'CtrlWindow', 'toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,width=1000')",
  )
);
$template->set_filenames(array('bodyheader' => 'reservasi/reservasi.layoutkursi.header.tpl'));
$template->pparse('bodyheader');

//RENDER KURSI
//**********************************************************************************************************************
$LayoutKendaraan->kode_booking_dipilih=$kode_booking_dipilih;

//mengambil data user dan levelnya
$nama_sopir				=	substr($nama_sopir,0,7);

$sql =
  "SELECT
        NomorKursi,StatusKursi, NoTiket,
        Nama,StatusBayar,KodeBooking,
        Session
			FROM tbl_posisi_detail
			WHERE
				((NoTiket!=''OR NoTiket IS NOT NULL)
				OR(
				  (NoTiket='' OR NoTiket IS NULL)
				  AND StatusKursi=1
				  AND (HOUR(TIMEDIFF(SessionTime,NOW()))*3600 + MINUTE(TIMEDIFF(SessionTime,NOW()))*60 + SECOND(TIMEDIFF(SessionTime,NOW())))<=$SESSION_TIME_EXPIRED
				))
				AND KodeJadwal='$kode_jadwal'
			  AND TglBerangkat='$tgl_mysql'
			GROUP BY NomorKursi";

$list_no_tiket	= "";

if (!$result = $db->sql_query($sql)){
  //die_error('Cannot Load Transaksi');//,__LINE__,__FILE__,$sql);
  echo("Err:".__LINE__);exit;
}

while ($row = $db->sql_fetchrow($result)){
  //list status kursi
  $nomor_kursi	= $row['NomorKursi'];

  $parameter_layout[$nomor_kursi]['NomorKursi']		  = $row['NomorKursi'];
  $parameter_layout[$nomor_kursi]['StatusKursi']	  = $row['StatusKursi'];
  $parameter_layout[$nomor_kursi]['NoTiket']			  = $row['NoTiket'];
  $parameter_layout[$nomor_kursi]['Nama']					  = $row['Nama'];
  $parameter_layout[$nomor_kursi]['Session']			  = $row['Session'];
  $parameter_layout[$nomor_kursi]['StatusBayar']	  = $row['StatusBayar'];
  $parameter_layout[$nomor_kursi]['KodeBookings']	  = $row['KodeBooking'];
  $parameter_layout[$nomor_kursi]['IsMulti']	      = $row['IsMulti'];
  $parameter_layout[$nomor_kursi]['Transit']	      = 0;
  $parameter_layout[$nomor_kursi]['PetugasPenjual'] = $row['Session'];
  //$parameter_layout[$nomor_kursi]['Debug']	      = $filter_tail;

  $list_no_tiket	.= "'".$row['NoTiket']."',";
}

$list_no_tiket	= substr($list_no_tiket,0,-1);

if($list_no_tiket!=""){
  $sql =
    "SELECT NomorKursi,KodeJadwal,PetugasPenjual
				FROM tbl_reservasi
				WHERE NoTiket IN ($list_no_tiket)";

  if (!$result = $db->sql_query($sql)){
    die_error("Err:".__LINE__);
  }

  while ($row = $db->sql_fetchrow($result)){
    //list status kursi
    $nomor_kursi	= $row['NomorKursi'];

    $parameter_layout[$nomor_kursi]['SubJadwal']			= ($kode_sub_jadwal==$row['KodeJadwal'])?0:1;
    $parameter_layout[$nomor_kursi]['PetugasPenjual']	= $row['PetugasPenjual'];
    $parameter_layout[$nomor_kursi]['ShowCetakCepat']	= $row['IdMember']==""?1:0;

  }
}

$template->destroy();
$template->assign_vars (
  array(
    'NAMA_SOPIR'    => $nama_sopir
  )
);

for($idx=1;$idx<=$jumlah_kursi;$idx++){
  $template->assign_vars (
    array(
      "KURSI_$idx"       => $LayoutKendaraan->MakeImageByStatus($idx,$parameter_layout[$idx]),
    )
  );
}

$file_template_layout =  "reservasi/reservasi.layoutkursi.$layout_kursi.tpl";

if(file_exists("./templates/".$file_template_layout)){
  $template->set_filenames(array('bodylayout' =>$file_template_layout));
  $template->pparse('bodylayout');
}
else{
  $template->assign_vars(array("PESAN"=>"Gagal me-'render' kursi<br>layout $layout_kursi tidak ditemukan!"));
  $template->set_filenames(array('bodyalert' => 'reservasi/reservasi.layoutkursi.alert.tpl'));
  $template->pparse('bodyalert');
}

//END RENDER KURSI *******************************************************************************************************

//RENDER TAIL
$template->destroy();
$template->set_filenames(array('bodytail' => 'reservasi/reservasi.layoutkursi.tail.tpl'));
$template->pparse('bodytail');

?>