<?php
// STANDARD
define('FRAMEWORK', true);
$adp_root_path = './';
include($adp_root_path . 'common.php');
include($adp_root_path . 'ClassMemberTransaksi.php');

// SESSION
$userdata = session_pagestart($user_ip,202);  // Master : 200
init_userprefs($userdata);

// SECURITY#######################################################################
// halaman ini hanya bisa diakses oleh intern
if(!$userdata['session_logged_in'] || $userdata['level_pengguna']!=$LEVEL_ADMIN){ 
  redirect('index.'.$phpEx,true); 
}
//#############################################################################

// PARAMETER
$perpage = $config['perpage'];
$mode    = $HTTP_GET_VARS['mode'];
$act   	 = $HTTP_GET_VARS['act'];
$pesan   = $HTTP_GET_VARS['pesan'];
$submode = isset($HTTP_GET_VARS['submode'])? $HTTP_GET_VARS['submode'] : 'EX'; // kalo submode kosong, defaultnya EXplorer Mode
$start   = (isset($HTTP_GET_VARS['start']) ) ? intval($HTTP_GET_VARS['start']) : 0;

$TransaksiMember	= new TransaksiMember();

switch($mode){

//TAMPILKAN DATA MUTASI SEMUA MEMBER ==========================================================================================================
case 'tampilkan_mutasi':
	
	$cari 			= $HTTP_GET_VARS['cari'];
	$tgl_mula		= $HTTP_GET_VARS['tgl_mula'];
	$tgl_akhir 	= $HTTP_GET_VARS['tgl_akhir'];
	$sortby    	= $HTTP_GET_VARS['sortby'];
	$ascending	= $HTTP_GET_VARS['ascending'];
	$ascending	= ($ascending==0)?"asc":"desc";
	
	//HEADER TABEL
	$hasil ="
		<table width='100%' class='border'>
    <tr>
      <th>#</th>
			<th><a href='#' onClick='setOrder(\"id_koreksi\")'>
				<font color='white'>ID Koreksi</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"id_member\")'>
				<font color='white'>ID Member</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"nama_member\")'>
				<font color='white'>Nama</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"telp_member\")'>
				<font color='white'>Telp</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"hp_member\")'>
				<font color='white'>Handphone</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"keterangan\")'>
				<font color='white'>Keterangan</font></a></th>
			
			<th><a href='#' onClick='setOrder(\"jumlah_koreksi\")'>
				<font color='white'>Jumlah Koreksi</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"waktu_koreksi\")'>
				<font color='white'>Waktu Koreksi</font></a></th>
				
			<th><a href='#' onClick='setOrder(\"operator\")'>
				<font color='white'>Operator</font></a></th>
    </tr>";
    
	$result	= $TransaksiMember->ambilLogKoreksiPoint($tgl_mula,$tgl_akhir,$cari,$sortby,$ascending);
				
	while ($row = $db->sql_fetchrow($result)){   
		$i++;
		
		$odd ='odd';
		if (($i % 2)==0){
			$odd = 'even';
		}
		
		$hasil .="
    <tr bgcolor='D0D0D0'>
      <td class='$odd'>$i</td>
      <td class='$odd'>$row[id_koreksi]</td>
      <td class='$odd'>$row[id_member]</td>
      <td class='$odd'>$row[nama_member]</td>
      <td class='$odd'>$row[telp_member]</td>
      <td class='$odd'>$row[hp_member]</td>
      <td class='$odd'>$row[keterangan]</td>
      <td class='$odd' align='right'>".number_format($row['jumlah_koreksi'],0,",",".")."</td>
			<td class='$odd'>$row[waktu_koreksi]</td>
			<td class='$odd'>$row[operator]</td>
    </tr>";
			
	}
		
	//jika tidak ditemukan data pada database
	if($i==0){
		$hasil .=
			"<tr><td colspan=9 td align='center' bgcolor='EFEFEF'>
				<font color='red'><strong>Data tidak ditemukan!</strong></font>
			</td></tr>";
	}
	
	$hasil .="</table>";
	
	echo($hasil);
	
exit;

}//switch mode

$template->set_filenames(array('body' => 'member_laporan_log_koreksi_point.tpl')); 
$template->assign_vars(array
  ( 'USERNAME'  	=>$userdata['username'],
   	'BCRUMP'    	=>'<a href="'.append_sid('main.'.$phpEx) .'">Home</a> \ <a href="'.append_sid('member_laporan_log_koreksi_point.'.$phpEx).'">Log Koreksi Poin Member</a>',
		'PESAN'				=>$pesan
  ));
include($adp_root_path . 'includes/page_header.php');
$template->pparse('body');
include($adp_root_path . 'includes/page_tail.php');
?>